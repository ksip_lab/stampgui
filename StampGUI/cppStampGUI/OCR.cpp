#pragma once

#include "OCR.h"

//MYcppGui::MYcppGui()
//{
//	win0 = false;
//}
//
//MYcppGui::~MYcppGui()
//{
//	cvDestroyAllWindows();
//}
//
//int MYcppGui::myCppLoadAndShowRGB(char* url, int maxWidth, int maxHeight, int nPicPerRow)
//{
//	// my OpenCV C++ codes uses only cxcore, highgui and imgproc 
//	return 0;
//}

ClassStampOCR::ClassStampOCR()
{

}
ClassStampOCR::~ClassStampOCR()
{

}

void ClassStampOCR::CopyParameter(int dark_val, int dark_thr, int box_size, int num_char, int feature_sampling_rate, int training_size, int training_char_size, int template_width, int template_height)
{
	this->dark_val = dark_val;
	this->dark_thr = dark_thr;
	this->box_size = box_size;
	this->num_char = num_char;

	this->feature_sampling_rate = feature_sampling_rate;
	this->training_size = training_size;
	this->training_char_size = training_char_size;

	this->template_width = template_width;
	this->template_height = template_height;
}

void ClassStampOCR::GetFilePath(string path, string file_ext, vector<string> *filepath)
{
	GetFileList(path + "*" + file_ext, *filepath);

	for (int i = 0; i < filepath->size(); i++)
	{
		filepath->at(i) = path + filepath->at(i);
	}
}

void ClassStampOCR::DetectCharPosition(KGImage<double> &in_img, vector<KCoor<int>> *topleft, vector<KCoor<int>> *bottomright)
{
	/*KGImage<double> in_img;
	in_img.Read(filepath);*/
	//in_img.Show("", false, true);

	vector<double> row_int_avg, region;
	for (int v = 0; v < in_img.height; v++)
	{
		double avg = 0.0;
		for (int u = 0; u < in_img.width; u++)
		{
			if (in_img.idata[u][v] < this->dark_val)
				avg++;
			//avg += (double)in_img.idata[u][v];
		}
		//avg /= (double)in_img.width;
		row_int_avg.push_back(avg);
		if (avg <= this->dark_thr)
			region.push_back(1.0);
		else
			region.push_back(0.0);
	}

	int chk = 0;
	int upper, lower;
	double prev = region.size() - 1;
	for (int i = region.size() - 2; i >= 0; i--)
	{
		if (region[i] - region[prev] == -1)
		{
			chk = 1;
			lower = i;
		}
		else if (chk == 1 && region[i] - region[prev] == 1)
		{
			upper = i;
			break;
		}
		prev = i;
	}

	upper -= box_size;
	lower += box_size;
	row_int_avg.clear();
	region.clear();
	for (int u = 0; u < in_img.width; u++)
	{
		double avg = 0.0;
		for (int v = upper; v <= lower; v++)
		{
			if (in_img.idata[u][v] < this->dark_val)
				avg++;
			//avg += (double)in_img.idata[u][v];
		}
		//avg /= (double)in_img.width;
		row_int_avg.push_back(avg);
		if (avg <= this->dark_thr / 10)
			region.push_back(1.0);
		else
			region.push_back(0.0);
	}

	chk = 0;
	vector<int> left, right, dist;
	prev = 0;
	for (int i = 1; i < region.size(); i++)
	{
		if (chk == 0 && region[i] - region[prev] == -1)
		{
			left.push_back(i);
			chk = 1;
		}
		else if (chk == 1 && region[i] - region[prev] == 1)
		{
			/*if (i - left.at(left.size() - 1) >= 10)
			{
			right.push_back(i);
			chk = 0;
			}*/
			right.push_back(i);
			dist.push_back(right.at(right.size() - 1) - left.at(left.size() - 1));
			chk = 0;
		}
		prev = i;
	}

	int min_size;
	if (left.size() < right.size())
		min_size = left.size();
	else
		min_size = right.size();

	for (int j = dist.size(); j > this->num_char; j--)
	{
		int min_dist = 0;
		for (int i = 1; i < dist.size(); i++)
		{
			if (dist[i] < dist[min_dist])
				min_dist = i;
		}

		if (min_dist == 0)
		{
			left.erase(left.begin() + min_dist + 1);
			right.erase(right.begin() + min_dist);
		}
		else if (min_dist == dist.size() - 1)
		{
			left.erase(left.begin() + min_dist);
			right.erase(right.begin() + min_dist - 1);
		}
		else
		{
			if (left[min_dist + 1] - right[min_dist] > left[min_dist] - right[min_dist - 1])
			{
				left.erase(left.begin() + min_dist);
				right.erase(right.begin() + min_dist - 1);
			}
			else
			{
				left.erase(left.begin() + min_dist + 1);
				right.erase(right.begin() + min_dist);
			}
		}
		min_size--;
		dist.clear();
		for (int i = 0; i < min_size; i++)
		{
			dist.push_back(right.at(i) - left.at(i));
		}

		/*left.erase(left.begin() + min_dist);
		right.erase(right.begin() + min_dist);
		dist.erase(dist.begin() + min_dist);*/

	}

	double thrOtsu;
	KGImage<double> tmp;
	double char_length = num_char;
	if (left.size() < num_char)
		char_length = left.size();
	for (int i = 0; i < char_length; i++)
	{
		/*left[i] -= box_size/2;
		right[i] += box_size/2;*/

		int stx, sty, endx, endy;
		if (i == 0)
		{
			stx = left[i] - box_size;
			sty = upper;
			endx = (right[i] + left[i + 1]) / 2;
			endy = lower;

		}
		else if (i == min_size - 1)
		{
			stx = (left[i] + right[i - 1]) / 2;
			sty = upper;
			endx = right[i] + box_size;
			endy = lower;
		}
		else
		{
			stx = (left[i] + right[i - 1]) / 2;
			sty = upper;
			endx = (right[i] + left[i + 1]) / 2;
			endy = lower;
		}

		KCoor<int> tmppoint;
		tmppoint.x = stx;
		tmppoint.y = sty;
		topleft->push_back(tmppoint);
		tmppoint.x = endx;
		tmppoint.y = endy;
		bottomright->push_back(tmppoint);

		//tmp.Allocate(endx - stx + 1, endy - sty + 1);
		//for (int u = stx; u <= endx; u++)
		//{
		//	for (int v = sty; v <= endy; v++)
		//	{
		//		tmp.idata[u - stx][v - sty] = in_img.idata[u][v];
		//	}
		//}
		////Thresholding<double, int, int, int>(&tmp, dark_val, 255, 0);
		//thrOtsu = ThresholdingOtsu<double, int, int, int>(&tmp, 1, 255, 0);
		//KResizeBound<double>(&tmp, template_width, template_height, 255);
	}
}

void ClassStampOCR::DetectCharPosition(string filepath, vector<KCoor<int>> *topleft, vector<KCoor<int>> *bottomright)
{
	KGImage<double> in_img;
	in_img.Read(filepath);
	this->DetectCharPosition(in_img, topleft, bottomright);
}

void ClassStampOCR::GetCharacterSize(vector<string> train_set, int *max_char_width, int *max_char_height)
{
	//cout << "Get Character Size Process";
	*max_char_width = 0;
	*max_char_height = 0;
	for (int fil = 0; fil < train_set.size(); fil++)
	{
		//cout << "File #" << to_string(fil + 1) << "\t" << train_set[fil] << endl;

		KGImage<double> in_img;
		in_img.Read(train_set[fil]);

		vector<KCoor<int>> tl, br;
		//DetectCharPosition(train_set[fil], &tl, &br);
		DetectCharPosition(in_img, &tl, &br);

		for (int i = 0; i < tl.size(); i++)
		{
			if (*max_char_width < br.at(i).x - tl.at(i).x + 1)
				*max_char_width = br.at(i).x - tl.at(i).x + 1;
			if (*max_char_height < br.at(i).y - tl.at(i).y + 1)
				*max_char_height = br.at(i).y - tl.at(i).y + 1;
		}
	}
	////cout << "\t\t\tDone!" << endl;
}

void ClassStampOCR::GetActualSerialNumber(string sn_path, vector<vector<int>> *sn_val, vector<string> *sn_string)
{
	//cout << "Get Actual Serial Number Process";
	ifstream sn_file;
	sn_file.open(sn_path);
	string str;
	while (getline(sn_file, str))
	{
		int idx_tab = 0;
		for (int i = 0; i < str.size(); i++)
		{
			if (str[i] == '\t')
			{
				idx_tab = i;
			}
		}

		vector<int> sn_stamp;
		string str2;
		for (int i = 1; i <= num_char; i++)
		{
			string ch = str.substr(idx_tab + i, 1);
			str2 += ch;
			int sn_num = stoi(ch);
			sn_stamp.push_back(sn_num);
		}
		sn_string->push_back(str2);
		sn_val->push_back(sn_stamp);
	}
	//cout << "\t\t\tDone!" << endl;
}

void ClassStampOCR::GenerateCharacterTemplate(vector<string> train_set, vector<vector<int>> sn_val, int template_char_width, int template_char_height, string sav_path, vector<vector<double>> *feature_template)
{
	//cout << "Generate Character Template Process";
	vector<vector<vector<double>>> feature_set;
	for (int fil = 0; fil < train_set.size(); fil++)
	{
		//cout << "File #" << to_string(fil + 1) << "\t" << train_set[fil] << endl;
		KGImage<double> in_img;
		in_img.Read(train_set[fil]);
		vector<KCoor<int>> tl, br;
		//DetectCharPosition(train_set[fil], &tl, &br);
		DetectCharPosition(in_img, &tl, &br);

		vector<vector<double>> feature_stamp;
		for (int i = 0; i < tl.size(); i++)
		{
			vector<double> feat;
			KGImage<double> tmp;
			tmp.Allocate(br.at(i).x - tl.at(i).x + 1, br.at(i).y - tl.at(i).y + 1);
			for (int u = tl.at(i).x; u <= br.at(i).x; u++)
			{
				for (int v = tl.at(i).y; v <= br.at(i).y; v++)
				{
					tmp.idata[u - tl.at(i).x][v - tl.at(i).y] = in_img.idata[u][v];
				}
			}
			//Thresholding<double, int, int, int>(&tmp, dark_val, 255, 0);
			ThresholdingOtsu<double, int, int, int>(&tmp, 1, 255, 0);
			KResizeBound<double>(&tmp, template_width, template_height, 255);

			vector<double> feature_char;
			for (int uu = 0; uu < template_char_width; uu++)
			{
				for (int vv = 0; vv < template_char_height; vv++)
				{
					feature_char.push_back((double)tmp.idata[uu][vv]);
				}
			}
			feature_stamp.push_back(feature_char);
		}

		feature_set.push_back(feature_stamp);
	}

	int feature_length = template_char_width*template_char_height;
	vector<vector<double>> avg_feature;
	vector<int> num_ch;
	for (int i = 0; i < 10; i++)
	{
		vector<double> hu;
		for (int j = 0; j < feature_length; j++)
		{
			hu.push_back(0.0);
		}
		avg_feature.push_back(hu);
		num_ch.push_back(0);
	}

	//vector<vector<double>> feat_train, u_reduce;
	for (int i = 0; i < training_size; i++)
	{
		for (int j = 0; j < num_char; j++)
		{
			//feat_train.push_back(feature_set.at(i).at(j));
			/*for (int k = 0; k < feature_length; k++)
			{
			avg_feature.at(sn_val.at(i).at(j)).at(k) += feature_set.at(i).at(j).at(k);
			}
			num_ch.at(sn_val.at(i).at(j))++;*/
			if (num_ch.at(sn_val.at(i).at(j)) < training_char_size)
			{
				for (int k = 0; k < feature_length; k++)
				{
					avg_feature.at(sn_val.at(i).at(j)).at(k) += feature_set.at(i).at(j).at(k);
				}
				num_ch.at(sn_val.at(i).at(j))++;
			}
		}
	}

	//KPCADimensionReduction<double, double>(&feat_train, 20, &u_reduce);

	ofstream feat_tmp;
	feat_tmp.open(sav_path);
	//feat_tmp << "Char Width: " << template_char_width << endl;
	//feat_tmp << "Char Height: " << template_char_height << endl;
	feat_tmp << template_char_width << "\t" << template_char_height << endl;

	for (int i = 0; i < 10; i++)
	{
		for (int k = 0; k < feature_length; k++)
		{
			avg_feature.at(i).at(k) /= num_ch.at(i);
			feat_tmp << avg_feature.at(i).at(k) << "\t";
		}
		feat_tmp << endl;
	}
	feat_tmp.close();

	*feature_template = avg_feature;
	cout << "\t\t\tDone!" << endl;
}

void ClassStampOCR::GetTemplate(string template_path, int *template_w, int *template_h, vector<vector<double>> *feature_template)
{
	cout << "Get Template Process";
	ifstream sn_file;
	sn_file.open(template_path);
	string str;
	int line_num = 0;
	while (getline(sn_file, str))
	{
		vector<double> tmp_feat;
		if (line_num == 0)
		{
			int idx_tab = 0;
			for (int i = 0; i < str.size(); i++)
			{
				if (str[i] == '\t')
				{
					idx_tab = i;
				}
			}

			string ch = str.substr(0, idx_tab);
			*template_w = stoi(ch);
			ch = str.substr(idx_tab + 1, str.size() - idx_tab - 1);
			*template_h = stoi(ch);

		}
		else
		{
			vector<int> idx_tab;
			idx_tab.push_back(-1);
			for (int i = 0; i < str.size(); i++)
			{
				if (str[i] == '\t')
				{
					idx_tab.push_back(i);
				}
			}
			//idx_tab.push_back(str.size());

			for (int i = 0; i < idx_tab.size() - 1; i++)
			{
				string ch = str.substr(idx_tab[i] + 1, idx_tab[i + 1] - idx_tab[i] - 1);
				double ch_num = stod(ch);
				tmp_feat.push_back(ch_num);
			}
			feature_template->push_back(tmp_feat);
		}
		line_num++;
	}
	cout << "\t\t\tDone!" << endl;
}

void ClassStampOCR::TemplateDownsampling(vector<vector<double>> *feature_template)
{
	/*KGImage<int> tmp_im;
	tmp_im.Allocate(template_width*template_height / feature_sampling_rate, 10);

	vector<vector<double>> tmp;
	tmp = *feature_template;
	feature_template->clear();
	for (int i = 0; i < 10; i++)
	{
		vector<double> tt;
		for (int k = 0; k < template_width*template_height; k++)
		{
			if (k % feature_sampling_rate == 0)
			{
				tt.push_back(tmp.at(i).at(k));
				tmp_im.idata[tt.size() - 1][i] = tmp.at(i).at(k);
			}
		}
		feature_template->push_back(tt);
	}

	tmp_im.Show("", false, true);*/

	vector<vector<double>> tmp;
	tmp = *feature_template;
	feature_template->clear();
	for (int i = 0; i < 10; i++)
	{
		vector<double> tt;
		for (int k = 0; k < template_width*template_height; k++)
		{
			if (k % this->feature_sampling_rate == 0)
				tt.push_back(tmp.at(i).at(k));
		}
		feature_template->push_back(tt);
	}
}

void ClassStampOCR::OCRProcess(string filepath, vector<vector<double>> *feature_template, vector<KCoor<int>> *topleft, vector<KCoor<int>> *bottomright, string *serialnumber, vector<double> *char_score, double *ocr_score)
{
	KGImage<double> in_img;
	in_img.Read(filepath);

	vector<KCoor<int>> tl, br;
	//DetectCharPosition(filepath, &tl, &br);
	DetectCharPosition(in_img, &tl, &br);
	*topleft = tl;
	*bottomright = br;
	*ocr_score = 0.0;
	for (int i = 0; i < tl.size(); i++)
	{
		vector<double> feat;
		KGImage<double> tmp;
		tmp.Allocate(br.at(i).x - tl.at(i).x + 1, br.at(i).y - tl.at(i).y + 1);
		for (int u = tl.at(i).x; u <= br.at(i).x; u++)
		{
			for (int v = tl.at(i).y; v <= br.at(i).y; v++)
			{
				tmp.idata[u - tl.at(i).x][v - tl.at(i).y] = in_img.idata[u][v];
			}
		}
		//Thresholding<double, int, int, int>(&tmp, dark_val, 255, 0);
		ThresholdingOtsu<double, int, int, int>(&tmp, 1, 255, 0);
		KResizeBound<double>(&tmp, template_width, template_height, 255);

		int kk = 0;
		for (int u = 0; u < template_width; u++)
		{
			for (int v = 0; v < template_height; v++)
			{
				if (kk % feature_sampling_rate == 0)
					feat.push_back(tmp.idata[u][v]);

				kk++;
			}
		}

		vector<distidx> dist;
		//int min_dist = 0, min2nd, max_dist = 0;
		double avg = 0.0;
		for (int l = 0; l < 10; l++)
		{
			distidx tmp_distidx;
			double tmp_dist = 0.0;
			for (int k = 0; k < feat.size(); k++)
			{
				//tmp_dist += (feat.at(k) - feature_template->at(l).at(k));
				tmp_dist += pow(feat.at(k) - feature_template->at(l).at(k), 2);
			}
			tmp_dist = sqrt(tmp_dist);
			tmp_distidx.dist = tmp_dist;
			avg += tmp_dist;
			tmp_distidx.idx = l;
			dist.push_back(tmp_distidx);
		}
		avg /= 10.0;
		sort(dist.begin(), dist.end(), sort_distidx());
		serialnumber->push_back(dist[0].idx + '0');
		double sc = (abs(dist[0].dist - avg) - abs(dist[1].dist - avg)) / (abs(dist[0].dist - avg) - abs(dist[2].dist - avg));
		char_score->push_back(sc);
		*ocr_score += sc;
	}
	*ocr_score /= tl.size();

	if (tl.size() != num_char)
		*ocr_score = 0.0;
}
