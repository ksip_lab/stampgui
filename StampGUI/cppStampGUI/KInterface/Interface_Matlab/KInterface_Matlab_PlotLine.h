// ------------------------------------------------ //
// contain:		Interface for Matlab [PlotLine]
//
// developer:	Krisada Phromsuthirak
// ------------------------------------------------ //

#ifndef _KInterface_Matlab_PlotLine_H
#define _KInterface_Matlab_PlotLine_H

#include "KHeader.h"
#include "KBasic.h"
#include "KDirectories.h"
#include "KArray1D.h"
#include "KList1D.h"

#include "KInterface_Matlab_Define.h"

class KInterface_Matlab_PlotLine
{
private:

	bool						haveYData;
	bool						haveXData;
	int							numData;

	bool						isSetXTickLabel;
	bool						isSetYTickLabel;
	bool						isSetAxisLimit_X;
	bool						isSetAxisLimit_Y;

	bool						isShowLegend;
	bool						isShowGridX;
	bool						isShowGridY;
	bool						isShowBox;

	string						BackgroundColor;
	string						GridColor;

	bool						isLogScaleGridX;
	bool						isLogScaleGridY;

	bool						isSetNewFigure;

	string						label_Title;
	string						label_xAxis;
	string						label_yAxis;

	KList1D<double>				list_xTick;
	KList1D<double>				list_yTick;
	KList1D<string>				list_xLabel;
	KList1D<string>				list_yLabel;

	double						axisLimit_xMin;
	double						axisLimit_xMax;
	double						axisLimit_yMin;
	double						axisLimit_yMax;

	KList1D<KArray1D<double>*>	list_YData;
	KArray1D<double>			list_XData;
	KList1D<string>				list_DataLabel;
	KList1D<string>				list_lineStyle;
	KList1D<int>				list_lineSize;
	KList1D<string>				list_lineColor;
	KList1D<string>				list_markStyle;
	KList1D<int>				list_markSize;
	KList1D<string>				list_markColorEdge;
	KList1D<string>				list_markColorFace;

public:

	KInterface_Matlab_PlotLine();
	~KInterface_Matlab_PlotLine();

	void ClearData();
	void ClearMemory();

	void Set_ShowLegend(bool ShowLegend = true);
	void Set_ShowGrid(bool ShowGridX = true, bool ShowGridY = true);
	void Set_ShowBox(bool ShowBox = true);
	void Set_LogScale(bool LogScaleGridX = false, bool LogScaleGridY = false);
	void Set_FigureColor(string ColorForBackground = KINTERFACE_MATLAB_COLOR_WHITE, string ColorForGrid = KINTERFACE_MATLAB_COLOR_BLACK);
	void Set_Label_Title(string Label_Title);
	void Set_Label_xAxis(string Label_xAxis);
	void Set_Label_yAxis(string Label_yAxis);
	void Set_AxisLimit_X(double xMin, double xMax);
	void Set_AxisLimit_Y(double yMin, double yMax);
	void Set_NewFigure(bool NewFigure = true);

	void SavePlotFile(string mFilePath, long FigureNumber = 1);

	template <typename T> void Set_xTickLabel(KList1D<T> &listXValue, KList1D<string> &listXLabel);
	template <typename T> void Set_yTickLabel(KList1D<T> &listYValue, KList1D<string> &listYLabel);
	template <typename T> void InsertXData(KArray1D<T> &x_data);
	template <typename T> void InsertYData(KArray1D<T> &y_data, string DataLabel = "", int LineSize = 2, string LineColor = KINTERFACE_MATLAB_LINECOLOR_BLACK, string LineStyle = KINTERFACE_MATLAB_LINESTYLE_SOLID, int MarkSize = 2, string MarkColorEdge = KINTERFACE_MATLAB_MARKCOLOR_BLACK, string MarkColorFace = KINTERFACE_MATLAB_MARKCOLOR_BLACK, string MarkStyle = KINTERFACE_MATLAB_MARKSTYLE_NON);

};

template <typename T> void KInterface_Matlab_PlotLine::Set_xTickLabel(KList1D<T> &listXValue, KList1D<string> &listXLabel)
{
	if (listXValue.size() != listXLabel.size())
		ErrorMessage("mapping of value and label should be same length");

	list_xTick.FreeMem();
	list_xLabel.FreeMem();
	for (unsigned int z = 0; z<listXValue.size(); z++)
	{
		list_xTick.push_back((double)listXValue.at(z));
		list_xLabel.push_back(listXLabel.at(z));
	}
	isSetXTickLabel = true;
}
template <typename T> void KInterface_Matlab_PlotLine::Set_yTickLabel(KList1D<T> &listYValue, KList1D<string> &listYLabel)
{
	if (listYValue.size() != listYLabel.size())
		ErrorMessage("mapping of value and label should be same length");

	list_yTick.FreeMem();
	list_yLabel.FreeMem();
	for (unsigned int z = 0; z<listYValue.size(); z++)
	{
		list_yTick.push_back((double)listYValue.at(z));
		list_yLabel.push_back(listYLabel.at(z));
	}
	isSetYTickLabel = true;
}
template <typename T> void KInterface_Matlab_PlotLine::InsertXData(KArray1D<T> &x_data)
{
	if (haveYData)
	if (list_YData.at(0)->length != x_data.length)
		ErrorMessage("X-Axis data should be same size of Y-Axis data");

	list_XData.Allocate(x_data.length);
	for (int z = 0; z<x_data.length; z++)
		list_XData.idata[z] = (double)x_data.idata[z];

	haveXData = true;
}
template <typename T> void KInterface_Matlab_PlotLine::InsertYData(KArray1D<T> &y_data, string DataLabel, int LineSize, string LineColor, string LineStyle, int MarkSize, string MarkColorEdge, string MarkColorFace, string MarkStyle)
{
	if (haveXData)
	if (list_XData.length != y_data.length)
		ErrorMessage("Y-Axis data should be same size of X-Axis data");

	KArray1D<double> *tmp_data;
	tmp_data = new KArray1D<double>[1];
	tmp_data->Allocate(y_data.length);

	for (int z = 0; z<tmp_data->length; z++)
		tmp_data->idata[z] = (double)y_data.idata[z];

	if (list_YData.size() != 0)
	if (list_YData.at(0)->length != y_data.length)
		ErrorMessage("Size don't match ,cannot insert this data to .M file");
	if (DataLabel == "")
		list_DataLabel.push_back("Untitled" + ToString(list_YData.size()));
	else
		list_DataLabel.push_back(DataLabel);
	list_YData.push_back(tmp_data);
	list_lineSize.push_back(LineSize);
	list_lineColor.push_back(LineColor);
	list_lineStyle.push_back(LineStyle);
	list_markSize.push_back(MarkSize);
	list_markColorEdge.push_back(MarkColorEdge);
	list_markColorFace.push_back(MarkColorFace);
	list_markStyle.push_back(MarkStyle);

	if (!haveYData)
		haveYData = true;
}

#endif