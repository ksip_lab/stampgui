#include "KInterface_Matlab_Define.h"

string KINTERFACE_MATLAB_CUSTOMCOLOR(int R, int G, int B)
{
	double r, g, b;

	r = (double)R / (double)255.0;
	g = (double)G / (double)255.0;
	b = (double)B / (double)255.0;

	if (r < 0.0)
		r = 0.0;
	if (g < 0.0)
		g = 0.0;
	if (b < 0.0)
		b = 0.0;
	if (r > 1.0)
		r = 1.0;
	if (g > 1.0)
		g = 1.0;
	if (b > 1.0)
		b = 1.0;

	return "[" + ToString(r) + " " + ToString(g) + " " + ToString(b) + "]";
}