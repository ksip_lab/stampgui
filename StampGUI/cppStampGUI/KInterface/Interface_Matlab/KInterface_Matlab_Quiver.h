// ------------------------------------------------ //
// contain:		Interface for Matlab [Quiver]
//
// developer:	Krisada Phromsuthirak
// ------------------------------------------------ //

#ifndef _KInterface_Matlab_Quiver_H
#define _KInterface_Matlab_Quiver_H

#include "KHeader.h"
#include "KBasic.h"
#include "KDirectories.h"
#include "KArray2D.h"
#include "KList1D.h"

#include "KInterface_Matlab_Define.h"

class KInterface_Matlab_Quiver
{
private:

	int							numData;

	bool						isSetColorMap;
	bool						isSetOrientationMode;

	bool						isSetXTickLabel;
	bool						isSetYTickLabel;
	bool						isSetAxisLimit_X;
	bool						isSetAxisLimit_Y;
	bool						isSetInvertX;
	bool						isSetInvertY;

	bool						isShowLegend;
	bool						isShowGridX;
	bool						isShowGridY;
	bool						isShowBox;

	string						BackgroundColor;
	string						GridColor;
	string						MapColor;

	bool						isLogScaleGridX;
	bool						isLogScaleGridY;

	string						label_Title;
	string						label_xAxis;
	string						label_yAxis;

	KList1D<double>				list_xTick;
	KList1D<double>				list_yTick;
	KList1D<string>				list_xLabel;
	KList1D<string>				list_yLabel;

	double						axisLimit_xMin;
	double						axisLimit_xMax;
	double						axisLimit_yMin;
	double						axisLimit_yMax;

	KList1D<string>				list_DataLabel;
	KList1D<string>				list_lineStyle;
	KList1D<int>				list_lineSize;
	KList1D<string>				list_lineColor;

	bool						isContourShowText;
	bool						isContourShading;
	int							contourLineNumber;

	bool						haveMeshGridData;
	bool						haveContour;

	KArray2D<double>			meshGridX;
	KArray2D<double>			meshGridY;
	KArray2D<double>			contour;

	KList1D<double**>			listDataGX;
	KList1D<double**>			listDataGY;
	KList1D<int>				listWidth;
	KList1D<int>				listHeight;

	bool						isSetNewFigure;

public:

	KInterface_Matlab_Quiver();
	~KInterface_Matlab_Quiver();

	void ClearData();
	void ClearMemory();
	void SavePlotFile(string mFilePath, long FigureNumber = 1);

	void Set_ColorMap(string ColorMap = KINTERFACE_MATLAB_COLORMAP_JET);
	void Set_OrientationMode(bool OrientationMode = true);
	void Set_ShowLegend(bool ShowLegend = true);
	void Set_ShowGrid(bool ShowGridX = true, bool ShowGridY = true);
	void Set_ShowBox(bool ShowBox = true);
	void Set_LogScale(bool LogScaleGridX = false, bool LogScaleGridY = false);
	void Set_FigureColor(string ColorForBackground = KINTERFACE_MATLAB_COLOR_WHITE, string ColorForGrid = KINTERFACE_MATLAB_COLOR_BLACK);
	void Set_Label_Title(string Label_Title);
	void Set_Label_xAxis(string Label_xAxis);
	void Set_Label_yAxis(string Label_yAxis);
	void Set_AxisLimit_X(double xMin, double xMax);
	void Set_AxisLimit_Y(double yMin, double yMax);
	void Set_InvertAxis(bool InvertX = false, bool InvertY = true);
	void Set_NewFigure(bool NewFigure = true);

	template <typename T> void Set_xTickLabel(KList1D<T> &listXValue, KList1D<string> &listXLabel);
	template <typename T> void Set_yTickLabel(KList1D<T> &listYValue, KList1D<string> &listYLabel);
	template <typename T> void InsertMeshGrid(KArray2D<T> &inputMx, KArray2D<T> &inputMy);
	template <typename T> void InsertDataGradient(KArray2D<T> &inputGx, KArray2D<T> &inputGy, string DataLabel = "", int LineSize = 1, string LineColor = KINTERFACE_MATLAB_LINECOLOR_BLACK, string LineStyle = KINTERFACE_MATLAB_LINESTYLE_SOLID);
	template <typename T> void InsertDataVector(KArray2D<T> &inputMagnitude, KArray2D<T> &inputDirection, string DataLabel = "", int LineSize = 1, string LineColor = KINTERFACE_MATLAB_LINECOLOR_BLACK, string LineStyle = KINTERFACE_MATLAB_LINESTYLE_SOLID);
	template <typename T> void InsertContour(KArray2D<T> &inputContour, bool showLabel = false, int numContourLine = 0, bool showShading = false);
	
};

template <typename T> void KInterface_Matlab_Quiver::Set_xTickLabel(KList1D<T> &listXValue, KList1D<string> &listXLabel)
{
	if (listXValue.size() != listXLabel.size())
		ErrorMessage("mapping of value and label should be same length");

	list_xTick.FreeMem();
	list_xLabel.FreeMem();
	for (unsigned int z = 0; z<listXValue.size(); z++)
	{
		list_xTick.push_back((double)listXValue.at(z));
		list_xLabel.push_back(listXLabel.at(z));
	}
	isSetXTickLabel = true;
}
template <typename T> void KInterface_Matlab_Quiver::Set_yTickLabel(KList1D<T> &listYValue, KList1D<string> &listYLabel)
{
	if (listYValue.size() != listYLabel.size())
		ErrorMessage("mapping of value and label should be same length");

	list_yTick.FreeMem();
	list_yLabel.FreeMem();
	for (unsigned int z = 0; z<listYValue.size(); z++)
	{
		list_yTick.push_back((double)listYValue.at(z));
		list_yLabel.push_back(listYLabel.at(z));
	}
	isSetYTickLabel = true;
}
template <typename T> void KInterface_Matlab_Quiver::InsertMeshGrid(KArray2D<T> &inputMx, KArray2D<T> &inputMy)
{
	if ((!inputMx.isAlloc) || (!inputMy.isAlloc))
		ErrorMessage("Error [KInterface_Matlab_Quiver] : data (Mx,My) are empty");

	if (inputMx.IsSameSize(inputMy.width, inputMy.height))
	{
		meshGridX.Resize(inputMx.width, inputMx.height);
		meshGridY.Resize(inputMy.width, inputMy.height);
		for (int y = 0; y < inputMx.height; y++)
		for (int x = 0; x < inputMx.width; x++)
		{
			meshGridX.idata[x][y] = inputMx.idata[x][y];
			meshGridY.idata[x][y] = inputMy.idata[x][y];
		}
		haveMeshGridData = true;
	}
	else
		ErrorMessage("Error [KInterface_Matlab_Quiver] : Mx & My must be same size");
}
template <typename T> void KInterface_Matlab_Quiver::InsertDataGradient(KArray2D<T> &inputGx, KArray2D<T> &inputGy, string DataLabel, int LineSize, string LineColor, string LineStyle)
{
	if ((!inputGx.isAlloc) || (!inputGy.isAlloc))
		ErrorMessage("Error [KInterface_Matlab_Quiver] : data (Gx,Gy) are empty");

	if (inputGx.IsSameSize(inputGy.width, inputGy.height))
	{
		double **dataGx;
		double **dataGy;

		listWidth.push_back(inputGx.width);
		listHeight.push_back(inputGx.height);
		dataGx = Alloc<double>(inputGx.width, inputGx.height);
		dataGy = Alloc<double>(inputGy.width, inputGy.height);

		for (int y = 0; y < inputGx.height; y++)
		for (int x = 0; x < inputGx.width; x++)
		{
			dataGx[x][y] = (double)inputGx.idata[x][y];
			dataGy[x][y] = (double)inputGy.idata[x][y];
		}

		listDataGX.push_back(dataGx);
		listDataGY.push_back(dataGy);

		if (DataLabel == "")
			list_DataLabel.push_back("Untitled" + ToString(numData));
		else
			list_DataLabel.push_back(DataLabel);
		list_lineSize.push_back(LineSize);
		list_lineColor.push_back(LineColor);
		list_lineStyle.push_back(LineStyle);

		numData++;
	}
	else
		ErrorMessage("Error [KInterface_Matlab_Quiver] : Gx & Gy must be same size");
}
template <typename T> void KInterface_Matlab_Quiver::InsertDataVector(KArray2D<T> &inputMagnitude, KArray2D<T> &inputDirection, string DataLabel, int LineSize, string LineColor, string LineStyle)
{
	if ((!inputMagnitude.isAlloc) || (!inputDirection.isAlloc))
		ErrorMessage("Error [KInterface_Matlab_Quiver] : data (Magnitude,Direction) are empty");

	if (inputMagnitude.IsSameSize(inputDirection.width, inputDirection.height))
	{
		KArray2D<T> Gx, Gy;
		Gx.Allocate(inputDirection.width, inputDirection.height);
		Gy.Allocate(inputDirection.width, inputDirection.height);
		for (int y = 0; y<inputDirection.height; y++)
		for (int x = 0; x<inputDirection.width; x++)
		{
			Gx.idata[x][y] = inputMagnitude.idata[x][y] * cos(inputDirection.idata[x][y]);
			Gy.idata[x][y] = inputMagnitude.idata[x][y] * sin(inputDirection.idata[x][y]);
		}
		InsertDataGradient(Gx, Gy, DataLabel, LineSize, LineColor, LineStyle);
		Gx.FreeMem();
		Gy.FreeMem();
	}
	else
		ErrorMessage("Error [KInterface_Matlab_Quiver] : Magnitude & Direction must be same size");
}
template <typename T> void KInterface_Matlab_Quiver::InsertContour(KArray2D<T> &inputContour, bool showLabel, int numContourLine, bool showShading)
{
	if (numData > 0)
	{
		contour.Allocate(inputContour.width, inputContour.height);
		for (int y = 0; y < contour.height; y++)
		for (int x = 0; x < contour.width; x++)
			contour.idata[x][y] = (double)inputContour.idata[x][y];

		isContourShowText = showLabel;
		isContourShading = showShading;
		contourLineNumber = numContourLine;

		haveContour = true;
	}
	else
		ErrorMessage("Error [KInterface_Matlab_Quiver] : must insert 'data' before insert 'contour'");
}

#endif