#include "KInterface_Matlab_PlotLine.h"

KInterface_Matlab_PlotLine::KInterface_Matlab_PlotLine()
{
	numData = 0;
	label_Title = "Untitled";
	label_xAxis = "";
	label_yAxis = "";

	BackgroundColor = KINTERFACE_MATLAB_COLOR_WHITE;
	GridColor = KINTERFACE_MATLAB_COLOR_BLACK;

	haveYData = false;
	haveXData = false;
	isSetXTickLabel = false;
	isSetYTickLabel = false;
	isSetAxisLimit_X = false;
	isSetAxisLimit_Y = false;
	isShowLegend = false;
	isShowGridX = false;
	isShowGridY = false;
	isShowBox = false;
	isLogScaleGridX = false;
	isLogScaleGridY = false;
	isSetNewFigure = false;
}
KInterface_Matlab_PlotLine::~KInterface_Matlab_PlotLine()
{
	ClearMemory();
}
void KInterface_Matlab_PlotLine::ClearData()
{
	list_xTick.FreeMem();
	list_yTick.FreeMem();
	list_xLabel.FreeMem();
	list_yLabel.FreeMem();
	for (unsigned int z = 0; z < list_YData.size(); z++)
		list_YData.at(z)->FreeMem();
	list_YData.FreeMem();
	list_XData.FreeMem();
	list_DataLabel.FreeMem();
	list_lineStyle.FreeMem();
	list_lineSize.FreeMem();
	list_lineColor.FreeMem();
	list_markStyle.FreeMem();
	list_markSize.FreeMem();
	list_markColorEdge.FreeMem();
	list_markColorFace.FreeMem();

	haveYData = false;
	haveXData = false;
	isSetXTickLabel = false;
	isSetYTickLabel = false;
	isSetAxisLimit_X = false;
	isSetAxisLimit_Y = false;

	numData = 0;
}
void KInterface_Matlab_PlotLine::ClearMemory()
{
	ClearData();

	label_Title = "";
	label_xAxis = "";
	label_yAxis = "";
	isShowLegend = false;
	isShowGridX = false;
	isShowGridY = false;
	isShowBox = false;
	isLogScaleGridX = false;
	isLogScaleGridY = false;
	isSetNewFigure = false;
}

void KInterface_Matlab_PlotLine::Set_ShowLegend(bool ShowLegend)
{
	isShowLegend = ShowLegend;
}
void KInterface_Matlab_PlotLine::Set_ShowGrid(bool ShowGridX, bool ShowGridY)
{
	isShowGridX = ShowGridX;
	isShowGridY = ShowGridY;
}
void KInterface_Matlab_PlotLine::Set_ShowBox(bool ShowBox)
{
	isShowBox = ShowBox;
}
void KInterface_Matlab_PlotLine::Set_LogScale(bool LogScaleGridX, bool LogScaleGridY)
{
	isLogScaleGridX = LogScaleGridX;
	isLogScaleGridY = LogScaleGridY;
}
void KInterface_Matlab_PlotLine::Set_FigureColor(string ColorForBackground, string ColorForGrid)
{
	BackgroundColor = ColorForBackground;
	GridColor = ColorForGrid;
}
void KInterface_Matlab_PlotLine::Set_Label_Title(string Label_Title)
{
	label_Title = Label_Title;
}
void KInterface_Matlab_PlotLine::Set_Label_xAxis(string Label_xAxis)
{
	label_xAxis = Label_xAxis;
}
void KInterface_Matlab_PlotLine::Set_Label_yAxis(string Label_yAxis)
{
	label_yAxis = Label_yAxis;
}
void KInterface_Matlab_PlotLine::Set_AxisLimit_X(double xMin, double xMax)
{
	axisLimit_xMin = xMin;
	axisLimit_xMax = xMax;
	if (!isSetAxisLimit_X)
		isSetAxisLimit_X = true;
}
void KInterface_Matlab_PlotLine::Set_AxisLimit_Y(double yMin, double yMax)
{
	axisLimit_yMin = yMin;
	axisLimit_yMax = yMax;
	if (!isSetAxisLimit_Y)
		isSetAxisLimit_Y = true;
}
void KInterface_Matlab_PlotLine::Set_NewFigure(bool NewFigure)
{
	isSetNewFigure = NewFigure;
}
void KInterface_Matlab_PlotLine::SavePlotFile(string mFilePath, long FigureNumber)
{
	if (haveYData)
	{
		if (FigureNumber <= 0)
			ErrorMessage("FigureNumer should more than zero");

		if (GetFileExtension(mFilePath) != ".m")
			ErrorMessage("should be save in [.m] file");

		ofstream	outFile;

		outFile.open((char*)mFilePath.c_str());

		outFile << "clear" << endl;
		if (isSetNewFigure)
			outFile << "figure1 = figure;" << endl << endl;
		else
			outFile << "figure(" << ToString(FigureNumber) << ")" << endl;

		outFile << "axes" << ToString(FigureNumber) << "=axes('Parent'," << "figure(" << ToString(FigureNumber) << ")";
		if (isShowGridX)
			outFile << ",'XGrid','on'";
		if (isShowGridY)
			outFile << ",'YGrid','on'";
		if (isLogScaleGridX)
			outFile << ",'XScale','log'";
		if (isLogScaleGridY)
			outFile << ",'YScale','log'";

		outFile << ",'Color'," << ToString(BackgroundColor);
		outFile << ",'XColor'," << ToString(GridColor);
		outFile << ",'YColor'," << ToString(GridColor);

		outFile << ");" << endl;
		if (isShowBox)
			outFile << "box(axes" << ToString(FigureNumber) << ",'on');";
		outFile << "hold(" << "axes" << ToString(FigureNumber) << ",'all');" << endl << endl;

		if (haveXData)
		{
			outFile << "xAxis=[";
			for (int x = 0; x<list_XData.length; x++)
			{
				outFile << ToString(list_XData.idata[x]);
				if (x<list_XData.length - 1)
					outFile << " ";
			}
			outFile << "];" << endl;
		}
		else
		{
			outFile << "xAxis=[";
			for (int x = 0; x<list_YData.at(0)->length; x++)
			{
				outFile << x;
				if (x<list_YData.at(0)->length - 1)
					outFile << " ";
			}
			outFile << "];" << endl;
		}

		for (unsigned int y = 0; y<list_YData.size(); y++)
		{
			outFile << "yAxis" << ToString(y) << "=[";
			for (int x = 0; x<list_YData.at(y)->length; x++)
			{
				outFile << list_YData.at(y)->idata[x];
				if (x<list_YData.at(y)->length - 1)
					outFile << " ";
			}
			outFile << "];" << endl;
		}
		outFile << endl;

		for (unsigned int z = 0; z<list_YData.size(); z++)
		{
			outFile << "plot(xAxis,yAxis" << ToString(z);

			outFile << ",'Color'," << list_lineColor.at(z);
			outFile << ",'LineStyle'," << list_lineStyle.at(z);
			outFile << ",'LineWidth'," << ToString(list_lineSize.at(z));
			outFile << ",'Marker'," << list_markStyle.at(z);
			outFile << ",'MarkerEdgeColor'," << list_markColorEdge.at(z);
			outFile << ",'MarkerFaceColor'," << list_markColorFace.at(z);
			outFile << ",'MarkerSize'," << ToString(list_markSize.at(z));
			outFile << ",'DisplayName','" << list_DataLabel.at(z) << "'";

			outFile << ");" << endl;
		}
		outFile << endl;

		if (isSetAxisLimit_X)
		{
			outFile << "xlim(" << "axes" << ToString(FigureNumber) << ",[" << axisLimit_xMin << " " << axisLimit_xMax << "]);" << endl;
			outFile << endl;
		}
		if (isSetAxisLimit_Y)
		{
			outFile << "ylim(" << "axes" << ToString(FigureNumber) << ",[" << axisLimit_yMin << " " << axisLimit_yMax << "]);" << endl;
			outFile << endl;
		}

		outFile << "title(" << "'" << label_Title << "'" << ");" << endl;
		outFile << "xlabel(" << "'" << label_xAxis << "'" << ");" << endl;
		outFile << "ylabel(" << "'" << label_yAxis << "'" << ");" << endl;

		outFile << endl;
		if (isShowLegend)
			outFile << "legend(" << "axes" << ToString(FigureNumber) << ",'show');" << endl << endl;

		if (isSetXTickLabel)
		{
			outFile << "set(gca,'XTick',[";
			for (unsigned int t = 0; t<list_xTick.size(); t++)
			{
				outFile << ToString(list_xTick.at(t));
				if (t<list_xTick.size() - 1)
					outFile << " ";
			}
			outFile << "]);" << endl;

			outFile << "set(gca,'XTickLabel',{";
			for (unsigned int t = 0; t<list_xTick.size(); t++)
			{
				outFile << "'" << ToString(list_xLabel.at(t)) << "'";
				if (t<list_xLabel.size() - 1)
					outFile << ",";
			}
			outFile << "});" << endl;
		}
		if (isSetYTickLabel)
		{
			outFile << "set(gca,'YTick',[";
			for (unsigned int t = 0; t<list_yTick.size(); t++)
			{
				outFile << ToString(list_yTick.at(t));
				if (t<list_yTick.size() - 1)
					outFile << " ";
			}
			outFile << "]);" << endl;

			outFile << "set(gca,'YTickLabel',{";
			for (unsigned int t = 0; t<list_yTick.size(); t++)
			{
				outFile << "'" << ToString(list_yLabel.at(t)) << "'";
				if (t<list_yLabel.size() - 1)
					outFile << ",";
			}
			outFile << "});" << endl;
		}

		outFile.close();
	}
	else
	{
		ErrorMessage("no data to plot");
	}
}