#include "KInterface_PCT.h"

void KInterface_PCT::jpegdecomp(unsigned char *indata,int width,int height,int depth,unsigned char *outbuffer)
{
	unsigned char pred_type;						/*predictor type used*/
	int num_pixels;									/*number of pixels image will expand too*/
	unsigned char huffbits[NIST_DEC_MEMSIZE2]; 		/*defines number of Huffman codes of each code length*/
	unsigned char huffvalues[NIST_DEC_MEMSIZE2];	/*defines order of Huffman code lengths in relation to code sizes*/
	int bit_count = 0;								/*marks the bit to receive from the input NIST_DEC_BYTE*/
		
	PTR_HUFFCODE ptr_huffcode_table;				/*used to store the sizes and codes for the huffman coding procedure*/
	int last_size;									/*last huffbit*/
	short int maxcode[11];							/*points to maximum code value for a given code length*/
	short int mincode[11];							/*points to minimum code value for a given code length*/
	int valptr[11];									/*points to first code in the huffman code table for a given code length*/
	int ptr_huff_decoder[10][512];					/*holds the code for all possible difference values that occur when encoding*/
	long int pixel;									/*current pixel number*/
	int diff_cat;									/*code word category*/
	unsigned int diff_code;							/*"raw" difference pixel*/
	int full_diff_code;								/*difference code extend to full precision*/
	int data_pred;									/*prediction of pixel value*/

	num_pixels = width * height;					/*number of pixels in original image*/
	this->define_huff_table(huffbits, huffvalues, &indata);
	pred_type = *indata++;							/*predictor type*/

	ptr_huffcode_table = build_huffsizes(&last_size, huffbits, 11);
	build_huffcodes(ptr_huffcode_table);			/*this routine builds a set of three tables used in decoding the compressed data*/
	decode_table_gen(ptr_huffcode_table, maxcode, mincode, valptr, huffbits);

	free(ptr_huffcode_table);						/*this routine builds a table used in decoding coded difference pixels*/
	build_huff_decode_table(ptr_huff_decoder);		/*decompress the pixel "differences" sequentially*/

	for(pixel = 0; pixel < num_pixels; pixel++) 
	{
		/*get next huffman category code from compressed input data stream*/
		diff_cat = decode_data(mincode, maxcode, valptr, huffvalues, &indata, &bit_count);

		/*get the required bits (given by huffman code) to reconstruct the difference value for the pixel*/
		diff_code = nextbits(&indata, &bit_count, diff_cat);

		/*extend the difference value to full precision*/
		full_diff_code = ptr_huff_decoder[diff_cat][diff_code];

		/*reverse the pixel prediction and store the pixel value in the output buffer*/
		data_pred = predict(outbuffer, width, pixel, depth, pred_type);
		*outbuffer = full_diff_code + data_pred;
		outbuffer++;
	}
}
void KInterface_PCT::define_huff_table(unsigned char huffbits[],unsigned char huffvalues[],unsigned char ** indata)
{
	int inx;						/*increment variable*/
	unsigned short int number;		/*number of huffbits and huffvalues*/
	number = *(*indata)++;
	for (inx = 0; inx < NIST_DEC_MEMSIZE2;  inx++)
		huffbits[inx] = *(*indata)++;
	for (inx = 0; inx < (number - NIST_DEC_MEMSIZE2); inx ++)
		huffvalues[inx] = *(*indata)++;
}
void KInterface_PCT::decode_table_gen(PTR_HUFFCODE ptr_huffcode_table,short int maxcode[],short int mincode[],int valptr[],unsigned char huffbits[])
{
	int inx, inx2 = 0;			/*increment variables*/
	for(inx = 1; inx <= NIST_DEC_MEMSIZE2; inx++) 
	{
		if(huffbits[inx - 1] == 0) 
		{
			maxcode[inx] = -1;
			continue;
		}
		valptr[inx] = inx2;
		mincode[inx] = (ptr_huffcode_table + inx2) -> code;
		inx2 = inx2 + huffbits[inx - 1] - 1;
		maxcode[inx] = (ptr_huffcode_table + inx2) -> code;
		inx2++;
	}
}
void KInterface_PCT::build_huff_decode_table(int ptr_huff_decoder[10][512])
{
	int cat, count, diff;	/*variables used to obtain desired codes for difference values*/
	int bit, difftemp;		/*variables used to determine codes for -ve difference codes*/
	for(count = NIST_DEC_SMALLESTDIFF; count < NIST_DEC_LARGESTDIFF; count++) 
	{
		diff = count;
		cat = categorize(diff);
		if(diff < 0) 
		{
			diff--;
			difftemp = 0;
			for(bit = 0; bit < cat; bit++)
			{
				if(((diff >> bit) & NIST_DEC_LSBITMASK) != 0)
					difftemp |= (NIST_DEC_LSBITMASK << bit);
			}
			diff = difftemp;
		}
		ptr_huff_decoder[cat][diff] = count;
	}
	return;
}
int KInterface_PCT::decode_data(short int mincode[], short int maxcode[], int valptr[], unsigned char huffvalues[], unsigned char **indata, int *bit_count)
{
	int inx, inx2;		/*increment variables*/
	int code;			/*becomes a huffman code word (one bit at a time)*/
	int diff_cat;		/*category of the huffman code word*/
	code = nextbits(indata, bit_count, 1);
	for(inx = 1; code > maxcode[inx]; inx++)
		code = (code << 1) + (nextbits(indata, bit_count, 1));
	inx2 = valptr[inx];
	inx2 = inx2 + code - mincode[inx];
	diff_cat = huffvalues[inx2];
	return diff_cat;
}
unsigned short int KInterface_PCT::nextbits(unsigned char **indata, int *bit_count, int bits_req)
{
	static unsigned char code;	/*next NIST_DEC_BYTE of data*/
	unsigned short int bits;	/*bits of current data NIST_DEC_BYTE requested*/
	int bits_needed;			/*additional bits required to finish request*/
	/*used to "mask out" n number of bits from data stream*/
	static unsigned char bit_mask[9] = {0x00,0x01,0x03,0x07,0x0f,0x1f,0x3f,0x7f,0xff};

	if(*bit_count == 0) 
	{
		code = *(*indata)++;
		*bit_count = NIST_DEC_BYTE;
	}
	if(bits_req <= *bit_count) 
	{
		bits = (code >>(*bit_count - bits_req)) & (bit_mask[bits_req]);
		*bit_count -= bits_req;
		code &= bit_mask[*bit_count];
	}
	else 
	{
		bits_needed = bits_req - *bit_count;
		bits = code << bits_needed;
		*bit_count = 0;
		bits |= nextbits(indata, bit_count, bits_needed);
	}
	return bits;
}
KInterface_PCT::PTR_HUFFCODE KInterface_PCT::build_huffsizes(int *temp_size,unsigned char *huffbits,int size)
{
	PTR_HUFFCODE ptr_huffcode_table;	/*table of huffman codes and sizes*/
	int code_size;						/*code sizes*/
	int number_of_codes = 1;			/*the number codes for a given code size*/
	if((ptr_huffcode_table = (PTR_HUFFCODE)calloc(size,sizeof(HUFFCODE))) == NULL)
		NIST_syserr("build_huffsizes", "calloc", "ptr_huffcode_table");
	*temp_size = 0;
	for(code_size = 1; code_size < size; code_size++) 
	{
		while(number_of_codes <= huffbits[code_size - 1])
		{
			(ptr_huffcode_table + *temp_size)->size = code_size;
			(*temp_size)++;
			number_of_codes++;
		}
		number_of_codes = 1;
	}
	(ptr_huffcode_table + *temp_size)->size = 0;
	return ptr_huffcode_table;
}
void KInterface_PCT::build_huffcodes(PTR_HUFFCODE ptr_huffcode_table)
{
	int pointer = 0;			/*pointer to code word information*/
	int temp_code = 0;			/*used to construct code word*/
	int temp_size;				/*used to construct code size*/

	temp_size = ptr_huffcode_table->size;

	do {
		do {
			(ptr_huffcode_table + pointer)->code = temp_code;
			temp_code++;
			pointer++;
		}
		while((ptr_huffcode_table + pointer)->size == temp_size);

		if((ptr_huffcode_table + pointer)->size == 0)
			return;

		do {
			temp_code <<= 1;
			temp_size++;
		}
		while((ptr_huffcode_table + pointer)->size != temp_size);
	}
	while((ptr_huffcode_table + pointer)->size == temp_size);
}
int KInterface_PCT::predict(unsigned char *indata, int width, long int pixel_num,int depth, int pred_type)
{
	int data_pred;			/*pixel prediction*/
	int multiples;			/*integer power function exponent*/

	if(pixel_num == 0)		/*determine predictor for first pixel*/
	{			
		multiples = depth - 1;
		for(data_pred = 1; multiples > 0; --multiples)
			data_pred *= NIST_DEC_BASE;
		return data_pred;
	}
	if(pixel_num > 0 && pixel_num < width) 
	{
		data_pred = *(indata - 1);	/*determine predictor for first line in the non-interleaved component*/
		return data_pred;
	}
	/*determine predictor for the rest of the pixels in the component*/
	if(pixel_num > (width - 1))  
	{
		if((pixel_num % width) == 0)	/*predictor if pixel is at the beginning of a line*/
			data_pred = *(indata - width);
		else 
			switch(pred_type) /*various predictor types defined in the standard*/
		{
			case 1:
				data_pred = *(indata - 1);
				break;
			case 2:
				data_pred = *(indata - width);
				break;
			case 3:
				data_pred = *(indata -(width + 1));
				break;
			case 4:
				data_pred = *(indata - 1) + *(indata - width) - *(indata -(width + 1));
				break;
			case 5:
				data_pred = *(indata - 1) + ((*(indata - width) >> 1) - (*(indata -(width + 1)) >> 1));
				break;
			case 6:
				data_pred = *(indata - width) + ((*(indata - 1) >> 1) - (*(indata -(width + 1)) >> 1));
				break;
			case 7:
				data_pred = (*(indata -1) + *(indata - width)) / 2;
				break;
			default:
				NIST_fatalerr("predict","Invalid prediction type.  Must be a value 1-7.",(char *) NULL);
				break;
		}
	}
	return data_pred;
}
short int KInterface_PCT::categorize(short int difference)
{
	int bit;				/*bit pointer to difference value*/
	if(difference == 0)
		return (0);   		/*difference category zero*/
	if (difference < 0)
		difference *= -1;
	for (bit = 0; bit < NIST_DEC_SHORTCODELENGTH; bit++) 
	{
		if ((difference & NIST_DEC_CATMASK) != 0)
			return (NIST_DEC_SHORTCODELENGTH - bit);
		difference <<= 1;
	}
	return (0);
}
void KInterface_PCT::NIST_syserr(char *funcname, char *syscall, char *msg)
{
   exit(-1);
}
void KInterface_PCT::NIST_fatalerr(char *s1,char *s2,char *s3)
{
	extern char *getenv();

	(void) fflush(stdout);
	if (s2 == (char *) NULL)
		(void) fprintf_s(stderr,"ERROR: %s\n",s1);
	else if (s3 == (char *) NULL)
		(void) fprintf_s(stderr,"ERROR: %s: %s\n",s1,s2);
	else
		(void) fprintf_s(stderr,"ERROR: %s: %s: %s\n",s1,s2,s3);
	(void) fflush(stderr);

	printf("FATAL ERROR\n");
	system("pause");
	exit(1);
}
void KInterface_PCT::low_priority_abort()
{
	exit(1);		// should never get here 
}
float KInterface_PCT::NIST_PixPerByte(int depth)
{
   float pixperbyte;

   switch(depth){
   case 1:
	  pixperbyte = 8.0;
	  break;
   case 2:
	  pixperbyte = 4.0;
	  break;
   case 4:
	  pixperbyte = 2.0;
	  break;
   case 8:
	  pixperbyte = 1.0;
	  break;
   case 16:
	  pixperbyte = 0.5;
	  break;
   case 32:
	  pixperbyte = 0.25;
	  break;
   default:
	  NIST_fatalerr("PixPerByte",
	"depth is not a power of 2 between 1 and 32 (inclusive)",
	NULL);
   }
   return pixperbyte;
}
int KInterface_PCT::NIST_SizeFromDepth(int pixwidth,int pixheight,int depth)
{
   int filesize;
   float pixperbyte;

   pixperbyte = NIST_PixPerByte(depth);
   filesize = ((int) ceil(pixwidth/pixperbyte) * pixheight);
   return filesize;
}
void KInterface_PCT::NIST_ReadiheadRaster(char *file,NIST_HEAD **head,unsigned char **data,int *width,int *height,int *depth)
{
   FILE *fp;
   //NIST_HEAD *readNISThdr();
   int comp, filesize, complen, n;
   unsigned char *indata, *outdata;

   // open the image file 
   fopen_s(&fp,file,"rb");
   if (fp == NULL)
	  NIST_syserr("ReadIheadRaster",file,"fopen failed");

   // read in the image header 
   *head = NIST_readihdr(fp);

   n = sscanf_s((*head)->compress,"%d",&comp);
   if (n != 1)
	  NIST_fatalerr("ReadIheadRaster",file,"sscanf_s failed on compress field");

   // convert string fields to integers 
   n = sscanf_s((*head)->depth,"%d",depth);
   if (n != 1)
	  NIST_fatalerr("ReadIheadRaster",file,"sscanf_s failed on depth field");
   n = sscanf_s((*head)->width,"%d",width);
   if (n != 1)
	  NIST_fatalerr("ReadIheadRaster",file,"sscanf_s failed on width field");
   n = sscanf_s((*head)->height,"%d",height);
   if (n != 1)
	  NIST_fatalerr("ReadIheadRaster",file,"sscanf_s failed on height field");
   n = sscanf_s((*head)->complen,"%d",&complen);
   if (n != 1)
	  NIST_fatalerr("ReadIheadRaster",file,"sscanf_s failed on complen field");

   // allocate a raster data buffer 
   filesize = NIST_SizeFromDepth(*width,*height,*depth);
   outdata = (unsigned char *) malloc(filesize);
   if(outdata == NULL)
	  NIST_syserr("ReadIheadRaster",file,"malloc (outdata)");

   // read in the raster data 
   if(comp == UNCOMP) 
   {   // file is uncompressed 
	  n = (int)fread(outdata,1,filesize,fp);
	  if (n != filesize) {
		 (void) fprintf_s(stderr,
		"ReadIheadRaster: %s: fread returned %d (expected %d)\n",
		file,n,filesize);
		 exit(1);
	  } 
	  indata = (unsigned char *)malloc(complen);
	  // IF 
   } 
   else
   {
	  indata = (unsigned char *) malloc(complen);
	  if (indata == NULL)
		NIST_syserr("ReadIheadRaster",file,"malloc (indata)");
		n = (int)fread(indata,1,complen,fp); // file compressed 
	  if (n != complen) {
		  (void)fprintf_s(stderr, "ReadIheadRaster: %s: fread returned %d (expected %d)\n", "", n, complen);
		 exit(1);
	  } // IF 
   }

   switch (comp) {

	  case JPEG:
		jpegdecomp(indata,*width,*height,*depth,outdata);
		//bzero((*head)->complen,SHORT_CHARS);
		//bzero((*head)->compress,SHORT_CHARS);
		//(void) sprintf((*head)->complen,"%d",0);		
		//(void) sprintf((*head)->compress,"%d",UNCOMP);
		(void) sprintf_s( (*head)->complen, 255,"%d",0);
		(void) sprintf_s((*head)->compress, 255,"%d",UNCOMP);
		*data = outdata;
		free((char *)indata);
	  break;

	  case UNCOMP:
		*data = outdata;
	  break;
	  default:
		 NIST_fatalerr("ReadIheadRaster",file,"Invalid compression code");
	  break;
   }
   // close the image file 
   (void) fclose(fp);
}
void KInterface_PCT::NIST_ReadiheadRaster2(char *file,unsigned char **data,int *width,int *height,int *depth)
{
   FILE *fp;
   NIST_HEAD *head;
  // NIST_HEAD *readNISThdr();
   int comp, filesize, complen, n;
   unsigned char *indata, *outdata;

   // open the image file 
   fopen_s(&fp,file,"rb");
   if (fp == NULL)
	  NIST_syserr("ReadIheadRaster",file,"fopen failed");

   // read in the image header 
   head = NIST_readihdr(fp);

   n = sscanf_s((head)->compress,"%d",&comp);
   if (n != 1)
	  NIST_fatalerr("ReadIheadRaster",file,"sscanf_s failed on compress field");

   // convert string fields to integers 
   n = sscanf_s((head)->depth,"%d",depth);
   if (n != 1)
	  NIST_fatalerr("ReadIheadRaster",file,"sscanf_s failed on depth field");
   n = sscanf_s((head)->width,"%d",width);
   if (n != 1)
	  NIST_fatalerr("ReadIheadRaster",file,"sscanf_s failed on width field");
   n = sscanf_s((head)->height,"%d",height);
   if (n != 1)
	  NIST_fatalerr("ReadIheadRaster",file,"sscanf_s failed on height field");
   n = sscanf_s((head)->complen,"%d",&complen);
   if (n != 1)
	  NIST_fatalerr("ReadIheadRaster",file,"sscanf_s failed on complen field");

   // allocateNIST_ a raster data buffer 
   filesize = NIST_SizeFromDepth(*width,*height,*depth);
   outdata = (unsigned char *) malloc(filesize);
   if(outdata == NULL)
	  NIST_syserr("ReadIheadRaster",file,"malloc (outdata)");

   // read in the raster data 
   if(comp == UNCOMP) {   // file is uncompressed 
	  n = (int)fread(outdata,1,filesize,fp);
	  if (n != filesize) {
		 (void) fprintf_s(stderr,
		"ReadIheadRaster: %s: fread returned %d (expected %d)\n",
		file,n,filesize);
		 exit(1);
	  } 
	  // IF 
   } else {
	  indata = (unsigned char *) malloc(complen);
	  if (indata == NULL)
		NIST_syserr("ReadIheadRaster",file,"malloc (indata)");
		n = (int)fread(indata,1,complen,fp); // file compressed 
	  if (n != complen) 
	  {
		  (void)fprintf_s(stderr, "ReadIheadRaster: %s: fread returned %d (expected %d)\n", "", n, complen);
		 exit(1);
	  } // IF 
   }

   switch (comp) {
	  case UNCOMP:
		*data = outdata;
	  break;
	  default:
		 NIST_fatalerr("ReadIheadRaster",file,"Invalid compression code");
	  break;
   }
   // close the image file 
   (void) fclose(fp);
   free(head);
}
void KInterface_PCT::NIST_writeihdrfile(char *file,NIST_HEAD *head,unsigned char *data)
{
   FILE *fp;
   int width,height,depth,code,filesize,n;

   // reopen the image file for writing 
   fopen_s(&fp,file,"w");
   if (fp == NULL)
	  NIST_syserr("NIST_writeihdrfile","fopen",file);

   n = sscanf_s(head->width,"%d",&width);
   if (n != 1)
	  NIST_fatalerr("NIST_writeihdrfile","sscanf_s failed on width field",NULL);
   n = sscanf_s(head->height,"%d",&height);
   if (n != 1)
	  NIST_fatalerr("NIST_writeihdrfile","sscanf_s failed on height field",NULL);
   n = sscanf_s(head->depth,"%d",&depth);
   if (n != 1)
	  NIST_fatalerr("NIST_writeihdrfile","sscanf_s failed on depth field",NULL);
   n = sscanf_s(head->compress, "%d", &code);
   if (n != 1)
	  NIST_fatalerr("NIST_writeihdrfile","sscanf_s failed on compression code field",NULL);

   filesize = NIST_SizeFromDepth(width,height,depth);

   if(code == UNCOMP){
	  //sprintf(head->complen, "%d", 0);
	   sprintf_s(head->complen,255, "%d", 0);
	  NIST_writeihdr(fp,head);
	  n = (int)fwrite(data,1,filesize,fp);
	  if (n != filesize)
		 NIST_syserr("writeNISThdrfile", "fwrite", file);
   }
   else{
	  NIST_fatalerr("writeNISThdrfile","Unknown compression",NULL);
   }

   // closes the new file 
   (void) fclose(fp);
}
void KInterface_PCT::NIST_printihdr(NIST_HEAD *head,FILE *fp)
{
   fprintf_s(fp,"IMAGE FILE HEADER\n");
   fprintf_s(fp,"~~~~~~~~~~~~~~~~~\n");
   fprintf_s(fp,"Identity\t:  %s\n", head->id);
   fprintf_s(fp,"Header Size\t:  %d (bytes)\n",(int)sizeof(NIST_HEAD));
   fprintf_s(fp,"Date Created\t:  %s\n", head->created);
   fprintf_s(fp,"Width\t\t:  %s (pixels)\n",head->width);
   fprintf_s(fp,"Height\t\t:  %s (pixels)\n",head->height);
   fprintf_s(fp,"Bits per Pixel\t:  %s\n",head->depth);
   fprintf_s(fp,"Resolution\t:  %s (ppi)\n",head->density);
   fprintf_s(fp,"Compression\t:  %s (code)\n",head->compress);
   fprintf_s(fp,"Compress Length\t:  %s (bytes)\n",head->complen);
   fprintf_s(fp,"Scan Alignment\t:  %s (bits)\n",head->align);
   fprintf_s(fp,"Image Data Unit\t:  %s (bits)\n",head->unitsize);
   if(head->byte_order == HILOW)
	  fprintf_s(fp,"Byte Order\t:  High-Low\n");
   else
	  fprintf_s(fp,"Byte Order\t:  Low-High\n");
   if(head->sigbit == MSBF)
	  fprintf_s(fp,"MSBit\t\t:  First\n");
   else
	  fprintf_s(fp,"MSBit\t\t:  Last\n");
   fprintf_s(fp,"Column Offset\t:  %s (pixels)\n",head->pix_offset);
   fprintf_s(fp,"White Pixel\t:  %s\n",head->whitepix);
   if(head->issigned == SIGNED)
	  fprintf_s(fp,"Data Units\t:  Signed\n");
   else
	  fprintf_s(fp,"Data Units\t:  Unsigned\n");
   fprintf_s(fp,"Scan Order\t:  ");
   if(head->rm_cm == ROW_MAJ)
	  fprintf_s(fp,"Row Major,\n");
   else
	  fprintf_s(fp,"Column Major,\n");
   if(head->tb_bt == TOP2BOT)
	  fprintf_s(fp,"\t\t   Top to Bottom,\n");
   else
	  fprintf_s(fp,"\t\t   Bottom to Top,\n");
   if(head->lr_rl == LEFT2RIGHT)
	  fprintf_s(fp,"\t\t   Left to Right\n");
   else
	  fprintf_s(fp,"\t\t   Right to Left\n");

   if(*(head->parent) != NULL){
	  fprintf_s(fp,"Parent\t\t:  %s\n",head->parent);
	  fprintf_s(fp,"X Origin\t:  %s (pixels)\n",head->par_x);
	  fprintf_s(fp,"Y Origin\t:  %s (pixels)\n",head->par_y);
   }
}
KInterface_PCT::NIST_HEAD* KInterface_PCT::NIST_readihdr(FILE *fp)
{
   NIST_HEAD *head;
   char lenstr[SHORT_CHARS];
   int len;

   fread(lenstr,sizeof(char),SHORT_CHARS,fp);
   sscanf_s(lenstr,"%d",&len);
   if(len != IHDR_SIZE){
	  fprintf_s(stderr,
		 "Record Sync Error: Header not found or old format.\n");
	  exit(-1);
   }
   head = (NIST_HEAD *)malloc(sizeof(NIST_HEAD));
   fread(head,sizeof(char),len,fp);
   return(head);
}
void KInterface_PCT::NIST_writeihdr(FILE *fp,NIST_HEAD *ihead)
{
   int i;
   char lenstr[SHORT_CHARS];

   for (i = 0; i < SHORT_CHARS; i++)
	  lenstr[i] = '\0';
   // creates a string from of header size 
   //sprintf(lenstr,"%d",sizeof(NIST_HEAD));
   sprintf_s(lenstr,255,"%d",(int)sizeof(NIST_HEAD));
   // writes the length string in headerto the file 
   fwrite(lenstr,sizeof(char),SHORT_CHARS,fp);
   // writes the given header to the file 
   fwrite(ihead,sizeof(NIST_HEAD),1,fp);
}

void KInterface_PCT::LoadFile(char *fileName,KArray1D<unsigned char> *imgData,int *Width,int *Height)
{
	FILE			*fp;
	NIST_HEAD		*head;
	int				comp, filesize, complen, n;
	unsigned char	*indata,*outdata;
	int				depth,width,height;

	fopen_s(&fp,fileName,"rb");			// open the image file 
	if (fp == NULL)
	{
		printf("cannot read image: [%s]\n",fileName);
		system("pause");
		exit(0);
	}

	// read in the image header
	head = NIST_readihdr(fp);
	n = sscanf_s((head)->compress,"%d",&comp);
	if (n != 1)
		NIST_fatalerr("ReadNIST header","","sscanf_s failed on compress field");

	// convert string fields to integers
	n = sscanf_s((head)->depth,"%d",&depth);
	if (n != 1)      NIST_fatalerr("ReadNIST header","","sscanf_s failed on depth field");
	n = sscanf_s((head)->width,"%d",&width);
	if (n != 1)      NIST_fatalerr("ReadNIST header","","sscanf_s failed on width field");
	n = sscanf_s((head)->height,"%d",&height);
	if (n != 1)      NIST_fatalerr("ReadNIST header","","sscanf_s failed on height field");
	n = sscanf_s((head)->complen,"%d",&complen);
	if (n != 1)      NIST_fatalerr("ReadNIST header","","sscanf_s failed on complen field");

	// allocate a raster data buffer 
	filesize = NIST_SizeFromDepth(width,height,depth);
	//this->Allocate(width,height);
	outdata = Alloc<unsigned char>(filesize);		//   outdata	= this->idata[0];

	if(outdata == NULL)
		NIST_syserr("ReadNIST","","malloc (outdata)");

	// read in the raster data 
	if(comp == UNCOMP) // file is uncompressed 
	{
		n = (int)fread(outdata,1,filesize,fp);
		if (n != filesize) 
		{
			(void) fprintf_s(stderr,"ReadNIST: %s: fread returned %d (expected %d)\n","",n,filesize);
			exit(1);
		} 
		indata = Alloc<unsigned char>(complen);
	} 
	else
	{
		indata = Alloc<unsigned char>(complen);
		if (indata == NULL)
			NIST_syserr("ReadNIST","","malloc (indata)");
		n = (int)fread(indata,1,complen,fp); // file compressed 
		if (n != complen) {
			(void)fprintf_s(stderr, "ReadNIST: %s: fread returned %d (expected %d)\n", "", n, complen);
			exit(1);
		}
	}
	switch (comp) 
	{
	case JPEG:
		jpegdecomp(indata,width,height,depth,outdata);
		(void) sprintf_s((head)->complen,"%d",0);
		(void) sprintf_s((head)->compress,"%d",UNCOMP);
		Free(indata,complen);
		break;
	case UNCOMP:
		break;
	default:
		NIST_fatalerr("ReadNIST","","Invalid compression code");
		break;
	}
	imgData->Allocate(filesize);
	for(int n=0;n<filesize;n++)
		imgData->idata[n] = outdata[n];
	
	*Width	= width;
	*Height	= height;

	Free<unsigned char>(outdata,filesize);
	free(head);
	fclose(fp);
}