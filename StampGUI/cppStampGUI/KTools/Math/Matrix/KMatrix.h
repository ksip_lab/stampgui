// ------------------------------------------------ //
// contain:		Matrix Tools
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KMatrix_H
#define _KMatrix_H

#include "KGeneral.h"

//========================================//
// ------------ Matrix Tools ------------ //
// [0] Cross product
// [1] Transpose
// [2] Determinant
// [3] Trace
// [4] Inverse
// [5] Eigen (doesn't work --> OpenCV Bug)
//========================================//
template <typename T1,typename T2,typename T3> void KMatrix_CrossProduct(KArray2D<T1> *m1,KArray2D<T2> *m2,KArray2D<T3> *out);
template <typename T1,typename T2> void KMatrix_Transpose(KArray2D<T1> *in,KArray2D<T2> *out);
template <typename T1,typename T2> void KMatrix_Determinant(KArray2D<T1> *m,T2 *det);
template <typename T1,typename T2> void KMatrix_Trace(KArray2D<T1> *m,T2 *trace);
template <typename T> bool KMatrix_Inverse(KArray2D<T> *m);
template <typename T> void KMatrix_Eigen(KArray2D<T> *m,KArray2D<T> *eigenVector,KArray1D<T> *eigenValue);

//===========================================================//
template <typename T1,typename T2,typename T3> void KMatrix_CrossProduct(KArray2D<T1> *m1,KArray2D<T2> *m2,KArray2D<T3> *out)
{
	double sum;
	if(m1->width==m2->height)
	{
		out->Resize(m2->width,m1->height);

		for(int out_y=0;out_y<out->height;out_y++)
		for(int out_x=0;out_x<out->width;out_x++)
		{
			sum	= 0;
			for(int c=0;c<m1->width;c++)
				sum += (double)( (double)m1->idata[c][out_y] * (double)m2->idata[out_x][c] );
			out->idata[out_x][out_y] = (T3)sum;
		}
	}
	else
		ErrorMessage("Rows and Columns number not suitable for [CrossProduct]");
}
template <typename T1,typename T2> void KMatrix_Transpose(KArray2D<T1> *in,KArray2D<T2> *out)
{
	out->Resize(in->height,in->width);
	for(int c=0;c<in->width;c++)
		for(int r=0;r<in->height;r++)
			out->idata[r][c] = (T2)in->idata[c][r];
}
template <typename T1,typename T2> void KMatrix_Determinant(KArray2D<T1> *m,T2 *det)
{
	if(m->width==m->height)
	{
		Mat		M(m->height,m->width,DataType<double>::type);
		for(int c=0;c<m->width;c++)
			for(int r=0;r<m->height;r++)
				M.at<double>(r,c)=(double)m->idata[c][r];
		*det = (T2)determinant(M);
		M.release();
	}
	else
		ErrorMessage("[Determinant] Only be use for square matrix");
}
template <typename T1,typename T2> void KMatrix_Trace(KArray2D<T1> *m,T2 *trace)
{
	if(m->width==m->height)
	{
		Mat		M(m->height,m->width,DataType<double>::type);
		for(int c=0;c<m->width;c++)
			for(int r=0;r<m->height;r++)
				M.at<double>(r,c)=(double)m->idata[c][r];
		*trace = (T2)trace(M);
		M.release();
	}
	else
		ErrorMessage("[Trace] Only be use for square matrix");
}
template <typename T> bool KMatrix_Inverse(KArray2D<T> *m)
{
	bool isInverInvertible;
	if(m->width==m->height)
	{
		Mat		M(m->height,m->width,DataType<double>::type);
		double	chk;
		for(int c=0;c<m->width;c++)
			for(int r=0;r<m->height;r++)
				M.at<double>(r,c)=(double)m->idata[c][r];
		chk = invert(M,M,DECOMP_LU);
		isInverInvertible = true;
		if (chk == 0)
		{
			chk = invert(M, M, DECOMP_SVD);
			isInverInvertible = false;
		}
		for(int c=0;c<m->width;c++)
			for(int r=0;r<m->height;r++)
				m->idata[c][r] = (T)M.at<double>(r,c);
		M.release();
	}
	else
		ErrorMessage("[Inverse] Only be use for square matrix");
	return isInverInvertible;
}
template <typename T> void KMatrix_Eigen(KArray2D<T> *mInput,KArray2D<T> *eigenVector,KArray1D<T> *eigenValue)
{
	if(mInput->height==mInput->width)
	{
		eigenVector->Resize(mInput->width,mInput->height);
		eigenValue->Resize(1,mInput->height);

		CvMat* m			= cvCreateMat(mInput->height,mInput->width,CV_32FC1);
		CvMat* evects		= cvCreateMat(mInput->height,mInput->width,CV_32FC1);
		CvMat* evals		= cvCreateMat(mInput->height,1,CV_32FC1);

		float* ptrM			= m->data.fl;
		float* ptrEvects	= evects->data.fl;
		float* ptrEvals		= evals->data.fl;

		for(int c=0;c<mInput->width;c++)
			for(int r=0;r<mInput->height;r++)
				ptrM[r*m->cols+c] = (float)mInput->idata[c][r];

		cvEigenVV(m,evects,evals,DBL_EPSILON, 0, 0);
		ErrorMessage("the EigenVector & EigenValue is Wrong!!!");

		for(int r=0;r<evects->rows;r++)
		{
			for(int c=0;c<evects->cols;c++)
			{
				eigenVector->idata[c][r] = ptrEvects[r*evects->cols+c];
			}
			eigenValue->idata[0][r] = ptrEvals[r*evals->cols];
		}

		CvScalar tmp;
		for(int i = 0; i < evals->rows; i++ )
		{
			for(int j = 0; j < evals->cols; j++ )
			{
				tmp = cvGet2D(evects,i,j);
				eigenVector->idata[j][i] = tmp.val[0];
			}
			tmp = cvGet2D(evals,i,0);
			eigenValue->idata[0][i] = tmp.val[0];
		}

		cvReleaseMat(&m);
		cvReleaseMat(&evects);
		cvReleaseMat(&evals);
	}
	else
	{
		ErrorMessage("[Eigen] Only be use for square matrix");
	}
}

#endif