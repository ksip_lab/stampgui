// ------------------------------------------------ //
// contain:		Statistic tools
//
// developer:	Krisada Phromsuthirak, KSIP Member
//				Arucha Ruengchokanun, KSIP Member
// ------------------------------------------------ //

#ifndef _KStatistic_H
#define _KStatistic_H

#include "KGeneral.h"

template <typename T> T Min(KArray1D<T> *data, int strPos, int endPos, int *X = NULL);
template <typename T> T Min(KArray2D<T> *data, int strX, int endX, int strY, int endY, int *X = NULL, int *Y = NULL);
template <typename T> T Min(KArray3D<T> *data, int strD, int endD, int strX, int endX, int strY, int endY, int *D = NULL, int *X = NULL, int *Y = NULL);
template <typename T> T Min(KArray4D<T> *data, int strF, int endF, int strD, int endD, int strX, int endX, int strY, int endY, int *F = NULL, int *D = NULL, int *X = NULL, int *Y = NULL);

template <typename T> T Min(KArray1D<T> *data, int *X = NULL);
template <typename T> T Min(KArray2D<T> *data, int *X = NULL, int *Y = NULL);
template <typename T> T Min(KArray3D<T> *data, int *D = NULL, int *X = NULL, int *Y = NULL);
template <typename T> T Min(KArray4D<T> *data, int *F = NULL, int *D = NULL, int *X = NULL, int *Y = NULL);

template <typename T> T Max(KArray1D<T> *data, int strPos, int endPos, int *X = NULL);
template <typename T> T Max(KArray2D<T> *data, int strX, int endX, int strY, int endY, int *X = NULL, int *Y = NULL);
template <typename T> T Max(KArray3D<T> *data, int strD, int endD, int strX, int endX, int strY, int endY, int *D = NULL, int *X = NULL, int *Y = NULL);
template <typename T> T Max(KArray4D<T> *data, int strF, int endF, int strD, int endD, int strX, int endX, int strY, int endY, int *F = NULL, int *D = NULL, int *X = NULL, int *Y = NULL);

template <typename T> T Max(KArray1D<T> *data, int *X = NULL);
template <typename T> T Max(KArray2D<T> *data, int *X = NULL, int *Y = NULL);
template <typename T> T Max(KArray3D<T> *data, int *D = NULL, int *X = NULL, int *Y = NULL);
template <typename T> T Max(KArray4D<T> *data, int *F = NULL, int *D = NULL, int *X = NULL, int *Y = NULL);

template <typename T> T Median(KArray1D<T> *data, int strPos, int endPos);
template <typename T> T Median(KArray2D<T> *data, int strX, int endX, int strY, int endY);
template <typename T> T Median(KArray3D<T> *data, int strD, int endD, int strX, int endX, int strY, int endY);
template <typename T> T Median(KArray4D<T> *data, int strF, int endF, int strD, int endD, int strX, int endX, int strY, int endY);

template <typename T> T Median(KArray1D<T> *data);
template <typename T> T Median(KArray2D<T> *data);
template <typename T> T Median(KArray3D<T> *data);
template <typename T> T Median(KArray4D<T> *data);

template <typename T> double Mean(KArray1D<T> *data, int strPos, int endPos);
template <typename T> double Mean(KArray2D<T> *data, int strX, int endX, int strY, int endY);
template <typename T> double Mean(KArray3D<T> *data, int strD, int endD, int strX, int endX, int strY, int endY);
template <typename T> double Mean(KArray4D<T> *data, int strF, int endF, int strD, int endD, int strX, int endX, int strY, int endY);

template <typename T> double Mean(KArray1D<T> *data);
template <typename T> double Mean(KArray2D<T> *data);
template <typename T> double Mean(KArray3D<T> *data);
template <typename T> double Mean(KArray4D<T> *data);

template <typename T> double Sum(KArray1D<T> *data, int strPos, int endPos);
template <typename T> double Sum(KArray2D<T> *data, int strX, int endX, int strY, int endY);
template <typename T> double Sum(KArray3D<T> *data, int strD, int endD, int strX, int endX, int strY, int endY);
template <typename T> double Sum(KArray4D<T> *data, int strF, int endF, int strD, int endD, int strX, int endX, int strY, int endY);

template <typename T> double Sum(KArray1D<T> *data);
template <typename T> double Sum(KArray2D<T> *data);
template <typename T> double Sum(KArray3D<T> *data);
template <typename T> double Sum(KArray4D<T> *data);

template <typename T> double Variance(KArray1D<T> *data, int strPos, int endPos);
template <typename T> double Variance(KArray2D<T> *data, int strX, int endX, int strY, int endY);
template <typename T> double Variance(KArray3D<T> *data, int strD, int endD, int strX, int endX, int strY, int endY);
template <typename T> double Variance(KArray4D<T> *data, int strF, int endF, int strD, int endD, int strX, int endX, int strY, int endY);

template <typename T> double Variance(KArray1D<T> *data);
template <typename T> double Variance(KArray2D<T> *data);
template <typename T> double Variance(KArray3D<T> *data);
template <typename T> double Variance(KArray4D<T> *data);

template <typename T> void Normalization(KArray1D<T> *data, int strPos, int endPos, double mean, double variance);
template <typename T> void Normalization(KArray2D<T> *data, int strX, int endX, int strY, int endY, double mean, double variance);
template <typename T> void Normalization(KArray3D<T> *data, int strD, int endD, int strX, int endX, int strY, int endY, double mean, double variance);
template <typename T> void Normalization(KArray4D<T> *data, int strF, int endF, int strD, int endD, int strX, int endX, int strY, int endY, double mean, double variance);

template <typename T> void Normalization(KArray1D<T> *data, double mean, double variance);
template <typename T> void Normalization(KArray2D<T> *data, double mean, double variance);
template <typename T> void Normalization(KArray3D<T> *data, double mean, double variance);
template <typename T> void Normalization(KArray4D<T> *data, double mean, double variance);

template <typename T> double Moment(KArray1D<T> *data, double order);
template <typename T> double Moment(KArray2D<T> *data, double order1, double order2);
template <typename T> double Moment(KArray3D<T> *data, double order1, double order2,double order3);
template <typename T> double Moment(KArray4D<T> *data, double order1, double order2, double order3,double order4);

template <typename T> double Moment(KArray1D<T> *data, int strPos, int endPos, double order);
template <typename T> double Moment(KArray2D<T> *data, int strX, int endX, int strY, int endY, double order1, double order2);
template <typename T> double Moment(KArray3D<T> *data, int strD, int endD, int strX, int endX, int strY, int endY, double order1, double order2, double order3);
template <typename T> double Moment(KArray4D<T> *data, int strF, int endF, int strD, int endD, int strX, int endX, int strY, int endY, double order1, double order2, double order3, double order4);

template <typename T> double CentralMoment(KArray2D<T> *data, double order1, double order2);
template <typename T> double CentralMoment(KArray2D<T> *data, int strX, int strY, int endX, int endY, double order1, double order2);

template <typename T> void Convariance(KArray2D<T> *data, KArray2D<T> *cov);
template <typename T> void Convariance(KArray2D<T> *data, int strX, int strY, int endX, int endY, KArray2D<T> *cov);

template <typename T1, typename T2> void Centroid(KArray2D<T1> *data, KCoor<T2> &centroid);
template <typename T1, typename T2> void Centroid(KList1D<KCoor<T1>> *listCoor, KCoor<T2> &centroid);
template <typename T1, typename T2, typename T3> void Centroid(KArray2D<T1> *data, KList1D<KCoor<T1>> *listCoor, KCoor<T2> &centroid);

template <typename T> void ABS(KArray1D<T> *data);
template <typename T> void ABS(KArray2D<T> *data);
template <typename T> void ABS(KArray3D<T> *data);
template <typename T> void ABS(KArray4D<T> *data);

template <typename T> void ABS(KArray1D<T> *data)
{
	for (int l = 0; l < data->length; l++)
		data->idata[l] = ABS(data->idata[l]);
}
template <typename T> void ABS(KArray2D<T> *data)
{
	for (int y = 0; y < data->height;y++)
	for (int x = 0; x < data->width; x++)
		data->idata[x][y] = ABS(data->idata[x][y]);
}
template <typename T> void ABS(KArray3D<T> *data)
{
	for (int d = 0; d < data->depth; d++)
	for (int y = 0; y < data->height; y++)
	for (int x = 0; x < data->width; x++)
		data->idata[d][x][y] = ABS(data->idata[d][x][y]);
}
template <typename T> void ABS(KArray4D<T> *data)
{
	for (int f = 0; f < data->frame; f++)
	for (int d = 0; d < data->depth; d++)
	for (int y = 0; y < data->height; y++)
	for (int x = 0; x < data->width; x++)
		data->idata[f][d][x][y] = ABS(data->idata[f][d][x][y]);
}
template <typename T1, typename T2> void Centroid(KArray2D<T1> *data, KCoor<T2> &centroid)
{
	if (!data->isAlloc)
		ErrorMessage("Error [Centroid] : no data");
	
	double cx, cy, sum;
	sum = cx = cy = 0;
	for (int y = 0; y < data->height; y++)
	for (int x = 0; x < data->width; x++)
	{
		cx = cx + (double)((double)x*(double)data->idata[x][y]);
		cy = cy + (double)((double)y*(double)data->idata[x][y]);
		sum = sum + (double)data->idata[x][y];
	}

	if (sum != 0)
	{
		centroid.x = (T2)(cx / sum);
		centroid.y = (T2)(cy / sum);
	}
	else
	{
		centroid.x = (T2)UNDEFINED;
		centroid.y = (T2)UNDEFINED;
	}
}
template <typename T1, typename T2> void Centroid(KList1D<KCoor<T1>> *listCoor, KCoor<T2> &centroid)
{
	if (listCoor->size()<=0)
		ErrorMessage("Error [Centroid] : no data");

	double cx, cy;
	cx = cy = 0;

	for (unsigned int z = 0; z < listCoor->size(); z++)
	{
		cx = cx + (double)listCoor->at(z).x;
		cy = cy + (double)listCoor->at(z).y;
	}

	centroid.x = (T2)(cx / (double)listCoor->size());
	centroid.y = (T2)(cy / (double)listCoor->size());
}
template <typename T1, typename T2, typename T3> void Centroid(KArray2D<T1> *data, KList1D<KCoor<T1>> *listCoor, KCoor<T2> &centroid)
{
	if (listCoor->size() <= 0)
		ErrorMessage("Error [Centroid] : no data");

	if (!data->isAlloc)
		ErrorMessage("Error [Centroid] : no data");

	double sum, cx, cy;
	sum = cx = cy = 0;

	for (unsigned int z = 0; z < listCoor->size(); z++)
	{
		if (data->IsInBound((int)listCoor->at(z).x, (int)listCoor->at(z).y))
		{
			cx = cx + (double)((double)(listCoor->at(z).x)*(double)(data->idata[(int)(listCoor->at(z).x)][(int)(listCoor->at(z).y)]));
			cy = cy + (double)((double)(listCoor->at(z).y)*(double)(data->idata[(int)(listCoor->at(z).x)][(int)(listCoor->at(z).y)]));
			sum = sum + (double)(data->idata[(int)(listCoor->at(z).x)][(int)(listCoor->at(z).y)]);
		}
	}

	if (sum != 0)
	{
		centroid.x = (T2)(cx / sum);
		centroid.y = (T2)(cy / sum);
	}
	else
	{
		centroid.x = (T2)UNDEFINED;
		centroid.y = (T2)UNDEFINED;
	}
}

template <typename T> T Min(KArray1D<T> *data, int strPos, int endPos, int *X)
{
	if (data->isAlloc && data->length > 0)
	{
		T ans = data->idata[0];
		for (int z = strPos; z < endPos; z++)
		{
			if (data->IsInBound(z))
			{
				if (data->idata[z] <= ans)
				{
					ans = data->idata[z];
					if (X != NULL)
					{
						*X = z;
					}
				}
			}
		}

		return ans;
	}
	else
		return UNDEFINED;
}
template <typename T> T Min(KArray2D<T> *data, int strX, int endX, int strY, int endY, int *X, int *Y)
{
	if (data->isAlloc && data->width > 0 && data->height > 0)
	{
		T ans = data->idata[0][0];
		for (int h = strY; h < endY; h++)
		for (int w = strX; w < endX; w++)
		{
			if (data->IsInBound(w, h))
			{
				if (data->idata[w][h] <= ans)
				{
					ans = data->idata[w][h];
					if (X != NULL && Y != NULL)
					{
						*X = w;
						*Y = h;
					}
				}
			}
		}

		return ans;
	}
	else
		return UNDEFINED;
}
template <typename T> T Min(KArray3D<T> *data, int strD, int endD, int strX, int endX, int strY, int endY, int *D, int *X, int *Y)
{
	if (data->isAlloc && data->depth > 0 && data->width > 0 && data->height > 0)
	{
		T ans = data->idata[0][0][0];
		for (int d = strD; d < endD; d++)
		for (int h = strY; h < endY; h++)
		for (int w = strX; w < endX; w++)
		{
			if (data->IsInBound(d, w, h))
			{
				if (data->idata[d][w][h] <= ans)
				{
					ans = data->idata[d][w][h];
					if (D != NULL && X != NULL && Y != NULL)
					{
						*D = d;
						*X = w;
						*Y = h;
					}
				}
			}
		}

		return ans;
	}
	else
		return UNDEFINED;
}
template <typename T> T Min(KArray4D<T> *data, int strF, int endF, int strD, int endD, int strX, int endX, int strY, int endY, int *F, int *D, int *X, int *Y)
{
	if (data->isAlloc && data->frame && data->depth > 0 && data->width > 0 && data->height > 0)
	{
		T ans = data->idata[0][0][0][0];
		for (int f = strF; f < endF; f++)
		for (int d = strD; d < endD; d++)
		for (int h = strY; h < endY; h++)
		for (int w = strX; w < endX; w++)
		{
			if (data->IsInBound(f, d, w, h))
			{
				if (data->idata[f][d][w][h] <= ans)
				{
					ans = data->idata[f][d][w][h];
					if (F != NULL && D != NULL && X != NULL && Y != NULL)
					{
						*F = f;
						*D = d;
						*X = w;
						*Y = h;
					}
				}
			}
		}

		return ans;
	}
	else
		return UNDEFINED;
}
template <typename T> T Min(KArray1D<T> *data, int *X)
{
	if (X != NULL)
	{
		return Min(data, 0, data->length, X);
	}
	else
	{
		return Min(data, 0, data->length);
	}

}
template <typename T> T Min(KArray2D<T> *data, int *X, int *Y)
{
	if (X != NULL && Y != NULL)
	{
		return Min(data, 0, data->width, 0, data->height, X, Y);
	}
	else
	{
		return Min(data, 0, data->width, 0, data->height);
	}
}
template <typename T> T Min(KArray3D<T> *data, int *D, int *X, int *Y)
{
	if (D != NULL && X != NULL && Y != NULL)
	{
		return Min(data, 0, data->depth, 0, data->width, 0, data->height, D, X, Y);
	}
	else
	{
		return Min(data, 0, data->depth, 0, data->width, 0, data->height);
	}
}
template <typename T> T Min(KArray4D<T> *data, int *F, int *D, int *X, int *Y)
{
	if (F != NULL && D != NULL && X != NULL && Y != NULL)
	{
		return Min(data, 0, data->frame, 0, data->depth, 0, data->width, 0, data->height, F, D, X, Y);
	}
	else
	{
		return Min(data, 0, data->frame, 0, data->depth, 0, data->width, 0, data->height);
	}
}
template <typename T> T Max(KArray1D<T> *data, int strPos, int endPos, int *X)
{
	if (data->isAlloc && data->length > 0)
	{
		T ans = data->idata[0];
		for (int z = strPos; z < endPos; z++)
		{
			if (data->IsInBound(z))
			{
				if (data->idata[z] >= ans)
				{
					ans = data->idata[z];
					if (X != NULL)
					{
						*X = z;
					}
				}
			}
		}
		return ans;
	}
	else
		return UNDEFINED;
}
template <typename T> T Max(KArray2D<T> *data, int strX, int endX, int strY, int endY, int *X, int *Y)
{
	if (data->isAlloc && data->width > 0 && data->height > 0)
	{
		T ans = data->idata[0][0];
		for (int h = strY; h < endY; h++)
		for (int w = strX; w < endX; w++)
		{
			if (data->IsInBound(w, h))
			{
				if (data->idata[w][h] >= ans)
				{
					ans = data->idata[w][h];
					if (X != NULL && Y != NULL)
					{
						*X = w;
						*Y = h;
					}
				}
			}
		}

		return ans;
	}
	else
		return UNDEFINED;
}
template <typename T> T Max(KArray3D<T> *data, int strD, int endD, int strX, int endX, int strY, int endY, int *D, int *X, int *Y)
{
	if (data->isAlloc && data->depth > 0 && data->width > 0 && data->height > 0)
	{
		T ans = data->idata[0][0][0];
		for (int d = strD; d < endD; d++)
		for (int h = strY; h < endY; h++)
		for (int w = strX; w < endX; w++)
		{
			if (data->IsInBound(d, w, h))
			{
				if (data->idata[d][w][h] >= ans)
				{
					ans = data->idata[d][w][h];
					if (D != NULL && X != NULL && Y != NULL)
					{
						*D = d;
						*X = w;
						*Y = h;
					}
				}
			}
		}
		return ans;
	}
	else
		return UNDEFINED;
}
template <typename T> T Max(KArray4D<T> *data, int strF, int endF, int strD, int endD, int strX, int endX, int strY, int endY, int *F, int *D, int *X, int *Y)
{
	if (data->isAlloc && data->frame && data->depth > 0 && data->width > 0 && data->height > 0)
	{
		T ans = data->idata[0][0][0][0];
		for (int f = strF; f < endF; f++)
		for (int d = strD; d < endD; d++)
		for (int h = strY; h < endY; h++)
		for (int w = strX; w < endX; w++)
		{
			if (data->IsInBound(f, d, w, h))
			{
				if (data->idata[f][d][w][h] >= ans)
				{
					ans = data->idata[f][d][w][h];
					if (F != NULL && D != NULL && X != NULL && Y != NULL)
					{
						*F = f;
						*D = d;
						*X = w;
						*Y = h;
					}
				}
			}
		}
		return ans;
	}
	else
		return UNDEFINED;
}
template <typename T> T Max(KArray1D<T> *data, int *X)
{
	if (X != NULL)
	{
		return Max(data, 0, data->length, X);
	}
	else
	{
		return Max(data, 0, data->length);
	}

}
template <typename T> T Max(KArray2D<T> *data, int *X, int *Y)
{
	if (X != NULL && Y != NULL)
	{
		return Max(data, 0, data->width, 0, data->height, X, Y);
	}
	else
	{
		return Max(data, 0, data->width, 0, data->height);
	}
}
template <typename T> T Max(KArray3D<T> *data, int *D, int *X, int *Y)
{
	if (D != NULL && X != NULL && Y != NULL)
	{
		return Max(data, 0, data->depth, 0, data->width, 0, data->height, D, X, Y);
	}
	else
	{
		return Max(data, 0, data->depth, 0, data->width, 0, data->height);
	}
}
template <typename T> T Max(KArray4D<T> *data, int *F, int *D, int *X, int *Y)
{
	if (F != NULL && D != NULL && X != NULL && Y != NULL)
	{
		return Max(data, 0, data->frame, 0, data->depth, 0, data->width, 0, data->height, F, D, X, Y);
	}
	else
	{
		return Max(data, 0, data->frame, 0, data->depth, 0, data->width, 0, data->height);
	}

}
template <typename T> T Median(KArray1D<T> *data, int strPos, int endPos)
{
	if (!data->isAlloc)
		ErrorMessage("Error [Median] : no data");

	KList1D<T> listValue;
	for (int z = strPos; z < endPos; z++)
	if (data->IsInBound(z))
		listValue.push_back(data->idata[z]);

	if (listValue.size()>0)
	{
		sort(listValue.begin(), listValue.end());
		return listValue.at(listValue.size() >> 1);
	}
	else
	{
		ErrorMessage("Error [Median] : cannot access data");
		return UNDEFINED;
	}
}
template <typename T> T Median(KArray2D<T> *data, int strX, int endX, int strY, int endY)
{
	if (!data->isAlloc)
		ErrorMessage("Error [Median] : no data");

	KList1D<T> listValue;
	for (int y = strY; y < endY; y++)
	for (int x = strX; x < endX; x++)
	if (data->IsInBound(x, y))
		listValue.push_back(data->idata[x][y]);

	if (listValue.size()>0)
	{
		sort(listValue.begin(), listValue.end());
		return listValue.at(listValue.size() >> 1);
	}
	else
	{
		ErrorMessage("Error [Median] : cannot access data");
		return UNDEFINED;
	}
}
template <typename T> T Median(KArray3D<T> *data, int strD, int endD, int strX, int endX, int strY, int endY)
{
	if (!data->isAlloc)
		ErrorMessage("Error [Median] : no data");

	KList1D<T> listValue;
	for (int d = strD; d < endD; d++)
	for (int y = strY; y < endY; y++)
	for (int x = strX; x < endX; x++)
	if (data->IsInBound(d, x, y))
		listValue.push_back(data->idata[d][x][y]);

	if (listValue.size()>0)
	{
		sort(listValue.begin(), listValue.end());
		return listValue.at(listValue.size() >> 1);
	}
	else
	{
		ErrorMessage("Error [Median] : cannot access data");
		return UNDEFINED;
	}
}
template <typename T> T Median(KArray4D<T> *data, int strF, int endF, int strD, int endD, int strX, int endX, int strY, int endY)
{
	if (!data->isAlloc)
		ErrorMessage("Error [Median] : no data");

	KList1D<T> listValue;
	for (int f = strF; f < endF; f++)
	for (int d = strD; d < endD; d++)
	for (int y = strY; y < endY; y++)
	for (int x = strX; x < endX; x++)
	if (data->IsInBound(f, d, x, y))
		listValue.push_back(data->idata[f][d][x][y]);

	if (listValue.size()>0)
	{
		sort(listValue.begin(), listValue.end());
		return listValue.at(listValue.size() >> 1);
	}
	else
	{
		ErrorMessage("Error [Median] : cannot access data");
		return UNDEFINED;
	}
}
template <typename T> T Median(KArray1D<T> *data)
{
	return Median(data, 0, data->length);
}
template <typename T> T Median(KArray2D<T> *data)
{
	return Median(data, 0, data->width, 0, data->height);
}
template <typename T> T Median(KArray3D<T> *data)
{
	return Median(data, 0, data->depth, 0, data->width, 0, data->height);
}
template <typename T> T Median(KArray4D<T> *data)
{
	return Median(data, 0, data->frame, 0, data->depth, 0, data->width, 0, data->height);
}
template <typename T> double Mean(KArray1D<T> *data, int strPos, int endPos)
{
	if (data->isAlloc && data->length > 0)
	{
		double ans = 0;
		double N = (double)(endPos - strPos);
		for (int z = strPos; z < endPos; z++)
		if (data->IsInBound(z))
			ans += data->idata[z];

		return (double)(ans / N);
	}
	else
		return UNDEFINED;
}
template <typename T> double Mean(KArray2D<T> *data, int strX, int endX, int strY, int endY)
{
	if (data->isAlloc && data->width > 0 && data->height > 0)
	{
		double ans = 0.0;
		double N = (double)(endX - strX) * (double)(endY - strY);
		for (int h = strY; h < endY; h++)
		for (int w = strX; w < endX; w++)
		if (data->IsInBound(w, h))
			ans += data->idata[w][h];

		return (double)(ans / N);
	}
	else
		return UNDEFINED;
}
template <typename T> double Mean(KArray3D<T> *data, int strD, int endD, int strX, int endX, int strY, int endY)
{
	if (data->isAlloc && data->depth > 0 && data->width > 0 && data->height > 0)
	{
		double ans = 0;
		double N = (double)(endX - strX) * (double)(endY - strY) * (double)(endD - strD);
		for (int d = strD; d < endD; d++)
		for (int h = strY; h < endY; h++)
		for (int w = strX; w < endX; w++)
		if (data->IsInBound(d, w, h))
			ans += data->idata[d][w][h];

		return (double)(ans / N);
	}
	else
		return UNDEFINED;
}
template <typename T> double Mean(KArray4D<T> *data, int strF, int endF, int strD, int endD, int strX, int endX, int strY, int endY)
{
	if (data->isAlloc && data->frame && data->depth > 0 && data->width > 0 && data->height > 0)
	{
		double ans = 0;
		double N = (double)(endX - strX) * (double)(endY - strY) * (double)(endD - strD) * (double)(endF - strF);
		for (int f = strF; f < endF; f++)
		for (int d = strD; d < endD; d++)
		for (int h = strY; h < endY; h++)
		for (int w = strX; w < endX; w++)
		if (data->IsInBound(f, d, w, h))
			ans += data->idata[f][d][w][h];

		return (double)(ans / N);
	}
	else
		return UNDEFINED;
}
template <typename T> double Mean(KArray1D<T> *data)
{
	return Mean(data, 0, data->length);
}
template <typename T> double Mean(KArray2D<T> *data)
{
	return Mean(data, 0, data->width, 0, data->height);
}
template <typename T> double Mean(KArray3D<T> *data)
{
	return Mean(data, 0, data->depth, 0, data->width, 0, data->height);
}
template <typename T> double Mean(KArray4D<T> *data)
{
	return Mean(data, 0, data->frame, 0, data->depth, 0, data->width, 0, data->height);
}

template <typename T> double Sum(KArray1D<T> *data, int strPos, int endPos)
{
	if (data->isAlloc && data->length > 0)
	{
		double ans = 0;
		double N = (double)(endPos - strPos);
		for (int z = strPos; z < endPos; z++)
		if (data->IsInBound(z))
			ans += data->idata[z];

		return (double)(ans);
	}
	else
		return UNDEFINED;
}
template <typename T> double Sum(KArray2D<T> *data, int strX, int endX, int strY, int endY)
{
	if (data->isAlloc && data->width > 0 && data->height > 0)
	{
		double ans = 0;
		double N = (double)(endX - strX) * (double)(endY - strY);
		for (int h = strY; h < endY; h++)
		for (int w = strX; w < endX; w++)
		if (data->IsInBound(w, h))
			ans += data->idata[w][h];

		return (double)(ans);
	}
	else
		return UNDEFINED;
}
template <typename T> double Sum(KArray3D<T> *data, int strD, int endD, int strX, int endX, int strY, int endY)
{
	if (data->isAlloc && data->depth > 0 && data->width > 0 && data->height > 0)
	{
		double ans = 0;
		double N = (double)(endX - strX) * (double)(endY - strY) * (double)(endD - strD);
		for (int d = strD; d < endD; d++)
		for (int h = strY; h < endY; h++)
		for (int w = strX; w < endX; w++)
		if (data->IsInBound(d, w, h))
			ans += data->idata[d][w][h];

		return (double)(ans);
	}
	else
		return UNDEFINED;
}
template <typename T> double Sum(KArray4D<T> *data, int strF, int endF, int strD, int endD, int strX, int endX, int strY, int endY)
{
	if (data->isAlloc && data->frame && data->depth > 0 && data->width > 0 && data->height > 0)
	{
		double ans = 0;
		double N = (double)(endX - strX) * (double)(endY - strY) * (double)(endD - strD) * (double)(endF - strF);
		for (int f = strF; f < endF; f++)
		for (int d = strD; d < endD; d++)
		for (int h = strY; h < endY; h++)
		for (int w = strX; w < endX; w++)
		if (data->IsInBound(f, d, w, h))
			ans += data->idata[f][d][w][h];

		return (double)(ans);
	}
	else
		return UNDEFINED;
}
template <typename T> double Sum(KArray1D<T> *data)
{
	return Sum(data, 0, data->length);
}
template <typename T> double Sum(KArray2D<T> *data)
{
	return Sum(data, 0, data->width, 0, data->height);
}
template <typename T> double Sum(KArray3D<T> *data)
{
	return Sum(data, 0, data->depth, 0, data->width, 0, data->height);
}
template <typename T> double Sum(KArray4D<T> *data)
{
	return Sum(data, 0, data->frame, 0, data->depth, 0, data->width, 0, data->height);
}


template <typename T> double Variance(KArray1D<T> *data, int strPos, int endPos)
{
	if (data->isAlloc && data->length > 0)
	{
		double sum1 = 0, sum2 = 0;
		double mean, N;

		N = endPos - strPos;
		for (int z = strPos; z < endPos; z++)
		{
			if (data->IsInBound(z))
			{
				sum1 += data->idata[z];
				sum2 += data->idata[z] * data->idata[z];
			}
		}
		mean = (double)(sum1 / (double)N);

		return (double)((sum2 / (N)) - (mean*mean));
	}
	else
		return UNDEFINED;
}
template <typename T> double Variance(KArray2D<T> *data, int strX, int endX, int strY, int endY)
{
	if (data->isAlloc && data->width > 0 && data->height > 0)
	{
		double sum1 = 0, sum2 = 0;
		double mean, N;

		N = (endX - strX) * (endY - strY);
		for (int h = strY; h < endY; h++)
		for (int w = strX; w < endX; w++)
		{
			if (data->IsInBound(w, h))
			{
				sum1 += data->idata[w][h];
				sum2 += data->idata[w][h] * data->idata[w][h];
			}
		}
		mean = (double)(sum1 / (double)N);

		return (double)((sum2 / (N)) - (mean*mean));
	}
	else
		return UNDEFINED;
}
template <typename T> double Variance(KArray3D<T> *data, int strD, int endD, int strX, int endX, int strY, int endY)
{
	if (data->isAlloc && data->depth && data->width > 0 && data->height > 0)
	{
		double sum1 = 0, sum2 = 0;
		double mean, N;

		N = (endX - strX) * (endY - strY) * (endD - strD);
		for (int d = strD; d < endD; d++)
		for (int h = strY; h < endY; h++)
		for (int w = strX; w < endX; w++)
		{
			if (data->IsInBound(d, w, h))
			{
				sum1 += data->idata[d][w][h];
				sum2 += data->idata[d][w][h] * data->idata[d][w][h];
			}
		}
		mean = (double)(sum1 / (double)N);

		return (double)((sum2 / (N)) - (mean*mean));
	}
	else
		return UNDEFINED;
}
template <typename T> double Variance(KArray4D<T> *data, int strF, int endF, int strD, int endD, int strX, int endX, int strY, int endY)
{
	if (data->isAlloc && data->frame && data->depth && data->width > 0 && data->height > 0)
	{
		double sum1 = 0, sum2 = 0;
		double mean, N;

		N = (endX - strX) * (endY - strY) * (endD - strD) * (endF - strF);
		for (int f = strF; f < endF; f++)
		for (int d = strD; d < endD; d++)
		for (int h = strY; h < endY; h++)
		for (int w = strX; w < endX; w++)
		{
			if (data->IsInBound(f, d, w, h))
			{
				sum1 += data->idata[f][d][w][h];
				sum2 += data->idata[f][d][w][h] * data->idata[f][d][w][h];
			}
		}
		mean = (double)(sum1 / (double)N);

		return (double)((sum2 / (N)) - (mean*mean));
	}
	else
		return UNDEFINED;
}
template <typename T> double Variance(KArray1D<T> *data)
{
	return Variance(data, 0, data->length);
}
template <typename T> double Variance(KArray2D<T> *data)
{
	return Variance(data, 0, data->width, 0, data->height);
}
template <typename T> double Variance(KArray3D<T> *data)
{
	return Variance(data, 0, data->depth, 0, data->width, 0, data->height);
}
template <typename T> double Variance(KArray4D<T> *data)
{
	return Variance(data, 0, data->frame, 0, data->depth, 0, data->width, 0, data->height);
}
template <typename T> void Normalization(KArray1D<T> *data, int strPos, int endPos, double mean, double variance)
{
	double M, V, S;

	M = Mean(data, strPos, endPos);
	V = Variance(data, strPos, endPos);

	if (M != UNDEFINED && V != UNDEFINED && V != 0 && variance != 0)
	{
		S = variance / V;
		for (int z = strPos; z < endPos; z++)
		{
			if (data->IsInBound(z))
			{
				if (data->idata[z]>(T)M)
					data->idata[z] = (T)(mean + (double)SQRT(Sqrf(data->idata[z] - M)*S));
				else
					data->idata[z] = (T)(mean - (double)SQRT(Sqrf(data->idata[z] - M)*S));
			}
		}
	}
	else
	{
		for (int z = strPos; z < endPos; z++)
		if (data->IsInBound(z))
			data->idata[z] = (T)0;
	}
}
template <typename T> void Normalization(KArray2D<T> *data, int strX, int endX, int strY, int endY, double mean, double variance)
{
	double M, V, S;

	M = Mean(data, strX, endX, strY, endY);
	V = Variance(data, strX, endX, strY, endY);

	if (M != UNDEFINED && V != UNDEFINED && V != 0 && variance != 0)
	{
		S = variance / V;
		for (int y = strY; y < endY; y++)
		for (int x = strX; x < endX; x++)
		{
			if (data->IsInBound(x, y))
			{
				if (data->idata[x][y]>(T)M)
					data->idata[x][y] = (T)(mean + (double)SQRT(Sqrf(data->idata[x][y] - M)*S));
				else
					data->idata[x][y] = (T)(mean - (double)SQRT(Sqrf(data->idata[x][y] - M)*S));
			}
		}
	}
	else
	{
		for (int y = strY; y < endY; y++)
		for (int x = strX; x<endX; x++)
		if (data->IsInBound(x, y))
			data->idata[x][y] = (T)0;
	}
}
template <typename T> void Normalization(KArray3D<T> *data, int strD, int endD, int strX, int endX, int strY, int endY, double mean, double variance)
{
	double M, V, S;

	M = Mean(data, strD, endD, strX, endX, strY, endY);
	V = Variance(data, strD, endD, strX, endX, strY, endY);

	if (M != UNDEFINED && V != UNDEFINED && V != 0 && variance != 0)
	{
		S = variance / V;
		for (int d = strD; d < endD; d++)
		for (int y = strY; y < endY; y++)
		for (int x = strX; x < endX; x++)
		{
			if (data->IsInBound(d, x, y))
			{
				if (data->idata[d][x][y]>(T)M)
					data->idata[d][x][y] = (T)(mean + (double)SQRT(Sqrf(data->idata[d][x][y] - M)*S));
				else
					data->idata[d][x][y] = (T)(mean - (double)SQRT(Sqrf(data->idata[d][x][y] - M)*S));
			}
		}
	}
	else
	{
		for (int d = strD; d < endD; d++)
		for (int y = strY; y < endY; y++)
		for (int x = strX; x < endX; x++)
		if (data->IsInBound(d, x, y))
			data->idata[d][x][y] = (T)0;
	}
}
template <typename T> void Normalization(KArray4D<T> *data, int strF, int endF, int strD, int endD, int strX, int endX, int strY, int endY, double mean, double variance)
{
	double M, V, S;

	M = Mean(data, strF, endF, strD, endD, strX, endX, strY, endY);
	V = Variance(data, strF, endF, strD, endD, strX, endX, strY, endY);

	if (M != UNDEFINED && V != UNDEFINED && V != 0 && variance != 0)
	{
		S = variance / V;
		for (int f = strF; f < endF; f++)
		for (int d = strD; d < endD; d++)
		for (int y = strY; y < endY; y++)
		for (int x = strX; x < endX; x++)
		{
			if (data->IsInBound(f, d, x, y))
			{
				if (data->idata[f][d][x][y]>(T)M)
					data->idata[f][d][x][y] = (T)(mean + (double)SQRT(Sqrf(data->idata[f][d][x][y] - M)*S));
				else
					data->idata[f][d][x][y] = (T)(mean - (double)SQRT(Sqrf(data->idata[f][d][x][y] - M)*S));
			}
		}
	}
	else
	{
		for (int f = strF; f < endF; f++)
		for (int d = strD; d < endD; d++)
		for (int y = strY; y < endY; y++)
		for (int x = strX; x < endX; x++)
		if (data->IsInBound(f, d, x, y))
			data->idata[f][d][x][y] = (T)0;
	}
}
template <typename T> void Normalization(KArray1D<T> *data, double mean, double variance)
{
	Normalization(data, 0, data->length, mean, variance);
}
template <typename T> void Normalization(KArray2D<T> *data, double mean, double variance)
{
	Normalization(data, 0, data->width, 0, data->height, mean, variance);
}
template <typename T> void Normalization(KArray3D<T> *data, double mean, double variance)
{
	Normalization(data, 0, data->depth, 0, data->width, 0, data->height, mean, variance);
}
template <typename T> void Normalization(KArray4D<T> *data, double mean, double variance)
{
	Normalization(data, 0, data->frame, 0, data->depth, 0, data->width, 0, data->height, mean, variance);
}
template <typename T> double Moment(KArray1D<T> *data, double order)
{
	return Moment(data, 0, data->length, order);
}
template <typename T> double Moment(KArray2D<T> *data, double order1, double order2)
{
	return Moment(data, 0, data->width, 0, data->height, order1, order2);
}
template <typename T> double Moment(KArray3D<T> *data, double order1, double order2, double order3)
{
	return Moment(data, 0, data->depth, 0, data->width, 0, data->height, order1, order2);
}
template <typename T> double Moment(KArray4D<T> *data, double order1, double order2, double order3, double order4)
{
	return Moment(data, 0, data->frame, 0, data->depth, 0, data->width, 0, data->height);
}
template <typename T> double Moment(KArray1D<T> *data, int strPos, int endPos, double order)
{
	if (data->isAlloc)
	{
		double moment;
		moment = 0;
		for (int z = strPos, zz = 0; z < endPos; z++, zz++)
		if (data->IsInBound(z))
			moment = moment + (double)((double)Pow(zz, order) * (double)data->idata[z]);
		return moment;
	}
	else
	{
		ErrorMessage("Error [Moment] : no data");
		return UNDEFINED;
	}
}
template <typename T> double Moment(KArray2D<T> *data, int strX, int endX, int strY, int endY, double order1, double order2)
{
	if (data->isAlloc)
	{
		double moment;
		moment = 0;
		for (int y = strY, v = 0; y < endY; y++, v++)
		for (int x = strX, u = 0; x < endX; x++, u++)
		if (data->IsInBound(x, y))
			moment = moment + (double)((double)Pow(u, order1) * (double)Pow(v, order2) * (double)data->idata[x][y]);
		return moment;
	}
	else
	{
		ErrorMessage("Error [Moment] : no data");
		return UNDEFINED;
	}
}
template <typename T> double Moment(KArray3D<T> *data, int strD, int endD, int strX, int endX, int strY, int endY, double order1, double order2, double order3)
{
	if (data->isAlloc)
	{
		double moment;
		moment = 0;
		for (int d = strD, dd = 0; d < endD; d++, dd++)
		for (int y = strY, yy = 0; y < endY; y++, yy++)
		for (int x = strX, xx = 0; x < endX; x++, xx++)
		if (data->IsInBound(d, x, y))
			moment = moment + (double)((double)Pow(xx, order1) * (double)Pow(yy, order2) * (double)Pow(dd, order3) * (double)data->idata[d][x][y]);
		return moment;
	}
	else
	{
		ErrorMessage("Error [Moment] : no data");
		return UNDEFINED;
	}
}
template <typename T> double Moment(KArray4D<T> *data, int strF, int endF, int strD, int endD, int strX, int endX, int strY, int endY, double order1, double order2, double order3, double order4)
{
	if (data->isAlloc)
	{
		double moment;
		moment = 0;
		for (int f = 0, ff = 0; f < data->frame; f++, ff++)
		for (int d = 0, dd = 0; d < data->depth; d++, dd++)
		for (int y = 0, yy = 0; y < data->height; y++, yy++)
		for (int x = 0, xx = 0; x < data->width; x++, xx++)
		if (data->IsInBound(f, d, x, y))
			moment = moment + (double)((double)Pow(xx, order1) * (double)Pow(yy, order2) * (double)Pow(dd, order3) * (double)Pow(ff, order4) * (double)data->idata[f][d][x][y]);
		return moment;
	}
	else
	{
		ErrorMessage("Error [Moment] : no data");
		return UNDEFINED;
	}
}
template <typename T> double CentralMoment(KArray2D<T> *data, double order1, double order2)
{
	return CentralMoment(data, 0, data->width, 0, data->height, order1, order2);
}
template <typename T> double CentralMoment(KArray2D<T> *data, int strX, int strY, int endX, int endY, double order1, double order2)
{
	if (!data->isAlloc)
	{
		ErrorMessage("Error [CentralMoment] : no data");
		return UNDEFINED;
	}

	double centralMoment;
	double moment00, moment10, moment01;
	double mean_x, mean_y;

	centralMoment = 0;
	moment00 = 0;
	moment10 = 0;
	moment01 = 0;

	for (int y = strY, v = 0; y < endY; y++, v++)
	for (int x = strX, u = 0; x < endX; x++, u++)
	{
		if (data->IsInBound(x, y))
		{
			moment00 = moment00 + (double)data->idata[x][y];
			moment10 = moment10 + (double)((double)u * (double)data->idata[x][y]);
			moment01 = moment01 + (double)((double)v * (double)data->idata[x][y]);
		}
	}

	if (moment00 != 0)
	{
		mean_x = (double)moment10 / (double)moment00;
		mean_y = (double)moment01 / (double)moment00;
	}
	else
	{
		mean_x = 0;
		mean_y = 0;
	}

	for (int y = strY, v = 0; y < endY; y++, v++)
	for (int x = strX, u = 0; x < endX; x++, u++)
	if (data->IsInBound(x, y))
		centralMoment = centralMoment + (double)(Pow(((double)u - (double)mean_x), order1)*Pow(((double)v - (double)mean_y), order2)*(double)data->idata[x][y]);

	return centralMoment;
}
template <typename T> void Convariance(KArray2D<T> *data, KArray2D<T> *cov)
{
	Convariance(data, 0, data->width, 0, data->height, cov);
}
template <typename T> void Convariance(KArray2D<T> *data, int strX, int strY, int endX, int endY, KArray2D<T> *cov)
{
	if (!data->isAlloc)
		ErrorMessage("Error [Convariance] : no data");

	double centralMoment20, centralMoment11, centralMoment02;
	double moment00, moment10, moment01;
	double mean_x, mean_y;

	centralMoment20 = 0;
	centralMoment11 = 0;
	centralMoment02 = 0;

	moment00 = 0;
	moment10 = 0;
	moment01 = 0;

	for (int y = strY, v = 0; y < endY; y++, v++)
	for (int x = strX, u = 0; x < endX; x++, u++)
	{
		if (data->IsInBound(x, y))
		{
			moment00 = moment00 + (double)data->idata[x][y];
			moment10 = moment10 + (double)((double)u * (double)data->idata[x][y]);
			moment01 = moment01 + (double)((double)v * (double)data->idata[x][y]);
		}
	}

	if (moment00 != 0)
	{
		mean_x = (double)moment10 / (double)moment00;
		mean_y = (double)moment01 / (double)moment00;
	}
	else
	{
		mean_x = 0;
		mean_y = 0;
	}

	for (int y = strY, v = 0; y < endY; y++, v++)
	for (int x = strX, u = 0; x < endX; x++, u++)
	{
		if (data->IsInBound(x, y))
		{
			centralMoment20 = centralMoment20 + (double)(Pow(((double)u - (double)mean_x), 2)*Pow(((double)v - (double)mean_y), 0)*(double)data->idata[x][y]);
			centralMoment11 = centralMoment11 + (double)(Pow(((double)u - (double)mean_x), 1)*Pow(((double)v - (double)mean_y), 1)*(double)data->idata[x][y]);
			centralMoment02 = centralMoment02 + (double)(Pow(((double)u - (double)mean_x), 0)*Pow(((double)v - (double)mean_y), 2)*(double)data->idata[x][y]);
		}
	}

	cov->Resize(2, 2);
	cov->idata[0][0] = (T)centralMoment20;
	cov->idata[1][0] = (T)centralMoment11;
	cov->idata[0][1] = (T)centralMoment11;
	cov->idata[1][1] = (T)centralMoment02;
}

#endif