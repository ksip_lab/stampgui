// ------------------------------------------------ //
// contain:		Adjustment Tools
//
// developer:	Krisada Phromsuthirak, KSIP Member
//				Watcharapong Chaidee, KSIP Member
// ------------------------------------------------ //

#ifndef _KAdjustment_H
#define _KAdjustment_H

#include "KGeneral.h"
#include "KStatistic.h"
#include "KHistogram.h"
#include "KConvolution.h"

//===========================================//
// ------------ Adjustment Tools ------------ //
// [0] Thresholding
// [1] Thresholding (Otsu)
// [2] HistogramEqualization
// [3] Unsharp Masking
//===========================================//

//===========================================//
template <typename T1, typename T2, typename T3, typename T4> void Thresholding(KArray2D<T1> *in, T2 thresholdValue, T3 upperValue = 1, T4 lowerValue = 0);
template <typename T1, typename T2, typename T3, typename T4> double ThresholdingOtsu(KArray2D<T1> *in, T2 thresholdInterval, T3 upperValue = 1, T4 lowerValue = 0, bool isBinarizedData = true);
template <typename T1, typename T2, typename T3, typename T4> double ThresholdingOtsuIgnoreZero(KArray2D<T1> *in, T2 thresholdInterval, T3 upperValue = 1, T4 lowerValue = 0, bool isBinarizedData = true);

template <typename T1, typename T2> void KHistogramEqualization(KArray2D<T1> *input, KArray2D<T1> *output, KHistogram<T1, T2> *hist);
template <typename T1, typename T2> void KHistogramEqualization(KArray2D<T1> &input, KArray2D<T1> &output, KHistogram<T1, T2> &hist);
template <typename T1, typename T2> void KHistogramEqualization(KArray2D<T1> *input, KArray2D<T1> *output, T1 min, T1 max, T2 resolution_scale);
template <typename T1, typename T2> void KHistogramEqualization(KArray2D<T1> &input, KArray2D<T1> &output, T1 min, T1 max, T2 resolution_scale);

template <typename T1, typename T2, typename T3> void KUnsharpMasking(KArray2D<T1> *data, KArray2D<T2> *filterSmooth, T3 weight, bool ROIcondition(KArray2D<T2>*, int, int) = NULL);
template <typename T1, typename T2, typename T3> void KUnsharpMasking(KArray2D<T1> &data, KArray2D<T2> &filterSmooth, T3 weight, bool ROIcondition(KArray2D<T2>*, int, int) = NULL);
//===========================================//

template <typename T1, typename T2, typename T3, typename T4> void Thresholding(KArray2D<T1> *in, T2 thresholdValue, T3 upperValue, T4 lowerValue)
{
	if (!in->isAlloc)
		ErrorMessage("image not allocate");

	for (int y = 0; y < in->height;y++)
	for (int x = 0; x < in->width; x++)
	{
		if (in->idata[x][y] >= (T1)thresholdValue)
			in->idata[x][y] = (T1)upperValue;
		else
			in->idata[x][y] = (T1)lowerValue;
	}
}
template <typename T1, typename T2, typename T3, typename T4> double ThresholdingOtsu(KArray2D<T1> *in, T2 thresholdInterval, T3 upperValue, T4 lowerValue, bool isBinarizedData)
{
	if (!in->isAlloc)
		ErrorMessage("image not allocate");

	if (thresholdInterval <= 0)
		ErrorMessage("Error [ThresholdingOtsu] : thresholdInterval must be more than zero");

	KArray1D<double>	histData;
	T1					maxData;
	int					histLength;
	int					selectedSlot;
	double				thresholdValue;

	maxData = Max(in);
	histLength = (int)((double)maxData / (double)thresholdInterval);
	histData.Allocate(histLength);

	for (int z = 0; z < histData.length; z++)
		histData.idata[z] = (double)0.0;

	for (int y = 0; y < in->height;y++)
	for (int x = 0; x < in->width; x++)
	{
		selectedSlot = (int)(((double)in->idata[x][y] / (double)thresholdInterval) - 1);
		histData.idata[selectedSlot]++;
	}

	for (int z = 0; z < histData.length; z++)
		histData.idata[z] = (double)((double)histData.idata[z] / (double)(in->width*in->height));

	double	mew_1, mew_2;
	double	omega_1, omega_2;
	double	variance;
	double	temp_variance = 0;

	for (int z = 0; z < histData.length; z++)
	{
		mew_1	= 0;
		mew_2	= 0;
		omega_1	= 0;
		omega_2	= 0;

		for (int l = 0; l < z; l++)
		{
			omega_1 += histData.idata[l];
			mew_1	+= histData.idata[l] * l;
		}
		mew_1 = mew_1 / omega_1;

		for (int l = z; l < histData.length; l++)
		{
			omega_2 += histData.idata[l];
			mew_2 += histData.idata[l] * l;
		}
		mew_2 = mew_2 / omega_2;

		variance = omega_1*omega_2*(mew_1 - mew_2)*(mew_1 - mew_2);

		if (variance>temp_variance)
		{
			temp_variance	= variance;
			thresholdValue	= (double)z;
		}
	}

	thresholdValue = thresholdValue * thresholdInterval;

	if (isBinarizedData)
		Thresholding(in, thresholdValue, upperValue, lowerValue);
	histData.FreeMem();
	
	return thresholdValue;
}

template <typename T1, typename T2, typename T3, typename T4> double ThresholdingOtsuIgnoreZero(KArray2D<T1> *in, T2 thresholdInterval, T3 upperValue, T4 lowerValue, bool isBinarizedData)
{
	if (!in->isAlloc)
		ErrorMessage("image not allocate");

	if (thresholdInterval <= 0)
		ErrorMessage("Error [ThresholdingOtsu] : thresholdInterval must be more than zero");

	KArray1D<double>	histData;
	T1					maxData;
	int					histLength;
	int					selectedSlot;
	double				thresholdValue;

	maxData = Max(in);
	histLength = (int)((double)maxData / (double)thresholdInterval);
	histData.Allocate(histLength);

	for (int z = 0; z < histData.length; z++)
		histData.idata[z] = (double)0.0;

	for (int y = 0; y < in->height; y++)
	for (int x = 0; x < in->width; x++)
	{
		selectedSlot = (int)(((double)in->idata[x][y] / (double)thresholdInterval) - 1);
		histData.idata[selectedSlot]++;
	}

	int bias = 50;
	for (int z = 0; z < bias; z++)
		histData.idata[z] = 0;

	for (int z = 0; z < histData.length; z++)
		histData.idata[z] = (double)((double)histData.idata[z] / (double)(in->width*in->height));

	double	mew_1, mew_2;
	double	omega_1, omega_2;
	double	variance;
	double	temp_variance = 0;

	for (int z = 0; z < histData.length; z++)
	{
		mew_1 = 0;
		mew_2 = 0;
		omega_1 = 0;
		omega_2 = 0;

		for (int l = 0; l < z; l++)
		{
			omega_1 += histData.idata[l];
			mew_1 += histData.idata[l] * l;
		}
		mew_1 = mew_1 / omega_1;

		for (int l = z; l < histData.length; l++)
		{
			omega_2 += histData.idata[l];
			mew_2 += histData.idata[l] * l;
		}
		mew_2 = mew_2 / omega_2;

		variance = omega_1*omega_2*(mew_1 - mew_2)*(mew_1 - mew_2);

		if (variance>temp_variance)
		{
			temp_variance = variance;
			thresholdValue = (double)z;
		}
	}

	thresholdValue = thresholdValue * thresholdInterval;

	if (isBinarizedData)
		Thresholding(in, thresholdValue, upperValue, lowerValue);
	histData.FreeMem();

	return thresholdValue;
}

template <typename T1, typename T2> void KHistogramEqualization(KArray2D<T1> *input, KArray2D<T1> *output, KHistogram<T1, T2> *hist)
{
	KArray1D<T2> cdf;

	cdf.Allocate(hist->length);
	output->Resize(input->width, input->height);

	for (int z = 0; z < cdf.length; z++)
		cdf.idata[z] = (T2)0;

	cdf.idata[0] = hist->idata[0];
	for (int z = 1; z < hist->length; z++)
	{
		for (int s = 0; s <= z; s++)
		{
			cdf.idata[z] += hist->idata[s];
		}
	}

	for (int y = 0; y < output->height; y++)
	for (int x = 0; x < output->width; x++)
		output->idata[x][y] = (T2)(((double)Round((double)((double)((double)(cdf.idata[(int)(input->idata[x][y] * hist->resolution_scale)]) - (double)(cdf.idata[0]))*(double)((double)hist->max_value - 2.0)) / (double)((double)(input->width*input->height) - (double)(cdf.idata[0])))) + 1.0);

	cdf.FreeMem();
}
template <typename T1, typename T2> void KHistogramEqualization(KArray2D<T1> &input, KArray2D<T1> &output, KHistogram<T1, T2> &hist)
{
	HistogramEqualization(&input, &output, &hist);
}
template <typename T1, typename T2> void KHistogramEqualization(KArray2D<T1> *input, KArray2D<T1> *output, T1 min, T1 max, T2 resolution_scale)
{
	KHistogram<T1, T2> hist;
	hist.Initialization(min, max, (double)resolution_scale);
	hist.AddData(input, 1);

	HistogramEqualization(input, output, &hist);

	hist.ClearMemory();
}
template <typename T1, typename T2> void KHistogramEqualization(KArray2D<T1> &input, KArray2D<T1> &output, T1 min, T1 max, T2 resolution_scale)
{
	HistogramEqualization(&input, &output, min, max, resolution_scale);
}
template <typename T1, typename T2, typename T3> void KUnsharpMasking(KArray2D<T1> *data, KArray2D<T2> *filterSmooth, T3 weight, bool ROIcondition(KArray2D<T2>*, int, int))
{
	KArray2D<T1> data_smooth;

	data_smooth = *data;
	KConvolution2D(&data_smooth, filterSmooth);

	for (int y = 0; y < data->height; y++)
	for (int x = 0; x < data->width; x++)
	{
		if (ROIcondition(data, x, y))
			data->idata[x][y] = (T1)((double)data->idata[x][y] + (double)(weight * (double)(data->idata[x][y] - data_smooth.idata[x][y])));
	}

	data_smooth.FreeMem();
}
template <typename T1, typename T2, typename T3> void KUnsharpMasking(KArray2D<T1> &data, KArray2D<T2> &filterSmooth, T3 weight, bool ROIcondition(KArray2D<T2>*, int, int))
{
	KUnsharpMasking(&data, &filterSmooth, weight, ROIcondition);
}

#endif