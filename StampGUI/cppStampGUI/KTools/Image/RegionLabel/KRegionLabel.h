// ------------------------------------------------ //
// contain:		Region Labeling Tools
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KRegionLabel_H
#define _KRegionLabel_H

#include "KGeneral.h"
#include "KImage.h"

//=================================================//
// ------------ Region Labeling Tools ------------ //
// [0] Flood Fill (Region labeling)
//=================================================//

#define FLOODFILL_MODE_4WAY				0
#define FLOODFILL_MODE_8WAY				1

#define FLOODFILL_METHOD_BREADTHFIRST	0
#define FLOODFILL_METHOD_DEPTHFIRST		1

struct con_comp{
	int x;
	int y;
	int size;
};

struct by_con_comp {
	bool operator()(con_comp const &a, con_comp const &b) {
		return a.size > b.size;
	}
};

template <typename T1, typename T2, typename T3> void KFloodFill(KArray2D<T1> *in, int x, int y, T2 selectedValue, T3 filledValue, char mode = FLOODFILL_MODE_8WAY, char method = FLOODFILL_METHOD_BREADTHFIRST);
template <typename T1, typename T2, typename T3, typename T4> void KFloodFillWithRegion(KArray2D<T1> *in, int x, int y, T2 selectedValue, T3 filledValue, KArray2D<T4> *allow_region, char mode = FLOODFILL_MODE_8WAY, char method = FLOODFILL_METHOD_BREADTHFIRST);
template <typename T1, typename T2, typename T3, typename T4> void KFloodFillWithRegion(KArray2D<T1> *in, int x, int y, T2 selectedValue, T3 filledValue, KCoor<T4> &min_allow, KCoor<T4> &max_allow, char mode = FLOODFILL_MODE_8WAY, char method = FLOODFILL_METHOD_BREADTHFIRST);
template <typename T1, typename T2> void KConnectedComponent(KArray2D<T1> *src, T2 selectedValue, vector<con_comp> *list_con, char mode = FLOODFILL_MODE_8WAY);
template <typename T1, typename T2, typename T3> void KConnectedComponentWithRegion(KArray2D<T1> *src, T2 selectedValue, vector<con_comp> *list_con, KCoor<T3> &min_allow, KCoor<T3> &max_allow, char mode = FLOODFILL_MODE_8WAY);

template <typename T1, typename T2, typename T3> void KFloodFill(KArray2D<T1> *in, int x, int y, T2 selectedValue, T3 filledValue, char mode, char method)
{
	if (method == FLOODFILL_METHOD_BREADTHFIRST)
	{
		KQueue<KCoor<int>>	tmpQ;
		KCoor<int>			tmpCoor1, tmpCoor2;
		tmpCoor1.x = x;
		tmpCoor1.y = y;
		tmpQ.Enqueue(tmpCoor1);
		while (!tmpQ.empty())
		{
			tmpCoor1 = tmpQ.Dequeue();
			if (in->IsInBound(tmpCoor1.x, tmpCoor1.y) && in->idata[tmpCoor1.x][tmpCoor1.y] == (T1)selectedValue)
			{
				in->idata[tmpCoor1.x][tmpCoor1.y] = (T1)filledValue;
				tmpCoor2.x = tmpCoor1.x + 1;		tmpCoor2.y = tmpCoor1.y;			tmpQ.Enqueue(tmpCoor2);
				tmpCoor2.x = tmpCoor1.x;			tmpCoor2.y = tmpCoor1.y + 1;		tmpQ.Enqueue(tmpCoor2);
				tmpCoor2.x = tmpCoor1.x - 1;		tmpCoor2.y = tmpCoor1.y;			tmpQ.Enqueue(tmpCoor2);
				tmpCoor2.x = tmpCoor1.x;			tmpCoor2.y = tmpCoor1.y - 1;		tmpQ.Enqueue(tmpCoor2);
				if (mode == FLOODFILL_MODE_8WAY)
				{
					tmpCoor2.x = tmpCoor1.x + 1;	tmpCoor2.y = tmpCoor1.y + 1;		tmpQ.Enqueue(tmpCoor2);
					tmpCoor2.x = tmpCoor1.x + 1;	tmpCoor2.y = tmpCoor1.y - 1;		tmpQ.Enqueue(tmpCoor2);
					tmpCoor2.x = tmpCoor1.x - 1;	tmpCoor2.y = tmpCoor1.y + 1;		tmpQ.Enqueue(tmpCoor2);
					tmpCoor2.x = tmpCoor1.x - 1;	tmpCoor2.y = tmpCoor1.y - 1;		tmpQ.Enqueue(tmpCoor2);
				}
			}
		}
		tmpQ.FreeMem();
	}
	else if (method == FLOODFILL_METHOD_DEPTHFIRST)
	{
		KStack<KCoor<int>>	tmpS;
		KCoor<int>			tmpCoor1, tmpCoor2;
		tmpCoor1.x = x;
		tmpCoor1.y = y;
		tmpS.Push(tmpCoor1);
		while (!tmpS.empty())
		{
			tmpCoor1 = tmpS.Pop();
			if (in->IsInBound(tmpCoor1.x, tmpCoor1.y) && in->idata[tmpCoor1.x][tmpCoor1.y] == (T1)selectedValue)
			{
				in->idata[tmpCoor1.x][tmpCoor1.y] = (T1)filledValue;
				tmpCoor2.x = tmpCoor1.x + 1;		tmpCoor2.y = tmpCoor1.y;			tmpS.Push(tmpCoor2);
				tmpCoor2.x = tmpCoor1.x;			tmpCoor2.y = tmpCoor1.y + 1;		tmpS.Push(tmpCoor2);
				tmpCoor2.x = tmpCoor1.x - 1;		tmpCoor2.y = tmpCoor1.y;			tmpS.Push(tmpCoor2);
				tmpCoor2.x = tmpCoor1.x;			tmpCoor2.y = tmpCoor1.y - 1;		tmpS.Push(tmpCoor2);
				if (mode == FLOODFILL_MODE_8WAY)
				{
					tmpCoor2.x = tmpCoor1.x + 1;	tmpCoor2.y = tmpCoor1.y + 1;		tmpS.Push(tmpCoor2);
					tmpCoor2.x = tmpCoor1.x + 1;	tmpCoor2.y = tmpCoor1.y - 1;		tmpS.Push(tmpCoor2);
					tmpCoor2.x = tmpCoor1.x - 1;	tmpCoor2.y = tmpCoor1.y + 1;		tmpS.Push(tmpCoor2);
					tmpCoor2.x = tmpCoor1.x - 1;	tmpCoor2.y = tmpCoor1.y - 1;		tmpS.Push(tmpCoor2);
				}
			}
		}
		tmpS.FreeMem();
	}
	else
		ErrorMessage("Error [KFloodFill] : wrong method");
}

template <typename T1, typename T2, typename T3, typename T4> void KFloodFillWithRegion(KArray2D<T1> *in, int x, int y, T2 selectedValue, T3 filledValue, KArray2D<T4> *allow_region, char mode, char method)
{
	KCoor<int> max_allow, min_allow;
	int chk = 0;
	for (int u = 0; u < allow_region->width; u++)
	{
		for (int v = 0; v < allow_region->height; v++)
		{
			if (allow_region->idata[u][v] == (T4)filledValue)
			{
				if (chk == 0)
				{
					max_allow.x = u;
					max_allow.y = v;
					min_allow.x = u;
					min_allow.y = v;
				}
				else
				{
					if (u > max_allow.x)
						max_allow.x = u;
					if (v > max_allow.y)
						max_allow.y = v;
					if (u < min_allow.x)
						min_allow.x = u;
					if (v < min_allow.y)
						min_allow.y = v;
				}
			}
		}
	}

	if (method == FLOODFILL_METHOD_BREADTHFIRST)
	{
		KQueue<KCoor<int>>	tmpQ;
		KCoor<int>			tmpCoor1, tmpCoor2;
		tmpCoor1.x = x;
		tmpCoor1.y = y;
		tmpQ.Enqueue(tmpCoor1);
		while (!tmpQ.empty())
		{
			tmpCoor1 = tmpQ.Dequeue();
			if (in->IsInBound(tmpCoor1.x, tmpCoor1.y) && in->idata[tmpCoor1.x][tmpCoor1.y] == (T1)selectedValue)
			{
				in->idata[tmpCoor1.x][tmpCoor1.y] = (T1)filledValue;
				tmpCoor2.x = tmpCoor1.x + 1;		tmpCoor2.y = tmpCoor1.y;			
				if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
					tmpQ.Enqueue(tmpCoor2);
				tmpCoor2.x = tmpCoor1.x;			tmpCoor2.y = tmpCoor1.y + 1;
				if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
					tmpQ.Enqueue(tmpCoor2);
				tmpCoor2.x = tmpCoor1.x - 1;		tmpCoor2.y = tmpCoor1.y;
				if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
					tmpQ.Enqueue(tmpCoor2);
				tmpCoor2.x = tmpCoor1.x;			tmpCoor2.y = tmpCoor1.y - 1;
				if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
					tmpQ.Enqueue(tmpCoor2);
				if (mode == FLOODFILL_MODE_8WAY)
				{
					tmpCoor2.x = tmpCoor1.x + 1;	tmpCoor2.y = tmpCoor1.y + 1;
					if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
						tmpQ.Enqueue(tmpCoor2);
					tmpCoor2.x = tmpCoor1.x + 1;	tmpCoor2.y = tmpCoor1.y - 1;
					if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
						tmpQ.Enqueue(tmpCoor2);
					tmpCoor2.x = tmpCoor1.x - 1;	tmpCoor2.y = tmpCoor1.y + 1;
					if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
						tmpQ.Enqueue(tmpCoor2);
					tmpCoor2.x = tmpCoor1.x - 1;	tmpCoor2.y = tmpCoor1.y - 1;
					if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
						tmpQ.Enqueue(tmpCoor2);
				}
			}
		}
		tmpQ.FreeMem();
	}
	else if (method == FLOODFILL_METHOD_DEPTHFIRST)
	{
		KStack<KCoor<int>>	tmpS;
		KCoor<int>			tmpCoor1, tmpCoor2;
		tmpCoor1.x = x;
		tmpCoor1.y = y;
		tmpS.Push(tmpCoor1);
		while (!tmpS.empty())
		{
			tmpCoor1 = tmpS.Pop();
			if (in->IsInBound(tmpCoor1.x, tmpCoor1.y) && in->idata[tmpCoor1.x][tmpCoor1.y] == (T1)selectedValue)
			{
				in->idata[tmpCoor1.x][tmpCoor1.y] = (T1)filledValue;
				tmpCoor2.x = tmpCoor1.x + 1;		tmpCoor2.y = tmpCoor1.y;
				if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
					tmpS.Push(tmpCoor2);
				tmpCoor2.x = tmpCoor1.x;			tmpCoor2.y = tmpCoor1.y + 1;
				if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
					tmpS.Push(tmpCoor2);
				tmpCoor2.x = tmpCoor1.x - 1;		tmpCoor2.y = tmpCoor1.y;
				if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
					tmpS.Push(tmpCoor2);
				tmpCoor2.x = tmpCoor1.x;			tmpCoor2.y = tmpCoor1.y - 1;
				if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
					tmpS.Push(tmpCoor2);
				if (mode == FLOODFILL_MODE_8WAY)
				{
					tmpCoor2.x = tmpCoor1.x + 1;	tmpCoor2.y = tmpCoor1.y + 1;
					if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
						tmpS.Push(tmpCoor2);
					tmpCoor2.x = tmpCoor1.x + 1;	tmpCoor2.y = tmpCoor1.y - 1;
					if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
						tmpS.Push(tmpCoor2);
					tmpCoor2.x = tmpCoor1.x - 1;	tmpCoor2.y = tmpCoor1.y + 1;
					if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
						tmpS.Push(tmpCoor2);
					tmpCoor2.x = tmpCoor1.x - 1;	tmpCoor2.y = tmpCoor1.y - 1;
					if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
						tmpS.Push(tmpCoor2);
				}
			}
		}
		tmpS.FreeMem();
	}
	else
		ErrorMessage("Error [KFloodFill] : wrong method");
}

template <typename T1, typename T2, typename T3, typename T4> void KFloodFillWithRegion(KArray2D<T1> *in, int x, int y, T2 selectedValue, T3 filledValue, KCoor<T4> &min_allow, KCoor<T4> &max_allow, char mode, char method)
{
	if (method == FLOODFILL_METHOD_BREADTHFIRST)
	{
		KQueue<KCoor<int>>	tmpQ;
		KCoor<int>			tmpCoor1, tmpCoor2;
		tmpCoor1.x = x;
		tmpCoor1.y = y;
		tmpQ.Enqueue(tmpCoor1);
		while (!tmpQ.empty())
		{
			tmpCoor1 = tmpQ.Dequeue();
			if (in->IsInBound(tmpCoor1.x, tmpCoor1.y) && in->idata[tmpCoor1.x][tmpCoor1.y] == (T1)selectedValue)
			{
				in->idata[tmpCoor1.x][tmpCoor1.y] = (T1)filledValue;
				tmpCoor2.x = tmpCoor1.x + 1;		tmpCoor2.y = tmpCoor1.y;
				if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
					tmpQ.Enqueue(tmpCoor2);
				tmpCoor2.x = tmpCoor1.x;			tmpCoor2.y = tmpCoor1.y + 1;
				if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
					tmpQ.Enqueue(tmpCoor2);
				tmpCoor2.x = tmpCoor1.x - 1;		tmpCoor2.y = tmpCoor1.y;
				if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
					tmpQ.Enqueue(tmpCoor2);
				tmpCoor2.x = tmpCoor1.x;			tmpCoor2.y = tmpCoor1.y - 1;
				if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
					tmpQ.Enqueue(tmpCoor2);
				if (mode == FLOODFILL_MODE_8WAY)
				{
					tmpCoor2.x = tmpCoor1.x + 1;	tmpCoor2.y = tmpCoor1.y + 1;
					if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
						tmpQ.Enqueue(tmpCoor2);
					tmpCoor2.x = tmpCoor1.x + 1;	tmpCoor2.y = tmpCoor1.y - 1;
					if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
						tmpQ.Enqueue(tmpCoor2);
					tmpCoor2.x = tmpCoor1.x - 1;	tmpCoor2.y = tmpCoor1.y + 1;
					if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
						tmpQ.Enqueue(tmpCoor2);
					tmpCoor2.x = tmpCoor1.x - 1;	tmpCoor2.y = tmpCoor1.y - 1;
					if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
						tmpQ.Enqueue(tmpCoor2);
				}
			}
		}
		tmpQ.FreeMem();
	}
	else if (method == FLOODFILL_METHOD_DEPTHFIRST)
	{
		KStack<KCoor<int>>	tmpS;
		KCoor<int>			tmpCoor1, tmpCoor2;
		tmpCoor1.x = x;
		tmpCoor1.y = y;
		tmpS.Push(tmpCoor1);
		while (!tmpS.empty())
		{
			tmpCoor1 = tmpS.Pop();
			if (in->IsInBound(tmpCoor1.x, tmpCoor1.y) && in->idata[tmpCoor1.x][tmpCoor1.y] == (T1)selectedValue)
			{
				in->idata[tmpCoor1.x][tmpCoor1.y] = (T1)filledValue;
				tmpCoor2.x = tmpCoor1.x + 1;		tmpCoor2.y = tmpCoor1.y;
				if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
					tmpS.Push(tmpCoor2);
				tmpCoor2.x = tmpCoor1.x;			tmpCoor2.y = tmpCoor1.y + 1;
				if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
					tmpS.Push(tmpCoor2);
				tmpCoor2.x = tmpCoor1.x - 1;		tmpCoor2.y = tmpCoor1.y;
				if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
					tmpS.Push(tmpCoor2);
				tmpCoor2.x = tmpCoor1.x;			tmpCoor2.y = tmpCoor1.y - 1;
				if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
					tmpS.Push(tmpCoor2);
				if (mode == FLOODFILL_MODE_8WAY)
				{
					tmpCoor2.x = tmpCoor1.x + 1;	tmpCoor2.y = tmpCoor1.y + 1;
					if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
						tmpS.Push(tmpCoor2);
					tmpCoor2.x = tmpCoor1.x + 1;	tmpCoor2.y = tmpCoor1.y - 1;
					if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
						tmpS.Push(tmpCoor2);
					tmpCoor2.x = tmpCoor1.x - 1;	tmpCoor2.y = tmpCoor1.y + 1;
					if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
						tmpS.Push(tmpCoor2);
					tmpCoor2.x = tmpCoor1.x - 1;	tmpCoor2.y = tmpCoor1.y - 1;
					if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
						tmpS.Push(tmpCoor2);
				}
			}
		}
		tmpS.FreeMem();
	}
	else
		ErrorMessage("Error [KFloodFill] : wrong method");
}

template <typename T1, typename T2> void KConnectedComponent(KArray2D<T1> *src, T2 selectedValue, vector<con_comp> *list_con, char mode)
{
	KArray2D<T1> in;
	in = *src;
	for (int u = 0; u < in.width; u++)
	{
		for (int v = 0; v < in.height; v++)
		{
			if (in.idata[u][v] == selectedValue)
			{
				KQueue<KCoor<int>>	tmpQ;
				KCoor<int>			tmpCoor1, tmpCoor2;
				tmpCoor1.x = u;
				tmpCoor1.y = v;
				tmpQ.Enqueue(tmpCoor1);

				int tmp_size = 0;

				while (!tmpQ.empty())
				{
					tmpCoor1 = tmpQ.Dequeue();
					if (in.IsInBound(tmpCoor1.x, tmpCoor1.y))
					{
						if (in.idata[tmpCoor1.x][tmpCoor1.y] == (T1)selectedValue)
						{
							in.idata[tmpCoor1.x][tmpCoor1.y] = (T1)abs(255 - selectedValue);
							tmp_size++;
							tmpCoor2.x = tmpCoor1.x + 1;		tmpCoor2.y = tmpCoor1.y;		tmpQ.Enqueue(tmpCoor2);
							tmpCoor2.x = tmpCoor1.x;			tmpCoor2.y = tmpCoor1.y + 1;	tmpQ.Enqueue(tmpCoor2);
							tmpCoor2.x = tmpCoor1.x - 1;		tmpCoor2.y = tmpCoor1.y;		tmpQ.Enqueue(tmpCoor2);
							tmpCoor2.x = tmpCoor1.x;			tmpCoor2.y = tmpCoor1.y - 1;	tmpQ.Enqueue(tmpCoor2);

							if (mode == FLOODFILL_MODE_8WAY)
							{
								tmpCoor2.x = tmpCoor1.x + 1;	tmpCoor2.y = tmpCoor1.y + 1;	tmpQ.Enqueue(tmpCoor2);
								tmpCoor2.x = tmpCoor1.x + 1;	tmpCoor2.y = tmpCoor1.y - 1;	tmpQ.Enqueue(tmpCoor2);
								tmpCoor2.x = tmpCoor1.x - 1;	tmpCoor2.y = tmpCoor1.y + 1;	tmpQ.Enqueue(tmpCoor2);
								tmpCoor2.x = tmpCoor1.x - 1;	tmpCoor2.y = tmpCoor1.y - 1;	tmpQ.Enqueue(tmpCoor2);
							}
						}
					}
				}
				tmpQ.FreeMem();

				con_comp tmp_con;
				tmp_con.size = tmp_size;
				tmp_con.x = u;
				tmp_con.y = v;
				list_con->push_back(tmp_con);
			}
		}
	}

	sort(list_con->begin(), list_con->end(), by_con_comp());
}

template <typename T1, typename T2, typename T3> void KConnectedComponentWithRegion(KArray2D<T1> *src, T2 selectedValue, vector<con_comp> *list_con, KCoor<T3> &min_allow, KCoor<T3> &max_allow, char mode)
{
	KArray2D<T1> in;
	in = *src;
	for (int u = min_allow.x; u <= max_allow.x && u < in.width; u++)
	{
		for (int v = min_allow.y; v <= max_allow.y && v < in.height; v++)
		{
			if (in.idata[u][v] == selectedValue)
			{
				//in = *src;
				bool chk_not_in_ROI = false;
				KQueue<KCoor<int>>	tmpQ;
				KCoor<int>			tmpCoor1, tmpCoor2;
				tmpCoor1.x = u;
				tmpCoor1.y = v;
				tmpQ.Enqueue(tmpCoor1);

				int tmp_size = 0;

				while (!tmpQ.empty())
				{
					tmpCoor1 = tmpQ.Dequeue();
					if (in.IsInBound(tmpCoor1.x, tmpCoor1.y))
					{
						if (in.idata[tmpCoor1.x][tmpCoor1.y] == (T1)selectedValue)
						{
							in.idata[tmpCoor1.x][tmpCoor1.y] = (T1)abs(255 - selectedValue);
							tmp_size++;
							tmpCoor2.x = tmpCoor1.x + 1;		tmpCoor2.y = tmpCoor1.y; 
							if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
								tmpQ.Enqueue(tmpCoor2);
							else
								chk_not_in_ROI = true;
							tmpCoor2.x = tmpCoor1.x;			tmpCoor2.y = tmpCoor1.y + 1;
							if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
								tmpQ.Enqueue(tmpCoor2);
							else
								chk_not_in_ROI = true;
							tmpCoor2.x = tmpCoor1.x - 1;		tmpCoor2.y = tmpCoor1.y;
							if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
								tmpQ.Enqueue(tmpCoor2);
							else
								chk_not_in_ROI = true;
							tmpCoor2.x = tmpCoor1.x;			tmpCoor2.y = tmpCoor1.y - 1;
							if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
								tmpQ.Enqueue(tmpCoor2);
							else
								chk_not_in_ROI = true;

							if (mode == FLOODFILL_MODE_8WAY)
							{
								tmpCoor2.x = tmpCoor1.x + 1;	tmpCoor2.y = tmpCoor1.y + 1;
								if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
								tmpQ.Enqueue(tmpCoor2);
								else
								chk_not_in_ROI = true;
								tmpCoor2.x = tmpCoor1.x + 1;	tmpCoor2.y = tmpCoor1.y - 1;
								if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
								tmpQ.Enqueue(tmpCoor2);
								else
								chk_not_in_ROI = true;
								tmpCoor2.x = tmpCoor1.x - 1;	tmpCoor2.y = tmpCoor1.y + 1;
								if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
								tmpQ.Enqueue(tmpCoor2);
								else
								chk_not_in_ROI = true;
								tmpCoor2.x = tmpCoor1.x - 1;	tmpCoor2.y = tmpCoor1.y - 1;
								if ((tmpCoor2.x >= min_allow.x && tmpCoor2.x <= max_allow.x) && (tmpCoor2.y >= min_allow.y && tmpCoor2.y <= max_allow.y))
								tmpQ.Enqueue(tmpCoor2);
								else
								chk_not_in_ROI = true;
							}
						}
					}
				}
				tmpQ.FreeMem();

				if (!chk_not_in_ROI)
				{
					/*KGImage<T1> show_in;
					show_in = in;
					show_in.Show("true", false, true);*/
					con_comp tmp_con;
					tmp_con.size = tmp_size;
					tmp_con.x = u;
					tmp_con.y = v;
					list_con->push_back(tmp_con);
				}
				/*else
				{
					in = *src;
					KGImage<T1> show_in;
					show_in = in;
					show_in.Show("false", false, true);
				}*/
			}
		}
	}

	sort(list_con->begin(), list_con->end(), by_con_comp());
}


#endif