// ------------------------------------------------ //
// contain:		Color Mapping Tools
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KColor_H
#define _KColor_H

#include "KGeneral.h"
#include "KGImage.h"
#include "KCImage.h"

#define COLORMAP_TYPE_HOT			0
#define COLORMAP_TYPE_OCEAN			1
#define COLORMAP_TYPE_HSV			2
#define COLORMAP_TYPE_JET			3

template <typename T1, typename T2> void KColorMap(T1 gray, T2 *color1, T2 *color2, T2 *color3, char type, T1 minValue, T1 maxValue);
template <typename T1, typename T2> void KColorMap(KGImage<T1> *grayImg, KCImage<T2> *colorImg, char type, T1 minValue, T1 maxValue);

template <typename T1,typename T2,typename T3,typename T4,typename T5,typename T6> void KColorConvert_HSVtoRGB(T1 H,T2 S,T3 V,T4 &R,T5 &G,T6 &B);
template <typename T1,typename T2,typename T3,typename T4,typename T5,typename T6> void KColorConvert_RGBtoHSV(T1 R,T2 G,T3 B,T4 &H,T5 &S,T6 &V);

template <typename T1> double f_for_CIELAB(T1 val);
template <typename T1> double f_inv_for_CIELAB(T1 val);
template <typename T1, typename T2> void KColorConvert_CIELABtoRGB(T1 CIE_L, T1 CIE_A, T1 CIE_B, T2 &R, T2 &G, T2 &B);
template <typename T1, typename T2> void KColorConvert_RGBtoCIELAB(T1 R, T1 G, T1 B, T2 &CIE_L, T2 &CIE_A, T2 &CIE_B);

template <typename T1, typename T2> void KColorConvert_CIEXYZtoRGB(T1 CIE_X, T1 CIE_Y, T1 CIE_Z, T2 &R, T2 &G, T2 &B);
template <typename T1, typename T2> void KColorConvert_RGBtoCIEXYZ(T1 R, T1 G, T1 B, T2 &CIE_X, T2 &CIE_Y, T2 &CIE_Z);

//===========================================================//
// ----------------- Color Mapping Tools ----------------- //
// [0] Mapping gray level value to color value
// [1] Convert gray image to color image
//===========================================================//

template <typename T1, typename T2> void KColorMap(T1 gray, T2 *color1, T2 *color2, T2 *color3, char type, T1 minValue, T1 maxValue)
{
	switch(type)
	{
	case COLORMAP_TYPE_HOT:
		{
			if(gray>=minValue && gray<=maxValue)
			{
				double lengthValue = (double)maxValue - (double)minValue;
				double grayInLength = (double)gray - (double)minValue;
				double lengthValue_3 = ((double)lengthValue / 3.0);
				double lengthValue2_3 = ((double)lengthValue*2.0 / 3.0);

				*color1 = (T2)((double)255.0 * ((double)grayInLength) / lengthValue_3);
				*color2 = (T2)((double)255.0 * ((double)grayInLength - lengthValue_3) / lengthValue_3);
				*color3 = (T2)((double)255.0 * ((double)grayInLength - lengthValue2_3) / lengthValue_3);

				if (*color1 > (T2)255)		*color1 = (T2)255;
				if (*color1 < (T2)0)		*color1 = (T2)0;

				if (*color2 >(T2)255)		*color2 = (T2)255;
				if (*color2 < (T2)0)		*color2 = (T2)0;

				if (*color3 >(T2)255)		*color3 = (T2)255;
				if (*color3 < (T2)0)		*color3 = (T2)0;
			}
			else
			{
				*color1 = (T2)(0);
				*color2 = (T2)(255);
				*color3 = (T2)(0);
			}
		}
		break;

	case COLORMAP_TYPE_OCEAN:
		{
			if (gray >= minValue && gray <= maxValue)
			{
				double lengthValue = (double)maxValue - (double)minValue;
				double grayInLength = (double)gray - (double)minValue;
				double lengthValue_3 = ((double)lengthValue / 3.0);
				double lengthValue2_3 = ((double)lengthValue*2.0 / 3.0);

				*color1 = (T2)((double)255.0 * ((double)grayInLength - lengthValue2_3) / lengthValue_3);
				*color2 = (T2)((double)255.0 * ((double)grayInLength - lengthValue_3) / lengthValue_3);
				*color3 = (T2)((double)255.0 * ((double)grayInLength) / lengthValue_3);

				if (*color1 > (T2)255)		*color1 = (T2)255;
				if (*color1 < (T2)0)		*color1 = (T2)0;

				if (*color2 >(T2)255)		*color2 = (T2)255;
				if (*color2 < (T2)0)		*color2 = (T2)0;

				if (*color3 >(T2)255)		*color3 = (T2)255;
				if (*color3 < (T2)0)		*color3 = (T2)0;
			}
			else
			{
				*color1 = (T2)(0);
				*color2 = (T2)(255);
				*color3 = (T2)(0);
			}
		}
		break;

	case COLORMAP_TYPE_HSV:
		{
			if (gray >= minValue && gray <= maxValue)
			{
				double lengthValue = (double)maxValue - (double)minValue;
				double grayInLength = (double)gray - (double)minValue;

				double sector0 = (double)0.0;
				double sector1 = (((double)lengthValue / (double)6.0)*(double)1.0);
				double sector2 = (((double)lengthValue / (double)6.0)*(double)2.0);
				double sector3 = (((double)lengthValue / (double)6.0)*(double)3.0);
				double sector4 = (((double)lengthValue / (double)6.0)*(double)4.0);
				double sector5 = (((double)lengthValue / (double)6.0)*(double)5.0);
				double sector6 = (double)lengthValue;

				if (grayInLength >= sector0 && grayInLength < sector1)
				{
					*color1 = (T2)255;
					*color2 = (T2)(grayInLength / sector1 * (double)255);
					*color3 = (T2)0;
				}
				else if (grayInLength >= sector1 && grayInLength < sector2)
				{
					*color1 = (T2)((double)255-((grayInLength - sector1) / sector1 * (double)255));
					*color2 = (T2)255;
					*color3 = (T2)0;
				}
				else if (grayInLength >= sector2 && grayInLength < sector3)
				{
					*color1 = (T2)0;
					*color2 = (T2)255;
					*color3 = (T2)((grayInLength - sector2) / sector1 * (double)255);
				}
				else if (grayInLength >= sector3 && grayInLength < sector4)
				{
					*color1 = (T2)0;
					*color2 = (T2)((double)255-((grayInLength - sector3) / sector1 * (double)255));
					*color3 = (T2)255;
				}
				else if (grayInLength >= sector4 && grayInLength < sector5)
				{
					*color1 = (T2)((grayInLength - sector4) / sector1 * (double)255);
					*color2 = (T2)0;
					*color3 = (T2)255;
				}
				else if (grayInLength >= sector5 && grayInLength < sector6)
				{
					*color1 = (T2)255;
					*color2 = (T2)0;
					*color3 = (T2)((double)255-((grayInLength - sector5) / sector1 * (double)255));
				}
				else
				{
					*color1 = (T2)0;
					*color2 = (T2)0;
					*color3 = (T2)0;
				}
			}
			else
			{
				*color1 = (T2)0;
				*color2 = (T2)0;
				*color3 = (T2)0;
			}
		}
		break;

	case COLORMAP_TYPE_JET:
		{
			if (gray >= minValue && gray <= maxValue)
			{
				double lengthValue = (double)maxValue - (double)minValue;
				double grayInLength = (double)gray - (double)minValue;
				double lengthValue_3 = ((double)lengthValue / 3.0);
				double lengthValue2_3 = ((double)lengthValue*2.0 / 3.0);

				if (grayInLength >= 0 && grayInLength < lengthValue_3)
				{
					*color1 = (T2)((double)0.0);
					*color2 = (T2)((double)255.0 * ((double)grayInLength) / lengthValue_3);
					*color3 = (T2)((double)255.0);
				}
				else if (grayInLength >= lengthValue_3 && grayInLength < lengthValue2_3)
				{
					*color1 = (T2)((double)255.0 * ((double)grayInLength - lengthValue_3) / lengthValue_3);
					*color2 = (T2)((double)255.0);
					*color3 = (T2)((double)(255.0 - 255.0*((double)grayInLength - lengthValue_3) / lengthValue_3));
				}
				else if (grayInLength >= lengthValue2_3 && grayInLength <= lengthValue)
				{
					*color1 = (T2)((double)255.0);
					*color2 = (T2)((double)(255.0 - 255.0*((double)grayInLength - lengthValue2_3) / lengthValue_3));
					*color3 = (T2)((double)0.0);
				}
				else
				{
					*color1 = (T2)((double)0.0);
					*color2 = (T2)((double)0.0);
					*color3 = (T2)((double)0.0);
				}

				if (*color1 > (T2)255)		*color1 = (T2)255;
				if (*color1 < (T2)0)		*color1 = (T2)0;

				if (*color2 >(T2)255)		*color2 = (T2)255;
				if (*color2 < (T2)0)		*color2 = (T2)0;

				if (*color3 >(T2)255)		*color3 = (T2)255;
				if (*color3 < (T2)0)		*color3 = (T2)0;
			}
			else
			{
				*color1 = (T2)(0);
				*color2 = (T2)(0);
				*color3 = (T2)(0);
			}
		}
		break;

	default:
		{
			*color1 = *color2 = *color3 = (T2)gray;
		}
		break;
	}
}
template <typename T1, typename T2> void KColorMap(KGImage<T1> *grayImg, KCImage<T2> *colorImg, char type, T1 minValue, T1 maxValue)
{
	if(grayImg->isAlloc)
	{
		if(!colorImg->isAlloc)
			colorImg->Allocate(grayImg->width,grayImg->height);
		if(!colorImg->IsSameSize(grayImg->width,grayImg->height))
			colorImg->Resize(grayImg->width,grayImg->height);
	
		T2 color1,color2,color3;
		for(int y=0;y<grayImg->height;y++)
		for(int x=0;x<grayImg->width;x++)
		{
			KColorMap(grayImg->idata[x][y], &color1, &color2, &color3, type, minValue, maxValue);
			colorImg->Set(x,y,color1,color2,color3);
		}
	}
	else
		ErrorMessage("[should be allocated Image]");
}
template <typename T1,typename T2,typename T3,typename T4,typename T5,typename T6> void KColorConvert_HSVtoRGB(T1 H,T2 S,T3 V,T4 &R,T5 &G,T6 &B)
{
	double		C,HH,X,M;
	double		tR,tG,tB;

	if((double)S<0||(double)S>1)	ErrorMessage("[Saturation] value not in [0,1]");
	if((double)V<0||(double)V>1)	ErrorMessage("[Value] value not in [0,1]");

	C	= (double)V*(double)S;

	if(H<0 || (double)H>(double)(2.0*PI) ||H==UNDEFINED)
	{
		tR	= 0;
		tG	= 0;
		tB	= 0;
	}
	else
	{
		HH	= (double)(H*180/PI)/60.0;
		X	= C*(1.0-abs((fmod(HH,2.0))-1.0));

		if(HH>=0.0 && HH<1.0)		{	tR=C;	tG=X;	tB=0;	}
		else if(HH>=1.0 && HH<2.0)	{	tR=X;	tG=C;	tB=0;	}
		else if(HH>=2.0 && HH<3.0)	{	tR=0;	tG=C;	tB=X;	}
		else if(HH>=3.0 && HH<4.0)	{	tR=0;	tG=X;	tB=C;	}
		else if(HH>=4.0 && HH<5.0)	{	tR=X;	tG=0;	tB=C;	}
		else if(HH>=4.0 && HH<=6.0)	{	tR=C;	tG=0;	tB=X;	}
		else						{	tR=0;	tG=0;	tB=0;	}
	}

	M	= (double)V - C;
	R	= (T4)((tR+M)*255.0);
	G	= (T5)((tG+M)*255.0);
	B	= (T6)((tB+M)*255.0);

	if((double)R<0.0)		R	= (T4)0;
	if((double)R>255.0)		R	= (T4)255;
	if((double)G<0.0)		G	= (T5)0;
	if((double)G>255.0)		G	= (T5)255;
	if((double)B<0.0)		B	= (T6)0;
	if((double)B>255.0)		B	= (T6)255;
}
template <typename T1,typename T2,typename T3,typename T4,typename T5,typename T6> void KColorConvert_RGBtoHSV(T1 R,T2 G,T3 B,T4 &H,T5 &S,T6 &V)
{
	double	minValue,maxValue,difValue;

	if((double)R<0||(double)R>255)	ErrorMessage("[Red] value not in [0,255]");
	if((double)G<0||(double)G>255)	ErrorMessage("[Green] value not in [0,255]");
	if((double)B<0||(double)B>255)	ErrorMessage("[Blue] value not in [0,255]");

	minValue	= Min((double)R,(double)G);
	minValue	= Min(minValue,(double)B);
	maxValue	= Max((double)R,(double)G);
	maxValue	= Max(maxValue,(double)B);
	difValue	= maxValue - minValue;

	V			= (T6)((double)maxValue/255.0);
	if(maxValue!=0)
		S		= (T5)(difValue/maxValue);
	else
		S		= (T5)0;

	if(S!=0)
	{
		if(difValue==0)
			H	= UNDEFINED;
		else if(R==maxValue)
			H	= (T4)(((double)G-(double)B)/difValue);
		else if(G==maxValue)
			H	= (T4)(2+ (((double)B-(double)R)/difValue) );
		else if(B==maxValue)
			H	= (T4)(4+ (((double)R-(double)G)/difValue) );
		else					
			H	= UNDEFINED;

		H			= (T4)(double)(H*PI/3.0);
		if(H<0)
			H		= (T4)((double)H + (2.0*PI));
	}

	if((double)H<0.0)		H	= (T4)0;
	if((double)H>(2.0*PI))	H	= (T4)0;
	if((double)S<0.0)		S	= (T5)0;
	if((double)S>1.0)		S	= (T5)1;
	if((double)V<0.0)		V	= (T6)0;
	if((double)V>1.0)		V	= (T6)1;
}


template <typename T1> double f_for_CIELAB(T1 val)
{
	if (val > pow(6.0 / 29.0, 3.0))
	{
		return (pow((double)val, 1.0 / 3.0));
	}
	else
	{
		return (pow(29.0 / 6.0, 2)*(double)val / 3.0 + 4.0 / 29.0);
	}
}
template <typename T1> double f_inv_for_CIELAB(T1 val)
{
	if (val > 6.0 / 29.0)
	{
		return (pow((double)val, 3.0));
	}
	else
	{
		return (3 * pow(6.0 / 29.0, 2)*((double)val - 4.0 / 29.0));
	}
}
template <typename T1, typename T2> void KColorConvert_CIELABtoRGB(T1 CIE_L, T1 CIE_A, T1 CIE_B, T2 &R, T2 &G, T2 &B)
{
	double X, Y, Z, Xn, Yn, Zn;
	KColorConvert_RGBtoCIEXYZ<int, double>(255, 255, 255, Xn, Yn, Zn);

	X = Xn * f_inv_for_CIELAB<double>((double)(CIE_L + 16.0) / 116.0 + CIE_A / 500.0);
	Y = Yn * f_inv_for_CIELAB<double>((double)(CIE_L + 16.0) / 116.0);
	Z = Zn * f_inv_for_CIELAB<double>((double)(CIE_L + 16.0) / 116.0 - CIE_B / 200.0);

	KColorConvert_CIEXYZtoRGB<double, T2>(X, Y, Z, R, G, B);
}
template <typename T1, typename T2> void KColorConvert_RGBtoCIELAB(T1 R, T1 G, T1 B, T2 &CIE_L, T2 &CIE_A, T2 &CIE_B)
{
	double X, Y, Z, Xn, Yn, Zn;
	KColorConvert_RGBtoCIEXYZ<T1, double>(R, G, B, X, Y, Z);
	KColorConvert_RGBtoCIEXYZ<int, double>(255, 255, 255, Xn, Yn, Zn);

	CIE_L = (T2)((double)116.0 * f_for_CIELAB<double>(Y / Yn) - 16.0);
	CIE_A = (T2)((double)500.0*(f_for_CIELAB<double>(X / Xn) - f_for_CIELAB<double>(Y / Yn)));
	CIE_B = (T2)((double)200.0*(f_for_CIELAB<double>(Y / Yn) - f_for_CIELAB<double>(Z / Zn)));
}
template <typename T1, typename T2> void KColorConvert_CIEXYZtoRGB(T1 CIE_X, T1 CIE_Y, T1 CIE_Z, T2 &R, T2 &G, T2 &B)
{
	double M_array[3][3] =
	{ 1.91, -0.532, -0.288,
	-0.985, 1.999, -0.028,
	0.058, -0.118, 0.898 };

	KArray2D<double> M;
	M.Allocate(3, 3);
	for (int i = 0; i<3; i++)
	for (int j = 0; j<3; j++)
		M.idata[i][j] = M_array[i][j];

	KArray2D<T1> XYZ;
	XYZ.Allocate(1, 3);
	XYZ.idata[0][0] = CIE_X;
	XYZ.idata[0][1] = CIE_Y;
	XYZ.idata[0][2] = CIE_Z;

	KArray2D<T2> RGB;
	KMatrix_CrossProduct<double, T1, T2>(&M, &XYZ, &RGB);

	R = RGB.idata[0][0];
	G = RGB.idata[0][1];
	B = RGB.idata[0][2];
}
template <typename T1, typename T2> void KColorConvert_RGBtoCIEXYZ(T1 R, T1 G, T1 B, T2 &CIE_X, T2 &CIE_Y, T2 &CIE_Z)
{
	double M_array[3][3] = 
	{ 0.607, 0.174, 0.200, 
		0.299, 0.587, 0.114, 
		0.0, 0.066, 1.116 };

	KArray2D<double> M;
	M.Allocate(3, 3);
	for (int i = 0; i<3; i++)
	for (int j = 0; j<3; j++)
		M.idata[i][j] = M_array[i][j];

	KArray2D<T1> RGB;
	RGB.Allocate(1, 3);
	RGB.idata[0][0] = R;
	RGB.idata[0][1] = G;
	RGB.idata[0][2] = B;

	KArray2D<T2> XYZ;
	KMatrix_CrossProduct<double, T1, T2>(&M, &RGB, &XYZ);

	CIE_X = XYZ.idata[0][0];
	CIE_Y = XYZ.idata[0][1];
	CIE_Z = XYZ.idata[0][2];
}

#endif