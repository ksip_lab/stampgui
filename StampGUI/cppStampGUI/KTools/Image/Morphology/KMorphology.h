// ------------------------------------------------ //
// contain:		Morphology Tools
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KMorphology_H
#define _KMorphology_H

#include "KGeneral.h"
#include "KMorphologyElement.h"
#include "KTools\Math\Statistic\KStatistic.h"
#include "KTools\Adjustment\KAdjustment.h"

//============================================//
// ------------ Morphology Tools ------------ //
// [0] Dilation
// [1] Erosion
// [2] Closing
// [3] Opening
//============================================//
template <typename T1, typename T2, typename T3> void KDilation(KArray2D<T1> *in, KArray2D<T2> *mask, KArray2D<T3> *out);
template <typename T1, typename T2, typename T3> void KErosion(KArray2D<T1> *in, KArray2D<T2> *mask, KArray2D<T3> *out);
template <typename T1, typename T2, typename T3> void KClosing(KArray2D<T1> *in, KArray2D<T2> *mask, KArray2D<T3> *out);
template <typename T1, typename T2, typename T3> void KOpening(KArray2D<T1> *in, KArray2D<T2> *mask, KArray2D<T3> *out);
template <typename T1, typename T2, typename T3> void KThinning(KArray2D<T1> *in, KArray2D<T2> *mask, KArray2D<T3> *out);

//===========================================================//
template <typename T1,typename T2,typename T3> void KDilation(KArray2D<T1> *in,KArray2D<T2> *mask,KArray2D<T3> *out)
{
	if (!mask->isAlloc)
		ErrorMessage("should allocated [Mask] before using morphological process");
	if (!out->IsSameSize(in->width, in->height))
		out->Resize(in->width, in->height);

	int x, y, xx, yy, xxx, yyy, w_2, h_2;
	T1 max_value;

	w_2 = mask->width >> 1;
	h_2 = mask->height >> 1;
	
	for (x = 0; x < in->width; x++)
	for (y = 0; y < in->height; y++)
	{
		max_value = (T1)in->idata[x][y] + (T1)mask->idata[0][0];
		for (xx = 0, xxx = x - w_2; xx < mask->width; xx++, xxx++)
		for (yy = 0, yyy = y - h_2; yy < mask->height; yy++, yyy++)
		{
			if (out->IsInBound(xxx,yyy))
			{
				max_value = (T1) MAX(max_value, (T1)in->idata[xxx][yyy] + (T1)mask->idata[xx][yy]);
			}
		}
		out->idata[x][y] = (T3) max_value;
	}
}
template <typename T1, typename T2, typename T3> void KErosion(KArray2D<T1> *in, KArray2D<T2> *mask, KArray2D<T3> *out)
{
	if (!mask->isAlloc)
		ErrorMessage("should allocated [Mask] before using morphological process");
	if (!out->IsSameSize(in->width, in->height))
		out->Resize(in->width, in->height);

	int x, y, xx, yy, xxx, yyy, w_2, h_2;
	T1 min_value;

	w_2 = mask->width >> 1;
	h_2 = mask->height >> 1;

	for (x = 0; x < in->width; x++)
	for (y = 0; y < in->height; y++)
	{
		min_value = (T1)in->idata[x][y] - (T1)mask->idata[0][0];
		for (xx = 0, xxx = x - w_2; xx < mask->width; xx++, xxx++)
		for (yy = 0, yyy = y - h_2; yy < mask->height; yy++, yyy++)
		{
			if (out->IsInBound(xxx, yyy))
			{
				min_value = (T1) MIN(min_value, (T1)in->idata[xxx][yyy] - (T1)mask->idata[xx][yy]);
			}
		}
		out->idata[x][y] = (T3) min_value;
	}
}
template <typename T1, typename T2, typename T3> void KClosing(KArray2D<T1> *in, KArray2D<T2> *mask, KArray2D<T3> *out)
{
	KArray2D<T3> buf;
	KDilation(in, mask, &buf);
	KErosion(&buf, mask, out);
	buf.FreeMem();
}
template <typename T1, typename T2, typename T3> void KOpening(KArray2D<T1> *in, KArray2D<T2> *mask, KArray2D<T3> *out)
{
	KArray2D<T3> buf;
	KErosion(in, mask, &buf);
	KDilation(&buf, mask, out);
	buf.FreeMem();
}

template <typename T1, typename T2> void KThinning(KArray2D<T1> *in, KArray2D<T2> *out)
{
	*out = *in;
	Thresholding<T2, double, int, int>(out, Mean<T2>(out), 1, 0);
	KGImage<double> tmp;
	/*tmp = *out;
	tmp.Show("", true, true);
	tmp.FreeMem();*/

	/*tmp = *out;
	tmp.Scaling();
	tmp.Save("thin" + to_string(0) + ".bmp");
	tmp.FreeMem();*/

	KArray2D<T2> previ,diff,marker;
	previ = *out;

	int iii = 1;
	do {
		marker.Allocate(out->width,out->height);
		for (int i = 1; i < out->width - 1; i++)
		{
			for (int j = 1; j < out->height - 1; j++)
			{
				int p2 = out->idata[i - 1][j];
				int p3 = out->idata[i - 1][j + 1];
				int p4 = out->idata[i][j + 1];
				int p5 = out->idata[i + 1][j + 1];
				int p6 = out->idata[i + 1][j];
				int p7 = out->idata[i + 1][j - 1];
				int p8 = out->idata[i][j - 1];
				int p9 = out->idata[i - 1][j - 1];

				int A = (p2 == 0 && p3 == 1) + (p3 == 0 && p4 == 1) +
					(p4 == 0 && p5 == 1) + (p5 == 0 && p6 == 1) +
					(p6 == 0 && p7 == 1) + (p7 == 0 && p8 == 1) +
					(p8 == 0 && p9 == 1) + (p9 == 0 && p2 == 1);
				int B = p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9;
				int m1 = p2 * p4 * p6;
				int m2 = p4 * p6 * p8;

				if (A == 1 && (B >= 2 && B <= 6) && m1 == 0 && m2 == 0)
				{
					marker.idata[i][j] = 1;
				}		
			}
		}

		for (int i = 0; i < out->width; i++)
		{
			for (int j = 0; j < out->height; j++)
			{
				if (marker.idata[i][j] == 1)
					out->idata[i][j] = 0;
			}
		}
		/*tmp = *out;
		tmp.Scaling();
		tmp.Save("thin" + to_string(iii++) + ".bmp");
		tmp.FreeMem();*/
		marker.FreeMem();
		marker.Allocate(out->width, out->height);

		for (int i = 1; i < out->width - 1; i++)
		{
			for (int j = 1; j < out->height - 1; j++)
			{
				int p2 = out->idata[i - 1][j];
				int p3 = out->idata[i - 1][j + 1];
				int p4 = out->idata[i][j + 1];
				int p5 = out->idata[i + 1][j + 1];
				int p6 = out->idata[i + 1][j];
				int p7 = out->idata[i + 1][j - 1];
				int p8 = out->idata[i][j - 1];
				int p9 = out->idata[i - 1][j - 1];

				int A = (p2 == 0 && p3 == 1) + (p3 == 0 && p4 == 1) +
					(p4 == 0 && p5 == 1) + (p5 == 0 && p6 == 1) +
					(p6 == 0 && p7 == 1) + (p7 == 0 && p8 == 1) +
					(p8 == 0 && p9 == 1) + (p9 == 0 && p2 == 1);
				int B = p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9;
				int m1 = p2 * p4 * p8;
				int m2 = p2 * p6 * p8;

				if (A == 1 && (B >= 2 && B <= 6) && m1 == 0 && m2 == 0)
				{
					marker.idata[i][j] = 1;
				}
			}
		}

		for (int i = 0; i < out->width; i++)
		{
			for (int j = 0; j < out->height; j++)
			{
				if (marker.idata[i][j] == 1)
					out->idata[i][j] = 0;
			}
		}
		/*tmp = *out;
		tmp.Scaling();
		tmp.Save("thin" + to_string(iii++) + ".bmp");
		tmp.FreeMem();*/
		marker.FreeMem();

		diff.FreeMem();
		diff.Allocate(out->width, out->height);
		for (int i = 0; i < out->width; i++)
		{
			for (int j = 0; j < out->height; j++)
			{
				diff.idata[i][j] = abs(out->idata[i][j] - previ.idata[i][j]);
			}
		}

		previ.FreeMem();
		previ = *out;
	} while (Sum<T2>(&diff) > 5);

	marker.FreeMem();
	diff.FreeMem();
	previ.FreeMem();
}

#endif