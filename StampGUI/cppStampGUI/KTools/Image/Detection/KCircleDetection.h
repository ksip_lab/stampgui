// ------------------------------------------------ //
// contain:		Circle Detection Tools
//
// developer:	Krisada Phromsuthirak
// ------------------------------------------------ //

#ifndef _KCircleDetection_H
#define _KCircleDetection_H

#include "KGeneral.h"

template <typename T1, typename T2> void KFindHoughCircle(KArray2D<T1> &in, KArray3D<T2> &out, int MinRadialSize, int MaxRadialSize);
template <typename T1, typename T2> void KFindHoughCircle(KArray2D<T1> *in, KArray3D<T2> *out, int MinRadialSize, int MaxRadialSize);

template <typename T1, typename T2> void KFindHoughCircle(KArray2D<T1> &in, KArray3D<T2> &out, int MinRadialSize, int MaxRadialSize)
{
	KFindHoughCircle(&in, &out, MinRadialSize, MaxRadialSize);
}
template <typename T1, typename T2> void KFindHoughCircle(KArray2D<T1> *in, KArray3D<T2> *out, int MinRadialSize, int MaxRadialSize)
{
	out->Resize(MaxRadialSize - MinRadialSize + 1, in->width, in->height);

	for (int d = 0; d < out->depth; d++)
	for (int x = 0; x < out->width; x++)
	for (int y = 0; y < out->height; y++)
		out->idata[d][x][y] = (T2)0.0;

	for (int v = 0; v < in->height; v++)
	for (int u = 0; u < in->width; u++)
	{
		for (int d = 0; d < out->depth; d++)
		for (int x = 0; x < out->width; x++)
		for (int y = 0; y < out->height; y++)
		{
			if ((int)(Pow((double)(u - x), 2.0) + Pow((double)(v - y), 2.0)) == (int)Pow((d + MinRadialSize), 2))
			{
				out->idata[d][x][y] = out->idata[d][x][y] + in->idata[u][v];
			}
		}
	}
}

#endif