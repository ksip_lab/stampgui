// ------------------------------------------------ //
// contain:		Line Detection Tools
//
// developer:	Krisada Phromsuthirak
// ------------------------------------------------ //

#ifndef _KLineDetection_H
#define _KLineDetection_H

#include "KGeneral.h"

template <typename T1, typename T2> void KFindHoughLine(KArray2D<T1> &in, KArray2D<T2> &out, int NumAngular, int NumRadial);
template <typename T1, typename T2> void KFindHoughLine(KArray2D<T1> *in, KArray2D<T2> *out, int NumAngular, int NumRadial);

template <typename T1, typename T2> void KFindHoughLine(KArray2D<T1> &in, KArray2D<T2> &out, int NumAngular, int NumRadial)
{
	int uc, vc, xc, yc, ir;
	double radius_max;
	double real_angle, real_radius;
	double angular_increment;
	double radial_increment;

	out.Resize(NumAngular, NumRadial);

	uc = in.width >> 1;
	vc = in.height >> 1;
	radius_max = (double)(SQRT(Pow((double)(uc), 2.0) + Pow((double)(vc), 2.0)));
	angular_increment = (double)(PI / (double)NumAngular);
	radial_increment = (double)((double)(2.0*radius_max) / (double)(NumRadial));

	for (int y = 0; y < out.height; y++)
	for (int x = 0; x < out.width; x++)
		out.idata[x][y] = (T2)0;

	for (int v = 0; v < in.height; v++)
	for (int u = 0; u < in.width; u++)
	{
		xc = u - uc;
		yc = v - vc;
		for (int ia = 0; ia < NumAngular; ia++)
		{
			real_angle = angular_increment * ((double)(ia));
			real_radius = ((double)xc * cos(real_angle)) + ((double)yc * sin(real_angle));
			ir = (int)(((double)NumRadial / 2.0) + Round(real_radius / radial_increment));
			if (out.IsInBound(ia, ir))
				out.idata[ia][ir] = (T2)((double)out.idata[ia][ir] + (double)in.idata[u][v]);
		}
	}
}
template <typename T1, typename T2> void KFindHoughLine(KArray2D<T1> *in, KArray2D<T2> *out, int NumAngular, int NumRadial)
{
	FindHoughAccumulate(*in, *out, NumAngular, NumRadial);
}

#endif