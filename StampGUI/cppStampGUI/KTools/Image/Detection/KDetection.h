// ------------------------------------------------ //
// contain:		Detection Tools
//
// developer:	Krisada Phromsuthirak
// ------------------------------------------------ //

#ifndef _KDetection_H
#define _KDetection_H

#include "KCornerDetection.h"
#include "KLineDetection.h"
#include "KCircleDetection.h"
#include "KEdge.h"

#endif