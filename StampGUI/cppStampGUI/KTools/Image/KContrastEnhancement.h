// ------------------------------------------------ //
// contain:		Contrast Enhancement Tools
//
// developer:	Krisada Phromsuthirak
// ------------------------------------------------ //

#ifndef _KContrastEnhancement_H
#define _KContrastEnhancement_H

#include "KGeneral.h"
#include "KStatistic.h"

template <typename T> void KContrastEnhancement(KArray2D<T> *img, int AverageFilterSize);
template <typename T> void KContrastEnhancementV2(KArray2D<T> *img, int AverageFilterSize);

template <typename T> void KContrastEnhancement(KArray2D<T> *img, int AverageFilterSize)
{
	double sg, lg, avg;
	int avgFilter_2;

	avgFilter_2 = AverageFilterSize >> 1;
	for (int y = 0; y < img->height; y++)
	for (int x = 0; x < img->width; x++)
	{
		avg = Mean(img, x - avgFilter_2, x + avgFilter_2, y - avgFilter_2, y + avgFilter_2);
		sg = (double)Sign((double)img->idata[x][y] - (double)avg);
		lg = (double)log(1 + ABS(img->idata[x][y] - avg));
		img->idata[x][y] = (T)(sg*lg);
	}
}

template <typename T> void KContrastEnhancementV2(KArray2D<T> *img, int AverageFilterSize)
{
	KArray2D<double> filter;
	KGImage<double> smooth, diff;
	filter.Allocate(AverageFilterSize, AverageFilterSize);
	for (int u = 0; u < filter.width; u++)
	for (int v = 0; v < filter.height; v++)
	{
		filter.idata[u][v] = 1.0 / double(AverageFilterSize*AverageFilterSize);
	}

	smooth = *img;
	KConvolution2D<double, double>(&smooth, &filter);

	diff = *img;
	diff -= smooth;
	for (int u = 0; u < img->width; u++)
	for (int v = 0; v < img->height; v++)
	{
		img->idata[u][v] = log(1.0 + abs(diff.idata[u][v]));
		if (diff.idata[u][v] <= 0)
			img->idata[u][v] = img->idata[u][v] * -1;
	}
}

#endif