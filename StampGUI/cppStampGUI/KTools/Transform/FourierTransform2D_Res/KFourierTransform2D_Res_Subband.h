#ifndef _KFourierTransform2D_Res_Subband_H
#define _KFourierTransform2D_Res_Subband_H

#include "KGImage.h"
#include "KFourierTransform2D_Res_Node.h"

class KFourierTransform2D_Res_Subband : public KArray2D<KFourierTransform2D_Res_Node>
{
private:
	bool	isAlloc;

public:
	KFourierTransform2D_Res_Subband();
	~KFourierTransform2D_Res_Subband();

	double	*subFrq;
	double	*subAng;
	int		*subW;
	int		*subH;
	int		u,v;

	void	Initialization(int U,int V,int &SubW,int &SubH,double &SubFrequency,double &SubAngular);
	void	ClearMemory();

	int		getSubW();
	int		getSubH();
	double	getSubFrq();
	double	getSubAng();
	double	getComp1(int x,int y);
	double	getComp2(int x,int y);

	void	setComp1(int x, int y,double Comp1);
	void	setComp2(int x, int y,double Comp2);

	void	getImageComp1(KGImage<double> *imgComp1);
	void	getImageComp2(KGImage<double> *imgComp2);

	void	setNode(long Label,int X,int Y,double *Comp1,double *Comp2);
};

#endif