#include "KFourierTransform2D_Res_Node.h"

KFourierTransform2D_Res_Node::KFourierTransform2D_Res_Node()
{
	isAlloc		= false;
}
KFourierTransform2D_Res_Node::~KFourierTransform2D_Res_Node()
{
	ClearMemory();
}
void KFourierTransform2D_Res_Node::Initialization(long Label,int U,int V,int X,int Y,double *Frequency,double *Angular,double *Comp1,double *Comp2)
{
	label		= Label;
	u			= U;
	v			= V;
	x			= X;
	y			= Y;
	frq			= Frequency;
	ang			= Angular;
	comp1		= Comp1;
	comp2		= Comp2;

	isAlloc		= true;
}
void KFourierTransform2D_Res_Node::ClearMemory()
{
	if(isAlloc)
	{
		ang			= NULL;
		frq			= NULL;
		comp1		= NULL;
		comp2		= NULL;

		isAlloc		= false;
	}
}
double KFourierTransform2D_Res_Node::getAng()
{
	if(isAlloc)
		return *ang;
	else
	{
		ErrorMessage("node didn't Initialize");
		return 0;
	}
}
double KFourierTransform2D_Res_Node::getFrq()
{
	if(isAlloc)
		return *frq;
	else
	{
		ErrorMessage("node didn't Initialize");
		return 0;
	}	
}
double KFourierTransform2D_Res_Node::getComp1()
{
	if(isAlloc)
		return *comp1;
	else
	{
		ErrorMessage("node didn't Initialize");
		return 0;
	}
}
double KFourierTransform2D_Res_Node::getComp2()
{
	if(isAlloc)
		return *comp2;
	else
	{
		ErrorMessage("node didn't Initialize");
		return 0;
	}
}
void KFourierTransform2D_Res_Node::setComp1(double Comp1)
{
	if (isAlloc)
		*comp1 = Comp1;
	else
		ErrorMessage("node didn't Initialize");
}
void KFourierTransform2D_Res_Node::setComp2(double Comp2)
{
	if (isAlloc)
		*comp2 = Comp2;
	else
		ErrorMessage("node didn't Initialize");
}
void KFourierTransform2D_Res_Node::report()
{
	cout << "node label   : " << label << endl;
	cout << "position(u,v): " << "(" << u << "," << v << ")" << endl;
	cout << "position(x,y): " << "(" << x << "," << y << ")" << endl;
	cout << "frequency    : " << getFrq() << endl;
	cout << "angular      : " << getAng() << endl;
	cout << "component1   : " << getComp1() << endl;
	cout << "component2   : " << getComp2() << endl;
}