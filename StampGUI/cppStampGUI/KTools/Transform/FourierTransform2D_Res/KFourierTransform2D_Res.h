// ------------------------------------------------ //
// contain:		Rearrange Subband of
//				Fourier Transform 2-Deimension
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KFourierTransform2D_Res_H
#define _KFourierTransform2D_Res_H

#include "KImage.h"
#include "KFourierTransform1D.h"
#include "KFourierTransform2D_Res_Subband.h"

#define FOURIERTRANSFORM_PADDING_IGNORE		0	//not use coordinate out of image size to compute fourier transform
#define FOURIERTRANSFORM_PADDING_MIRROR		1	//flip image coordinate if it out of image size
#define FOURIERTRANSFORM_PADDING_VALUE		2	//set value on out of image size

class KFourierTransform2D_Res : public KArray2D<KFourierTransform2D_Res_Subband>
{
private:

	bool					isInitial;
	bool					isShiftToCenter;
	char					component;
	char					padding;
	int						dataWidth;
	int						dataHeight;
	int						outputWidth;
	int						outputHeight;
	double					paddingValue;

	int						inner_2;						//Half Size of Inner blocksize
	int						outer_2;						//Half Size of Outer blocksize
	int						shift_2;						//Half Size of Overlap size

	int						pointStart,pointEnd;			//start&end point of InnerBlock
	int						pointOvlpStart,pointOvlpEnd;	//start&end point of OverlapBlock

	KArray1D<double>		tmp1DColComp1,tmp1DColComp2;	//forward transform temporary
	KArray1D<double>		tmp1DRowComp1,tmp1DRowComp2;	//forward transform temporary

	KArray2D<double>		tmp2DRowComp1,tmp2DRowComp2;	//forward transform temporary
	KArray2D<double>		tmp2DInComp1,tmp2DInComp2;		//forward transform temporary

	KArray1D<double>		inv1DColComp1,inv1DColComp2;	//inverse transform temporary
	KArray1D<double>		inv1DRowComp1,inv1DRowComp2;	//inverse transform temporary

	KArray2D<double>		inv2DRowComp1,inv2DRowComp2;	//inverse transform temporary
	KArray2D<double>		inv2DInComp1,inv2DInComp2;		//inverse transform temporary

	KArray2D<int>			imgROI;							//Region Of Interested zone

	KFourierTransform1D		transformCol;					//Fourier transform 1D
	KFourierTransform1D		transformRow;					//Fourier transform 1D

	void CreateFourierTable(KGImage<double> *TableOrientation,KGImage<double> *TableFrequency);
	void ReOrderToSubband(KGImage<double> *ImgComp1,KGImage<double> *ImgComp2);

public:

	KFourierTransform2D_Res();
	~KFourierTransform2D_Res();

	KGImage<double>		tableOrientation;
	KGImage<double>		tableFrequency;

	KGImage<double>		Spatial1,Spatial2;
	KGImage<double>		Comp1,Comp2;

	int					blocksizeInner;
	int					blocksizeOuter;
	int					shiftsize;
	int					subW,subH;

	void BlockToPixel(int block_X,int block_Y,int &pixel_X,int &pixel_Y);
	void PixelToBlock(int pixel_X,int pixel_Y,int &block_X,int &block_Y);

	void Initialization(int DataWidth,int DataHeight,int BlocksizeInner,int BlocksizeOuter,int ShiftSize,char Component=FOURIERTRANSFORM_COMPONENT_VECTOR,char Padding=FOURIERTRANSFORM_PADDING_MIRROR,double PaddingValue=0,bool IsShiftToCenter=true);
	void LoadSpatialImage(KArray2D<float> *imgSpatial1,KArray2D<float> *imgSpatial2);
	void LoadSpatialImage(KArray2D<double> *imgSpatial1,KArray2D<double> *imgSpatial2);
	void LoadSpatialImage(KArray2D<int> *imgSpatial1,KArray2D<int> *imgSpatial2);
	void LoadSpatialImage(KArray2D<float> *imgSpatial);
	void LoadSpatialImage(KArray2D<double> *imgSpatial);
	void LoadSpatialImage(KArray2D<int> *imgSpatial);
	void ClearMemory();

	void ForwardTransform(KArray2D<int> *ROI=NULL,KArray1D<double> *SpatialWindowRow=NULL,KArray1D<double> *SpatialWindowCol=NULL);
	void InverseTransform(KArray2D<int> *ROI=NULL);

	void Plot(KGImage<double> *imgComp1, KGImage<double> *imgComp2);

};

#endif