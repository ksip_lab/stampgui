// ------------------------------------------------ //
// contain:		Cosine Transform 2-Deimension
//				Short-Time
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KCosineTransform2D_ShortTime_H
#define _KCosineTransform2D_ShortTime_H

#include "KImage.h"
#include "KCosineTransform1D.h"

#define COSINETRANSFORM_PADDING_IGNORE		0	//not use coordinate out of image size to compute fourier transform
#define COSINETRANSFORM_PADDING_MIRROR		1	//flip image coordinate if it out of image size
#define COSINETRANSFORM_PADDING_VALUE		2	//set value on out of image size

class KCosineTransform2D_ShortTime
{
private:

	bool					isInitial;
	char					type;
	char					padding;
	int						dataWidth;
	int						dataHeight;
	int						outputWidth;
	int						outputHeight;
	int						blocksizeInner;
	int						blocksizeOuter;
	int						shiftsize;
	double					paddingValue;

	int						inner_2;						//Half Size of Inner blocksize
	int						outer_2;						//Half Size of Outer blocksize
	int						shift_2;						//Half Size of Overlap size

	int						pointStart,pointEnd;			//start&end point of InnerBlock
	int						pointOvlpStart,pointOvlpEnd;	//start&end point of OverlapBlock

	KArray1D<double>		tmp1DColComp;					//forward transform temporary
	KArray1D<double>		tmp1DRowComp;					//forward transform temporary

	KArray2D<double>		tmp2DRowComp;					//forward transform temporary
	KArray2D<double>		tmp2DInComp;					//forward transform temporary

	KArray1D<double>		inv1DColComp;					//inverse transform temporary
	KArray1D<double>		inv1DRowComp;					//inverse transform temporary

	KArray2D<double>		inv2DRowComp;					//inverse transform temporary
	KArray2D<double>		inv2DInComp;					//inverse transform temporary

	KArray2D<int>			imgROI;							//Region Of Interested zone

	KCosineTransform1D		transformCol;					//Cosine transform 1D
	KCosineTransform1D		transformRow;					//Cosine transform 1D

	void CreateCosineTable(KGImage<double> *TableOrientation, KGImage<double> *TableFrequency);

public:

	KCosineTransform2D_ShortTime();
	~KCosineTransform2D_ShortTime();

	KGImage<double>	tableOrientation;
	KGImage<double>	tableFrequency;

	KGImage<double>	Spatial;
	KGImage<double>	Dct;

	void BlockToPixel(int block_X,int block_Y,int &pixel_X,int &pixel_Y);
	void PixelToBlock(int pixel_X,int pixel_Y,int &block_X,int &block_Y);

	void Initialization(int DataWidth, int DataHeight, int BlocksizeInner, int BlocksizeOuter, int ShiftSize, char Type = COSINETRANSFORM_TYPE_4, char Padding = COSINETRANSFORM_PADDING_MIRROR, double PaddingValue = 0);
	void LoadSpatialImage(KArray2D<float> *imgSpatial);
	void LoadSpatialImage(KArray2D<double> *imgSpatial);
	void LoadSpatialImage(KArray2D<int> *imgSpatial);

	void ClearMemory();
	void ForwardTransform(KArray2D<int> *ROI=NULL,KArray1D<double> *SpatialWindowRow=NULL,KArray1D<double> *SpatialWindowCol=NULL);
	void InverseTransform(KArray2D<int> *ROI=NULL);

};

#endif