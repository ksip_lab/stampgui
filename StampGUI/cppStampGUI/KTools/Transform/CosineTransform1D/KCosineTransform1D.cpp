#include "KCosineTransform1D.h"

KCosineTransform1D::KCosineTransform1D()
{
	isInitial		= false;
}
KCosineTransform1D::~KCosineTransform1D()
{
	ClearMemory();
}
void KCosineTransform1D::Initialization(int DataSize, char Type)
{
	type = Type;
	dataSize = DataSize;
	tempData.Allocate(dataSize);

	PI_DATASIZE		= PI / (double)dataSize;
	PI_2DATASIZE	= PI / (double)(2.0*dataSize);
	SQRT1_DATASIZE	= SQRT((double)(1.0 / (double)dataSize));
	SQRT2_DATASIZE	= SQRT((double)(2.0 / (double)dataSize));

	isInitial = true;
}
void KCosineTransform1D::ClearMemory()
{
	if(isInitial)
	{
		tempData.FreeMem();

		isInitial	= false;
	}
}
void KCosineTransform1D::ForwardTransform(KArray1D<double> *data, KArray1D<double> *SpatialWindow)
{
	if(isInitial)
	{
		if(data->length!=dataSize)
			ErrorMessage("[data] size doesn't match to Initialization parameter");
		if(SpatialWindow!=NULL)
			if(SpatialWindow->length!=dataSize)
				ErrorMessage("[SpatialWindow] size doesn't match to Initialization parameter");

		if(/*IsPowOf2(data->length)*/0)
		{
		}
		else
		{
			for (int m = 0; m < dataSize; m++)
			{
				tempData.idata[m] = 0;
				if (type == COSINETRANSFORM_TYPE_1)
				{
				}
				else if (type == COSINETRANSFORM_TYPE_2)
				{
					for (int u = 0; u < dataSize; u++)
						tempData.idata[m] += data->idata[u] * cos((PI_2DATASIZE)*(2.0*(u + 1) - 1.0)*(m));
					if (m == 0)
						tempData.idata[m] *= SQRT1_DATASIZE;
					else
						tempData.idata[m] *= SQRT2_DATASIZE;
				}
				else if (type == COSINETRANSFORM_TYPE_3)
				{
				}
				else if (type == COSINETRANSFORM_TYPE_4)
				{
					for (int u = 0; u < dataSize; u++)
						tempData.idata[m] += data->idata[u] * cos(PI_DATASIZE * ((double)m + 0.5) * ((double)u + 0.5));
					tempData.idata[m] = tempData.idata[m] * SQRT2_DATASIZE;
				}
				else
					ErrorMessage("Error [KCosineTransform1D]: invalid type of cosine transform");
			}
			for (int m = 0; m < dataSize; m++)
				data->idata[m] = tempData.idata[m];
		}
	}
	else
		ErrorMessage("Should be Initialization before using <ForwardTransform>");
}
void KCosineTransform1D::InverseTransform(KArray1D<double> *data)
{
	if(isInitial)
	{
		if(data->length!=dataSize)
			ErrorMessage("[data] size doesn't match to Initilization parameter");

		if(/*IsPowOf2(data->length)*/0)
		{
			
		}
		else
		{
			for (int u = 0; u < dataSize; u++)
			{
				tempData.idata[u] = 0;
				if (type == COSINETRANSFORM_TYPE_1)
				{
				}
				else if (type == COSINETRANSFORM_TYPE_2)
				{
					for (int m = 0; m < dataSize; m++)
					{
						if (m == 0)
							tempData.idata[u] += SQRT1_DATASIZE * data->idata[m] * cos((PI_2DATASIZE)*(2.0*(u + 1) - 1.0)*(m));
						else
							tempData.idata[u] += SQRT2_DATASIZE * data->idata[m] * cos((PI_2DATASIZE)*(2.0*(u + 1) - 1.0)*(m));
					}
				}
				else if (type == COSINETRANSFORM_TYPE_3)
				{
				}
				else if (type == COSINETRANSFORM_TYPE_4)
				{
					for (int m = 0; m < dataSize; m++)
						tempData.idata[u] += data->idata[m] * cos(PI_DATASIZE * ((double)u + 0.5) * ((double)m + 0.5));
					tempData.idata[u] = tempData.idata[u] * SQRT2_DATASIZE;
				}
				else
					ErrorMessage("Error [KCosineTransform1D]: invalid type of cosine transform");
			}
			for (int u = 0; u < dataSize; u++)
				data->idata[u] = tempData.idata[u];
		}
	}
	else
		ErrorMessage("Should be Initialization before using <InverseTransform>");
}