// ------------------------------------------------ //
// contain:		Fourier Transform 1-Deimension
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KFourierTransform1D_H
#define _KFourierTransform1D_H

#include "KGeneral.h"

#define FOURIERTRANSFORM_COMPONENT_COMPLEX		0	//set output component Output1 [real] & Output2 [imaginary]
#define FOURIERTRANSFORM_COMPONENT_VECTOR		1	//set output component Output1 [magnitude] & Output2 [phase]

class KFourierTransform1D
{
private:
	bool				isInitial;
	bool				isShiftToCenter;
	char				component;
	int					dataSize;

	KArray1D<double>		tempRe,tempIm;

	void Scamble(KArray1D<double> *Comp1,KArray1D<double> *Comp2,bool isForward,KArray1D<double> *SpatialWindow=NULL);
	void Butterfly(KArray1D<double> *Comp1,KArray1D<double> *Comp2,bool isForward);

public:
	KFourierTransform1D();
	~KFourierTransform1D();

	void Initialization(int DataSize, char Component = FOURIERTRANSFORM_COMPONENT_COMPLEX, bool IsShiftToCenter = true);
	void ClearMemory();
	void ForwardTransform(KArray1D<double> *Comp1, KArray1D<double> *Comp2, KArray1D<double> *SpatialWindow = NULL);
	void InverseTransform(KArray1D<double> *Comp1, KArray1D<double> *Comp2);

};

#endif