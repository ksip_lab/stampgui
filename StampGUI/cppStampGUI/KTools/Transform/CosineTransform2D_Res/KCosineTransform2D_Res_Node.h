#ifndef _KCosineTransform2D_Res_Node_H
#define _KCosineTransform2D_Res_Node_H

#include "KHeader.h"
#include "KBasic.h"

class KCosineTransform2D_Res_Node
{
private:

	bool	isAlloc;

public:

	KCosineTransform2D_Res_Node();
	~KCosineTransform2D_Res_Node();

	long	label;
	int		u,v,x,y;
	double	*frq;
	double	*ang;
	double	*mag;

	void	Initialization(long Label,int U,int V,int X,int Y,double *Frequency,double *Angular,double *Magnitude);
	void	ClearMemory();

	double	getAng();
	double	getFrq();
	double	getMag();

	void	setMag(double Mag);

	void	report();

};

#endif