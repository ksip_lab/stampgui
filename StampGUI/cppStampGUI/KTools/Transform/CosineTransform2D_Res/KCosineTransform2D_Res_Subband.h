#ifndef _KCosineTransform2D_Res_Subband_H
#define _KCosineTransform2D_Res_Subband_H

#include "KGImage.h"
#include "KCosineTransform2D_Res_Node.h"

class KCosineTransform2D_Res_Subband : public KArray2D<KCosineTransform2D_Res_Node>
{
private:
	bool	isAlloc;

public:

	KCosineTransform2D_Res_Subband();
	~KCosineTransform2D_Res_Subband();

	double	*subFrq;
	double	*subAng;
	int		*subW;
	int		*subH;
	int		u,v;

	void	Initialization(int U,int V,int &SubW,int &SubH,double &SubFrequency,double &SubAngular);
	void	ClearMemory();

	int		getSubW();
	int		getSubH();
	double	getSubFrq();
	double	getSubAng();
	double	getMag(int x,int y);

	void	setMag(int x, int y,double Mag);

	void	getImageMag(KGImage<double> *imgMagnitude);

	void	setNode(long Label,int X,int Y,double *Magnitude);
};

#endif