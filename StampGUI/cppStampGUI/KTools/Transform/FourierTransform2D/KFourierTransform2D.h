// ------------------------------------------------ //
// contain:		Fourier Transform 2-Deimension
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KFourierTransform2D_H
#define _KFourierTransform2D_H

#include "KImage.h"
#include "KFourierTransform1D.h"

class KFourierTransform2D
{
private:
	bool					isInitial;
	bool					isShiftToCenter;
	char					component;
	int						dataWidth;
	int						dataHeight;

	KArray1D<double>		tempColComp1,tempColComp2;
	KArray1D<double>		tempRowComp1,tempRowComp2;
	KFourierTransform1D		transformCol;
	KFourierTransform1D		transformRow;

	void CreateFourierTable(KGImage<double> *TableOrientation,KGImage<double> *TableFrequency);

public:
	KFourierTransform2D();
	~KFourierTransform2D();

	KGImage<double>			tableOrientation;
	KGImage<double>			tableFrequency;

	void Initialization(int DataWidth,int DataHeight,char Component=FOURIERTRANSFORM_COMPONENT_VECTOR,bool IsShiftToCenter=true);
	void ClearMemory();
	void ForwardTransform(KArray2D<double> *Comp1,KArray2D<double> *Comp2,KArray1D<double> *SpatialWindowRow=NULL,KArray1D<double> *SpatialWindowCol=NULL);
	void InverseTransform(KArray2D<double> *Comp1,KArray2D<double> *Comp2);
};

#endif