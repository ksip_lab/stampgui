#include "KCosineTransform2D.h"

void KCosineTransform2D::CreateCosineTable(KGImage<double> *TableOrientation, KGImage<double> *TableFrequency)
{
	if (TableOrientation->width != dataWidth || TableOrientation->height != dataHeight)
		ErrorMessage("[TableOrientation] size error");
	if (TableFrequency->width != dataWidth || TableFrequency->height != dataHeight)
		ErrorMessage("[TableFrequency] size error");

	double	tmpOF;
	for (int y = 0; y<dataHeight; y++)
	for (int x = 0; x<dataWidth; x++)
	{
		tmpOF = FindOrientationHalf(0, 0, x, y) - PI_2;
		if (tmpOF<0)
			tmpOF = tmpOF + PI;

		TableOrientation->idata[x][y] = tmpOF;
		TableFrequency->idata[x][y] = RSS(x, y);
	}
}
KCosineTransform2D::KCosineTransform2D()
{
	isInitial		= false;
}
KCosineTransform2D::~KCosineTransform2D()
{
	ClearMemory();
}
void KCosineTransform2D::Initialization(int DataWidth, int DataHeight, char Type)
{
	dataWidth		= DataWidth;
	dataHeight		= DataHeight;
	type			= Type;
	
	tempColComp.Allocate(dataWidth);
	tempRowComp.Allocate(dataHeight);

	transformCol.Initialization(dataWidth, type);
	transformRow.Initialization(dataHeight, type);

	tableOrientation.Allocate(dataWidth, dataHeight);
	tableFrequency.Allocate(dataWidth, dataHeight);

	CreateCosineTable(&tableOrientation, &tableFrequency);

	isInitial		= true;
}
void KCosineTransform2D::ClearMemory()
{
	if(isInitial)
	{
		tempColComp.FreeMem();
		tempRowComp.FreeMem();

		transformCol.ClearMemory();
		transformRow.ClearMemory();
		
		tableOrientation.FreeMem();
		tableFrequency.FreeMem();

		isInitial	= false;
	}
}
void KCosineTransform2D::ForwardTransform(KArray2D<double> *Data, KArray1D<double> *SpatialWindowRow, KArray1D<double> *SpatialWindowCol)
{
	if(isInitial)
	{
		if (!Data->IsSameSize(dataWidth, dataHeight))
			ErrorMessage("[Data] size doesn't match to Initialization parameter");
		if(SpatialWindowRow!=NULL)
			if(!SpatialWindowRow->IsSameSize(dataHeight))
				ErrorMessage("[SpatialWindowRow] size doesn't match to Initialization parameter");
		if(SpatialWindowCol!=NULL)
			if(!SpatialWindowCol->IsSameSize(dataWidth))
				ErrorMessage("[SpatialWindowCol] size doesn't match to Initialization parameter");

		for(int j=0;j<Data->height;j++)
		{
			for(int i=0;i<Data->width;i++)
				tempColComp.idata[i] = Data->idata[i][j];
			transformCol.ForwardTransform(&tempColComp, SpatialWindowCol);
			for(int i=0;i<Data->width;i++)
				Data->idata[i][j] = tempColComp.idata[i];
		}
		for(int i=0;i<Data->width;i++)
		{
			for(int j=0;j<Data->height;j++)
				tempRowComp.idata[j] = Data->idata[i][j];
			transformRow.ForwardTransform(&tempRowComp,SpatialWindowRow);
			for(int j=0;j<Data->height;j++)
				Data->idata[i][j] = tempRowComp.idata[j];
		}
	}
	else
		ErrorMessage("Should be Initialization before using <ForwardTransform>");
}
void KCosineTransform2D::InverseTransform(KArray2D<double> *Data)
{
	if(isInitial)
	{
		if(!Data->IsSameSize(dataWidth,dataHeight))
			ErrorMessage("[Data] size doesn't match to Initialization parameter");

		for(int j=0;j<Data->height;j++)
		{
			for(int i=0;i<Data->width;i++)
				tempColComp.idata[i] = Data->idata[i][j];
			transformCol.InverseTransform(&tempColComp);
			for(int i=0;i<Data->width;i++)
				Data->idata[i][j] = tempColComp.idata[i];
		}
		for(int i=0;i<Data->width;i++)
		{
			for(int j=0;j<Data->height;j++)
				tempRowComp.idata[j] = Data->idata[i][j];
			transformRow.InverseTransform(&tempRowComp);
			for(int j=0;j<Data->height;j++)
				Data->idata[i][j] = tempRowComp.idata[j];
		}
	}
	else
		ErrorMessage("Should be Initialization before using <InverseTransform>");
}