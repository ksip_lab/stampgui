// ------------------------------------------------ //
// contain:		Convolution Tools
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KConvolution_H
#define _KConvolution_H

#include "KGeneral.h"

#define CORRELATION1D_MODE_NORMAL		0
#define CORRELATION1D_MODE_SAME			1

#define CONVOLUTION1D_MODE_NORMAL		0
#define CONVOLUTION1D_MODE_SAME			1

#define CORRELATION2D_MODE_IGNORE		0
#define CORRELATION2D_MODE_ZERO			1
#define CORRELATION2D_MODE_MIRROR		2

#define CONVOLUTION2D_MODE_IGNORE		0
#define CONVOLUTION2D_MODE_ZERO			1
#define CONVOLUTION2D_MODE_MIRROR		2

//==========================================================================//
// ---------------- Image Correlation & Convolution Tools ----------------- //
// [1] Correlation 2D (separable,normal,root sum square,horizontal,vertical)
// [2] Convolution 2D (separable,normal,root sum square,horizontal,vertical)
//==========================================================================//
template <typename T1, typename T2> void KCorrelation1D(KArray1D<T1> *Input, KArray1D<T2> *Filter, char mode = CORRELATION1D_MODE_NORMAL);
template <typename T1, typename T2> void KConvolution1D(KArray1D<T1> *Input, KArray1D<T2> *Filter, char mode = CONVOLUTION1D_MODE_NORMAL);

template <typename T1,typename T2> void KCorrelation2D_Hor(KArray2D<T1> *Image,KArray1D<T2> *Filter,char mode=CORRELATION2D_MODE_MIRROR);
template <typename T1,typename T2> void KConvolution2D_Hor(KArray2D<T1> *Image,KArray1D<T2> *Filter,char mode=CORRELATION2D_MODE_MIRROR);

template <typename T1,typename T2> void KCorrelation2D_Ver(KArray2D<T1> *Image,KArray1D<T2> *Filter,char mode=CORRELATION2D_MODE_MIRROR);
template <typename T1,typename T2> void KConvolution2D_Ver(KArray2D<T1> *Image,KArray1D<T2> *Filter,char mode=CORRELATION2D_MODE_MIRROR);

template <typename T1,typename T2> void KCorrelation2D(KArray2D<T1> *Image,KArray1D<T2> *FilterRow,KArray1D<T2> *FilterCol,char mode=CORRELATION2D_MODE_MIRROR);
template <typename T1,typename T2> void KConvolution2D(KArray2D<T1> *Image,KArray1D<T2> *FilterRow,KArray1D<T2> *FilterCol,char mode=CONVOLUTION2D_MODE_MIRROR);

template <typename T1,typename T2> void KCorrelation2D(KArray2D<T1> *Image,KArray2D<T2> *Filter,char mode=CORRELATION2D_MODE_MIRROR);
template <typename T1,typename T2> void KConvolution2D(KArray2D<T1> *Image,KArray2D<T2> *Filter,char mode=CONVOLUTION2D_MODE_MIRROR);

template <typename T1,typename T2> void KCorrelation2D_RSS(KArray2D<T1> *Image,KArray2D<T2> *FilterReal,KArray2D<T2> *FilterImagin,char mode=CORRELATION2D_MODE_MIRROR);
template <typename T1,typename T2> void KConvolution2D_RSS(KArray2D<T1> *Image,KArray2D<T2> *FilterReal,KArray2D<T2> *FilterImagin,char mode=CONVOLUTION2D_MODE_MIRROR);
//===========================[END DEFINE]=============================//

template <typename T1, typename T2> void KCorrelation1D(KArray1D<T1> *Input, KArray1D<T2> *Filter, char mode)
{
	if (Input->isAlloc&&Filter->isAlloc)
	{
		KArray1D<T1> temp;
		int in_len, fil_len;
		in_len = Input->length;
		fil_len = Filter->length;
		double sum;

		temp.Allocate(in_len + fil_len - 1);
		/*for (int i = 0; i < temp.length; i++)
		{
			if (i < in_len)
				temp.idata[i] = Input->idata[i];
			else
				temp.idata[i] = (T1)0;
		}*/

		for (int i = 0; i < temp.length; i++)
		{
			int k = i;
			T1 sum = (T1)0.0;
			for (int j = fil_len - 1; j >= 0; j--)
			{
				if (Input->IsInBound(k))
				{
					sum += Input->idata[k] * Filter->idata[j];
				}
				k--;
			}
			temp.idata[i] = sum;
		}

		if (mode == CORRELATION1D_MODE_SAME)
		{
			int over = temp.length - in_len;
			int front, rear;
			rear = over / 2;
			front = over - rear;
			for (int i = front; i < front + in_len; i++)
			{
				Input->idata[i - front] = temp.idata[i];
			}
		}
		else // if (mode == CORRELATION1D_MODE_NORMAL)
			*Input = temp;
	}
	else
		ErrorMessage("[KCorrelation1D: should be allocated Input or Filter]");
}

template <typename T1, typename T2> void KConvolution1D(KArray1D<T1> *Input, KArray1D<T2> *Filter, char mode)
{
	if (Input->isAlloc&&Filter->isAlloc)
	{
		KArray1D<T2> Filter_tmp;
		Filter_tmp.Allocate(Filter->length);
		for (int i = Filter->length - 1; i >= 0; i--)
		{
			Filter_tmp.idata[Filter->length - 1 - i] = Filter->idata[i];
		}
		KCorrelation1D<T1, T2>(Input, &Filter_tmp, mode);
	}
	else
		ErrorMessage("[KConvolution1D: should be allocated Input or Filter]");
}

template <typename T1, typename T2> void KCorrelation2D_Hor(KArray2D<T1> *Image, KArray1D<T2> *Filter, char mode)
{
	KArray2D<double>	imgTmp;
	double				Sum;
	int					FilterRowLength_2;

	imgTmp = *Image;
	FilterRowLength_2 = Filter->length >> 1;

	switch (mode)
	{
		case CONVOLUTION2D_MODE_IGNORE:
		{
			for (int y = 0; y < Image->height; y++)
			{
				for (int x = 0; x < Image->width; x++)
				{
					Sum = 0;
					for (int u = x - FilterRowLength_2, f = 0; u < x + FilterRowLength_2; u++, f++)
					{
						if (Image->IsInBound(u, y))
							Sum += imgTmp.idata[u][y] * Filter->idata[f];
					}
					Image->idata[x][y] = (T1)Sum;
				}
			}
		}
		break;
		case CONVOLUTION2D_MODE_ZERO:
		{
			for (int y = 0; y < Image->height; y++)
			{
				for (int x = 0; x < Image->width; x++)
				{
					if (x<FilterRowLength_2 || x>Image->width - FilterRowLength_2)
					{
						Image->idata[x][y] = 0;
					}
					else
					{
						Sum = 0;
						for (int u = x - FilterRowLength_2, f = 0; u < x + FilterRowLength_2; u++, f++)
						{
							if (Image->IsInBound(u, y))
								Sum += imgTmp.idata[u][y] * Filter->idata[f];
						}
						Image->idata[x][y] = (T1)Sum;
					}
				}
			}
		}
		break;
		case CONVOLUTION2D_MODE_MIRROR:
		{
			for (int y = 0; y < Image->height; y++)
			{
				for (int x = 0; x < Image->width; x++)
				{
					Sum = 0;
					for (int u = x - FilterRowLength_2, f = 0; u < x + FilterRowLength_2; u++, f++)
					{
						if (Image->IsInBound(u, y))
							Sum += imgTmp.idata[u][y] * Filter->idata[f];
						else if (u < 0)
							Sum += imgTmp.idata[(-1 * u)][y] * Filter->idata[f];
						else
							Sum += imgTmp.idata[u - (u - Image->width + 1)][y] * Filter->idata[f];
					}
					Image->idata[x][y] = (T1)Sum;
				}
			}
		}
		break;
		default:
		{
			KCorrelation2D_Hor(Image, Filter, CORRELATION2D_MODE_MIRROR);
		}
		break;
	}
}
template <typename T1, typename T2> void KConvolution2D_Hor(KArray2D<T1> *Image, KArray1D<T2> *Filter, char mode)
{
	if (Image->isAlloc&&Filter->isAlloc)
	{
		KArray1D<T2> FlipFilter;
		FlipFilter.Allocate(Filter->length);

		for (int z = 0; z < FlipFilter.length; z++)
			FlipFilter.idata[z] = Filter->idata[Filter->length - z - 1];

		KConvolution2D_Hor(Image, &FlipFilter, mode);
		FlipFilter.FreeMem();
	}
	else
		ErrorMessage("[should be allocated Image]");
}
template <typename T1, typename T2> void KCorrelation2D_Ver(KArray2D<T1> *Image, KArray1D<T2> *Filter, char mode)
{
	KArray2D<double>	imgTmp;
	double				Sum;
	int					FilterColLength_2;

	imgTmp = *Image;
	FilterColLength_2 = Filter->length >> 1;

	switch (mode)
	{
		case 0:
		{
			for (int x = 0; x < Image->width; x++)
			{
				for (int y = 0; y < Image->height; y++)
				{
					Sum = 0;
					for (int v = y - FilterColLength_2, f = 0; v < y + FilterColLength_2; v++, f++)
					{
						if (Image->IsInBound(x, v))
							Sum += imgTmp.idata[x][v] * Filter->idata[f];
					}
					Image->idata[x][y] = (T1)Sum;
				}
			}
		}
		break;
		case 1:
		{
			for (int x = 0; x < Image->width; x++)
			{
				for (int y = 0; y < Image->height; y++)
				{
					if (y<FilterColLength_2 || y>Image->width - FilterColLength_2)
					{
						Image->idata[x][y] = 0;
					}
					else
					{
						Sum = 0;
						for (int v = y - FilterColLength_2, f = 0; v < y + FilterColLength_2; v++, f++)
						{
							if (Image->IsInBound(x, v))
								Sum += imgTmp.idata[x][v] * Filter->idata[f];
						}
						Image->idata[x][y] = (T1)Sum;
					}
				}
			}
		}
		break;
		case 2:
		{
			for (int x = 0; x < Image->width; x++)
			{
				for (int y = 0; y < Image->height; y++)
				{
					Sum = 0;
					for (int v = y - FilterColLength_2, f = 0; v < y + FilterColLength_2; v++, f++)
					{
						if (Image->IsInBound(x, v))
							Sum += imgTmp.idata[x][v] * Filter->idata[f];
						else if (v < 0)
							Sum += imgTmp.idata[x][(-1 * v)] * Filter->idata[f];
						else
							Sum += imgTmp.idata[x][v - (v - Image->height + 1)] * Filter->idata[f];
					}
					Image->idata[x][y] = (T1)Sum;
				}
			}
		}
		break;
		default:
		{
			KCorrelation2D_Ver(Image, Filter, CORRELATION2D_MODE_MIRROR);
		}
		break;
	}
}
template <typename T1, typename T2> void KConvolution2D_Ver(KArray2D<T1> *Image, KArray1D<T2> *Filter, char mode)
{
	if (Image->isAlloc&&Filter->isAlloc)
	{
		KArray1D<T2> FlipFilter;
		FlipFilter.Allocate(Filter->length);

		for (int z = 0; z < FlipFilter.length; z++)
			FlipFilter.idata[z] = Filter->idata[Filter->length - z - 1];

		KConvolution2D_Ver(Image, &FlipFilter, mode);
		FlipFilter.FreeMem();
	}
	else
		ErrorMessage("[should be allocated Image]");
}
template <typename T1, typename T2> void KCorrelation2D(KArray2D<T1> *Image, KArray1D<T2> *FilterRow, KArray1D<T2> *FilterCol, char mode)
{
	if (Image->isAlloc&&FilterRow->isAlloc&&FilterCol->isAlloc)
	{
		KArray2D<double>	HorizontalTmp;
		double				Sum;
		int					FilterRowLength_2;
		int					FilterColLength_2;

		HorizontalTmp.Allocate(Image->width, Image->height);
		FilterRowLength_2 = (FilterRow->length >> 1);
		FilterColLength_2 = (FilterCol->length >> 1);
		switch (mode)
		{
			case CORRELATION2D_MODE_IGNORE:
			{
				for (int y = 0; y < Image->height; y++)
				{
					for (int x = 0; x < Image->width; x++)
					{
						Sum = 0;
						for (int u = x - FilterRowLength_2, f = 0; u < x + FilterRowLength_2; u++, f++)
						{
							if (Image->IsInBound(u, y))
								Sum += Image->idata[u][y] * FilterRow->idata[f];
						}
						HorizontalTmp.idata[x][y] = (double)Sum;
					}
				}
				for (int x = 0; x < Image->width; x++)
				{
					for (int y = 0; y < Image->height; y++)
					{
						Sum = 0;
						for (int v = y - FilterColLength_2, f = 0; v < y + FilterColLength_2; v++, f++)
						{
							if (Image->IsInBound(x, v))
								Sum += HorizontalTmp.idata[x][v] * FilterCol->idata[f];
						}
						Image->idata[x][y] = (T1)Sum;
					}
				}
			}
				break;
			case CORRELATION2D_MODE_ZERO:
			{
				for (int y = 0; y < Image->height; y++)
				{
					for (int x = 0; x < Image->width; x++)
					{
						Sum = 0;
						for (int u = x - FilterRowLength_2, f = 0; u < x + FilterRowLength_2; u++, f++)
						{
							if (Image->IsInBound(u, y))
								Sum += Image->idata[u][y] * FilterRow->idata[f];
						}
						HorizontalTmp.idata[x][y] = (double)Sum;
					}
				}
				for (int x = 0; x < Image->width; x++)
				{
					for (int y = 0; y < Image->height; y++)
					{
						if (x<FilterRowLength_2 || x>Image->width - FilterRowLength_2 || y<FilterColLength_2 || y>Image->height - FilterColLength_2)
						{
							Image->idata[x][y] = 0;
						}
						else
						{
							Sum = 0;
							for (int v = y - FilterColLength_2, f = 0; v < y + FilterColLength_2; v++, f++)
							{
								if (Image->IsInBound(x, v))
									Sum += HorizontalTmp.idata[x][v] * FilterCol->idata[f];
							}
							Image->idata[x][y] = (T1)Sum;
						}
					}
				}
			}
			break;
			case CORRELATION2D_MODE_MIRROR:
			{
				for (int y = 0; y < Image->height; y++)
				{
					for (int x = 0; x < Image->width; x++)
					{
						Sum = 0;
						for (int u = x - FilterRowLength_2, f = 0; u < x + FilterRowLength_2; u++, f++)
						{
							if (Image->IsInBound(u, y))
								Sum += Image->idata[u][y] * FilterRow->idata[f];
							else if (u < 0)
								Sum += Image->idata[(-1 * u)][y] * FilterRow->idata[f];
							else
								Sum += Image->idata[u - (u - Image->width + 1)][y] * FilterRow->idata[f];
						}
						HorizontalTmp.idata[x][y] = (double)Sum;
					}
				}
				for (int x = 0; x < Image->width; x++)
				{
					for (int y = 0; y < Image->height; y++)
					{
						Sum = 0;
						for (int v = y - FilterColLength_2, f = 0; v < y + FilterColLength_2; v++, f++)
						{
							if (Image->IsInBound(x, v))
								Sum += HorizontalTmp.idata[x][v] * FilterCol->idata[f];
							else if (v < 0)
								Sum += HorizontalTmp.idata[x][(-1)*v] * FilterCol->idata[f];
							else
								Sum += HorizontalTmp.idata[x][v - (v - Image->height + 1)] * FilterCol->idata[f];
						}
						Image->idata[x][y] = (T1)Sum;
					}
				}
			}
			break;
			default:
			{
				ErrorMessage("Correlation mode Error");
			}
			break;
		}
		HorizontalTmp.FreeMem();
	}
	else
		ErrorMessage("[should be allocated Image]");
}
template <typename T1, typename T2> void KConvolution2D(KArray2D<T1> *Image, KArray1D<T2> *FilterRow, KArray1D<T2> *FilterCol, char mode)
{
	if (Image->isAlloc&&FilterRow->isAlloc&&FilterCol->isAlloc)
	{
		KArray1D<T2> FlipRowFilter, FlipColFilter;
		FlipRowFilter.Allocate(FilterRow->length);
		FlipColFilter.Allocate(FilterCol->length);

		for (int z = 0; z < FlipRowFilter.length; z++)	FlipRowFilter.idata[z] = FilterRow->idata[FilterRow->length - z - 1];
		for (int z = 0; z < FlipColFilter.length; z++)	FlipColFilter.idata[z] = FilterCol->idata[FilterCol->length - z - 1];

		KCorrelation2D(Image, &FlipRowFilter, &FlipColFilter, mode);
		FlipRowFilter.FreeMem();
		FlipColFilter.FreeMem();
	}
	else
		ErrorMessage("[should be allocated Image]");
}

template <typename T1, typename T2> void KCorrelation2D(KArray2D<T1> *Image, KArray2D<T2> *Filter, char mode)
{
	if (Image->isAlloc&&Filter->isAlloc)
	{
		KArray2D<T1> temp;
		temp = *Image;
		int w1, h1, w2, h2, outw, outh;
		int x, y, x1, y1, x2, y2;
		w1 = Image->width;
		h1 = Image->height;
		w2 = Filter->width;
		h2 = Filter->height;
		const int w1_2 = (w1 - 1) / 2, h1_2 = (h1 - 1) / 2;
		const int w2_2 = (w2 - 1) / 2, h2_2 = (h2 - 1) / 2;
		double sum;

		switch (mode)
		{
			case CORRELATION2D_MODE_IGNORE:
			{
				outw = w1;
				outh = h1;
				for (x = 0; x < outw; x++)
				for (y = 0; y < outh; y++)
				{
					sum = 0;
					for (x2 = -w2_2; x2 <= w2_2; x2++)
					{
						x1 = x + x2;
						if (x1 >= 0 && x1 < w1)
						for (y2 = -h2_2; y2 <= h2_2; y2++)
						{
							y1 = y + y2;
							if (y1 >= 0 && y1 < h1)
								sum += (temp.idata[x1][y1] * Filter->idata[x2 + w2_2][y2 + h2_2]);
						}
					}
					Image->idata[x][y] = (T1)sum;
				}
			}
			break;
			case CORRELATION2D_MODE_ZERO:
			{
				outw = w1;
				outh = h1;
				for (x = 0; x < w2_2; x++)
				{
					for (y = 0; y < h2_2; y++)	Image->idata[x][y] = 0;
					for (y = outh - h2_2; y < outh; y++)	Image->idata[x][y] = 0;
				}
				for (x = outw - w2_2; x < outw; x++)
				{
					for (y = 0; y < h2_2; y++)	Image->idata[x][y] = 0;
					for (y = outh - h2_2; y < outh; y++)	Image->idata[x][y] = 0;
				}
				for (x = w2_2; x < outw - w2_2; x++)
				for (y = h2_2; y < outh - h2_2; y++)
				{
					sum = 0;
					for (x2 = -w2_2; x2 <= w2_2; x2++)
					{
						x1 = x + x2;
						for (y2 = -h2_2; y2 <= h2_2; y2++)
						{
							y1 = y + y2;
							sum += (temp.idata[x1][y1] * Filter->idata[x2 + w2_2][y2 + h2_2]);
						}
					}
					Image->idata[x][y] = (T1)sum;
				}
			}
			break;
			case CORRELATION2D_MODE_MIRROR:
			{
				outw = w1;
				outh = h1;
				const int w1x2 = w1 * 2 - 1;
				const int h1x2 = h1 * 2 - 1;
				for (x = 0; x < outw; x++)
				for (y = 0; y < outh; y++)
				{
					sum = 0;
					for (x2 = -w2_2; x2 <= w2_2; x2++)
					{
						x1 = x + x2;
						if (x1 < 0)				x1 = -x1;
						else	if (x1 >= w1)		x1 = w1x2 - x1;
						for (y2 = -h2_2; y2 <= h2_2; y2++)
						{
							y1 = y + y2;
							if (y1 < 0)			y1 = -y1;
							else	if (y1 >= h1)  y1 = h1x2 - y1;
							sum += (temp.idata[x1][y1] * Filter->idata[x2 + w2_2][y2 + h2_2]);
						}
					}
					Image->idata[x][y] = (T1)sum;
				}
			}
			break;
			default:
			{
				ErrorMessage("Correlation mode Error");
			}
			break;
		}
		temp.FreeMem();
	}
	else
		ErrorMessage("[should be allocated Image]");
}
template <typename T1, typename T2> void KConvolution2D(KArray2D<T1> *Image, KArray2D<T2> *Filter, char mode)
{
	if (Image->isAlloc&&Filter->isAlloc)
	{
		KArray2D<T2> Flipfilter;
		Flipfilter.Allocate(Filter->width, Filter->height);
		for (int y = 0; y < Filter->height; y++)
		for (int x = 0; x < Filter->width; x++)
			Flipfilter.idata[Filter->width - x - 1][Filter->height - y - 1] = Filter->idata[x][y];
		KCorrelation2D(Image, &Flipfilter, mode);
		Flipfilter.FreeMem();
	}
	else
		ErrorMessage("[should be allocated Image]");
}
template <typename T1, typename T2> void KCorrelation2D_RSS(KArray2D<T1> *Image, KArray2D<T2> *FilterReal, KArray2D<T2> *FilterImagin, char mode)
{
	if (Image->isAlloc&&FilterReal->isAlloc&&FilterImagin->isAlloc)
	{
		if (!FilterReal->IsSameSize(FilterImagin))
			ErrorMessage("Filter Size Error");

		KArray2D<T1> temp;
		temp = *Image;
		int w1, h1, w2, h2, outw, outh;
		int x, y, x1, y1, x2, y2;
		w1 = Image->width;
		h1 = Image->height;
		w2 = FilterReal->width;
		h2 = FilterReal->height;
		const int w1_2 = (w1 - 1) / 2, h1_2 = (h1 - 1) / 2;
		const int w2_2 = (w2 - 1) / 2, h2_2 = (h2 - 1) / 2;
		double sumR, sumI;

		switch (mode)
		{
			case CORRELATION2D_MODE_IGNORE:
			{
				outw = w1;
				outh = h1;
				for (x = 0; x < outw; x++)
				for (y = 0; y < outh; y++)
				{
					sumR = 0;
					sumI = 0;
					for (x2 = -w2_2; x2 <= w2_2; x2++)
					{
						x1 = x + x2;
						if (x1 >= 0 && x1 < w1)
						for (y2 = -h2_2; y2 <= h2_2; y2++)
						{
							y1 = y + y2;
							if (y1 >= 0 && y1 < h1)
							{
								sumR += (temp.idata[x1][y1] * FilterReal->idata[x2 + w2_2][y2 + h2_2]);
								sumI += (temp.idata[x1][y1] * FilterImagin->idata[x2 + w2_2][y2 + h2_2]);
							}
						}
					}
					Image->idata[x][y] = (T1)sqrt((sumR*sumR) + (sumI*sumI));
				}
			}
			break;
			case CORRELATION2D_MODE_ZERO:
			{
				outw = w1;
				outh = h1;
				for (x = 0; x < w2_2; x++)
				{
					for (y = 0; y < h2_2; y++)	Image->idata[x][y] = 0;
					for (y = outh - h2_2; y < outh; y++)	Image->idata[x][y] = 0;
				}
				for (x = outw - w2_2; x < outw; x++)
				{
					for (y = 0; y < h2_2; y++)	Image->idata[x][y] = 0;
					for (y = outh - h2_2; y < outh; y++)	Image->idata[x][y] = 0;
				}
				for (x = w2_2; x < outw - w2_2; x++)
				for (y = h2_2; y < outh - h2_2; y++)
				{
					sumR = 0;
					sumI = 0;
					for (x2 = -w2_2; x2 <= w2_2; x2++)
					{
						x1 = x + x2;
						for (y2 = -h2_2; y2 <= h2_2; y2++)
						{
							y1 = y + y2;
							sumR += (temp.idata[x1][y1] * FilterReal->idata[x2 + w2_2][y2 + h2_2]);
							sumI += (temp.idata[x1][y1] * FilterImagin->idata[x2 + w2_2][y2 + h2_2]);
						}
					}
					Image->idata[x][y] = (T1)sqrt((sumR*sumR) + (sumI*sumI));
				}
			}
			break;
			case CORRELATION2D_MODE_MIRROR:
			{
				outw = w1;
				outh = h1;
				int w1x2 = w1 * 2 - 1;
				int h1x2 = h1 * 2 - 1;
				for (x = 0; x < outw; x++)
				for (y = 0; y < outh; y++)
				{
					sumR = 0;
					sumI = 0;
					for (x2 = -w2_2; x2 <= w2_2; x2++)
					{
						x1 = x + x2;
						if (x1 < 0)				x1 = -x1;
						else	if (x1 >= w1)		x1 = w1x2 - x1;
						for (y2 = -h2_2; y2 <= h2_2; y2++)
						{
							y1 = y + y2;
							if (y1 < 0)			y1 = -y1;
							else	if (y1 >= h1)  y1 = h1x2 - y1;
							sumR += (temp.idata[x1][y1] * FilterReal->idata[x2 + w2_2][y2 + h2_2]);
							sumI += (temp.idata[x1][y1] * FilterImagin->idata[x2 + w2_2][y2 + h2_2]);
						}
					}
					Image->idata[x][y] = (T1)sqrt((sumR*sumR) + (sumI*sumI));
				}
			}
			break;
			default:
			{
				ErrorMessage("Correlation mode Error");
			}
			break;
		}
		temp.FreeMem();
	}
	else
		ErrorMessage("[should be allocated Image]");
}
template <typename T1, typename T2> void KConvolution2D_RSS(KArray2D<T1> *Image, KArray2D<T2> *FilterReal, KArray2D<T2> *FilterImagin, char mode)
{
	if (Image->isAlloc&&FilterReal->isAlloc&&FilterImagin->isAlloc)
	{
		if (!FilterReal->IsSameSize(FilterImagin))
			ErrorMessage("Filter Size Error");
		KArray2D<T2> FlipfilterR, FlipfilterI;
		FlipfilterR.Allocate(FilterReal->width, FilterReal->height);
		FlipfilterI.Allocate(FilterImagin->width, FilterImagin->height);
		for (int y = 0; y < FilterReal->height; y++)
		for (int x = 0; x < FilterReal->width; x++)
		{
			FlipfilterR.idata[FilterReal->width - x - 1][FilterReal->height - y - 1] = FilterReal->idata[x][y];
			FlipfilterI.idata[FilterImagin->width - x - 1][FilterImagin->height - y - 1] = FilterImagin->idata[x][y];
		}
		KCorrelation2D_RSS(Image, &FlipfilterR, &FlipfilterI, mode);
		FlipfilterR.FreeMem();
		FlipfilterI.FreeMem();
	}
	else
		ErrorMessage("[should be allocated Image]");
}

#endif