#ifndef _KSignal_Interpolation_H
#define _KSignal_Interpolation_H

#include "KGeneral.h"
#include <limits>

#define Interpolation_1D_zero 0
#define Interpolation_1D_linear 1

template <typename T1, typename T2> void KInterpolation1D(KArray1D<T1> *in, KArray1D<T2> *out, int sampled_size, int mode = Interpolation_1D_linear);

template <typename T1, typename T2> void KInterpolation1D(KArray1D<T1> *in, KArray1D<T2> *out, int sampled_size, int mode)
{
	int in_size = in->length;
	out->Allocate(sampled_size);
	if (in_size > sampled_size) //downsampling
	{
		double ratio = (double)(in_size - 1) / (double)(sampled_size - 1);
		double idx = 0.0;
		for (int i = 0; i < sampled_size; i++)
		{
			if (ceil(idx) < in_size)
			{
				out->idata[i] = (T2)in->idata[(int)ceil(idx)];
			}
			else
				break;

			idx += ratio;
		}
	}
	else if (in_size < sampled_size) //upsampling
	{
		double ratio = (double)(in_size - 1) / (double)(sampled_size - 1);
		double idx = 0.0;
		int last_in = -1;
		for (int i = 0; i < sampled_size; i++)
		{
			if (floor(idx) > last_in)
			{
				last_in = floor(idx);
				out->idata[i] = (T2)in->idata[last_in];
			}
			else
				out->idata[i] = (T2)numeric_limits<double>::infinity();;

			idx += ratio;
		}
	}
	else
	{
		for (int i = 0; i < in_size; i++)
		{
			out->idata[i] = (T2)in->idata[i];
		}
	}

	if (in_size != sampled_size)
	{
		if (mode == Interpolation_1D_linear)
		{
			/*KArray1D<double> filter;
			int fil_size = 5;
			filter.Allocate(fil_size);
			for (int i = 0; i < fil_size; i++)
			{
				filter.idata[i] = 1.0 / (double)fil_size;
			}*/

			for (int i = 0; i < out->length; i++)
			{
				if (out->idata[i] == (T2)numeric_limits<double>::infinity())
				{
					int j = 1;
					while (out->idata[i + j] == (T2)numeric_limits<double>::infinity()) j++;

					T2 y1 = out->idata[i - 1];
					T2 y2 = out->idata[i + j];
					double m = (y2 - y1) / (j + 1);
				}

			}
		}
	}
}

#endif