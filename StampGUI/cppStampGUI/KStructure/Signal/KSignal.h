// ------------------------------------------------ //
// contain:		Signal Structure
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KSignal_H
#define _KSignal_H

#include "KGeneral.h"

template <typename T>
class KSignal :public KArray1D<T>
{
public:

	KSignal()				{ isAlloc = false; }
	KSignal(int Length)		{ isAlloc = false;	this->Allocate(Length); }
	~KSignal()				{ this->FreeMem(); }

	void Set(int x, T value);

	template <typename T2> KSignal<T>& operator = (const T2 &value);
	template <typename T2> KSignal<T>& operator +=(const T2 &value);
	template <typename T2> KSignal<T>& operator -=(const T2 &value);
	template <typename T2> KSignal<T>& operator *=(const T2 &value);
	template <typename T2> KSignal<T>& operator /=(const T2 &value);

	template <typename T2> KSignal<T>& operator =  (const KSignal<T2> &prototype);
	template <typename T2> KSignal<T>& operator += (const KSignal<T2> &prototype);
	template <typename T2> KSignal<T>& operator -= (const KSignal<T2> &prototype);
	template <typename T2> KSignal<T>& operator *= (const KSignal<T2> &prototype);
	template <typename T2> KSignal<T>& operator /= (const KSignal<T2> &prototype);
};

// ========================= //
// --- Operator Function --- //
// ========================= //
template <typename T> template <typename T2> KSignal<T>& KSignal<T>::operator = (const T2 &value)
{
	for (int z = 0; z<this->length; z++)
		this->Set(z, (T)value);
}
template <typename T> template <typename T2> KSignal<T>& KSignal<T>::operator+= (const T2 &value)
{
	for (int z = 0; z<this->length; z++)
		this->Set(z, this->idata[z] + (T)value);
}
template <typename T> template <typename T2> KSignal<T>& KSignal<T>::operator-= (const T2 &value)
{
	for (int z = 0; z<this->length; z++)
		this->Set(z, this->idata[z] - (T)value);
}
template <typename T> template <typename T2> KSignal<T>& KSignal<T>::operator*= (const T2 &value)
{
	for (int z = 0; z<this->length; z++)
		this->Set(z, this->idata[z] * (T)value);
}
template <typename T> template <typename T2> KSignal<T>& KSignal<T>::operator/= (const T2 &value)
{
	for (int z = 0; z<this->length; z++)
	if (value == 0)
		this->Set(z, INFINITY);
	else
		this->Set(z, this->idata[z] / (T)value);
}
template <typename T> template <typename T2> KSignal<T>& KSignal<T>::operator = (const KSignal<T2> &prototype)
{
	if (!this->isSameSize(prototype.length))
		this->Resize(prototype.length);
	for (int z = 0; z<this->length; z++)
		this->Set(z, (T)prototype.idata[z]);
}
template <typename T> template <typename T2> KSignal<T>& KSignal<T>::operator+= (const KSignal<T2> &prototype)
{
	if (this->isSameSize(prototype.length))
		ErrorMessage("Array size error");
	for (int z = 0; z<this->length; z++)
		this->Set(z, this->idata[z] + (T)prototype.idata[z]);
}
template <typename T> template <typename T2> KSignal<T>& KSignal<T>::operator-= (const KSignal<T2> &prototype)
{
	if (this->isSameSize(prototype.length))
		ErrorMessage("Array size error");
	for (int z = 0; z<this->length; z++)
		this->Set(z, this->idata[z] - (T)prototype.idata[z]);
}
template <typename T> template <typename T2> KSignal<T>& KSignal<T>::operator*= (const KSignal<T2> &prototype)
{
	if (this->isSameSize(prototype.length))
		ErrorMessage("Array size error");
	for (int z = 0; z<this->length; z++)
		this->Set(z, this->idata[z] * (T)prototype.idata[z]);
}
template <typename T> template <typename T2> KSignal<T>& KSignal<T>::operator/= (const KSignal<T2> &prototype)
{
	if (this->isSameSize(prototype.length))
		ErrorMessage("Array size error");
	for (int z = 0; z<this->length; z++)
	if (prototype.idata[z] == 0)
		this->Set(z, this->idata[z] / (T)prototype.idata[z]);
	else
		this->Set(z, this->idata[z] / (T)prototype.idata[z]);
}

#endif