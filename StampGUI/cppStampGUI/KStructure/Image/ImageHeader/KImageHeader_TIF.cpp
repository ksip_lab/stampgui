#include "KImageHeader_TIF.h"

KImageHeader_TIF::KImageHeader_TIF()
{
	iCompression = 0;
	iPredictor = 2;
}
KImageHeader_TIF::~KImageHeader_TIF()
{

}
istream& operator>> (istream& in, KImageHeader_TIF& obj)
{
	in >> obj.iCompression;
	in >> obj.iPredictor;

	return in;
}
ostream& operator<< (ostream& out, const KImageHeader_TIF& obj)
{
	out << obj.iCompression << ' ';
	out << obj.iPredictor << ' ';

	return out;
}