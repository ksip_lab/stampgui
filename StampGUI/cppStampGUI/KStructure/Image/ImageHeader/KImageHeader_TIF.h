// ------------------------------------------------ //
// contain:		Header of .TIF file
//
// developer:	Krisada Phromsuthirak
// ------------------------------------------------ //

#ifndef _KImageHeader_TIF_H
#define _KImageHeader_TIF_H

#include "KImageHeader.h"

class KImageHeader_TIF : public KImageHeader
{
public:

#define IMAGE_HEADER_TIF_COMPRESSION_NO		0
#define IMAGE_HEADER_TIF_COMPRESSION_ZIP	1
#define IMAGE_HEADER_TIF_COMPRESSION_MAC	2

	int		iCompression;
	int		iPredictor;

	KImageHeader_TIF();
	~KImageHeader_TIF();

	friend istream& operator >> (istream& in, KImageHeader_TIF& obj);
	friend ostream& operator << (ostream& out, const KImageHeader_TIF& obj);

};

#endif
