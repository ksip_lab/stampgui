// ------------------------------------------------ //
// contain:		Gray-scale Image Structure
//
// developer:	Krisada Phromsuthirak
// ------------------------------------------------ //

#ifndef _KGImage_H
#define _KGImage_H

#include "KGeneral.h"

#include "KImageHeader.h"
#include "KImageHeader_BMP.h"
#include "KImageHeader_TIF.h"
#include "KImageHeader_PNG.h"
#include "KImageHeader_JPG.h"

#include "KInterface_OpenCV.h"
#include "KInterface_PCT.h"

#define IMAGE_SAVE_FORMAT_BMP24			2					//BMP	- Windows Bitmap Graphics (24-bit)
#define IMAGE_SAVE_FORMAT_TIF			3					//TIFF	- Tagged Information File Format  (no LZW compression)
#define IMAGE_SAVE_FORMAT_PNG			4					//PNG	- Portable Network Graphics
#define IMAGE_SAVE_FORMAT_JPG			5					//JPEG	- Joint Photographic Experts Group		
#define IMAGE_SAVE_FORMAT_RGB			6					//RGB	- Silicon Graphics International (uncompressed)
#define IMAGE_SAVE_FORMAT_TGA			7					//TGA	- TrueVision Targa Graphic
#define IMAGE_SAVE_FORMAT_JP2			8					//JP2	- JPEG-2000 Part-1 images
#define IMAGE_SAVE_FORMAT_JPC			9					//JPC	- JPEG-2000 Code Stream images
#define IMAGE_SAVE_FORMAT_BMP8			10					//BMP	- Windows Bitmap Graphics (8-bit)
#define IMAGE_SAVE_FORMAT_TEXT			11					//TXT	- Windows Text file

#define IMAGE_ADD_MODE_RIGHT			0					//Add another image to the right
#define IMAGE_ADD_MODE_BOTTOM			1					//Add another image to the bottom
#define IMAGE_ADD_MODE_LEFT				2					//Add another image to the left
#define IMAGE_ADD_MODE_TOP				3					//Add another image to the top

#define IMAGE_SHOW_WINDOW_NORMAL		CV_WINDOW_NORMAL
#define IMAGE_SHOW_WINDOW_AUTO			CV_WINDOW_AUTOSIZE
#define IMAGE_SHOW_WINDOW_FULL			CV_WINDOW_FULLSCREEN

#define IMAGE_OVERLAY_MODE_DARK			0
#define IMAGE_OVERLAY_MODE_LIGHT		1
#define IMAGE_OVERLAY_MODE_AVERAGE		2

template <typename T>
class KGImage:public KArray2D<T>
{
private:
	bool				isSetHeader;
	KImageHeader		*imageHeader;
	
public:

	KGImage()									{ isAlloc = false;	path = "";	isSetHeader = false; }
	KGImage(int Width, int Height)				{ isAlloc = false;	this->Allocate(Width, Height);	path = "";	isSetHeader = false; }
	~KGImage()									{ this->FreeMem(); }

	string	path;

	void FreeMem();

	void Crop(int stx,int sty,int endx,int endy);
	void Crop(int Width,int Height);
	void Scaling(T outmin=(T)0,T outmax=(T)255);

	template <typename T2> void Set(int x,int y,T2 value);
	template <typename T2> void Add(KGImage<T2> *image, int mode = IMAGE_ADD_MODE_RIGHT);
	template <typename T2> void Pad(KGImage<T2> *image,int x,int y);
	template <typename T2> void Copy(KGImage<T2> *image,int stx,int sty,int endx,int endy);
	template <typename T2> void Copy(KGImage<T2> *image);

	void ExtendBound(int boundSize,T boundValue);
	void ExtendBound(int boundSizeX, int boundSizeY, T boundValue);
	void ReduceBound(int boundSize);
	void ReduceBound(int boundSizeX, int boundSizeY);

	template <typename T2> void Overlay(KGImage<T2> *image, char mode = IMAGE_OVERLAY_MODE_AVERAGE);

	void Read(string &fileName,bool debug=false);
	void Read(char *fileName,bool debug=false);
	void Read(string &fileName,int stx,int sty,int endx,int endy,bool debug=false);
	void Read(char *fileName,int stx,int sty,int endx,int endy,bool debug=false);
	void Save(string fileName, int format = IMAGE_SAVE_FORMAT_BMP8, bool debug = false, bool showDialog = false);
	void Save(char *fileName, int format = IMAGE_SAVE_FORMAT_BMP8, bool debug = false, bool showDialog = false);
	void SetImageHeader(KImageHeader *ImageHeader);
	void GetImageHeader(KImageHeader *ImageHeader, int savedFormat);

	void Show(char *windowName = NULL, bool scaling = false, bool debug = false, int window = IMAGE_SHOW_WINDOW_NORMAL, void MouseHandler(int, int, int, int, void*) = NULL);

	template <typename T2> KGImage<T>& operator +=(const T2 &value);
	template <typename T2> KGImage<T>& operator -=(const T2 &value);
	template <typename T2> KGImage<T>& operator *=(const T2 &value);
	template <typename T2> KGImage<T>& operator /=(const T2 &value);

	template <typename T2> KGImage<T>& operator =  (const KGImage<T2> &prototype);
	template <typename T2> KGImage<T>& operator += (const KGImage<T2> &prototype);
	template <typename T2> KGImage<T>& operator -= (const KGImage<T2> &prototype);
	template <typename T2> KGImage<T>& operator *= (const KGImage<T2> &prototype);
	template <typename T2> KGImage<T>& operator /= (const KGImage<T2> &prototype);

	template <typename T2> KGImage<T>& operator += (const KArray2D<T2> &prototype);
	template <typename T2> KGImage<T>& operator -= (const KArray2D<T2> &prototype);
	template <typename T2> KGImage<T>& operator *= (const KArray2D<T2> &prototype);
	template <typename T2> KGImage<T>& operator /= (const KArray2D<T2> &prototype);

	using KArray2D::operator =;

	friend istream& operator >> (istream& in, KGImage<T>& obj)
	{
		in >> obj.path;
		int w, h;
		in >> w;
		in >> h;
		if (obj.width != w || obj.height != h)
			obj.Resize(w, h);
		for (int y = 0; y<obj.height; y++)
		{
			for (int x = 0; x<obj.width; x++)
			{
				in >> obj.idata[x][y];
			}
		}
		return in;
	}
	friend ostream& operator << (ostream& out, const KGImage<T>& obj)
	{
		out << obj.path << endl;
		out << obj.width << endl;
		out << obj.height << endl << endl;
		for (int y = 0; y<obj.height; y++)
		{
			for (int x = 0; x<obj.width; x++)
			{
				out << obj.idata[x][y] << ' ';
			}
			out << endl;
		}
		return out;
	}
};

// ========================= //
// --- Operator Function --- //
// ========================= //
template <typename T> template <typename T2> KGImage<T>& KGImage<T>::operator+=(const T2 &value)
{
	if(this->isAlloc)
	{
		for (int y = 0; y < this->height; y++)
		for (int x = 0; x < this->width; x++)
			this->idata[x][y] += (T)value;
	}
	else
		ErrorMessage("[must be allocated before operated to any image]");
	return *this;
}
template <typename T> template <typename T2> KGImage<T>& KGImage<T>::operator-=(const T2 &value)
{
	if(this->isAlloc)
	{
		for (int y = 0; y < this->height; y++)
		for (int x = 0; x < this->width; x++)
			this->idata[x][y] -= (T)value;
	}
	else
		ErrorMessage("[must be allocated before operated to any image]");
	return *this;
}
template <typename T> template <typename T2> KGImage<T>& KGImage<T>::operator*=(const T2 &value)
{
	if(this->isAlloc)
	{
		for (int y = 0; y < this->height; y++)
		for (int x = 0; x < this->width; x++)
			this->idata[x][y] *= (T)value;
	}
	else
		ErrorMessage("[must be allocated before operated to any image]");
	return *this;
}
template <typename T> template <typename T2> KGImage<T>& KGImage<T>::operator/=(const T2 &value)
{
	if(this->isAlloc)
	{
		for (int y = 0; y < this->height; y++)
		for (int x = 0; x < this->width; x++)
		{
			if (value == 0)
				this->idata[x][y] = (T)INFINITY;
			else
				this->idata[x][y] /= (T)value;
		}
	}
	else
		ErrorMessage("[must be allocated before operated to any image]");
	return *this;
}
template <typename T> template <typename T2> KGImage<T>& KGImage<T>::operator=(const KGImage<T2> &prototype)
{
	if(prototype.isAlloc)
	{
		if(!this->isAlloc)
			this->Allocate(prototype.width,prototype.height);
		if(!this->IsSameSize(prototype.width,prototype.height))
			this->Resize(prototype.width,prototype.height);

		for (int y = 0; y < this->height; y++)
		for (int x = 0; x < this->width; x++)
			this->idata[x][y] = (T)prototype.idata[x][y];
	}
	else
		ErrorMessage("[must be allocated before operated to any image]");
	return *this;
}
template <typename T> template <typename T2> KGImage<T>& KGImage<T>::operator+=(const KGImage<T2> &prototype)
{
	if(prototype.isAlloc&&this->isAlloc)
	{
		if(!this->IsSameSize(prototype.width,prototype.height))
			ErrorMessage("[image size error]");

		for (int y = 0; y < this->height; y++)
		for (int x = 0; x < this->width; x++)
			this->idata[x][y] += (T)prototype.idata[x][y];
	}
	else
		ErrorMessage("[must be allocated before operated to any image]");
	return *this;
}
template <typename T> template <typename T2> KGImage<T>& KGImage<T>::operator-=(const KGImage<T2> &prototype)
{
	if(prototype.isAlloc&&this->isAlloc)
	{
		if(!this->IsSameSize(prototype.width,prototype.height))
			ErrorMessage("[image size error]");

		for (int y = 0; y < this->height; y++)
		for (int x = 0; x < this->width; x++)
			this->idata[x][y] -= (T)prototype.idata[x][y];
	}
	else
		ErrorMessage("[must be allocated before operated to any image]");
	return *this;
}
template <typename T> template <typename T2> KGImage<T>& KGImage<T>::operator*=(const KGImage<T2> &prototype)
{
	if(prototype.isAlloc&&this->isAlloc)
	{
		if(!this->IsSameSize(prototype.width,prototype.height))
			ErrorMessage("[image size error]");

		for (int y = 0; y < this->height; y++)
		for (int x = 0; x < this->width; x++)
			this->idata[x][y] *= (T)prototype.idata[x][y];
	}
	else
		ErrorMessage("[must be allocated before operated to any image]");
	return *this;
}
template <typename T> template <typename T2> KGImage<T>& KGImage<T>::operator/=(const KGImage<T2> &prototype)
{
	if(this->isAlloc&&prototype.isAlloc)
	{
		if(this->IsSameSize(prototype.width,prototype.height))
		{
			for (int y = 0; y < this->height; y++)
			for (int x = 0; x < this->width; x++)
			{
				if (ptrProto[n] == 0)
					this->idata[x][y] = (T)INFINITY
				else
					this->idata[x][y] /= (T)prototype.idata[x][y];
			}
		}
		else
			ErrorMessage("[image size error]");
	}
	else
		ErrorMessage("[must be allocated before operated to any image]");

	return *this;
}
template <typename T> template <typename T2> KGImage<T>& KGImage<T>::operator+=(const KArray2D<T2> &prototype)
{
	if (prototype.isAlloc&&this->isAlloc)
	{
		if (!this->IsSameSize(prototype.width, prototype.height))
			ErrorMessage("[image size error]");

		for (int y = 0; y < this->height; y++)
		for (int x = 0; x < this->width; x++)
			this->idata[x][y] += (T)prototype.idata[x][y];
	}
	else
		ErrorMessage("[must be allocated before operated to any image]");
	return *this;
}
template <typename T> template <typename T2> KGImage<T>& KGImage<T>::operator-=(const KArray2D<T2> &prototype)
{
	if (prototype.isAlloc&&this->isAlloc)
	{
		if (!this->IsSameSize(prototype.width, prototype.height))
			ErrorMessage("[image size error]");

		for (int y = 0; y < this->height; y++)
		for (int x = 0; x < this->width; x++)
			this->idata[x][y] -= (T)prototype.idata[x][y];
	}
	else
		ErrorMessage("[must be allocated before operated to any image]");
	return *this;
}
template <typename T> template <typename T2> KGImage<T>& KGImage<T>::operator*=(const KArray2D<T2> &prototype)
{
	if (prototype.isAlloc&&this->isAlloc)
	{
		if (!this->IsSameSize(prototype.width, prototype.height))
			ErrorMessage("[image size error]");

		for (int y = 0; y < this->height; y++)
		for (int x = 0; x < this->width; x++)
			this->idata[x][y] *= (T)prototype.idata[x][y];
	}
	else
		ErrorMessage("[must be allocated before operated to any image]");
	return *this;
}
template <typename T> template <typename T2> KGImage<T>& KGImage<T>::operator/=(const KArray2D<T2> &prototype)
{
	if (this->isAlloc&&prototype.isAlloc)
	{
		if (this->IsSameSize(prototype.width, prototype.height))
		{
			for (int y = 0; y < this->height; y++)
			for (int x = 0; x < this->width; x++)
			{
				if (ptrProto[n] == 0)
					this->idata[x][y] = (T)INFINITY
				else
				this->idata[x][y] /= (T)prototype.idata[x][y];
			}
		}
		else
			ErrorMessage("[image size error]");
	}
	else
		ErrorMessage("[must be allocated before operated to any image]");

	return *this;
}
// ======================== //
// --- General Function --- //
// ======================== //
template <typename T> void KGImage<T>::FreeMem()
{
	if (isAlloc)
	{
		Free<T>(idata, width, height);
		width = 0;
		height = 0;
		path = "";
		isSetHeader = false;
		isAlloc = false;
	}
}
template <typename T> void KGImage<T>::Crop(int stx,int sty,int endx,int endy)
{
	if(this->isAlloc)
	{
		if(endx<=stx || endy<=sty)
		{
			ErrorMessage("Invalid Crop Position");
		}
		else
		{
			int		new_w = endx - stx;
			int		new_h = endy - sty;
			int		x,y,i,j;
			if(!this->IsSameSize(new_w,new_h))
			{
				KGImage<T>	tmp;
				tmp = (*this);
				this->Resize(new_w,new_h);
				for(x=stx,i=0; i<new_w; x++,i++)
				{
					for(y=sty,j=0; j<new_h; y++,j++)
					{
						if(tmp.IsInBound(x,y))
						{
							this->idata[i][j]=tmp.idata[x][y];
						}
					}
				}
				tmp.FreeMem();
			}
		}
	}
	else
		ErrorMessage("[must be allocated before pad any image]");
}
template <typename T> void KGImage<T>::Crop(int Width,int Height)
{
	int cx			= (int)(this->width>>1);
	int cy			= (int)(this->height>>1);
	int halfWidth	= (int)(Width>>1);
	int halfHeight	= (int)(Height>>1);
	int stx,sty,endx,endy;

	stx				= cx - halfWidth;
	sty				= cy - halfHeight;
	endx			= cx + halfWidth;
	endy			= cy + halfHeight;
	this->Crop(stx,sty,endx,endy);
}
template <typename T> void KGImage<T>::Scaling(T outmin=(T)0,T outmax=(T)255)
{
	if(!isAlloc)
		ErrorMessage("[no data to scaling]");

	T max_value = this->idata[0][0];
	T min_value = this->idata[0][0];
	T outrange;

	for (int y = 0; y < this->height; y++)
	for (int x = 0; x < this->width; x++)
	{
		if (this->idata[x][y] > max_value)
			max_value = this->idata[x][y];
		if (this->idata[x][y] < min_value)
			min_value = this->idata[x][y];
	}

	if (max_value != min_value)
	{
		outrange = outmax - outmin;
		for (int y = 0; y < this->height; y++)
		for (int x = 0; x < this->width; x++)
			this->idata[x][y] = (T)((this->idata[x][y] - min_value)*outrange / (max_value - min_value) + outmin);
	}
}
template <typename T> template <typename T2> void KGImage<T>::Set(int x,int y,T2 value)
{
	if(this->isAlloc)
	{
		if(this->IsInBound(x,y))
			this->idata[x][y] = (T)value;
		else
			ErrorMessage("Out of image bound");
	}
	else
		ErrorMessage("[must be allocated image]");
}
template <typename T> template <typename T2> void KGImage<T>::Add(KGImage<T2> *image, int mode = IMAGE_ADD_MODE_RIGHT)
{
	if(image->isAlloc)
	{
		if(this->isAlloc)
		{
			KGImage<T> tmp;
			tmp = *this;
			switch(mode)
			{
			case IMAGE_ADD_MODE_RIGHT:
			case IMAGE_ADD_MODE_LEFT:
				this->Resize((tmp.width+image->width),Max(tmp.height,image->height));
				break;
			case IMAGE_ADD_MODE_BOTTOM:
			case IMAGE_ADD_MODE_TOP:
				this->Resize(Max(tmp.width,image->width),(tmp.height+image->height));
				break;
			default:
				ErrorMessage("[Add mode error]");
				break;
			}
			switch(mode)
			{
			case IMAGE_ADD_MODE_RIGHT:
				this->Pad(&tmp,0,0);
				this->Pad(image,tmp.width,0);
				break;
			case IMAGE_ADD_MODE_BOTTOM:
				this->Pad(&tmp,0,0);
				this->Pad(image,0,tmp.height);
				break;
			case IMAGE_ADD_MODE_LEFT:
				this->Pad(image,0,0);
				this->Pad(&tmp,image->width,0);
				break;
			case IMAGE_ADD_MODE_TOP:
				this->Pad(image,0,0);
				this->Pad(&tmp,0,image->height);
				break;
			default:
				ErrorMessage("[Add mode error]");
				break;
			}
			tmp.FreeMem();
		}
		else
			*this = *image;
	}
	else
		ErrorMessage("[must be allocated image]");
}
template <typename T> template <typename T2> void KGImage<T>::Pad(KGImage<T2> *image,int x,int y)
{
	if(this->isAlloc&&image->isAlloc)
	{
		int i,j,u,v;
		for(j=0,v=y;j<image->height;j++,v++)
			for(i=0,u=x;i<image->width;i++,u++)
				if(this->IsInBound(u,v))
					this->idata[u][v] = (T)image->idata[i][j];
	}
	else
		ErrorMessage("[must be allocated before pad any image]");
}
template <typename T> template <typename T2> void KGImage<T>::Copy(KGImage<T2> *image,int stx,int sty,int endx,int endy)
{
	if(image->isAlloc)
	{
		if(this->isAlloc)
		{
			if(endx<stx || endy<sty || (endx-stx)>this->width || (endy-sty)>this->height)
			{
				ErrorMessage("Invalid Copy Position");
			}
			else
			{
				int x,y,u,v;
				for(v=sty,y=0;v<endy;v++,y++)
					for(u=stx,x=0;u<endx;u++,x++)
						if(this->IsInBound(x,y)&&image->IsInBound(u,v))
							this->idata[x][y] = (T)image->idata[u][v];
			}
		}
		else
		{
			int x,y,u,v;
			this->Allocate(endx-stx,endy-sty);
			for(v=sty,y=0;v<endy;v++,y++)
				for(u=stx,x=0;u<endx;u++,x++)
					if(this->IsInBound(x,y)&&image->IsInBound(u,v))
						this->idata[x][y] = (T)image->idata[u][v];
		}
	}
	else
		ErrorMessage("[must be allocated image]");
}
template <typename T> template <typename T2> void KGImage<T>::Copy(KGImage<T2> *image)
{
	Copy(image, 0, 0, image->width, image->height);
}
template <typename T> void KGImage<T>::ExtendBound(int boundSize,T boundValue)
{
	if(!isAlloc)
		ErrorMessage("[must be allocate before]");
	KGImage<T> tmp;
	tmp = *this;
	this->Resize(width+boundSize*2,height+boundSize*2);
	*this = boundValue;
	this->Pad(&tmp,boundSize,boundSize);
	tmp.FreeMem();
}
template <typename T> void KGImage<T>::ExtendBound(int boundSizeX, int boundSizeY, T boundValue)
{
	if (!isAlloc)
		ErrorMessage("[must be allocate before]");
	KGImage<T> tmp;
	tmp = *this;
	this->Resize(width + boundSizeX * 2, height + boundSizeY * 2);
	*this = boundValue;
	this->Pad(&tmp, boundSizeX, boundSizeY);
	tmp.FreeMem();
}
template <typename T> void KGImage<T>::ReduceBound(int boundSize)
{
	if(!isAlloc)
		ErrorMessage("[must be allocate before]");
	KGImage<T> tmp;
	int x,y,u,v;
	tmp = *this;
	this->Resize(width-boundSize*2,height-boundSize*2);
	for(y=0,v=boundSize;y<height;y++,v++)
		for(x=0,u=boundSize;x<width;x++,u++)
			this->idata[x][y] = tmp.idata[u][v];
	tmp.FreeMem();
}
template <typename T> void KGImage<T>::ReduceBound(int boundSizeX, int boundSizeY)
{
	if (!isAlloc)
		ErrorMessage("[must be allocate before]");
	KGImage<T> tmp;
	int x, y, u, v;
	tmp = *this;
	this->Resize(width - boundSizeX * 2, height - boundSizeY * 2);
	for (y = 0, v = boundSizeY; y<height; y++, v++)
	for (x = 0, u = boundSizeX; x<width; x++, u++)
		this->idata[x][y] = tmp.idata[u][v];
	tmp.FreeMem();
}
template <typename T> template <typename T2> void KGImage<T>::Overlay(KGImage<T2> *image, char mode = IMAGE_OVERLAY_MODE_AVERAGE)
{
	if (this->IsSameSize(image->width, image->height))
	{
		for (int y = 0; y < height; y++)
		for (int x = 0; x < width; x++)
		{
			if (mode == IMAGE_OVERLAY_MODE_DARK)
			{
				this->idata[x][y] = (T)Min((double)this->idata[x][y], (double)image->idata[x][y]);
			}
			else if (mode == IMAGE_OVERLAY_MODE_LIGHT)
			{
				this->idata[x][y] = (T)Max((double)this->idata[x][y], (double)image->idata[x][y]);
			}
			else if (mode == IMAGE_OVERLAY_MODE_AVERAGE)
			{
				this->idata[x][y] = (T)(((double)this->idata[x][y] + (double)image->idata[x][y]) / 2.0);
			}
			else
				ErrorMessage("error KGImage [Overlay] : wrong mode");
		}
	}
	else
		ErrorMessage("error KGImage [Overlay] : image must be same size");
}

// ========================= //
// --- FILE I/O Function --- //
// ========================= //
// Supported formats for reading:
// BMP - Windows bitmaps
// TIFF - Macintosh Tagged Information File Format images
// TIF - PC Tagged Information File Format images
// JPG, JPEG - Joint Photographic Experts Group images
// JP2 - JPEG-2000 Part-1 images
// JPC - JPEG-2000 Code Stream images
// JPE - JPE images
// PNG - Portable network graphic images
// GIF - Compuserve Graphic Interchange Format
// PSP - Paintshop Pro images
// JFIF - JFIF images
// RLE - Run length encoded Windows bitmaps
// DIB - Device independent Windows bitmaps
// TGA,WIN,VST,VDA,ICB - Truevision images 
// FAX - GFI fax images
// EPS - Encapsulated Postscript images
// PCX,PCC - ZSoft Paintbrush images
// SCR - Word 5.x screen capture images
// RPF,RLA - Alias/Wavefront images
// SGI,RGBA,RGB - SGI true color images
// BW - SGI black/white images
// PSD - Photoshop images
// PDD - Photoshop images
// PPM - Portable pixel map images
// PGM - Portable gray map images
// PBM - Portable bitmap images
// CEL - Autodesk images
// PIC - Autodesk images
// PCD - Kodak Photo-CD images
// CUT - Dr. Halo images
// IFF - Amiga Bitmap Graphic (8 bits)
// ICO - Windows icons
// WMF - Windows metafiles
// EMF - Windows enhanced meta files
// PCT - Image file that uses the Macintosh PICT format (NIST-Db4 format)
// TXT - Text file
// BMP,DIB - Windows bitmaps
// JPEG,JPG,JPE - JPEG files
// JP2 - JPEG 2000 files
// PNG - Portable Network Graphics
// PBM,PGM,PPm - Portable image format
// SR,RAS - Sun rasters
// TIFF,TIF - TIFF files
template <typename T> void KGImage<T>::Read(string &fileName,bool debug=false)
{
	Read((char*)fileName.c_str(),debug);
}
template <typename T> void KGImage<T>::Read(char *fileName,bool debug=false)
{
	if(isAlloc)
		this->FreeMem();

	string chkNIST,chkTXT;

	path	= fileName;
	chkNIST	= fileName;
	chkTXT	= fileName;
	if(chkNIST.substr(chkNIST.find_last_of(".") + 1) == "pct" || chkNIST.substr(chkNIST.find_last_of(".") + 1) == "PCT")
	{
		KInterface_PCT			InterfacePCT;
		KArray1D<unsigned char>	ptr;
		int						w,h,n;
		InterfacePCT.LoadFile(fileName,&ptr,&w,&h);
		this->Allocate(w,h);
		n = 0;
		for(int y=0; y<this->height; y++)
			for(int x=0; x<this->width; x++)	
				this->idata[x][y] = (T)ptr.idata[n++];
		ptr.FreeMem();

		if(debug)
			printf("read image from: %s\n",fileName);
	}
	else if(chkTXT.substr(chkTXT.find_last_of(".") + 1) == "txt" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "TXT")
	{
		ifstream inFile;
		inFile.open(fileName);
		inFile >> *this;
		inFile.close();
	}
	else if ( chkTXT.substr(chkTXT.find_last_of(".") + 1) == "jpc" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "JPC"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "jpe" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "JPE"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "gif" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "GIF"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "psp" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "PSP"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "jfif" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "JFIF"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "dib" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "DIB"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "tga" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "TGA"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "win" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "WIN"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "vst" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "VST"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "vda" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "VDA"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "icb" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "ICB"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "fax" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "FAX"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "eps" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "EPS"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "pcx" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "PCX"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "pcc" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "PCC"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "scr" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "SCR"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "rpf" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "RPF"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "rla" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "RLA"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "sgi" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "SGI"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "rgba" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "RGBA"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "rgb" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "RGB"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "bw" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "BW"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "psd" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "PSD"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "pdd" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "PDD"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "cel" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "CEL"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "pic" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "PIC"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "pcd" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "PCD"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "cut" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "CUT"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "iff" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "IFF"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "ico" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "ICO"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "wmf" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "WMF"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "emf" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "EMF")
	{
		if (debug)
			printf("read image from: %s\n", fileName);
	}
	else if (chkTXT.substr(chkTXT.find_last_of(".") + 1) == "bmp" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "BMP"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "dib" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "DIB"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "jpeg" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "JPEG"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "jpg" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "JPG"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "jpe" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "JPE"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "jp2" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "JP2"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "png" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "PNG"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "pbm" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "PBM"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "pgm" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "PGM"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "ppm" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "PPM"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "sr" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "SR"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "ras" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "RAS"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "tiff" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "TIFF"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "tif" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "TIF"
		)
	{
		KInterface_OpenCV Library_OpenCV;
		Library_OpenCV.ReadImage_Grayscale(fileName, this);
		if (debug)
			printf("read image from: %s\n", fileName);
	}
	else
	{
		printf("cannot read image: [%s]\n", fileName);
		system("pause");
		exit(0);
	}
}
template <typename T> void KGImage<T>::Read(string &fileName,int stx,int sty,int endx,int endy,bool debug=false)
{
	Read((char*)fileName.c_str(),stx,sty,endx,endy,debug);
}
template <typename T> void KGImage<T>::Read(char *fileName,int stx,int sty,int endx,int endy,bool debug=false)
{
	if(isAlloc)
		this->FreeMem();

	int new_w	= endx - stx;
	int new_h	= endy - sty;

	string chkNIST,chkTXT;

	chkNIST = fileName;
	chkTXT	= fileName;
	path	= fileName;
	if(chkNIST.substr(chkNIST.find_last_of(".") + 1) == "pct" || chkNIST.substr(chkNIST.find_last_of(".") + 1) == "PCT")
	{
		KInterface_PCT			InterfacePCT;
		KArray1D<unsigned char>	ptr;
		int						w,h,n;
		InterfacePCT.LoadFile(fileName,&ptr,&w,&h);
		this->Allocate(new_w,new_h);
		n = 0;
		for(int y=0; y<h; y++)
		for(int x=0; x<w; x++)
		{
			if(x<stx||y<sty||x>=endx||y>=endy)
				n++;
			else
				this->idata[x-stx][y-sty] = (T)ptr.idata[n++];
		}
		ptr.FreeMem();
	}
	else if(chkTXT.substr(chkTXT.find_last_of(".") + 1) == "txt" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "TXT")
	{
		KArray2D<T>	tmp;
		ifstream inFile;
		inFile.open(fileName);
		inFile >> tmp;
		inFile.close();
		for (int y = 0; y<height; y++)
		for (int x = 0; x<width; x++)
		{
			if (x >= stx && y >= sty && x < endx && y < endy)
				this->idata[x - stx][y - sty] = (T)tmp.idata[x][y];
		}
		tmp.FreeMem();
	}
	else if (chkTXT.substr(chkTXT.find_last_of(".") + 1) == "jpc" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "JPC"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "jpe" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "JPE"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "gif" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "GIF"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "psp" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "PSP"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "jfif" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "JFIF"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "dib" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "DIB"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "tga" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "TGA"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "win" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "WIN"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "vst" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "VST"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "vda" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "VDA"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "icb" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "ICB"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "fax" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "FAX"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "eps" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "EPS"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "pcx" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "PCX"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "pcc" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "PCC"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "scr" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "SCR"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "rpf" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "RPF"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "rla" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "RLA"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "sgi" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "SGI"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "rgba" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "RGBA"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "rgb" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "RGB"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "bw" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "BW"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "psd" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "PSD"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "pdd" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "PDD"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "cel" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "CEL"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "pic" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "PIC"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "pcd" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "PCD"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "cut" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "CUT"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "iff" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "IFF"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "ico" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "ICO"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "wmf" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "WMF"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "emf" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "EMF")
	{
		if (debug)
			printf("read image from: %s\n", fileName);
	}
	else if (chkTXT.substr(chkTXT.find_last_of(".") + 1) == "bmp" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "BMP"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "dib" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "DIB"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "jpeg" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "JPEG"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "jpg" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "JPG"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "jpe" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "JPE"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "jp2" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "JP2"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "png" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "PNG"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "pbm" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "PBM"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "pgm" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "PGM"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "ppm" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "PPM"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "sr" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "SR"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "ras" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "RAS"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "tiff" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "TIFF"
		|| chkTXT.substr(chkTXT.find_last_of(".") + 1) == "tif" || chkTXT.substr(chkTXT.find_last_of(".") + 1) == "TIF"
		)
	{
		KInterface_OpenCV Library_OpenCV;
		Library_OpenCV.ReadImage_Grayscale(fileName, this, stx, sty, endx, endy);
		if (debug)
			printf("read image from: %s\n", fileName);
	}
	else
	{
		printf("cannot read image: [%s]\n", fileName);
		system("pause");
		exit(0);
	}
}
template <typename T> void KGImage<T>::Save(string fileName, int format = IMAGE_SAVE_FORMAT_BMP8, bool debug = false, bool showDialog = false)
{
	Save((char*)fileName.c_str(), format, debug, showDialog);
}
template <typename T> void KGImage<T>::Save(char *fileName, int format = IMAGE_SAVE_FORMAT_BMP8, bool debug = false, bool showDialog = false)
{
	//  BMP  - Windows Bitmap Graphics											(format = 2)
	//  TIFF - Tagged Information File Format  (no LZW compression)				(format = 3)
	//  PNG  - Portable Network Graphics										(format = 4)
	//  JPEG - Joint Photographic Experts Group									(format = 5)
	//  RGB  - Silicon Graphics International (uncompressed)					(format = 6)
	//  TGA  - Truevision Targa Graphic											(format = 7)
	//  JP2  - JPEG-2000 Part-1 images											(format = 8)
	//  JPC  - JPEG-2000 Code Stream images										(format = 9)

	char *savePath = "";
	string saveTmp;

	if(isAlloc)
	{
		if (format == IMAGE_SAVE_FORMAT_TEXT)
		{
			saveTmp = ReplaceExtension(fileName, "txt");
			savePath = (char*)saveTmp.c_str();

			ofstream	outFile;
			outFile.open(savePath);
			outFile << *this;
			outFile.close();
		}
		else if (format == IMAGE_SAVE_FORMAT_BMP8)
		{
			saveTmp = ReplaceExtension(fileName, "bmp");
			savePath = (char*)saveTmp.c_str();

			int filelen;
			int count;
			RGBQUAD rgb[256];
			int i;
			int data_size = width*height;
			FILE *fp;
			errno_t err;
			long ppm;
			err = fopen_s(&fp, savePath, "wb");
			if (err != 0)
				ErrorMessage("error KGImage:[Save] cannot open file");

			// save bitmap file and info header
			BITMAPINFOHEADER bih;
			BITMAPFILEHEADER bfh;

			if (!isSetHeader)
				ppm = (long)(((double)499.9) / ((double)0.0254));	//500 dpi
			else
				ppm = (long)((((double)((KImageHeader_BMP*)imageHeader)->iPixelPerInch) - 0.1) / (double)(0.0254));

			bih.biSizeImage		= data_size;
			bih.biSize			= sizeof(bih);
			bih.biWidth			= width;
			bih.biHeight		= height;
			bih.biPlanes		= 1;
			bih.biBitCount		= 8;
			bih.biCompression	= BI_RGB;
			bih.biXPelsPerMeter = ppm;
			bih.biYPelsPerMeter	= ppm;
			bih.biClrUsed		= 0;
			bih.biClrImportant	= 0;

			filelen				= bih.biSizeImage+1078;
			bfh.bfSize			= filelen;
			bfh.bfType			= 0x4d42;
			bfh.bfOffBits		= 1078;
			bfh.bfReserved1		= bfh.bfReserved2=0;

			count=(int)fwrite(&bfh,sizeof(BITMAPFILEHEADER),1 ,fp);
			if(count!=1)
			{
				fclose(fp);
				ErrorMessage("write file error!");
			}

			count=(int)fwrite(&bih,sizeof(BITMAPINFOHEADER),1, fp);
			if(count!=1)
			{
				fclose(fp);
				ErrorMessage("write file error!");
			}

			for(i=0; i<256; i++)
			{
				rgb[i].rgbBlue=rgb[i].rgbGreen=rgb[i].rgbRed=i;
				rgb[i].rgbReserved=0;
			}

			count=(int)fwrite(rgb,sizeof(RGBQUAD),256,fp);
			if (count!=256)
			{
				fclose(fp);
				ErrorMessage("write file error!");
			}

			int padding = (bih.biWidth % 4) != 0 ? 4 -(bih.biWidth % 4) : 0; //determine padding needed for bitmap file
			BYTE* bytes = (BYTE*)malloc(bih.biWidth * bih.biHeight + padding *bih.biHeight); //create array to contain bitmap data with padding

			for (int y = 0; y < bih.biHeight; y++)
			{
				for (int x = 0; x < bih.biWidth; x++)
				{
					bytes[(bih.biHeight - 1 - y) * bih.biWidth + (bih.biHeight - 1 - y) * padding + x] = (byte)this->idata[x][y];
				}
				//add the padding
				for (int i = 0; i < padding; i++)
				{
					bytes[(bih.biHeight - y) * bih.biWidth + (bih.biHeight - 1 - y) * padding + i] = (byte)0;
				}
			}
			fwrite(bytes,bih.biWidth * bih.biHeight + padding *bih.biHeight,1,fp);
			free(bytes);

			fclose(fp);
		}
		else if (format == IMAGE_SAVE_FORMAT_BMP24)
		{
			saveTmp = ReplaceExtension(fileName, "bmp");
			savePath = (char*)saveTmp.c_str();

			int offset;
			int x, y, k;
			long ppm;
			BYTE *buf;
			FILE *fp;
			BITMAPINFOHEADER bih;
			BITMAPFILEHEADER bfh;

			if (!isSetHeader)
				ppm = 19685;	//500 dpi
			else
				ppm = (long)((double)((KImageHeader_BMP*)imageHeader)->iPixelPerInch / (double)(0.0254));

			fopen_s(&fp, savePath, "wb");
			if ((buf = (BYTE*)malloc((this->width * 3L + 3) & 0xffffffc)) == NULL)
				ErrorMessage("Out of memory. Cannot save image");
			if (fp == NULL)
			{
				free(buf);
				ErrorMessage("Out of memory. Cannot save image");
			}

			bfh.bfType = 0x4d42; //"BM"//
			bfh.bfSize = (0x36L + ((this->width * 3L + 3) & 0xffffffc)*this->height);
			bfh.bfReserved1 = 0;
			bfh.bfReserved2 = 0;
			bfh.bfOffBits = 0x36L;

			bih.biSize = 0x28L;
			bih.biWidth = (LONG)width;
			bih.biHeight = (LONG)height;
			bih.biPlanes = 1;
			bih.biBitCount = 24;
			bih.biCompression = 0L;
			bih.biSizeImage = ((width * 3L + 3) & 0xffffffc)*height;
			bih.biXPelsPerMeter = ppm;
			bih.biYPelsPerMeter = ppm;
			bih.biClrUsed = 0;
			bih.biClrImportant = 0;

			//////// write file header /////////
			fputc(bfh.bfType & 0xff, fp);								fputc((bfh.bfType >> 8) & 0xff, fp);
			fputc(bfh.bfSize & 0xff, fp);								fputc((bfh.bfSize >> 8) & 0xff, fp);
			fputc((bfh.bfSize >> 16) & 0xff, fp);						fputc((bfh.bfSize >> 24) & 0xff, fp);
			fputc(bfh.bfReserved1 & 0xff, fp);							fputc((bfh.bfReserved1 >> 8) & 0xff, fp);
			fputc(bfh.bfReserved2 & 0xff, fp);							fputc((bfh.bfReserved2 >> 8) & 0xff, fp);
			fputc(bfh.bfOffBits & 0xff, fp);							fputc((bfh.bfOffBits >> 8) & 0xff, fp);
			fputc((bfh.bfOffBits >> 16) & 0xff, fp);					fputc((bfh.bfOffBits >> 24) & 0xff, fp);
			//////// write info header /////////
			fputc(bih.biSize & 0xff, fp);								fputc((bih.biSize >> 8) & 0xff, fp);
			fputc((bih.biSize >> 16) & 0xff, fp);						fputc((bih.biSize >> 24) & 0xff, fp);
			fputc((DWORD)bih.biWidth & 0xff, fp);						fputc(((DWORD)bih.biWidth >> 8) & 0xff, fp);
			fputc(((DWORD)bih.biWidth >> 16) & 0xff, fp);				fputc(((DWORD)bih.biWidth >> 24) & 0xff, fp);
			fputc((DWORD)bih.biHeight & 0xff, fp);						fputc(((DWORD)bih.biHeight >> 8) & 0xff, fp);
			fputc(((DWORD)bih.biHeight >> 16) & 0xff, fp);				fputc(((DWORD)bih.biHeight >> 24) & 0xff, fp);
			fputc(bih.biPlanes & 0xff, fp);								fputc((bih.biPlanes >> 8) & 0xff, fp);
			fputc(bih.biBitCount & 0xff, fp);							fputc((bih.biBitCount >> 8) & 0xff, fp);
			fputc(bih.biCompression & 0xff, fp);						fputc((bih.biCompression >> 8) & 0xff, fp);
			fputc((bih.biCompression >> 16) & 0xff, fp);				fputc((bih.biCompression >> 24) & 0xff, fp);
			fputc(bih.biSizeImage & 0xff, fp);							fputc((bih.biSizeImage >> 8) & 0xff, fp);
			fputc((bih.biSizeImage >> 16) & 0xff, fp);					fputc((bih.biSizeImage >> 24) & 0xff, fp);
			fputc((DWORD)bih.biXPelsPerMeter & 0xff, fp);				fputc(((DWORD)bih.biXPelsPerMeter >> 8) & 0xff, fp);
			fputc(((DWORD)bih.biXPelsPerMeter >> 16) & 0xff, fp);		fputc(((DWORD)bih.biXPelsPerMeter >> 24) & 0xff, fp);
			fputc((DWORD)bih.biYPelsPerMeter & 0xff, fp);				fputc(((DWORD)bih.biYPelsPerMeter >> 8) & 0xff, fp);
			fputc(((DWORD)bih.biYPelsPerMeter >> 16) & 0xff, fp);		fputc(((DWORD)bih.biYPelsPerMeter >> 24) & 0xff, fp);
			fputc(bih.biClrUsed & 0xff, fp);							fputc((bih.biClrUsed >> 8) & 0xff, fp);
			fputc((bih.biClrUsed >> 16) & 0xff, fp);					fputc((bih.biClrUsed >> 24) & 0xff, fp);
			fputc(bih.biClrImportant & 0xff, fp);						fputc((bih.biClrImportant >> 8) & 0xff, fp);
			fputc((bih.biClrImportant >> 16) & 0xff, fp);				fputc((bih.biClrImportant >> 24) & 0xff, fp);
			/////// end write header /////////

			offset = (height - 1);
			for (y = 0; y < height; y++)
			{
				for (x = 0, k = 0; x < width; x++, k += 3)
				{
					buf[k + 2] = (BYTE)this->idata[x][offset];		//Red;  
					buf[k + 1] = (BYTE)this->idata[x][offset];		//Green;
					buf[k] = (BYTE)this->idata[x][offset];			//Blue; 
				}
				offset--;
				fwrite(buf, (width * 3L + 3) & 0xffffffc, 1, fp);
			}

			fclose(fp);
			free(buf);
		}
		else if (format == IMAGE_SAVE_FORMAT_PNG)
		{
			saveTmp = ReplaceExtension(fileName, "png");
			savePath = (char*)saveTmp.c_str();

			KInterface_OpenCV cv;
			Mat cvImg;
			cv.ConvertKArray2DToMat(this, &cvImg);

			if (isSetHeader == true)
			{
				vector<int> compression_params;
				int comp = ((KImageHeader_PNG*)imageHeader)->iCompression;
				compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
				if (comp < 0)
					comp = 0;
				if (comp > 9)
					comp = 9;
				compression_params.push_back(comp);
				imwrite(savePath, cvImg, compression_params);
				compression_params.clear();
			}
			else
				imwrite(savePath, cvImg);

			cvImg.release();
		}
		else if (format == IMAGE_SAVE_FORMAT_JPG)
		{
			saveTmp = ReplaceExtension(fileName, "jpg");
			savePath = (char*)saveTmp.c_str();

			KInterface_OpenCV cv;
			Mat cvImg;
			cv.ConvertKArray2DToMat(this, &cvImg);

			if (isSetHeader == true)
			{
				vector<int> quality_params;
				int comp = ((KImageHeader_JPG*)imageHeader)->iQuality;
				quality_params.push_back(CV_IMWRITE_JPEG_QUALITY);
				if (comp < 0)
					comp = 0;
				if (comp > 100)
					comp = 100;
				quality_params.push_back(comp);
				imwrite(savePath, cvImg, quality_params);
				quality_params.clear();
			}
			else
				imwrite(savePath, cvImg);

			cvImg.release();
		}
		else if (format == IMAGE_SAVE_FORMAT_TIF ||
			format == IMAGE_SAVE_FORMAT_RGB || format == IMAGE_SAVE_FORMAT_TGA ||
			format == IMAGE_SAVE_FORMAT_JP2 || format == IMAGE_SAVE_FORMAT_JPC)
		{
			if (format == IMAGE_SAVE_FORMAT_TIF)
				saveTmp = ReplaceExtension(fileName, "tif");
			else if (format == IMAGE_SAVE_FORMAT_RGB)
				saveTmp = ReplaceExtension(fileName, "rgb");
			else if (format == IMAGE_SAVE_FORMAT_TGA)
				saveTmp = ReplaceExtension(fileName, "tga");
			else if (format == IMAGE_SAVE_FORMAT_JP2)
				saveTmp = ReplaceExtension(fileName, "jp2");
			else if (format == IMAGE_SAVE_FORMAT_JPC)
				saveTmp = ReplaceExtension(fileName, "jpc");
			else
				saveTmp = fileName;
			savePath = (char*)saveTmp.c_str();

			HBITMAP		hBitmap;
			BITMAPINFO	bInfo;
			int bmBitsPixel = 8;
			int bmPlanes    = 3;

			ZeroMemory(&bInfo.bmiHeader, sizeof(BITMAPINFOHEADER));
			bInfo.bmiHeader.biSize			= sizeof(BITMAPINFOHEADER);
			bInfo.bmiHeader.biWidth			= this->width;
			bInfo.bmiHeader.biHeight		= this->height;
			bInfo.bmiHeader.biPlanes		= 1;
			bInfo.bmiHeader.biBitCount		= bmPlanes * bmBitsPixel;
			bInfo.bmiHeader.biCompression	= BI_RGB;
			bInfo.bmiHeader.biSizeImage		= this->width * this->height *bmPlanes;
			bInfo.bmiHeader.biXPelsPerMeter = 19685;
			bInfo.bmiHeader.biYPelsPerMeter = 19685;
			bInfo.bmiHeader.biClrUsed		= 0;
			bInfo.bmiHeader.biClrImportant  = 0;

			HDC	hdc				= CreateCompatibleDC(NULL);
			int  temple_size	= this->width*bmPlanes+bmPlanes;
			BYTE *tempScanLine	= Alloc<BYTE>(temple_size);
			BYTE * pBits = NULL;
			
			hBitmap = CreateDIBSection(hdc, &bInfo, DIB_RGB_COLORS, (void **)&pBits, NULL, 0);
			for(int y = 0,yy=this->height-1; y < this->height; ++y,--yy)
			{					
				for(int x = 0, xx = 0; xx < this->width; x+=bmPlanes,++xx)
				{
					for(int n = 0 ; n < bmPlanes; n++)
					{
						tempScanLine[x+n]   = (BYTE)this->idata[xx][yy];
					}
				}
				SetDIBits(hdc, (HBITMAP) hBitmap, y, 1, tempScanLine, &bInfo, DIB_RGB_COLORS);
			}

			Free(tempScanLine,temple_size);
			DeleteDC(hdc);
			DeleteObject(hBitmap);
		}
		else
			ErrorMessage("save format error");

		if(debug)
			printf("save image to:   %s\n", savePath);
	}
	else
		ErrorMessage("[no data to save. Please allocated this variable]",false);
}
template <typename T> void KGImage<T>::SetImageHeader(KImageHeader *ImageHeader)
{
	imageHeader = ImageHeader;
	isSetHeader = true;
}
template <typename T> void KGImage<T>::GetImageHeader(KImageHeader *ImageHeader, int savedFormat)
{
	if (savedFormat == IMAGE_SAVE_FORMAT_TIF)
	{
		((KImageHeader_TIF*)ImageHeader)->iCompression = ((KImageHeader_TIF*)imageHeader)->iCompression;
		((KImageHeader_TIF*)ImageHeader)->iPredictor = ((KImageHeader_TIF*)imageHeader)->iPredictor;
	}
	if (savedFormat == IMAGE_SAVE_FORMAT_PNG)
	{
		((KImageHeader_PNG*)ImageHeader)->iCompression = ((KImageHeader_PNG*)imageHeader)->iCompression;
	}
	if (savedFormat == IMAGE_SAVE_FORMAT_JPG)
	{
		((KImageHeader_JPG*)ImageHeader)->iQuality = ((KImageHeader_JPG*)imageHeader)->iQuality;
	}
	if (savedFormat == IMAGE_SAVE_FORMAT_BMP8 || savedFormat == IMAGE_SAVE_FORMAT_BMP24)
	{
		((KImageHeader_BMP*)ImageHeader)->iPixelPerInch = ((KImageHeader_BMP*)imageHeader)->iPixelPerInch;
	}
}
template <typename T> void KGImage<T>::Show(char *windowName = NULL, bool scaling = false, bool debug = false, int window = IMAGE_SHOW_WINDOW_NORMAL, void MouseHandler(int, int, int, int, void*) = NULL)
{
	if (this->isAlloc)
	{
		if (windowName == NULL)
			windowName = "";

		KGImage<T> tmp;
		if (scaling)
		{
			tmp = *this;
			tmp.Scaling();
		}

		IplImage	*cvData = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);
		char		*cvImage = cvData->imageData;
		for (int xc = 0; xc < cvData->height; xc++)
		for (int yc = 0; yc < cvData->width; yc++)
		{
			if (!scaling)
				cvImage[xc * cvData->widthStep + yc * cvData->nChannels] = (char)(this->idata[yc][xc]);
			else
				cvImage[xc * cvData->widthStep + yc * cvData->nChannels] = (char)(tmp.idata[yc][xc]);
		}
		cvNamedWindow(windowName, window);
		if (Max(this->width, this->height) < 125)
			cvResizeWindow(windowName, 125, 125);
		else
			cvResizeWindow(windowName, this->width, this->height);
		cvShowImage(windowName, cvData);

		if (!debug)
			cvWaitKey(1);
		else
		{
			cvSetMouseCallback(windowName, MouseHandler, this);
			cvWaitKey(0);
		}
		cvReleaseImage(&cvData);
		tmp.FreeMem();
	}
	else
		ErrorMessage("[no data to show]", false);
}

#endif