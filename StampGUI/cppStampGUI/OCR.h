#pragma once

#include "KGeneral\KGeneral.h"
//#include "KGeneral.h"
#include "KImage.h"
#include "KDetection.h"
#include "KStatistic.h"
#include "KEdge.h"
#include "KMorphology.h"
#include "KMorphologyElement.h"
#include "KGeometric.h"
#include "KAdjustment.h"
#include "KStatistic.h"
#include "KInterface_OpenCV.h"
#include "KDraw.h"

#include "KFeature.h"
#include "KFunction.h"
#include <boost\filesystem.hpp>

//class MYcppGui {
//public:
//	MYcppGui();
//	~MYcppGui();
//
//	int myCppLoadAndShowRGB(char* url, int maxWidth, int maxHeight, int nPicPerRow);
//
//private:
//	bool win0; // status of an OpenCV window
//};

struct distidx{
	double dist;
	int idx;
};

struct sort_distidx {
	bool operator()(distidx const &a, distidx const &b) {
		return a.dist < b.dist;
	}
};

class ClassStampOCR
{
public:

	ClassStampOCR();
	~ClassStampOCR();

	int dark_val;
	int dark_thr;
	int box_size;
	int num_char;

	int feature_sampling_rate;
	int training_size;
	int training_char_size;

	int template_width;
	int template_height;

	void CopyParameter(int dark_val, int dark_thr, int box_size, int num_char, int feature_sampling_rate, int training_size, int training_char_size, int template_width, int template_height);
	void GetFilePath(string path, string file_ext, vector<string> *filepath);
	void GetCharacterSize(vector<string> train_set, int *max_char_width, int *max_char_height);
	void GetActualSerialNumber(string sn_path, vector<vector<int>> *sn_val, vector<string> *sn_string);
	void GenerateCharacterTemplate(vector<string> train_set, vector<vector<int>> sn_val, int template_char_width, int template_char_height, string sav_path, vector<vector<double>> *feature_template);
	
	void DetectCharPosition(KGImage<double> &in_img, vector<KCoor<int>> *topleft, vector<KCoor<int>> *bottomright);
	void DetectCharPosition(string filepath, vector<KCoor<int>> *topleft, vector<KCoor<int>> *bottomright);

	void GetTemplate(string template_path, int *template_w, int *template_h, vector<vector<double>> *feature_template);
	void TemplateDownsampling(vector<vector<double>> *feature_template);
	void OCRProcess(string filepath, vector<vector<double>> *feature_template, vector<KCoor<int>> *topleft, vector<KCoor<int>> *bottomright, string *serialnumber, vector<double> *char_score, double *ocr_score);

	vector<string> filepath;
	vector<vector<int>> sn_val;
	vector<string> sn_str;

	vector<vector<double>> stored_template;
};