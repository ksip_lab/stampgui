// ------------------------------------------------ //
// contain:		class Queue
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KQueue_H
#define _KQueue_H

#include "KHeader.h"
#include "KList.h"

// ========== First In - First Out ========== //
template <class T> class KQueue :public KList1D<T>
{
public:

	KQueue()	{	}
	~KQueue()	{ 	}

	unsigned int size()
	{
		return this->KList1D::size();
	}
	void Enqueue(T data)
	{
		this->insert(this->begin(), data);
	}
	T Dequeue()
	{
		if (!this->empty())
		{
			T tmp = this->back();
			this->pop_back();
			return tmp;
		}
		else
		{
			cout << "[Out of Queue Size]" << endl;
			system("pause");
			exit(0);
			T tmp;
			return tmp;
		}
	}
};

#endif