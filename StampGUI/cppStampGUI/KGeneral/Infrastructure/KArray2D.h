// ------------------------------------------------ //
// contain:		class 2D Array
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KArray2D_H
#define _KArray2D_H

#include "KHeader.h"
#include "KMemory.h"
#include "KBasic.h"

template <class T> class KArray2D
{
public:

	KArray2D()						{ isAlloc = false; }
	KArray2D(int Width, int Height)	{ isAlloc = false; Allocate(Width, Height); }
	~KArray2D()						{ FreeMem(); }

	bool	isAlloc;
	int		width;
	int		height;
	T		**idata;

	void Allocate(int Width, int Height)
	{
		if (Width > 0 && Height > 0)
		{
			if (!isAlloc)
			{
				isAlloc = true;
				idata = Alloc<T>(Width, Height);
				width = Width;
				height = Height;
			}
			else
				Resize(Width, Height);
		}
		else
			ErrorMessage("Error KArray2D [Allocate] : Width & Height must be more than 0");
	}
	void Resize(int Width, int Height)
	{
		if (Width > 0 && Height > 0)
		{
			if (!isAlloc)
				Allocate(Width, Height);
			else
			{
				if (width != Width || height != Height)
				{
					FreeMem();
					Allocate(Width, Height);
				}
			}
		}
		else
			ErrorMessage("Error KArray2D [Resize] : Width & Height must be more than 0");
	}
	void FreeMem()
	{
		if (isAlloc)
		{
			Free<T>(idata, width, height);
			width = 0;
			height = 0;
			isAlloc = false;
		}
	}
	bool IsSameSize(int Width, int Height)
	{
		if (width == Width&&height == Height)	return true;
		else									return false;
	}
	bool IsSameSize(KArray2D<T> *Array2D)
	{
		if (width == Array2D->width && height == Array2D->height)	return true;
		else														return false;
	}
	bool IsInBound(int i, int j)
	{
		if (i >= 0 && i<width&&j >= 0 && j<height)	return true;
		else								return false;
	}
	template <typename T2>
	KArray2D<T>& operator = (const T2 &value)
	{
		if (isAlloc == false)
			ErrorMessage("Error [KArray2D] : must be allocated before add value");

		for (int y = 0; y < this->height; y++)
		for (int x = 0; x < this->width; x++)
			idata[x][y] = (T)value;
		return *this;
	}
	template <typename T2>
	KArray2D<T>&operator=(const KArray2D<T2> &prototype)
	{
		if (isAlloc == false)
			Allocate(prototype.width, prototype.height);
		else
		if (IsSameSize(prototype.width, prototype.height) == false)
			Resize(prototype.width, prototype.height);
		for (int y = 0; y < prototype.height; y++)
		for (int x = 0; x < prototype.width; x++)
			idata[x][y] = (T)prototype.idata[x][y];
		return *this;
	}
	KArray2D&operator=(const KArray2D<T> &prototype)
	{
		if (isAlloc == false)
			Allocate(prototype.width, prototype.height);
		else
		if (IsSameSize(prototype.width, prototype.height) == false)
			Resize(prototype.width, prototype.height);
		for (int y = 0; y<prototype.height; y++)
		for (int x = 0; x<prototype.width; x++)
			idata[x][y] = (T)prototype.idata[x][y];
		return *this;
	}
	T &operator() (int i, int j)
	{
		if (IsInBound(i, j))
			return idata[i][j];
		else
		{
			cout << "[attempt to access out of bound]" << endl;
			system("pause");
			return idata[0][0];
		}
	}
	friend istream& operator >> (istream& in, KArray2D<T>& obj)
	{
		int w, h;
		in >> w;
		in >> h;
		if (obj.width != w || obj.height != h)
			obj.Resize(w, h);
		for (int y = 0; y<obj.height; y++)
		{
			for (int x = 0; x<obj.width; x++)
			{
				in >> obj.idata[x][y];
			}
		}
		return in;
	}
	friend ostream& operator << (ostream& out, const KArray2D<T>& obj)
	{
		out << obj.width << endl;
		out << obj.height << endl << endl;
		for (int y = 0; y<obj.height; y++)
		{
			for (int x = 0; x<obj.width; x++)
			{
				out << obj.idata[x][y] << ' ';
			}
			out << endl;
		}
		return out;
	}
};

#endif