// ------------------------------------------------ //
// contain:		[0]	class 1D linked list
//				[1] class 2D linked list
//				[2] class 3D linked list
//				[3] class 4D linked list
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KList_H
#define _KList_H

#include "KHeader.h"

#include "KList1D.h"
#include "KList2D.h"
#include "KList3D.h"
#include "KList4D.h"

#endif