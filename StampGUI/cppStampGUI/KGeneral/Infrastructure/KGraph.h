// ------------------------------------------------ //
// contain:		General Graph Structure
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KGraph_H
#define _KGraph_H

#include "KHeader.h"
#include "KList1D.h"
#include "KList2D.h"

template <class tVertex, class tEdge> class KGraph
{
private:
	bool	isAlloc;

public:
	long	cumVertexLabel;
	long	cumEdgeLabel;

	class KVertex
	{
	public:
		KVertex()	{}
		~KVertex()	{}

		long	vLabel;
		tVertex	vData;

		friend istream& operator >> (istream& in, KVertex &obj)
		{
			char tmp;
			in >> tmp;
			in >> obj.vLabel;
			in >> tmp;
			in >> obj.vData;
			in >> tmp;
			return in;
		}
		friend ostream& operator << (ostream& out, const KVertex &obj)
		{
			out << '[' << obj.vLabel << ',' << obj.vData << ']';
			return out;
		}
	};
	class KEdge
	{
	public:
		KEdge()		{}
		~KEdge()	{}

		long	vertexStart;
		long	vertexStop;

		long	eLabel;
		tEdge	eData;

		friend istream& operator >> (istream& in, KEdge &obj)
		{
			char tmp;
			in >> tmp;
			in >> obj.eLabel;
			in >> tmp;
			in >> obj.eData;
			in >> tmp;
			in >> obj.vertexStart;
			in >> tmp;
			in >> obj.vertexStop;
			in >> tmp;
			return in;
		}
		friend ostream& operator << (ostream& out, const KEdge &obj)
		{
			out << '[' << obj.eLabel << ',' << obj.eData << ',' << obj.vertexStart << "," << obj.vertexStop << "]";
			return out;
		}
	};
	class KAdjacent
	{
	public:
		KAdjacent()		{}
		~KAdjacent()	{}

		long vertexLabel;
		long edgeLabel;

		friend istream& operator >> (istream& in, KAdjacent &obj)
		{
			char tmp;
			in >> tmp;
			in >> obj.vertexLabel;
			in >> tmp;
			in >> obj.edgeLabel;
			in >> tmp;
			return in;
		}
		friend ostream& operator << (ostream& out, const KAdjacent &obj)
		{
			out << '[' << obj.vertexLabel << ',' << obj.edgeLabel << ']';
			return out;
		}
	};

	KGraph()		{ isAlloc = false;	cumVertexLabel = cumEdgeLabel = 0; }
	~KGraph()		{ FreeMem(); }

	KList1D<KVertex>	listVertex;
	KList1D<KEdge>		listEdge;
	KList2D<KAdjacent>	adjMAP;

	void FreeMem();

	void GetVertexData(long vLabel, tVertex *vData);
	void GetEdgeData(long eLabel, tEdge *eData = NULL, long *vertexStartLabel = NULL, long *vertexStopLabel = NULL);

	long AddVertex(tVertex vertexData, long manualVertexLabel = NULL, bool isManualSet = false);
	long AddEdge(long vLabelStart, long vLabelStop, tEdge edgeData, long manualEdgeLabel = NULL, bool isManualSet = false);

	bool RemoveVertex(long vLabel);
	bool RemoveEdge(long eLabel);

	void ShowAdjacentVertex();
	void ShowAdjacentEdge();
	void ShowConnectedVertex();

	friend istream& operator >> (istream& in, KGraph<tVertex, tEdge> &obj)
	{
		char tmp;
		in >> tmp;
		in >> obj.isAlloc;
		in >> tmp;
		in >> obj.cumVertexLabel;
		in >> tmp;
		in >> obj.cumEdgeLabel;
		in >> tmp;
		in >> obj.listVertex;
		in >> obj.listEdge;
		in >> obj.adjMAP;
		return in;
	}
	friend ostream& operator << (ostream& out, KGraph<tVertex, tEdge> &obj)
	{
		out << '[' << obj.isAlloc << ',' << obj.cumVertexLabel << ',' << obj.cumEdgeLabel << "]";
		out << obj.listVertex;
		out << obj.listEdge;
		out << obj.adjMAP;
		return out;
	}
};

template <class tVertex, class tEdge> void KGraph<tVertex, tEdge>::FreeMem()
{
	if (isAlloc)
	{
		listVertex.FreeMem();
		listEdge.FreeMem();
		adjMAP.FreeMem();
		isAlloc = false;
	}
}
template <class tVertex, class tEdge> void KGraph<tVertex, tEdge>::GetVertexData(long vLabel, tVertex *vData)
{
	if (vLabel<0 || vLabel>listVertex.size())
	{
		cout << "out of vertex list" << endl;
		system("pause");
		exit(0);
	}
	for (KList1D<KGraph<tVertex,tEdge>::KVertex>::iterator it = listVertex.begin(); it != listVertex.end(); it++)
	{
		if ((*it).vLabel == vLabel)
		{
			*vData = (*it).vData;
			break;
		}
	}
}
template <class tVertex, class tEdge> void KGraph<tVertex, tEdge>::GetEdgeData(long eLabel, tEdge *eData, long *vertexStartLabel, long *vertexStopLabel)
{
	if (eLabel<0 || eLabel>(long)listEdge.size())
	{
		cout << "out of edge list" << endl;
		system("pause");
		exit(0);
	}
	for (KList1D<KGraph<tVertex, tEdge>::KEdge>::iterator it = listEdge.begin(); it != listEdge.end(); it++)
	{
		if ((*it).eLabel == eLabel)
		{
			if (eData != NULL)
				*eData = (*it).eData;
			if (vertexStartLabel != NULL)
				*vertexStartLabel = (*it).vertexStart;
			if (vertexStopLabel != NULL)
				*vertexStopLabel = (*it).vertexStop;
			break;
		}
	}
}
template <class tVertex, class tEdge> long KGraph<tVertex, tEdge>::AddVertex(tVertex vertexData, long manualVertexLabel, bool isManualSet)
{
	KGraph<tVertex, tEdge>::KVertex tmpVertex;
	if (!isManualSet)
	{
		tmpVertex.vLabel = (long)(cumVertexLabel);
		cumVertexLabel++;
	}
	else
		tmpVertex.vLabel = manualVertexLabel;
	tmpVertex.vData = vertexData;
	listVertex.push_back(tmpVertex);

	KList1D<KAdjacent>	tmpList;
	KAdjacent			tmpNode;

	tmpNode.vertexLabel = tmpVertex.vLabel;
	tmpNode.edgeLabel = -1;

	tmpList.push_back(tmpNode);
	adjMAP.push_back(tmpList);

	tmpList.FreeMem();

	if (!isAlloc)
		isAlloc = true;

	return (long)tmpNode.vertexLabel;
}
template <class tVertex, class tEdge> long KGraph<tVertex, tEdge>::AddEdge(long vLabelStart, long vLabelStop, tEdge edgeData, long manualEdgeLabel, bool isManualSet)
{
	if (isAlloc)
	{
		if (vLabelStart >= 0 && vLabelStart <= (long)adjMAP.size() && vLabelStop >= 0 && vLabelStop <= (long)adjMAP.size())
		{
			KGraph<tVertex, tEdge>::KEdge tmp;
			if (!isManualSet)
			{
				tmp.eLabel = (long)(cumEdgeLabel);
				cumEdgeLabel++;
			}
			else
				tmp.eLabel = manualEdgeLabel;
			tmp.eData = edgeData;
			tmp.vertexStart = vLabelStart;
			tmp.vertexStop = vLabelStop;

			listEdge.push_back(tmp);

			KAdjacent	tmpNode;
			tmpNode.vertexLabel = tmp.vertexStop;
			tmpNode.edgeLabel = tmp.eLabel;
			adjMAP.at(vLabelStart).push_back(tmpNode);

			return (long)tmpNode.edgeLabel;
		}
		else
			return (long)(-1);
	}
	else
	{
		cout << "out of graph" << endl;
		system("pause");
		exit(0);
		return (long)0;
	}
}
template <class tVertex, class tEdge> bool KGraph<tVertex, tEdge>::RemoveVertex(long vLabel)
{
	KList1D<long>	removeEdgeList;
	long			edgeLabel, startV, stopV;
	bool			isFound;
	isFound = false;
	for (KList1D<KGraph<tVertex, tEdge>::KEdge>::iterator it = listEdge.begin(); it != listEdge.end(); it++)
	{
		edgeLabel = (*it).eLabel;
		this->GetEdgeData(edgeLabel, NULL, &startV, &stopV);
		if (startV == vLabel || stopV == vLabel)
		{
			if (!isFound)
				isFound = true;
			removeEdgeList.push_back(edgeLabel);
		}
	}
	for (unsigned int z = 0; z < removeEdgeList.size(); z++)
		RemoveEdge(removeEdgeList[z]);
	removeEdgeList.FreeMem();
	return isFound;
}
template <class tVertex, class tEdge> bool KGraph<tVertex, tEdge>::RemoveEdge(long eLabel)
{
	long vStart_Remove, vStop_Remove;
	this->GetEdgeData(eLabel, NULL, &vStart_Remove, &vStop_Remove);
	for (unsigned int z = 0; z < this->adjMAP.at(vStart_Remove).size(); z++)
	{
		if (this->adjMAP.at(vStart_Remove).at(z).vertexLabel == vStop_Remove)
		{
			this->adjMAP.at(vStart_Remove).Remove(z);
			this->listEdge.at(eLabel).vertexStart = -1;
			this->listEdge.at(eLabel).vertexStop = -1;
			return true;
		}
	}
	return false;
}
template <class tVertex, class tEdge> void KGraph<tVertex, tEdge>::ShowAdjacentVertex()
{
	cout << "============= DISPLAY ADJACENT VERTEX LIST =============" << endl;
	for (unsigned int y = 0; y<adjMAP.size(); y++)
	{
		for (unsigned int x = 0; x<adjMAP.at(y).size(); x++)
		{
			cout << "[" << adjMAP.at(y).at(x).vertexLabel << "]->";
		}
		cout << endl;
	}
	cout << "========================================================" << endl << endl;
}
template <class tVertex, class tEdge> void KGraph<tVertex, tEdge>::ShowAdjacentEdge()
{
	cout << "============== DISPLAY ADJACENT EDGE LIST ==============" << endl;
	for (unsigned int y = 0; y<adjMAP.size(); y++)
	{
		for (unsigned int x = 0; x<adjMAP.at(y).size(); x++)
		{
			cout << "[" << adjMAP.at(y).at(x).edgeLabel << "]->";
		}
		cout << endl;
	}
	cout << "========================================================" << endl << endl;
}
template <class tVertex, class tEdge> void KGraph<tVertex, tEdge>::ShowConnectedVertex()
{
	long	showEdgeLabel;
	long	showStartVertexLabel, showStopVertexLabel;

	cout << "============= DISPLAY CONNECTED VERTEX ONLY ============" << endl;
	for (KList1D<KGraph<tVertex, tEdge>::KEdge>::iterator it = listEdge.begin(); it != listEdge.end(); it++)
	{
		showEdgeLabel = (*it).eLabel;
		this->GetEdgeData(showEdgeLabel, NULL, &showStartVertexLabel, &showStopVertexLabel);
		if (showStartVertexLabel != -1 || showStopVertexLabel != -1)
			cout << "V[" << showStartVertexLabel << "]-----E<" << showEdgeLabel << ">----->V[" << showStopVertexLabel << "]" << endl;
	}
	cout << "========================================================" << endl << endl;
}

#endif