// ------------------------------------------------ //
// contain:		class 1D linked list
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KList1D_H
#define _KList1D_H

#include "KHeader.h"

template <class T> class KList1D :public vector<T>
{
public:

	KList1D()	{	}
	~KList1D()	{ 	}

	unsigned int size()
	{
		return (unsigned int)this->vector::size();
	}
	void Insert(UINT x, T data)
	{
		if (IsInBound(x))
			this->emplace(this->begin() + x, data);
		else
			this->push_back(data);
	}
	bool Remove(UINT x)
	{
		if (IsInBound(x))
		{
			this->erase(this->begin() + x);
			return true;
		}
		else
			return false;
	}
	void FreeMem()
	{
		this->~KList1D();
	}
	bool IsSameSize(UINT Length)
	{
		if (this->size() == Length)	return true;
		else						return false;
	}
	bool IsSameSize(vector<T> *List1D)
	{
		if (this->size() == List1D->size())	return true;
		else								return false;
	}
	bool IsInBound(UINT x)
	{
		if (x >= 0 && x<this->size())	return true;
		else							return false;
	}
	T &operator() (UINT x)
	{
		if (IsInBound(x))
			return this->at(x);
		else
		{
			cout << "[attempt to access out of bound]" << endl;
			system("pause");
			exit(0);
			return this->at(0);
		}
	}
	friend istream& operator >> (istream& in, KList1D<T> &obj)
	{
		T				n;
		char			tmp;
		unsigned int	ListSize;
		in >> tmp;
		in >> ListSize;
		in >> tmp;
		for (unsigned int z = 0; z<ListSize; z++)
		{
			in >> n;
			obj.push_back(n);
		}
		return in;
	}
	friend ostream& operator << (ostream& out, KList1D<T> &obj)
	{
		KList1D<T>::iterator listIterator;
		out << "[" << obj.size() << "]" << endl;
		for (listIterator = obj.begin(); listIterator != obj.end(); ++listIterator)
			out << *listIterator << endl;
		return out;
	}
};

#endif