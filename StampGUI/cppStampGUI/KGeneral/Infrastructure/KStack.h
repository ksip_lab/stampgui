// ------------------------------------------------ //
// contain:		class Stack
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KStack_H
#define _KStack_H

#include "KHeader.h"
#include "KList.h"

// ========== First In - Last Out ========== //
template <class T> class KStack :public KList1D<T>
{
public:

	KStack()	{	}
	~KStack()	{	}

	unsigned int size()
	{
		return this->KList1D::size();
	}
	void Push(T data)
	{
		this->push_back(data);
	}
	T Pop()
	{
		if (!this->empty())
		{
			T tmp = this->back();
			this->pop_back();
			return tmp;
		}
		else
		{
			cout << "[Out of Stack Size]" << endl;
			system("pause");
			exit(0);
			T tmp;
			return tmp;
		}
	}
};

#endif