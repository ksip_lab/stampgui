// ------------------------------------------------ //
// contain:		class 3D Array
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KArray3D_H
#define _KArray3D_H

#include "KHeader.h"
#include "KMemory.h"
#include "KBasic.h"

template <class T> class KArray3D
{
public:

	KArray3D()									{ isAlloc = false; }
	KArray3D(int Depth, int Width, int Height)	{ isAlloc = false; Allocate(Depth, Width, Height); }
	~KArray3D()									{ FreeMem(); }

	bool	isAlloc;
	int		width;
	int		height;
	int		depth;
	T		***idata;

	void Allocate(int Depth, int Width, int Height)
	{
		if (Depth > 0 && Width > 0 && Height > 0)
		{
			if (!isAlloc)
			{
				isAlloc = true;
				idata = Alloc<T>(Depth, Width, Height);
				width = Width;
				height = Height;
				depth = Depth;
			}
			else
				Resize(Depth, Width, Height);
		}
		else
			ErrorMessage("Error KArray3D [Allocate] : Depth & Width & Height must be more than 0");
	}
	void Resize(int Width, int Height)
	{
		if (Width > 0 && Height > 0)
		{
			if (!isAlloc)
				Allocate(3, Width, Height);
			else
			{
				if (width != Width || height != Height)
				{
					FreeMem();
					Allocate(3, Width, Height);
				}
			}
		}
		else
			ErrorMessage("Error KArray3D [Resize] : Width & Height must be more than 0");
	}
	void Resize(int Depth, int Width, int Height)
	{
		if (Depth > 0 && Width > 0 && Height > 0)
		{
			if (!isAlloc)
				Allocate(Depth, Width, Height);
			else
			{
				if (width != Width || height != Height || depth != Depth)
				{
					FreeMem();
					Allocate(Depth, Width, Height);
				}
			}
		}
		else
			ErrorMessage("Error KArray3D [Resize] : Depth & Width & Height must be more than 0");
	}
	void FreeMem()
	{
		if (isAlloc)
		{
			Free<T>(idata, depth, width, height);
			width = 0;
			height = 0;
			depth = 0;
			isAlloc = false;
		}
	}
	bool IsSameSize(int Width, int Height)
	{
		if (width == Width&&height == Height)	return true;
		else									return false;
	}
	bool IsSameSize(int Depth, int Width, int Height)
	{
		if (width == Width&&height == Height&&depth == Depth)	return true;
		else													return false;
	}
	bool IsSameSize(KArray3D<T> *Array3D)
	{
		if (width == Array3D->width && height == Array3D->height && depth == Array3D->depth)	return true;
		else																				return false;
	}
	bool IsInBound(int x, int y)
	{
		if (x >= 0 && x<width&&y >= 0 && y<height)	return true;
		else								return false;
	}
	bool IsInBound(int z, int x, int y)
	{
		if (x >= 0 && x<width&&y >= 0 && y<height&&z >= 0 && z<depth)	return true;
		else												return false;
	}
	template <typename T2>
	KArray3D<T>& operator = (const T2 &value)
	{
		if (isAlloc == false)
			ErrorMessage("Error [KArray3D] : must be allocated before add value");

		for (int z = 0; z < this->depth; z++)
		for (int y = 0; y < this->height; y++)
		for (int x = 0; x < this->width; x++)
			idata[z][x][y] = (T)value;
		return *this;
	}
	template <typename T2>
	KArray3D<T>&operator=(const KArray3D<T2> &prototype)
	{
		if (isAlloc == false)
			Allocate(prototype.depth, prototype.width, prototype.height);
		else
		if (IsSameSize(prototype.width, prototype.height, prototype.depth) == false)
			Resize(prototype.depth, prototype.width, prototype.height);
		for (int z = 0; z<prototype.depth; z++)
		for (int y = 0; y<prototype.height; y++)
		for (int x = 0; x<prototype.width; x++)
			idata[z][x][y] = (T)prototype.idata[z][x][y];
		return *this;
	}
	KArray3D&operator=(const KArray3D<T> &prototype)
	{
		if (isAlloc == false)
			Allocate(prototype.depth, prototype.width, prototype.height);
		else
		if (IsSameSize(prototype.width, prototype.height, prototype.depth) == false)
			Resize(prototype.depth, prototype.width, prototype.height);
		for (int z = 0; z<prototype.depth; z++)
		for (int y = 0; y<prototype.height; y++)
		for (int x = 0; x<prototype.width; x++)
			idata[z][x][y] = (T)prototype.idata[z][x][y];
		return *this;
	}
	T &operator() (int z, int x, int y)
	{
		if (IsInBound(x, y))
			return idata[z][x][y];
		else
		{
			cout << "[attempt to access out of bound]" << endl;
			system("pause");
			return idata[0][0][0];
		}
	}
	friend istream& operator >> (istream& in, KArray3D<T>& obj)
	{
		int w, h, d;
		in >> d;
		in >> w;
		in >> h;
		if (obj.width != w || obj.height != h || obj.depth != d)
			obj.Resize(d, w, h);
		for (int z = 0; z<obj.depth; z++)
		{
			for (int y = 0; y<obj.height; y++)
			{
				for (int x = 0; x<obj.width; x++)
				{
					in >> obj.idata[z][x][y];
				}
			}
		}
		return in;
	}
	friend ostream& operator << (ostream& out, const KArray3D<T>& obj)
	{
		out << obj.depth << endl;
		out << obj.width << endl;
		out << obj.height << endl << endl;
		for (int z = 0; z<obj.depth; z++)
		{
			for (int y = 0; y<obj.height; y++)
			{
				for (int x = 0; x<obj.width; x++)
				{
					out << obj.idata[z][x][y] << ' ';
				}
				out << endl;
			}
			out << endl;
		}
		return out;
	}
};

#endif