// ------------------------------------------------ //
// contain:		class 1D Array
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KArray1D_H
#define _KArray1D_H

#include "KHeader.h"
#include "KMemory.h"
#include "KBasic.h"

template <class T> class KArray1D
{
public:

	KArray1D()				{ isAlloc = false; }
	KArray1D(int Length)	{ isAlloc = false;	Allocate(Length); }
	~KArray1D()				{ FreeMem(); }

	bool	isAlloc;
	int		length;
	T		*idata;

	void Allocate(int Length)
	{
		if (Length > 0)
		{
			if (!isAlloc)
			{
				isAlloc = true;
				idata = Alloc<T>(Length);
				length = Length;
			}
			else
				Resize(Length);
		}
		else
			ErrorMessage("Error KArray1D [Allocate] : Length must be more than 0");
	}
	void Resize(int Length)
	{
		if (Length > 0)
		{
			if (!isAlloc)
				Allocate(Length);
			else
			{
				if (length != Length)
				{
					FreeMem();
					Allocate(Length);
				}
			}
		}
		else
			ErrorMessage("Error KArray1D [Resize] : Length must be more than 0");
	}
	void FreeMem()
	{
		if (isAlloc)
		{
			Free<T>(idata, length);
			length = 0;
			isAlloc = false;
		}
	}
	bool IsSameSize(int Length)
	{
		if (length == Length)	return true;
		else					return false;
	}
	bool IsSameSize(KArray1D<T> *Array1D)
	{
		if (length == Array1D->length)	return true;
		else							return false;
	}
	bool IsInBound(int i)
	{
		if (i >= 0 && i<length)	return true;
		else					return false;
	}
	template <typename T2> 
	KArray1D<T>& operator = (const T2 &value)
	{
		if (isAlloc == false)
			ErrorMessage("Error [KArray1D] : must be allocated before add value");
		
		for (int n = 0; n < this->length; n++)
			idata[n] = (T)value;
		return *this;
	}
	template <typename T2>
	KArray1D<T>&operator=(const KArray1D<T2> &prototype)
	{
		if (isAlloc == false)
			Allocate(prototype.length);
		else
		if (IsSameSize(prototype.length) == false)
			Resize(prototype.length);
		for (int n = 0; n < prototype.length; n++)
			idata[n] = (T)prototype.idata[n];
		return *this;
	}
	KArray1D&operator=(const KArray1D<T> &prototype)
	{
		if (isAlloc == false)
			Allocate(prototype.length);
		else
		if (IsSameSize(prototype.length) == false)
			Resize(prototype.length);
		for (int n = 0; n<prototype.length; n++)
			idata[n] = (T)prototype.idata[n];
		return *this;
	}
	T & operator()(int i)
	{
		if (IsInBound(i))
			return idata[i];
		else
		{
			cout << "[attempt to access out of bound]" << endl;
			system("pause");
			return idata[0];
		}
	}
	friend istream& operator >> (istream& in, KArray1D<T>& obj)
	{
		int l;
		in >> l;
		if (obj.length != l)
			obj.Resize(l);
		for (int z = 0; z<obj.length; z++)
			in >> obj.idata[z];
		return in;
	}
	friend ostream& operator << (ostream& out, const KArray1D<T>& obj)
	{
		out << obj.length << endl << endl;
		for (int z = 0; z<obj.length; z++)
			out << obj.idata[z] << ' ';
		return out;
	}
};

#endif