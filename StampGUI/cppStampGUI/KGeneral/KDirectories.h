// ------------------------------------------------ //
// contain:		Directories management
//
// developer:	Krisada Phromsuthirak
// ------------------------------------------------ //

#ifndef _KDirectories_H
#define _KDirectories_H

#include "KHeader.h"
#include "KBasic.h"
#include "KString.h"

bool CreateFolder(char *foldername);
bool CreateFolder(string foldername);
bool RemoveFolder(char *foldername);
bool RemoveFolder(string foldername);
bool RemoveFile(char *filename, bool debug = true);
bool RemoveFile(string filename, bool debug = true);
bool Rename(char *oldFilename, char *newFilename);
bool Rename(string oldFilename, char *newFilename);
bool Rename(char *oldFilename, string newFilename);
bool Rename(string oldFilename, string newFilename);
bool IsFileExist(const char *filename);
bool IsFileExist(string filename);
bool GetFileList(const char *searchkey, vector<string> &list);
bool GetFileList(string searchkey, vector<string> &list);
bool GetFolderList(const char *searchkey, vector<string> &list);
bool GetFolderList(string searchkey, vector<string> &list);
void SortNumeric(vector<string> &list);
string GetFileName(string path);
string GetFileName(const char *path);
string GetFileExtension(string path);
string GetFileExtension(const char *path);
string GetFolderPath(string path);
string GetFolderPath(const char *path);
string GetFolderPathFormat(string path);
string GetFolderPathFormat(const char *path);
string ReplaceExtension(string path, string extension);
string ReplaceExtension(const char *path, const char *extension);

#endif