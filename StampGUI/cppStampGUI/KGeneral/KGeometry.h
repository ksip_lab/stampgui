// ------------------------------------------------ //
// contain:		Orientation & Direction Function
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KGeometry_H
#define _KGeometry_H

#include "KHeader.h"
#include "KBasic.h"
#include "KCoor.h"

//====================================================================//
// -------------------- General Coordinate Tools -------------------- //
//
// ***start 0 angle from x-axis (3 o'clock direction) with Clockwise(+)
// Angular system: OrientationFull	[-PI,PI]
// Angular system: OrientationHalf	[0,PI]
// Angular system: Direction		[0,2PI]
//
// [0]	FindOrientationFull			system:[OrientationFull],	output range:[-PI,PI]
// [1]	FindOrientationHalf			system:[OrientationHalf],	output range:[0,PI]
// [2]	FindDirection				system:[Direction],			output range:[0,2PI]
//
// [3]	FindDiffOrientationFull		system:[OrientationFull],	output range:[-PI,PI]
// [4]	FindDiffOrientationFullAbs	system:[OrientationFull],	output range:[0,PI]
// [5]	FindDiffOrientationHalf		system:[OrientationHalf],	output range:[-PI/2,PI/2]
// [6]	FindDiffOrientationHalfAbs	system:[OrientationHalf],	output range:[0,PI/2]
// [7]	FindDiffDirection			system:[Direction],			output range:[-PI,PI]
// [8]	FindDiffDirectionAbs		system:[Direction],			output range:[0,PI]
//
// [9]	FindDistance
// [10]	FindCenterPoint
// [11] FindCrossingPoint
//
//====================================================================//

template <typename T, typename T2> double FindOrientationFull(KCoor<T> pst, KCoor<T2> pend);
template <typename T, typename T2> double FindOrientationFull(T stx, T sty, T2 endx, T2 endy);
template <typename T, typename T2> double FindOrientationHalf(KCoor<T> pst, KCoor<T2> pend);
template <typename T, typename T2> double FindOrientationHalf(T stx, T sty, T2 endx, T2 endy);
template <typename T, typename T2> double FindDirection(KCoor<T> pst, KCoor<T2> pend);
template <typename T, typename T2> double FindDirection(T stx, T sty, T2 endx, T2 endy);

template <typename T, typename T2> double FindDiffOrientationFull(T startOrientation, T2 stopOrientation);
template <typename T, typename T2> double FindDiffOrientationFullAbs(T startOrientation, T2 stopOrientation);
template <typename T, typename T2> double FindDiffOrientationHalf(T startOrientation, T2 stopOrientation);
template <typename T, typename T2> double FindDiffOrientationHalfAbs(T startOrientation, T2 stopOrientation);
template <typename T, typename T2> double FindDiffDirection(T startDirection, T2 stopDirection);
template <typename T, typename T2> double FindDiffDirectionAbs(T startDirection, T2 stopDirection);

template <typename T, typename T2> double FindDistance(KCoor<T> pst, KCoor<T2> pend);
template <typename T, typename T2> double FindDistance(T stx, T sty, T2 endx, T2 endy);
template <typename T> KCoor<T> FindCenterPoint(KCoor<T> pst, KCoor<T> pend);
template <typename T> KCoor<T> FindCenterPoint(T stx, T sty, T endx, T endy);
template <typename T1, typename T2, typename T3> bool FindCrossingPoint(KCoor<T1> Pos1, T2 OF1, KCoor<T1> Pos2, T2 OF2, KCoor<T3> &crossPos);
template <typename T1, typename T2, typename T3> bool FindCrossingPoint(KCoor<T1> Pos1, T2 OF1, KCoor<T1> Pos2, T2 OF2, T3 &crossPosX, T3 &crossPosY);
template <typename T1, typename T2, typename T3> bool FindCrossingPoint(T1 x1, T1 y1, T2 OF1, T1 x2, T1 y2, T2 OF2, KCoor<T3> &crossPos);
template <typename T1, typename T2, typename T3> bool FindCrossingPoint(T1 x1, T1 y1, T2 OF1, T1 x2, T1 y2, T2 OF2, T3 &crossPosX, T3 &crossPosY);

template <typename T, typename T2> double FindOrientationFull(KCoor<T> pst, KCoor<T2> pend)
{
	return atan2((double)((double)pend.y - (double)pst.y), (double)((double)pend.x - (double)pst.x));
}
template <typename T, typename T2> double FindOrientationFull(T stx, T sty, T2 endx, T2 endy)
{
	return atan2((double)((double)endy - (double)sty), (double)((double)endx - (double)stx));
}
template <typename T, typename T2> double FindOrientationHalf(KCoor<T> pst, KCoor<T2> pend)
{
	double tmp = atan2((double)((double)pend.y - (double)pst.y), (double)((double)pend.x - (double)pst.x));
	if (tmp<0)	return (tmp + PI);
	else		return tmp;
}
template <typename T, typename T2> double FindOrientationHalf(T stx, T sty, T2 endx, T2 endy)
{
	double tmp = atan2((double)((double)endy - (double)sty), (double)((double)endx - (double)stx));
	if (tmp<0)	return (tmp + PI);
	else		return tmp;
}
template <typename T, typename T2> double FindDirection(KCoor<T> pst, KCoor<T2> pend)
{
	double tmp = atan2((double)((double)pend.y - (double)pst.y), (double)((double)pend.x - (double)pst.x));
	if (tmp<0)	return (tmp + (double)(2.0*PI));
	else		return tmp;
}
template <typename T, typename T2> double FindDirection(T stx, T sty, T2 endx, T2 endy)
{
	double tmp = atan2((double)((double)endy - (double)sty), (double)((double)endx - (double)stx));
	if (tmp<0)	return (tmp + (double)(2.0*PI));
	else		return tmp;
}
template <typename T, typename T2> double FindDiffOrientationFull(T startOrientation, T2 stopOrientation)
{
	if ((double)startOrientation != (double)stopOrientation)
	{
		double diffOrientation = (double)((double)stopOrientation - (double)startOrientation);
		if (diffOrientation<(double)(-PI))
			diffOrientation += (double)(2 * PI);
		else if (diffOrientation>(double)(PI))
			diffOrientation -= (double)(2 * PI);
		return diffOrientation;
	}
	else
		return (double)0;
}
template <typename T, typename T2> double FindDiffOrientationFullAbs(T startOrientation, T2 stopOrientation)
{
	if ((double)startOrientation != (double)stopOrientation)
	{
		double diffOrientation = (double)((double)stopOrientation - (double)startOrientation);
		if (diffOrientation<(double)(-PI))
			diffOrientation += (double)(2 * PI);
		else if (diffOrientation>(double)(PI))
			diffOrientation -= (double)(2 * PI);
		return abs(diffOrientation);
	}
	else
		return (float)0;
}
template <typename T, typename T2> double FindDiffOrientationHalf(T startOrientation, T2 stopOrientation)
{
	if ((double)startOrientation != (double)stopOrientation)
	{
		double diffOrientation = (double)((double)stopOrientation - (double)startOrientation);
		if (diffOrientation<(double)(-PI / 2))
			diffOrientation += (double)(PI);
		else if (diffOrientation>(double)(PI / 2))
			diffOrientation -= (double)(PI);
		return diffOrientation;
	}
	else
		return (double)0;
}
template <typename T, typename T2> double FindDiffOrientationHalfAbs(T startOrientation, T2 stopOrientation)
{
	if ((double)startOrientation != (double)stopOrientation)
	{
		double diffOrientation = (double)((double)stopOrientation - (double)startOrientation);
		if (diffOrientation<(double)(-PI / 2))
			diffOrientation += (double)(PI);
		else if (diffOrientation>(double)(PI / 2))
			diffOrientation -= (double)(PI);
		return abs(diffOrientation);
	}
	else
		return (double)0;
}
template <typename T, typename T2> double FindDiffDirection(T startDirection, T2 stopDirection)
{
	if ((double)startDirection != (double)stopDirection)
	{
		double diffDirection = (double)((double)stopDirection - (double)startDirection);
		if (diffDirection<(double)(-PI))
			diffDirection += (double)(2 * PI);
		else if (diffDirection>(double)(PI))
			diffDirection -= (double)(2 * PI);
		return diffDirection;
	}
	else
		return (double)0;
}
template <typename T, typename T2> double FindDiffDirectionAbs(T startDirection, T2 stopDirection)
{
	if ((double)startDirection != (double)stopDirection)
	{
		double diffDirection = (double)((double)stopDirection - (double)startDirection);
		if (diffDirection<(double)(-PI))
			diffDirection += (double)(2 * PI);
		else if (diffDirection>(double)(PI))
			diffDirection -= (double)(2 * PI);
		return abs(diffDirection);
	}
	else
		return (double)0;
}
template <typename T, typename T2> double FindDistance(KCoor<T> pst, KCoor<T2> pend)
{
	return (double)RSS((double)((double)pst.x - (double)pend.x), (double)((double)pst.y - (double)pend.y));
}
template <typename T, typename T2> double FindDistance(T stx, T sty, T2 endx, T2 endy)
{
	return (double)RSS((double)((double)stx - (double)endx), (double)((double)sty - (double)endy));
}
template <typename T> KCoor<T> FindCenterPoint(KCoor<T> pst, KCoor<T> pend)
{
	KCoor<T> tmp;
	tmp.x = (T)(((T)pst.x + (T)pend.x) / 2.0);
	tmp.y = (T)(((T)pst.y + (T)pend.y) / 2.0);
	return tmp;
}
template <typename T> KCoor<T> FindCenterPoint(T stx, T sty, T endx, T endy)
{
	KCoor<T> tmp;
	tmp.x = (T)(((T)stx + (T)endx) / 2.0);
	tmp.y = (T)(((T)sty + (T)endy) / 2.0);
	return tmp;
}
template <typename T1, typename T2, typename T3> bool FindCrossingPoint(KCoor<T1> Pos1, T2 OF1, KCoor<T1> Pos2, T2 OF2, KCoor<T3> &crossPos)
{
	double m1, m2, c1, c2;

	m1 = tan((double)(OF1));
	m2 = tan((double)(OF2));

	c1 = (double)((double)Pos1.y - m1*(double)Pos1.x);
	c2 = (double)((double)Pos2.y - m2*(double)Pos2.x);

	if (m2 == 0 || (double)Pos2.x == 0 || (double)Pos2.y == 0)
	{
		return false;
	}
	else if (m1 == m2)
	{
		crossPos.x = (T3)NULL;
		crossPos.y = (T3)NULL;
		return false;
	}
	else if ((((double)Pos1.x / (double)Pos2.x) == ((double)Pos1.y / (double)Pos2.y)) && (((double)Pos1.x / (double)Pos2.x) == (m1 / m2)))
	{
		crossPos.x = (T3)NULL;
		crossPos.y = (T3)NULL;
		return false;
	}
	else if ((c1 != NULL) || (c2 != NULL))
	{
		crossPos.x = (T3)((c1 - c2) / (m2 - m1));
		crossPos.y = (T3)(((m2*c1) - (m1*c2)) / (m2 - m1));
		return true;
	}
	else
	{
		ErrorMessage("crossing point condition error");
		return false;
	}
}
template <typename T1, typename T2, typename T3> bool FindCrossingPoint(KCoor<T1> Pos1, T2 OF1, KCoor<T1> Pos2, T2 OF2, T3 &crossPosX, T3 &crossPosY)
{
	KCoor<T3>	crossPos;
	crossPos.x = crossPosX;
	crossPos.y = crossPosY;
	return FindCrossingPoint(Pos1, OF1, Pos2, OF2, crossPos);
}
template <typename T1, typename T2, typename T3> bool FindCrossingPoint(T1 x1, T1 y1, T2 OF1, T1 x2, T1 y2, T2 OF2, KCoor<T3> &crossPos)
{
	KCoor<T1>	Pos1, Pos2;
	Pos1.x = x1;
	Pos1.y = y1;
	Pos2.x = x2;
	Pos2.y = y2;
	return FindCrossingPoint(Pos1, OF1, Pos2, OF2, crossPos);
}
template <typename T1, typename T2, typename T3> bool FindCrossingPoint(T1 x1, T1 y1, T2 OF1, T1 x2, T1 y2, T2 OF2, T3 &crossPosX, T3 &crossPosY)
{
	KCoor<T1>	Pos1, Pos2, crossPos;
	bool		isExist;
	Pos1.x = x1;
	Pos1.y = y1;
	Pos2.x = x2;
	Pos2.y = y2;
	isExist = FindCrossingPoint(Pos1, OF1, Pos2, OF2, crossPos);
	crossPosX = crossPos.x;
	crossPosY = crossPos.y;
	return isExist;
}

#endif