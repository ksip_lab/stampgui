// ------------------------------------------------ //
// contain:		Feature base structure
//
// developer:	Krisada Phromsuthirak
// ------------------------------------------------ //

#ifndef _KFeature_H
#define _KFeature_H

#include "KHeader.h"

class KFeature
{
private:

public:

	KFeature()
	{
		
	}
	~KFeature()
	{
		
	}
	virtual void ClearMemory()
	{
		
	}
	virtual void Reset()
	{
		
	}

};

#endif