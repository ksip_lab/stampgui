#include "KMemory.h"

bool DMC_Status = false;			// True and False is tune on and off counting.
unsigned long long DMC_Count = 0;	// Number counting memory .
unsigned long long DMC_Peak = 0;	// Record Peak consuming Memory.
unsigned long long DMC_Total = 0;	// Total consuming Memory.

void DMC_Reset()
{
	DMC_Count = 0;
}
void DMC_Start(char *s)
{
	if (s != NULL) puts(s);
	if (DMC_Status)	printf("Global Memory counting have already on!");
	else
	{
		DMC_Status = true;
		DMC_Count = 0;
	}
}
void DMC_Stop()
{
	if (DMC_Status)	{ DMC_Status = false; }
	else			{ printf("GlobalMemory counting have already off!"); }
}
void DMC_Display(char *s)
{
	printf("\n-------------------------------------------\n");
	if (s != NULL) printf("%s\n", s);
	if (DMC_Count<1024)					printf(">> Memory leak  =\t%9d Bytes  <<\n", DMC_Count);
	else if (DMC_Count<1048576)			printf(">> Memory leak  =\t%9.2f KBytes <<\n", (float)((float)DMC_Count / (float)(1024)));
	else if (DMC_Count<1073741824)		printf(">> Memory leak  =\t%9.2f MBytes <<\n", (float)((float)DMC_Count / (float)(1048576)));
	else if (DMC_Count<1099511627776)	printf(">> Memory leak  =\t%9.2f GBytes <<\n", (float)((float)DMC_Count / (float)(1073741824)));
	else								printf(">> Memory leak  =\t%9.2f TBytes <<\n", (float)((float)DMC_Count / (float)(1099511627776)));

	if (DMC_Peak<1024)					printf(">> Peak Memory  =\t%9d Bytes  <<\n", DMC_Peak);
	else if (DMC_Peak<1048576)			printf(">> Peak Memory  =\t%9.2f KBytes <<\n", (float)((float)DMC_Peak / (float)(1024)));
	else if (DMC_Peak<1073741824)		printf(">> Peak Memory  =\t%9.2f MBytes <<\n", (float)((float)DMC_Peak / (float)(1048576)));
	else if (DMC_Peak<1099511627776)		printf(">> Peak Memory  =\t%9.2f GBytes <<\n", (float)((float)DMC_Peak / (float)(1073741824)));
	else								printf(">> Peak Memory  =\t%9.2f TBytes <<\n", (float)((float)DMC_Peak / (float)(1099511627776)));

	if (DMC_Total<1024)					printf(">> Total memory =\t%9d Bytes  <<\n", DMC_Total);
	else if (DMC_Total<1048576)			printf(">> Total memory =\t%9.2f KBytes <<\n", (float)((float)DMC_Total / (float)(1024)));
	else if (DMC_Total<1073741824)		printf(">> Total memory =\t%9.2f MBytes <<\n", (float)((float)DMC_Total / (float)(1048576)));
	else if (DMC_Total<1099511627776)	printf(">> Total memory =\t%9.2f GBytes <<\n", (float)((float)DMC_Total / (float)(1073741824)));
	else								printf(">> Total memory =\t%9.2f TBytes <<\n", (float)((float)DMC_Total / (float)(1099511627776)));
	printf("-------------------------------------------\n");
}
void DMC_Add(unsigned long long memorySlot, unsigned long long variableSize)
{
	if (DMC_Status)
		DMC_Count += (unsigned long long) (memorySlot*variableSize);
	if (DMC_Peak < (unsigned long long) (memorySlot*variableSize))
		DMC_Peak = (unsigned long long) (memorySlot*variableSize);
	DMC_Total += (unsigned long long) (memorySlot*variableSize);
}
void DMC_Free(unsigned long long memorySlot, unsigned long long variableSize)
{
	if (DMC_Status)
		DMC_Count -= (unsigned long long) (memorySlot*variableSize);
}