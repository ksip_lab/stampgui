// ------------------------------------------------ //
// contain:		class Alphabet and Numeric storage
//
// developer:	Krisada Phromsuthirak
// ------------------------------------------------ //

#ifndef _KAlphaNumeric_H
#define _KAlphaNumeric_H

#include "KHeader.h"
#include "KMemory.h"
#include "KBasic.h"
#include "KList1D.h"

template <class Type_Numeric> class KAlphaNumeric
{
private:

	bool isAlpha;
	bool isNumeric;

public:

	string Alphabet;
	Type_Numeric Numeric;

	KAlphaNumeric()
	{
	}
	~KAlphaNumeric()
	{
	}

	bool IsAlphabet()
	{
		return isAlpha;
	}
	bool IsNumeric()
	{
		return isNumeric;
	}
	void SetAlphabet()
	{
		isAlpha = true;
		isNumeric = false;
	}
	void SetNumeric()
	{
		isAlpha = false;
		isNumeric = true;
	}

};

#endif