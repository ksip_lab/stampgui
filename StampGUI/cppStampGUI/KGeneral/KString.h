// ------------------------------------------------ //
// contain:		String management tools
//
// developer:	Krisada Phromsuthirak
// ------------------------------------------------ //

#ifndef _KString_H
#define _KString_H

#include "KHeader.h"
#include "KAlphaNumeric.h"
#include "KList1D.h"

template <typename Type_Numeric> void ConvertStringToListOfAlphaNumeric(string &s, KList1D<KAlphaNumeric<Type_Numeric>> &listAlphaNumeric)
{
	KAlphaNumeric<Type_Numeric> chk;
	string sNum;

	for (int c1 = 0; c1<s.length(); c1++)
	{
		if (IsNumeric(s[c1]))
		{
			chk.SetNumeric();
			sNum = s[c1];
			for (int c2 = c1 + 1; c2 < s.length(); c2++)
			{
				if (IsNotNumeric(s[c2]))
				{
					c1 = c2;
					break;
				}
				else
				{
					sNum = sNum + s[c2];

					if (c2 == s.length() - 1)
					{
						c1 = c2;
						break;
					}
				}
			}
			chk.Numeric = (Type_Numeric)stod(sNum);
			listAlphaNumeric.push_back(chk);
		}
		else
		{
			chk.SetAlphabet();
			chk.Alphabet = s[c1];
			listAlphaNumeric.push_back(chk);
		}
	}
}

#endif