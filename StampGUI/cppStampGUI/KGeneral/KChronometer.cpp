#include "KChronometer.h"

KChronometer::KChronometer()
{
	Reset();
}
KChronometer::~KChronometer()
{

}
void KChronometer::Reset()
{
	this->elapse = 0;
	this->status = 0;
}
void KChronometer::Start(char *s)
{
	if (s != NULL)
		cout << s << endl;

	if (this->status)
		cout << "chronometer already on!" << endl;
	else
	{
		this->status = 1;
		this->mark = clock();
	}
}
void KChronometer::Stop()
{
	if (this->status)
	{
		this->elapse += clock() - this->mark;
		this->status = 0;
	}
	else
		cout << "chronometer already off!" << endl;
}
float KChronometer::Read()
{
	if (this->status)
		return (float)(this->elapse + (clock() - this->mark)) / CLOCKS_PER_SEC;
	else
		return (float)this->elapse / CLOCKS_PER_SEC;
}
void KChronometer::Display(char *s)
{
	int hour, minute;
	float second;

	second = this->Read();
	//Calculate hour
	hour = (int)(second / 3600.0);
	second -= (float)(3600.0*hour);
	//Calculate minute
	minute = (int)(second / 60.0);
	second -= (float)(60.0*minute);

	//Display to monitor
	if (s != NULL) printf("%s", s);
	if (hour)
	{
		printf("%d hour", hour);
		if (hour>1) printf("s, "); else printf(", ");
	}
	if ((hour) || (minute))
	{
		printf("%3d minute", minute);
		if (minute>1) printf("s, and "); else printf(", and ");
	}
	printf("%3.5f seconds.\n", second);
}