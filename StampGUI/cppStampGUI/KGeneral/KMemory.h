// ------------------------------------------------ //
// DMC:			Dynamic Memory Counting
// contain:		[1] allocation and free dynamic memory function
//				[2] variable to record global dynamic memory using
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KMemory_H
#define _KMemory_H

#include "KHeader.h"

// ================================================== //
// --- declaration of global dynamic memory usage --- //
// ================================================== //

void DMC_Reset();
void DMC_Start(char *s = (char*)NULL);
void DMC_Stop();
void DMC_Display(char *s = (char*)NULL);

void DMC_Add(unsigned long long memorySlot, unsigned long long variableSize);
void DMC_Free(unsigned long long memorySlot, unsigned long long variableSize);

// ============================================== //
// --- declaration of dynamic memory function --- //
// ============================================== //
template <typename T> T * Alloc(UINT size1)
{
	T *i;
	try
	{
		i = new T[size1];
		DMC_Add(size1, (unsigned long long)sizeof(T));
	}
	catch (bad_alloc&)
	{
		cout << "cannot allocate memory" << endl;
		system("pause");
		exit(0);
	}
	if (i == NULL)
	{
		cout << "allocation error" << endl;
		system("pause");
		exit(0);
	}
	return i;
}
template <typename T> void Free(T *i, UINT size1)
{
	if (i != NULL)
	{
		if (size1 != 0)
			DMC_Free(size1, (unsigned long long)sizeof(T));
		delete[] i;
	}
}
template <typename T> T ** Alloc(UINT size1, UINT size2)
{
	T **i;
	try
	{
		i = new T *[size1];
		for (UINT s1 = 0; s1 < size1; ++s1)
			i[s1] = new T[size2];
		DMC_Add(size1*size2, (unsigned long long)sizeof(T));
	}
	catch (bad_alloc&)
	{
		cout << "cannot allocate memory" << endl;
		system("pause");
		exit(0);
	}
	if (i == NULL)
	{
		cout << "allocation error" << endl;
		system("pause");
		exit(0);
	}
	return i;
}
template <typename T> void Free(T **i, UINT size1, UINT size2)
{
	if (i != NULL)
	{
		if (size1 != 0 && size2 != 0)
			DMC_Free(size1*size2, (unsigned long long)sizeof(T));
		for (UINT s1 = 0; s1 < size1; s1++)
			delete[] i[s1];
		delete[] i;
	}
}
template <typename T> T *** Alloc(UINT size1, UINT size2, UINT size3)
{
	T ***i;
	try
	{
		i = new T **[size1];
		for (UINT s1 = 0; s1 < size1; ++s1)
			i[s1] = new T* [size2];
		for (UINT s1 = 0; s1 < size1; ++s1)
			for (UINT s2 = 0; s2 < size2; ++s2)
				i[s1][s2] = new T[size3];
			DMC_Add(size1*size2*size3, (unsigned long long)sizeof(T));
	}
	catch (bad_alloc&)
	{
		cout << "cannot allocate memory" << endl;
		system("pause");
		exit(0);
	}
	if (i == NULL)
	{
		cout << "allocation error" << endl;
		system("pause");
		exit(0);
	}
	return i;
}
template <typename T> void Free(T ***i, UINT size1, UINT size2, UINT size3)
{
	if (i != NULL)
	{
		if (size1 != 0 && size2 != 0 && size3 != 0)
			DMC_Free(size1*size2*size3, (unsigned long long)sizeof(T));

		for (UINT s1 = 0; s1 < size1; s1++)
		{
			for (UINT s2 = 0; s2 < size2; s2++)
			{
				delete[] i[s1][s2];
			}
			delete[] i[s1];
		}
		delete[] i;
	}
}
template <typename T> T **** Alloc(UINT size1, UINT size2, UINT size3, UINT size4)
{
	T ****i;
	try
	{
		i = new T ***[size1];
		for (UINT s1 = 0; s1 < size1; ++s1)
			i[s1] = new T*[size2];
		for (UINT s1 = 0; s1 < size1; ++s1)
			for (UINT s2 = 0; s2 < size2; ++s2)
				i[s1][s2] = new T[size3];
		for (UINT s1 = 0; s1 < size1; ++s1)
			for (UINT s2 = 0; s2 < size2; ++s2)
				for (UINT s3 = 0; s3 < size3; ++s3)
					i[s1][s2][s3] = new T[size4];
		DMC_Add(size1*size2*size3*size4, (unsigned long long)sizeof(T));
	}
	catch (bad_alloc&)
	{
		cout << "cannot allocate memory" << endl;
		system("pause");
		exit(0);
	}
	if (i == NULL)
	{
		cout << "allocation error" << endl;
		system("pause");
		exit(0);
	}
	return i;
}
template <typename T> void Free(T ****i, UINT size1, UINT size2, UINT size3, UINT size4)
{
	if (i != NULL)
	{
		if (size1 != 0 && size2 != 0 && size3 != 0 && size4 != 0)
			DMC_Free(size1*size2*size3*size4, (unsigned long long)sizeof(T));

		for (UINT s1 = 0; s1 < size1; s1++)
		{
			for (UINT s2 = 0; s2 < size2; s2++)
			{
				for (UINT s3 = 0; s3 < size3; s3++)
				{
					delete[] i[s1][s2][s3];
				}
				delete[] i[s1][s2];
			}
			delete[] i[s1];
		}
		delete[] i;
	}
}

#endif