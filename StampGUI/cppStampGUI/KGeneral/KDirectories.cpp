#include "KDirectories.h"

bool CreateFolder(char *foldername)
{
	stringstream	ss;
	string			s;
	ss << foldername;
	s = ss.str();
	ss.clear();
	return CreateFolder(s);
}
bool CreateFolder(string foldername)
{
	string	tmpPath;
	string	currentPath;
	string	cuttingPath;
	int		pos1, pos2;
	bool	isCreate;

	isCreate = false;
	tmpPath = foldername;
	if (tmpPath.find_last_of("\\") == -1 || tmpPath.find_last_of("\\") != tmpPath.size() - 1)
		tmpPath = tmpPath + "\\";

	currentPath = "";
	cuttingPath = tmpPath;
	pos1 = 0;
	pos2 = (int)cuttingPath.find_first_of("\\");
	while (pos2 != -1)
	{
		currentPath = currentPath + cuttingPath.substr(pos1, pos2) + "\\";
		cuttingPath = cuttingPath.substr(pos2 + 1, cuttingPath.size() - pos2 - 1);
		pos1 = 0;
		pos2 = (int)cuttingPath.find_first_of("\\");

		if (CreateDirectory((char*)currentPath.c_str(), NULL) || ERROR_ALREADY_EXISTS == GetLastError())
			isCreate = true;
	}
	return isCreate;
}
bool RemoveFolder(char *foldername)
{
	if (_rmdir(foldername) == -1)
	{
		cout << "Cannot remove: [" << foldername << "]\n";
		return false;
	}
	return true;
}
bool RemoveFolder(string foldername)
{
	return RemoveFolder((char*)foldername.c_str());
}
bool RemoveFile(char *filename, bool debug)
{
	char confirmFlag = 'y';
	if (debug)
	{
		do
		{
			cout << "Are you sure to remove (" << filename << ") [y/n] ? :";
			cin >> confirmFlag;
		} while (!cin.fail() && confirmFlag != 'y' && confirmFlag != 'n');
	}
	if (confirmFlag == 'y' || debug == false)
	{
		if (remove(filename) == -1)
		{
			cout << "Cannot remove: [" << filename << "]\n";
			return false;
		}
		else
			return true;
	}
	else
		return false;
}
bool RemoveFile(string filename, bool debug)
{
	return RemoveFile((char*)filename.c_str(), debug);
}
bool Rename(char *oldFilename, char *newFilename)
{
	if (rename(oldFilename, newFilename) == -1)
	{
		cout << "Cannot rename: [" << oldFilename << "]\n";
		return false;
	}
	else
		return true;
}
bool Rename(string oldFilename, char *newFilename)
{
	return Rename((char*)oldFilename.c_str(), newFilename);
}
bool Rename(char *oldFilename, string newFilename)
{
	return Rename(oldFilename, (char*)newFilename.c_str());
}
bool Rename(string oldFilename, string newFilename)
{
	return Rename((char *)oldFilename.c_str(), (char*)newFilename.c_str());
}
bool IsFileExist(const char *filename)
{
	WIN32_FIND_DATA fd;
	HANDLE h = FindFirstFile(filename, &fd);
	if (h == INVALID_HANDLE_VALUE)
		return false;
	else
		return true;
}
bool IsFileExist(string filename)
{
	return IsFileExist((char *)filename.c_str());
}
bool GetFileList(const char *searchkey, vector<string> &list)
{
	list.clear();

	WIN32_FIND_DATA fi;
	HANDLE h = FindFirstFile(searchkey, &fi);
	if (h != INVALID_HANDLE_VALUE)
	{
		do 
		{
			if (fi.cFileName[0] != '.')
				list.push_back(fi.cFileName);
		} while (FindNextFile(h, &fi));
		FindClose(h);
		return true;
	}
	else
		return false;
}
bool GetFileList(string searchkey, vector<string> &list)
{
	return GetFileList((char*)searchkey.c_str(), list);
}
bool GetFolderList(const char *searchkey, vector<string> &list)
{
	list.clear();

	WIN32_FIND_DATA fi;
	HANDLE h = FindFirstFileEx(searchkey, FindExInfoStandard, &fi, FindExSearchLimitToDirectories, NULL, 0);
	if (h != INVALID_HANDLE_VALUE)
	{
		do {
			if (fi.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			if (fi.cFileName[0] != '.')
				list.push_back(fi.cFileName);
		} while (FindNextFile(h, &fi));
		FindClose(h);
		return true;
	}
	else
		return false;
}
bool GetFolderList(string searchkey, vector<string> &list)
{
	return GetFolderList((char*)searchkey.c_str(), list);
}
bool SortNumeric_NumericStringCompare(const std::string& s1, const std::string& s2)
{
	KList1D<KAlphaNumeric<int>> chunk1, chunk2;
	unsigned int MaxLength;

	ConvertStringToListOfAlphaNumeric<int>((string)s1, chunk1);
	ConvertStringToListOfAlphaNumeric<int>((string)s2, chunk2);

	MaxLength = Max(chunk1.size(), chunk2.size());

	for (unsigned int c = 0; c < MaxLength; c++)
	{
		if (c < chunk1.size() && c < chunk2.size())
		{
			if (chunk1[c].IsAlphabet() && chunk2[c].IsAlphabet())
			{
				if (strcmp(chunk1[c].Alphabet.c_str(), chunk2[c].Alphabet.c_str()) < 0)
					return true;
				if (strcmp(chunk1[c].Alphabet.c_str(), chunk2[c].Alphabet.c_str()) > 0)
					return false;
			}
			else if (chunk1[c].IsNumeric() && chunk2[c].IsNumeric())
			{
				if (chunk1[c].Numeric < chunk2[c].Numeric)
					return true;
				if (chunk1[c].Numeric > chunk2[c].Numeric)
					return false;
			}
			else if (chunk1[c].IsAlphabet() && chunk2[c].IsNumeric())
			{
				if (isspace(chunk1[c].Alphabet[0]))
					return true;
				else
					return false;
			}				
			else if (chunk1[c].IsNumeric() && chunk2[c].IsAlphabet())
			{
				if (isspace(chunk2[c].Alphabet[0]))
					return false;
				else
					return true;
			}
			else
				return false;
		}
		if (c >= chunk1.size() || c >= chunk2.size())
		{
			if (chunk1.size() < chunk2.size())
				return true;
			else if (chunk1.size() > chunk2.size())
				return false;
			else
				return false;
		}
	}
	return false;
}
void SortNumeric(vector<string> &list)
{
	sort(list.begin(), list.end(), SortNumeric_NumericStringCompare);
}
string GetFileName(string path)
{
	int firstPos = (int)path.find_last_of("\\");
	int lastPos = (int)path.find_last_of(".");

	if (firstPos >= 0)
	{
		if (lastPos >= 0)
			return path.substr(firstPos + 1, lastPos - firstPos - 1);
		else
			return path.substr(firstPos + 1);
	}
	else
	{
		if (lastPos >= 0)
			return path.substr(0, lastPos);
		else
			return path;
	}
}
string GetFileName(const char *path)
{
	stringstream	ss;
	string			s;
	ss << path;
	s = ss.str();
	ss.clear();
	return GetFileName(s);
}
string GetFileExtension(string path)
{
	int lastPos = (int)path.find_last_of(".");
	if (lastPos >= 0)
		return path.substr(lastPos);
	else
		return "";
}
string GetFileExtension(const char *path)
{
	stringstream	ss;
	string			s;
	ss << path;
	s = ss.str();
	ss.clear();
	return GetFileExtension(s);
}
string GetFolderPath(string path)
{
	int lastPos = (int)path.find_last_of("\\");
	if (lastPos >= 0)
		return path.substr(0, lastPos) + "\\";
	else
		return "";
}
string GetFolderPath(const char *path)
{
	stringstream	ss;
	string			s;
	ss << path;
	s = ss.str();
	ss.clear();
	return GetFolderPath(s);
}
string GetFolderPathFormat(string path)
{
	if (path.find_last_of("\\") == -1 || path.find_last_of("\\") != path.size() - 1)
		return path + "\\";
	else
		return path;
}
string GetFolderPathFormat(const char *path)
{
	return GetFolderPathFormat(ToString(path));
}
string ReplaceExtension(string path, string extension)
{
	string out, tmpExtension;
	size_t lastDot;
	size_t lastSlash;

	if (extension.empty())
		ErrorMessage("Found problem in [ReplaceExtension]: extension is NULL");

	if (extension.find_first_of(".") != 0)
		tmpExtension = "." + extension;
	else
		tmpExtension = extension;

	lastSlash = path.find_last_of("\\");
	lastDot = path.find_last_of(".");

	if ((lastSlash > lastDot) && (lastSlash != path.npos))
		out = path + tmpExtension;
	else
	{
		if (lastDot == path.npos)
			out = path + tmpExtension;
		else
			out = path.substr(0, lastDot) + tmpExtension;
	}

	return out;
}
string ReplaceExtension(const char *path, const char *extension)
{
	string out, tmpExtension;
	out = ToString(path);
	tmpExtension = ToString(extension);
	out = ReplaceExtension(out, tmpExtension);
	return out;
}