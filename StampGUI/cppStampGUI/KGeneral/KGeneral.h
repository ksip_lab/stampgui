// ------------------------------------------------ //
// contain:		Base System for OudVision
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KGeneral_H
#define _KGeneral_H

#include "KHeader.h"			// standard header & defined variable
#include "KBasic.h"				// inline function (take some storage memory to decrease calling time)
#include "KMemory.h"			// dynamic memory management
#include "KChronometer.h"		// timer tool

#include "KCoor.h"				// coordiante structure
#include "KArray.h"				// array structure
#include "KList.h"				// link-list structure
#include "KQueue.h"				// queue structure (First-In-First-Out)
#include "KStack.h"				// stack structure (First-In-Last-Out)
#include "KTree.h"				// tree structure
#include "KGraph.h"				// graph structure

#include "KGeometry.h"			// coordinate system & some mathematic functions
#include "KDirectories.h"		// directories management
#include "KFeature.h"			// feature base structure

void EndLibrary();

#endif