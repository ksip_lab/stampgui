// ------------------------------------------------ //
// contain:		Basic function
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KBasic_H
#define _KBasic_H

#include "KHeader.h"

void ErrorMessage(char *message, bool isEndProcess = true);
void ErrorMessage(string message, bool isEndProcess = true);

template <typename T> inline T					ABS(T x)				{ return (T)abs(x); }								//Absolute
template <typename T> inline T					SQR(T x)				{ return (T)(x*x); }								//Square number
template <typename T> inline T					SQRT(T x)				{ return (T)sqrtf((float)x); }						//Square root
template <typename T> inline T					Max(T a, T b)			{ return (a > b ? a : b); }							//Get Max value
template <typename T> inline T					Min(T a, T b)			{ return (a < b ? a : b); }							//Get Min value
template <typename T> inline void				Swap(T *a, T *b)		{ T c = *a; *a = *b; *b = c; }						//Swap value
template <typename T> inline void				Swap(T &a, T &b)		{ T c = a; a = b; b = c; }							//Swap pointer
template <typename T> inline float				RSS(T x, T y)			{ return (float)sqrt((float)(x*x + y*y)); }			//Root Sum Square
template <typename T> inline float				Deg2Rad(T x)			{ return (float)(x*PI_180); }						//Degree to Radian
template <typename T> inline float				Rad2Deg(T x)			{ return (float)(x*_180_PI); }						//Radian to Degree
template <typename T1, typename T2> inline T1	Pow(T1 x, T2 n)			{ return (T1)(pow((double)x, (double)n)); }			//Find x^n
template <typename T> inline int    Sign(T x)    { return (int)(((double)x >(double)0.0) ? (1) : (-1)); }

inline bool	IsPowOf2(int num)					{ return ((num>0) && ((num&(num - 1)) == 0)); }								//Is 2^num ?
inline bool	IsPowOf2(UINT num)					{ return ((num>0) && ((num&(num - 1)) == 0)); }								//Is 2^num ?
inline int	PowOf2(int num)						{ int logN = 0; while (num >= 2){ num >>= 1; logN++; }return logN; }		//find x of 2^x = num
inline UINT	PowOf2(UINT num)					{ UINT logN = 0; while (num >= 2){ num >>= 1; logN++; }return logN; }		//find x of 2^x = num
inline long	Round(float x)						{ return (x >= 0.0 ? int(x + 0.5) : int(x - 0.5)); }						//rounding number
inline long	Round(double x)						{ return (x >= 0.0 ? long(x + 0.5) : long(x - 0.5)); }						//rounding number

template <typename T> T GetKey(char *s = NULL)
{
	string	a;
	T		out;
	if (s != NULL)	cout << s;
	else		cout << "[insert value]:";
	getline(cin, a);
	istringstream(a) >> out;
	return out;
}
template <typename T> inline string	ToString(const T& t)
{
	stringstream ss;
	ss << t;
	return ss.str();
}
inline bool IsNumeric(char c)
{
	if (isdigit(c) == 0)	return false;
	else					return true;
}
inline bool IsNotNumeric(char c)
{
	if (isdigit(c) == 0)	return true;
	else					return false;
}
inline bool IsAlphabet(char a)
{
	if (isalpha(a) == 0)	return false;
	else					return true;
}
inline bool IsNotAlphabet(char a)
{
	if (isalpha(a) == 0)	return true;
	else					return false;
}
inline bool IsAlphaNumeric(char n)
{
	if (isalnum(n) == 0)	return false;
	else					return true;
}
inline bool IsNotAlphaNumeric(char n)
{
	if (isalnum(n) == 0)	return true;
	else					return false;
}

#endif