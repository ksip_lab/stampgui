#ifndef _KFunction_H
#define _KFunction_H

#include "KGeneral.h"
#include "KImage.h"
#include "KGeometric.h"
#include <Eigen/Eigen>

using namespace Eigen;

template <typename T1> void KExtendBound(KCImage<T1> *cimg, int extend_size, T1 R, T1 G, T1 B);
template <typename T1> void KReduceBound(KCImage<T1> *cimg, int extend_size);
template <typename T1> void KExtendBound(KGImage<T1> *gimg, int extend_size, T1 color);
template <typename T1> void KReduceBound(KGImage<T1> *gimg, int extend_size);

template <typename T1, typename T2> void KPCADimensionReduction(vector<vector<T1>> *input, int number_dimension, vector<vector<T2>> *U_reduce);

template <typename T1> void KResizeBound(KCImage<T1> *cimg, int width, int height, T1 R, T1 G, T1 B);
template <typename T1> void KResizeBound(KGImage<T1> *gimg, int width, int height, T1 color);

template <typename T1> void KExtendBound(KCImage<T1> *cimg, int extend_size, T1 R, T1 G, T1 B)
{
	KCImage<T1> in;
	in = *cimg;
	cimg->Allocate(in.width + extend_size * 2, in.height + extend_size * 2);

	int ii = 0, jj = 0;
	for (int u = 0; u < cimg->width; u++)
	{
		for (int v = 0; v < cimg->height; v++)
		{
			cimg->idata[0][u][v] = R;
			cimg->idata[1][u][v] = G;
			cimg->idata[2][u][v] = B;

			if (u >= extend_size && v >= extend_size)
			{
				if (in.IsInBound(ii, jj))
				{
					cimg->idata[0][u][v] = in.idata[0][ii][jj];
					cimg->idata[1][u][v] = in.idata[1][ii][jj];
					cimg->idata[2][u][v] = in.idata[2][ii][jj];
					jj++;
				}
			}
		}

		if (u >= extend_size)
		{
			ii++;
			jj = 0;
		}
	}
}

template <typename T1> void KReduceBound(KCImage<T1> *cimg, int extend_size)
{
	KCImage<T1> in;
	in = *cimg;
	cimg->Allocate(in.width - extend_size * 2, in.height - extend_size * 2);

	for (int u = 0; u < cimg->width; u++)
	{
		for (int v = 0; v < cimg->height; v++)
		{
			cimg->idata[0][u][v] = in.idata[0][u + extend_size][v + extend_size];
			cimg->idata[1][u][v] = in.idata[1][u + extend_size][v + extend_size];
			cimg->idata[2][u][v] = in.idata[2][u + extend_size][v + extend_size];
		}
	}
}

template <typename T1> void KExtendBound(KGImage<T1> *gimg, int extend_size, T1 color)
{
	KGImage<T1> in;
	in = *gimg;
	gimg->Allocate(in.width + extend_size * 2, in.height + extend_size * 2);

	int ii = 0, jj = 0;
	for (int u = 0; u < gimg->width; u++)
	{
		for (int v = 0; v < gimg->height; v++)
		{
			gimg->idata[u][v] = color;

			if (u >= extend_size && v >= extend_size)
			{
				if (in.IsInBound(ii, jj))
				{
					gimg->idata[u][v] = in.idata[ii][jj];
					jj++;
				}
			}
		}

		if (u >= extend_size)
		{
			ii++;
			jj = 0;
		}
	}
}

template <typename T1> void KReduceBound(KGImage<T1> *gimg, int extend_size)
{
	KGImage<T1> in;
	in = *gimg;
	gimg->Allocate(in.width - extend_size * 2, in.height - extend_size * 2);

	for (int u = 0; u < gimg->width; u++)
	{
		for (int v = 0; v < gimg->height; v++)
		{
			gimg->idata[u][v] = in.idata[u + extend_size][v + extend_size];
		}
	}
}

template <typename T1> void KResizeBound(KCImage<T1> *cimg, int width, int height, T1 R, T1 G, T1 B)
{
	KCImage<T1> in;
	in = *cimg;
	cimg->Allocate(width, height);

	int hor = (width - in.width) / 2;
	int ver = (height - in.height) / 2;

	int ii = 0, jj = 0;
	if (hor < 0)
		ii = -hor;

	if (ver < 0)
		jj = -ver;

	for (int u = 0; u < cimg->width; u++)
	{
		for (int v = 0; v < cimg->height; v++)
		{
			cimg->idata[0][u][v] = R;
			cimg->idata[1][u][v] = G;
			cimg->idata[2][u][v] = B;

			if (u >= hor && v >= ver)
			{
				if (in.IsInBound(ii, jj))
				{
					cimg->idata[0][u][v] = in.idata[0][ii][jj];
					cimg->idata[1][u][v] = in.idata[1][ii][jj];
					cimg->idata[2][u][v] = in.idata[2][ii][jj];
					jj++;
				}
			}
		}

		if (u >= hor)
		{
			ii++;
			if (ver < 0)
				jj = -ver;
			else
				jj = 0;
		}
	}
}

template <typename T1> void KResizeBound(KGImage<T1> *gimg, int width, int height, T1 color)
{
	KGImage<T1> in;
	in = *gimg;
	gimg->Allocate(width, height);

	int hor = (width - in.width) / 2;
	int ver = (height - in.height) / 2;
	
	int ii = 0, jj = 0;
	if (hor < 0)
		ii = -hor;

	if (ver < 0)
		jj = -ver;

	for (int u = 0; u < gimg->width; u++)
	{
		for (int v = 0; v < gimg->height; v++)
		{
			gimg->idata[u][v] = color;

			if (u >= hor && v >= ver)
			{
				if (in.IsInBound(ii, jj))
				{
					gimg->idata[u][v] = in.idata[ii][jj];
					jj++;
				}
			}
		}

		if (u >= hor)
		{
			ii++;
			if (ver < 0)
				jj = -ver;
			else
				jj = 0;
		}
	}
}

template <typename T1, typename T2> void KPCADimensionReduction(vector<vector<T1>> *input, int number_dimension, vector<vector<T2>> *U_reduce)
{
	int dim = input->at(0).size();

	MatrixXf A = MatrixXf::Constant(dim, dim, 0);
	MatrixXf B(dim, 1);
	for (int i = 0; i < input->size(); i++)
	{
		for (int j = 0; j < dim; j++)
		{
			B(j, 0) = input->at(i).at(j);
		}
		A = A + B*B.transpose();
	}

	A = A / input->size();

	JacobiSVD<MatrixXf> svd(A, ComputeThinU | ComputeThinV);
	MatrixXf U = svd.matrixU();
	MatrixXf V = svd.matrixV();
	MatrixXf S = svd.singularValues();

	int dim_reduce = number_dimension;
	if (dim_reduce > dim)
		dim_reduce = dim;

	for (int i = 0; i < dim_reduce; i++)
	{
		vector<T2> tmp;
		for (int j = 0; j < dim; j++)
		{
			tmp.push_back(U(j, i));
		}
		U_reduce->push_back(tmp);
	}
}

#endif