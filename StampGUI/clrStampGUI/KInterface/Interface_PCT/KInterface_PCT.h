// ------------------------------------------------ //
// contain:		PCT file reader for NIST-Fingerprint Special Database 4
//				NIST iHeader format
//
// original:	Michael D. Garris
// developer:	Krisada Phromsuthirak
// ------------------------------------------------ //

#ifndef _KInterface_PCT_H
#define _KInterface_PCT_H

#include "KHeader.h"
#include "KArray.h"

class KInterface_PCT
{
private:
	#define IHDR_SIZE		288	// length of hdr record (always even bytes)
	#define SHORT_CHARS 	8	// # of ASCII chars to represent a short
	#define BUFSIZE			80	// default buffer size
	#define DATELEN			26	// character length of data string

	typedef struct ihead
	{
		char id[BUFSIZE];				// identification/comment field 
		char created[DATELEN];			// date created 
		char width[SHORT_CHARS];		// pixel width of image 
		char height[SHORT_CHARS];		// pixel height of image 
		char depth[SHORT_CHARS];		// bits per pixel 
		char density[SHORT_CHARS];		// pixels per inch 
		char compress[SHORT_CHARS];		// compression code 
		char complen[SHORT_CHARS];		// compressed data length 
		char align[SHORT_CHARS];		// scan line multiple: 8|16|32 
		char unitsize[SHORT_CHARS];		// bit size of image memory units 
		char sigbit;					// 0->sigbit first | 1->sigbit last 
		char byte_order;				// 0->highlow | 1->lowhigh
		char pix_offset[SHORT_CHARS];	// pixel column offset 
		char whitepix[SHORT_CHARS];		// intensity of white pixel 
		char issigned;					// 0->unsigned data | 1->signed data 
		char rm_cm;						// 0->row maj | 1->column maj 
		char tb_bt;						// 0->top2bottom | 1->bottom2top 
		char lr_rl;						// 0->left2right | 1->right2left 
		char parent[BUFSIZE];			// parent image file 
		char par_x[SHORT_CHARS];		// from x pixel in parent 
		char par_y[SHORT_CHARS];		// from y pixel in parent 
	}NIST_HEAD;

	// General Defines 
	#define UNCOMP			0
	#define CCITT_G3		1
	#define CCITT_G4		2
	#define LZW				3
	#define RL_LZW			4
	#define RL				5
	#define JPEG			6
	#define MSBF			'0'
	#define LSBF			'1'
	#define HILOW			'0'
	#define LOWHI			'1'
	#define UNSIGNED		'0'
	#define SIGNED			'1'
	#define ROW_MAJ			'0'
	#define COL_MAJ			'1'
	#define TOP2BOT			'0'
	#define BOT2TOP			'1'
	#define LEFT2RIGHT		'0'
	#define RIGHT2LEFT		'1'

	//*******************************
	//structure used in decompression
	//*******************************

	typedef struct hcode //structure for Huffman code table
	{
		int size;
		unsigned int code;
	}HUFFCODE;

	typedef struct hcode* PTR_HUFFCODE;	//ptr to Huffman coding structure

	//*******************************
	//variables used in decompression
	//*******************************

	#define   NIST_DEC_MEMSIZE1			11		//size memory needed for variable
	#define   NIST_DEC_MEMSIZE2			10		//size memory needed for variable
	#define   NIST_DEC_LSBITMASK		0x0001	//set and mask LSbit of 2 byte
	#define   NIST_DEC_CATMASK			0x0100	//mask used in categorize
	#define   NIST_DEC_SHORTCODELENGTH  9		//default value when searching for code length in categorize
	#define   NIST_DEC_FIRSTBIT			7 		//first bit used in a byte
	#define   NIST_DEC_NUMARGSDCP		2		//number of input arguments
	#define   NIST_DEC_BYTE				8		//used to shift bytes
	#define   NIST_DEC_BASE				2		//base in extend exponential operation
	#define   NIST_DEC_SMALLESTDIFF		-511	//smallest possible difference pixel
	#define   NIST_DEC_LARGESTDIFF		512		//largest possible difference pixel

	//***************************************************
	//defines return type of JPEG decompression algorithm
	//***************************************************

	void jpegdecomp(unsigned char *indata,int width,int height,int depth,unsigned char *outbuffer);
	void define_huff_table(unsigned char huffbits[],unsigned char huffvalues[],unsigned char ** indata);
	void decode_table_gen(PTR_HUFFCODE ptr_huffcode_table,short int maxcode[],short int mincode[],int valptr[],unsigned char huffbits[]);
	void build_huff_decode_table(int ptr_huff_decoder[10][512]);
	int decode_data(short int mincode[], short int maxcode[], int valptr[], unsigned char huffvalues[], unsigned char **indata, int *bit_count);
	unsigned short int nextbits(unsigned char **indata, int *bit_count, int bits_req);
	PTR_HUFFCODE build_huffsizes(int *temp_size,unsigned char *huffbits,int size);
	void build_huffcodes(PTR_HUFFCODE ptr_huffcode_table);
	int predict(unsigned char *indata, int width, long int pixel_num,int depth, int pred_type);
	short int categorize(short int difference);
	//==================================================//
	//==================================================//
	void NIST_syserr(char *funcname, char *syscall, char *msg);
	void NIST_fatalerr(char *s1,char *s2,char *s3);
	void low_priority_abort();
	//**********************************************************
	//         Routine:   PixPerByte()                          
	//         Author:    Michael D. Garris                     
	//                    Darrin Dimmick                        
	//         Date:      3/07/90                               
	//         Modifications:                                   
	//           9/20/90    (Stan Janet) error message          
	//**********************************************************
	//**********************************************************
	// PixPerByte() takes the pixel depth of an image and       
	// returns the corresponding pixperbyte factor.             
	//**********************************************************
	float NIST_PixPerByte(int depth);
	//**********************************************************
	//         Routine:   SizeFromDepth()                       
	//         Author:    Michael D. Garris                     
	//                    Darrin Dimmick                        
	//         Date:      2/20/90                               
	//         Modifications:                                   
	//           9/20/90   (Stan Janet) add ceil() [bug]        
	//**********************************************************
	//**********************************************************
	// SizeFromDepth() takes the pixel width of an image scan   
	// line along with the pixel height of the image, and using 
	// the argument "depth" computes the length of the image in 
	// bytes.                                                   
	//**********************************************************
	int NIST_SizeFromDepth(int pixwidth,int pixheight,int depth);
	//**********************************************************
	//         Routine:   ReadIheadRaster()                     
	//         Author:    Michael D. Garris                     
	//         Date:      4/28/89                               
	//         Modifications:                                   
	//           8/90 Stan Janet                                
	//                     only malloc 1 buffer if data is not  
	//                        compressed                        
	//                     free() up temp buffer                
	//           9/20/90 Stan Janet                             
	//                     check return codes                   
	//           1/11/91 Stan Janet                             
	//                     put filename in error messages       
	//**********************************************************
	//**********************************************************
	// ReadNISTheadRaster() reads in a "iheadered" raster file and 
	// returns an ihead structure, raster data, and integer file
	// specs.                                                   
	//**********************************************************

	//**************************************************************
	//	   This package of subroutines are used for reading	
	//	   and writing an image using the NIST Ihead format.	
	//								
	//		Contents:   NIST_ReadiheadRaster()			
	//			        NIST_writeihdrfile()			
	//								
	//**************************************************************
	void NIST_ReadiheadRaster(char *file,NIST_HEAD **head,unsigned char **data,int *width,int *height,int *depth);
	void NIST_ReadiheadRaster2(char *file,unsigned char **data,int *width,int *height,int *depth);
	//**********************************************************
	//         Routine:   NIST_Writeihdrfile()                       
	//         Author:    Michael D. Garris                     
	//         Date:      4/26/89                               
	//         Modifications:                                   
	//           9/20/90   (Stan Janet) check return codes      
	//           2/20/91   (MDG) compression capability         
	//**********************************************************
	//**********************************************************
	// WriteNISThdrfile() writes a ihead structure and correspon-  
	// ding image data to the specified file.                   
	//**********************************************************
	void NIST_writeihdrfile(char *file,NIST_HEAD *head,unsigned char *data);
	//**********************************************************
	//         Routine:   NIST_printihdr()                           
	//         Package:   NIST Standard Image Header            
	//         Date:      4/26/89                               
	//**********************************************************
	//**********************************************************
	// NIST_printihdr() prints the contents of an ihead structure to 
	// the passed file pointer.                                 
	//**********************************************************
	void NIST_printihdr(NIST_HEAD *head,FILE *fp);
	//**********************************************************
	//         Routine:   NIST_readihdr()                            
	//         Author:    Michael D. Garris                     
	//         Date:      4/26/89                               
	//**********************************************************
	//**********************************************************
	// NIST_readihdr() allocates and reads header information into an
	// NIST_HEAD structure and returns the initialized structure.   
	//**********************************************************
	NIST_HEAD *NIST_readihdr(FILE *fp);
	//**********************************************************
	//         Routine:   NIST_writeihdr()                           
	//         Author:    Michael D. Garris                     
	//         Date:      2/08/90                               
	//**********************************************************
	//**********************************************************
	// NIST_writeihdr() writes the fixed length field and the header 
	// passed to the given file pointer.                        
	//**********************************************************
	void NIST_writeihdr(FILE *fp,NIST_HEAD *ihead);

public:

	//Use to load file (*.PCT) from NIST Database
	void LoadFile(char *fileName,KArray1D<unsigned char> *imgData,int *Width,int *Height);
};

#endif