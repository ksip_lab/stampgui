// ------------------------------------------------ //
// contain:		Interface for OpenCV
//				to create simple 1D graph for display
//
// original:	Changbo (zoccob@gmail).
// developer:	Krisada Phromsuthirak
// ------------------------------------------------ //
//
// Matlab style plot functions for OpenCV
// plot and label:
//
// template<typename T>
// void plot(const string figure_name, const T* p, int count, int step = 1,
//		     int R = -1, int G = -1, int B = -1);
//
// figure_name: required. multiple calls of this function with same figure_name
//              plots multiple curves on a single graph.
// p          : required. pointer to data.
// count      : required. number of data.
// step       : optional. step between data of two points, default 1.
// R, G,B     : optional. assign a color to the curve.
//              if not assigned, the curve will be assigned a unique color automatically.
//
// void label(string lbl):
//
// label the most recently added curve with lbl.
//
// ------------------------------------------------ //

#ifndef _KInterface_OpenCV_cvPlot_H
#define _KInterface_OpenCV_cvPlot_H

#include "KGeneral.h"

namespace cvPlot
{
	// A curve.
	class Series
	{
	public:

		// number of points
		unsigned int count;
		float *data;
		// name of the curve
		string label;

		// allow automatic curve color
		bool auto_color;
		CvScalar color;

		Series(void);
		Series(const Series& s);
		~Series(void);

		// release memory
		void Clear();

		void SetData(int n, float *p);

		void SetColor(CvScalar color, bool auto_color = true);
		void SetColor(int R, int G, int B, bool auto_color = true);
	};

	// a figure comprises of several curves
	class Figure
	{
	private:
		// window name
		string figure_name;
		CvSize figure_size;

		// margin size
		int    border_size;

		CvScalar backgroud_color;
		CvScalar axis_color;
		CvScalar text_color;

		// several curves
		vector<Series> plots;

		// manual or automatic range
		bool custom_range_y;
		float y_max;
		float y_min;

		float y_scale;

		bool custom_range_x;
		float x_max;
		float x_min;

		float x_scale;

		// automatically change color for each curve
		int color_index;

		// KParameter
		int			data_markerSize = 3;
		int			data_lineThickness = 1;
		int			figure_size_width = 500;
		int			figure_size_height = 300;
		CvScalar	figure_backgroud_color = CV_RGB(255, 255, 255);
		CvScalar	figure_axis_color = CV_RGB(0, 0, 0);
		CvScalar	figure_text_color = CV_RGB(0, 0, 0);

	public:
		Figure(const string name);
		~Figure();

		string GetFigureName();
		Series* Add(const Series &s);
		void Clear();
		void DrawLabels(IplImage *output, int posx, int posy);

		// show plot window
		void Show();

		// set parameter
		void SetFigureParameter(int Data_MarkerSize = 3, int Data_LineThickness = 1, int Figure_SizeWidth = 600, int Figure_SizeHeight = 200);
		void SetFigureColor(CvScalar Figure_BackgroudColor = CV_RGB(255, 255, 255), CvScalar Figure_AxisColor = CV_RGB(0, 0, 0), CvScalar Figure_TextColor = CV_RGB(0, 0, 0));

	private:
		Figure();
		void DrawAxis(IplImage *output);
		void DrawPlots(IplImage *output);

		// call before plot
		void Initialize();
		CvScalar GetAutoColor();

	};

	// manage plot windows
	class PlotManager
	{
	private:
		vector<Figure> figure_list;
		Series *active_series;
		Figure *active_figure;

	public:

		// now useless
		bool HasFigure(string wnd);
		Figure* FindFigure(string wnd);

		void Plot(const string figure_name, const float* p, int count, int step, int R, int G, int B, int Data_MarkerSize, int Data_LineThickness, int Figure_SizeWidth, int Figure_SizeHeight, CvScalar Figure_BackgroudColor, CvScalar Figure_AxisColor, CvScalar Figure_TextColor);

		void Label(string lbl);

	};

	// handle different data types; static mathods;
	template<typename T> void plot(const string figure_name, const T* p, int count, int step = 1, int R = -1, int G = -1, int B = -1, int Figure_SizeWidth = 600, int Figure_SizeHeight = 200, int Data_MarkerSize = 3, int Data_LineThickness = 1, CvScalar Figure_BackgroudColor = CV_RGB(255, 255, 255), CvScalar Figure_AxisColor = CV_RGB(0, 0, 0), CvScalar Figure_TextColor = CV_RGB(0, 0, 0));
	void clear(const string figure_name);
	void label(string lbl);

};


#endif