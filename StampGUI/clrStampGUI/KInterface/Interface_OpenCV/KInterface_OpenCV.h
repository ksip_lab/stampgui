// ----------------------------------------------------------- //
// contain:		Interface for OpenCV
//
// developer:	Krisada Phromsuthirak
// ----------------------------------------------------------- //
//
// =========================================================== //
// OpenCV data types
// =========================================================== //
// CV_8U	8 - bit		unsigned integer			uchar
// CV_8S	8 - bit		signed interger				schar
// CV_16U	16 - bit	unsigned integer			ushort
// CV_16S	16 - bit	signed integer 				short
// CV_32S	32 - bit	signed integer 				int
// CV_32F	32 - bit	floating - point number		float
// CV_64F	64 - bit	floating - point number		double
// =========================================================== //
// C		Channel
// =========================================================== //
// CV_8UC3:		3-channel, 8-bit unsigned integer
// CV_64FC2:	2-channel, 64-bit floating-point number
// CV_32F:		1-channel, 32-bit floating-point number
// =========================================================== //

#ifndef _KInterface_OpenCV_H
#define _KInterface_OpenCV_H

#include "KGeneral.h"

class KInterface_OpenCV
{
private:

public:

	KInterface_OpenCV();
	~KInterface_OpenCV();

	template <typename T> void ConvertKArray1DToMat(KArray1D<T> *in, Mat *out);
	template <typename T> void ConvertKArray1DToMat(KArray1D<T> &in, Mat &out);
	template <typename T> void ConvertKArray2DToMat(KArray2D<T> *in, Mat *out);
	template <typename T> void ConvertKArray2DToMat(KArray2D<T> &in, Mat &out);
	template <typename T> void ConvertKArray3DToMat(KArray3D<T> *in, Mat *out);
	template <typename T> void ConvertKArray3DToMat(KArray3D<T> &in, Mat &out);

	template <typename T> void ConvertMatToKArray1D(Mat *in, KArray1D<T> *out);
	template <typename T> void ConvertMatToKArray1D(Mat &in, KArray1D<T> &out);
	template <typename T> void ConvertMatToKArray2D(Mat *in, KArray2D<T> *out);
	template <typename T> void ConvertMatToKArray2D(Mat &in, KArray2D<T> &out);
	template <typename T> void ConvertMatToKArray3D(Mat *in, KArray3D<T> *out);
	template <typename T> void ConvertMatToKArray3D(Mat &in, KArray3D<T> &out);

	template <typename T> void ConvertKArray2DToIplImage(KArray2D<T> *in, IplImage *out);
	template <typename T> void ConvertKArray3DToIplImage(KArray3D<T> *in, IplImage *out);

	template <typename T> void ConvertIplImageToKArray2D(IplImage *in, KArray2D<T> *out);
	template <typename T> void ConvertIplImageToKArray3D(IplImage *in, KArray3D<T> *out);

	template <typename T> void ConvertListOfArray2DToIplImage(KList1D<KArray2D<T>*> *in, IplImage *out);
	template <typename T> void ConvertIplImageToListOfArray2D(IplImage *in, KList1D<KArray2D<T>*> *out);
	
	template <typename T> void ReadImage_Grayscale(string &fileName, KArray2D<T> *img);
	template <typename T> void ReadImage_Grayscale(char *fileName, KArray2D<T> *img);
	template <typename T> void ReadImage_Grayscale(string &fileName, KArray2D<T> *img, int stx, int sty, int endx, int endy);
	template <typename T> void ReadImage_Grayscale(char *fileName, KArray2D<T> *img, int stx, int sty, int endx, int endy);

	template <typename T> void ReadImage_Color(string &fileName, KArray3D<T> *img);
	template <typename T> void ReadImage_Color(char *fileName, KArray3D<T> *img);
	template <typename T> void ReadImage_Color(string &fileName, KArray3D<T> *img, int stx, int sty, int endx, int endy);
	template <typename T> void ReadImage_Color(char *fileName, KArray3D<T> *img, int stx, int sty, int endx, int endy);
};
template <typename T> void KInterface_OpenCV::ConvertKArray1DToMat(KArray1D<T> *in, Mat *out)
{
	if (!in->isAlloc)
		ErrorMessage("Error [IOpenCV_ConvertKArray1DToMat]: [in] is not Allocate");

	if (typeid(*in->idata) == typeid(uchar))
		out->create(1, in->length, CV_8U);
	else if (typeid(*in->idata) == typeid(schar))
		out->create(1, in->length, CV_8S);
	else if (typeid(*in->idata) == typeid(ushort))
		out->create(1, in->length, CV_16U);
	else if (typeid(*in->idata) == typeid(short))
		out->create(1, in->length, CV_16S);
	else if (typeid(*in->idata) == typeid(int))
		out->create(1, in->length, CV_32S);
	else if (typeid(*in->idata) == typeid(float))
		out->create(1, in->length, CV_32F);
	else if (typeid(*in->idata) == typeid(double))
		out->create(1, in->length, CV_64F);
	else
		ErrorMessage("Error [IOpenCV_ConvertKArray1DToMat]: [in] data type not support");

	for (int x = 0; x < in->length; x++)
		out->at<T>(0, x) = (T)in->idata[x];
}
template <typename T> void KInterface_OpenCV::ConvertKArray1DToMat(KArray1D<T> &in, Mat &out)
{
	ConvertKArray1DToMat(&in, &out);
}
template <typename T> void KInterface_OpenCV::ConvertKArray2DToMat(KArray2D<T> *in, Mat *out)
{
	if (!in->isAlloc)
		ErrorMessage("Error [IOpenCV_ConvertKArray2DToMat]: [in] is not Allocate");

	if (typeid(**in->idata) == typeid(uchar))
		out->create(in->height, in->width, CV_8U);
	else if (typeid(**in->idata) == typeid(schar))
		out->create(in->height, in->width, CV_8S);
	else if (typeid(**in->idata) == typeid(ushort))
		out->create(in->height, in->width, CV_16U);
	else if (typeid(**in->idata) == typeid(short))
		out->create(in->height, in->width, CV_16S);
	else if (typeid(**in->idata) == typeid(int))
		out->create(in->height, in->width, CV_32S);
	else if (typeid(**in->idata) == typeid(float))
		out->create(in->height, in->width, CV_32F);
	else if (typeid(**in->idata) == typeid(double))
		out->create(in->height, in->width, CV_64F);
	else
		ErrorMessage("Error [IOpenCV_ConvertKArray2DToMat]: [in] data type not support");

	for (int y = 0; y < in->height; y++)
	for (int x = 0; x < in->width; x++)
		out->at<T>(y, x) = (T)in->idata[x][y];
}
template <typename T> void KInterface_OpenCV::ConvertKArray2DToMat(KArray2D<T> &in, Mat &out)
{
	ConvertKArray2DToMat(&in, &out);
}
template <typename T> void KInterface_OpenCV::ConvertKArray3DToMat(KArray3D<T> *in, Mat *out)
{
	if (!in->isAlloc)
		ErrorMessage("Error [IOpenCV_ConvertKArray3DToMat]: [in] is not Allocate");

	if (typeid(***in->idata) == typeid(uchar))
		out->create(in->height, in->width, CV_8UC3);
	else if (typeid(***in->idata) == typeid(schar))
		out->create(in->height, in->width, CV_8SC3);
	else if (typeid(***in->idata) == typeid(ushort))
		out->create(in->height, in->width, CV_16UC3);
	else if (typeid(***in->idata) == typeid(short))
		out->create(in->height, in->width, CV_16SC3);
	else if (typeid(***in->idata) == typeid(int))
		out->create(in->height, in->width, CV_32SC3);
	else if (typeid(***in->idata) == typeid(float))
		out->create(in->height, in->width, CV_32FC3);
	else if (typeid(***in->idata) == typeid(double))
		out->create(in->height, in->width, CV_64FC3);
	else
		ErrorMessage("Error [IOpenCV_ConvertKArray3DToMat]: [in] data type not support");

	for (int y = 0; y < in->height; y++)
	for (int x = 0; x < in->width; x++)
	{
		out->at<Vec<T, 3>>(y, x)[2] = (T)in->idata[0][x][y];
		out->at<Vec<T, 3>>(y, x)[1] = (T)in->idata[1][x][y];
		out->at<Vec<T, 3>>(y, x)[0] = (T)in->idata[2][x][y];
	}
}
template <typename T> void KInterface_OpenCV::ConvertKArray3DToMat(KArray3D<T> &in, Mat &out)
{
	ConvertKArray3DToMat(&in, &out);
}
template <typename T> void KInterface_OpenCV::ConvertMatToKArray1D(Mat *in, KArray1D<T> *out)
{
	out->Resize(in->cols);
	if ((in->type() == CV_8U) || (in->type() == CV_8UC1))
	{
		for (int x = 0; x < out->length; x++)
			out->idata[x] = (T)in->at<uchar>(0, x);
	}
	else if ((in->type() == CV_8S) || (in->type() == CV_8SC1))
	{
		for (int x = 0; x < out->length; x++)
			out->idata[x] = (T)in->at<schar>(0, x);
	}
	else if ((in->type() == CV_16U) || (in->type() == CV_16UC1))
	{
		for (int x = 0; x < out->length; x++)
			out->idata[x] = (T)in->at<ushort>(0, x);
	}
	else if ((in->type() == CV_16S) || (in->type() == CV_16SC1))
	{
		for (int x = 0; x < out->length; x++)
			out->idata[x] = (T)in->at<short>(0, x);
	}
	else if ((in->type() == CV_32S) || (in->type() == CV_32SC1))
	{
		for (int x = 0; x < out->length; x++)
			out->idata[x] = (T)in->at<int>(0, x);
	}
	else if ((in->type() == CV_32F) || (in->type() == CV_32FC1))
	{
		for (int x = 0; x < out->length; x++)
			out->idata[x] = (T)in->at<float>(0, x);
	}
	else if ((in->type() == CV_64F) || (in->type() == CV_64FC1))
	{
		for (int x = 0; x < out->length; x++)
			out->idata[x] = (T)in->at<double>(0, x);
	}
	else
		ErrorMessage("Error [IOpenCV_ConvertMatToKArray1D]: cannot handle this data type");
}
template <typename T> void KInterface_OpenCV::ConvertMatToKArray1D(Mat &in, KArray1D<T> &out)
{
	ConvertMatToKArray1D(&in, &out);
}
template <typename T> void KInterface_OpenCV::ConvertMatToKArray2D(Mat *in, KArray2D<T> *out)
{
	out->Resize(in->cols, in->rows);
	if ((in->type() == CV_8U) || (in->type() == CV_8UC1))
	{
		for (int y = 0; y < out->height; y++)
		for (int x = 0; x < out->width; x++)
			out->idata[x][y] = (T)in->at<uchar>(y, x);
	}
	else if ((in->type() == CV_8S) || (in->type() == CV_8SC1))
	{
		for (int y = 0; y < out->height; y++)
		for (int x = 0; x < out->width; x++)
			out->idata[x][y] = (T)in->at<schar>(y, x);
	}
	else if ((in->type() == CV_16U) || (in->type() == CV_16UC1))
	{
		for (int y = 0; y < out->height; y++)
		for (int x = 0; x < out->width; x++)
			out->idata[x][y] = (T)in->at<ushort>(y, x);
	}
	else if ((in->type() == CV_16S) || (in->type() == CV_16SC1))
	{
		for (int y = 0; y < out->height; y++)
		for (int x = 0; x < out->width; x++)
			out->idata[x][y] = (T)in->at<short>(y, x);
	}
	else if ((in->type() == CV_32S) || (in->type() == CV_32SC1))
	{
		for (int y = 0; y < out->height; y++)
		for (int x = 0; x < out->width; x++)
			out->idata[x][y] = (T)in->at<int>(y, x);
	}
	else if ((in->type() == CV_32F) || (in->type() == CV_32FC1))
	{
		for (int y = 0; y < out->height; y++)
		for (int x = 0; x < out->width; x++)
			out->idata[x][y] = (T)in->at<float>(y, x);
	}
	else if ((in->type() == CV_64F) || (in->type() == CV_64FC1))
	{
		for (int y = 0; y < out->height; y++)
		for (int x = 0; x < out->width; x++)
			out->idata[x][y] = (T)in->at<double>(y, x);
	}
	else
		ErrorMessage("Error [IOpenCV_ConvertMatToKArray2D]: cannot handle this data type");
}
template <typename T> void KInterface_OpenCV::ConvertMatToKArray2D(Mat &in, KArray2D<T> &out)
{
	ConvertMatToKArray2D(&in, &out);
}
template <typename T> void KInterface_OpenCV::ConvertMatToKArray3D(Mat *in, KArray3D<T> *out)
{
	out->Resize(3, in->cols, in->rows);
	if (in->type() == CV_8UC3)
	{
		for (int y = 0; y < out->height; y++)
		for (int x = 0; x < out->width; x++)
		{
			out->idata[0][x][y] = (T)in->at<Vec<uchar, 3>>(y, x)[2];
			out->idata[1][x][y] = (T)in->at<Vec<uchar, 3>>(y, x)[1];
			out->idata[2][x][y] = (T)in->at<Vec<uchar, 3>>(y, x)[0];
		}
	}
	else if (in->type() == CV_8SC3)
	{
		for (int y = 0; y < out->height; y++)
		for (int x = 0; x < out->width; x++)
		{
			out->idata[0][x][y] = (T)in->at<Vec<schar, 3>>(y, x)[2];
			out->idata[1][x][y] = (T)in->at<Vec<schar, 3>>(y, x)[1];
			out->idata[2][x][y] = (T)in->at<Vec<schar, 3>>(y, x)[0];
		}
	}
	else if (in->type() == CV_16UC3)
	{
		for (int y = 0; y < out->height; y++)
		for (int x = 0; x < out->width; x++)
		{
			out->idata[0][x][y] = (T)in->at<Vec<ushort, 3>>(y, x)[2];
			out->idata[1][x][y] = (T)in->at<Vec<ushort, 3>>(y, x)[1];
			out->idata[2][x][y] = (T)in->at<Vec<ushort, 3>>(y, x)[0];
		}
	}
	else if (in->type() == CV_16SC3)
	{
		for (int y = 0; y < out->height; y++)
		for (int x = 0; x < out->width; x++)
		{
			out->idata[0][x][y] = (T)in->at<Vec<short, 3>>(y, x)[2];
			out->idata[1][x][y] = (T)in->at<Vec<short, 3>>(y, x)[1];
			out->idata[2][x][y] = (T)in->at<Vec<short, 3>>(y, x)[0];
		}
	}
	else if (in->type() == CV_32SC3)
	{
		for (int y = 0; y < out->height; y++)
		for (int x = 0; x < out->width; x++)
		{
			out->idata[0][x][y] = (T)in->at<Vec<int, 3>>(y, x)[2];
			out->idata[1][x][y] = (T)in->at<Vec<int, 3>>(y, x)[1];
			out->idata[2][x][y] = (T)in->at<Vec<int, 3>>(y, x)[0];
		}
	}
	else if (in->type() == CV_32FC3)
	{
		for (int y = 0; y < out->height; y++)
		for (int x = 0; x < out->width; x++)
		{
			out->idata[0][x][y] = (T)in->at<Vec<float, 3>>(y, x)[2];
			out->idata[1][x][y] = (T)in->at<Vec<float, 3>>(y, x)[1];
			out->idata[2][x][y] = (T)in->at<Vec<float, 3>>(y, x)[0];
		}
	}
	else if (in->type() == CV_64FC3)
	{
		for (int y = 0; y < out->height; y++)
		for (int x = 0; x < out->width; x++)
		{
			out->idata[0][x][y] = (T)in->at<Vec<double, 3>>(y, x)[2];
			out->idata[1][x][y] = (T)in->at<Vec<double, 3>>(y, x)[1];
			out->idata[2][x][y] = (T)in->at<Vec<double, 3>>(y, x)[0];
		}
	}
	else
		ErrorMessage("Error [IOpenCV_ConvertMatToKArray3D]: cannot handle this data type");
}
template <typename T> void KInterface_OpenCV::ConvertMatToKArray3D(Mat &in, KArray3D<T> &out)
{
	ConvertMatToKArray3D(&in, &out);
}

template <typename T> void KInterface_OpenCV::ConvertKArray2DToIplImage(KArray2D<T> *in, IplImage *out)
{
	if (in->isAlloc)
	{
		out = cvCreateImage(cvSize(in->width, in->height), IPL_DEPTH_8U, 1);

		for (int y = 0; y < in->height; y++)
		for (int x = 0; x < in->width; x++)
			out->imageData[y*out->widthStep + x*out->nChannels] = (char)in->idata[x][y];
	}
	else
		ErrorMessage("Error [KInterface_OpenCV::ConvertKArray2DToIplImage] <in> hasn't allocated");
}
template <typename T> void KInterface_OpenCV::ConvertKArray3DToIplImage(KArray3D<T> *in, IplImage *out)
{
	if (in->isAlloc)
	{
		out = cvCreateImage(cvSize(in->width, in->height), IPL_DEPTH_8U, 3);

		for (int d = 0; d < in->depth; d++)
		for (int y = 0; y < in->height; y++)
		for (int x = 0; x < in->width; x++)
			out->imageData[y*out->widthStep + x*out->nChannels + d] = (char)in->idata[2 - d][x][y];
	}
	else
		ErrorMessage("Error [KInterface_OpenCV::ConvertKArray2DToIplImage] <in> hasn't allocated");
}
template <typename T> void KInterface_OpenCV::ConvertIplImageToKArray2D(IplImage *in, KArray2D<T> *out)
{
	if (out->isAlloc || out->width != in->width || out->height != in->height)
		out->Resize(in->width, in->height);

	for (int y = 0; y < out->height; y++)
	for (int x = 0; x < out->width; x++)
		out->idata[x][y] = (T)in->imageData[y*in->widthStep + x*in->nChannels];
}
template <typename T> void KInterface_OpenCV::ConvertIplImageToKArray3D(IplImage *in, KArray3D<T> *out)
{
	if (out->isAlloc || out->width != in->width || out->height != in->height)
		out->Resize(in->width, in->height);

	for (int d = 0; d < out->depth; d++)
	for (int y = 0; y < out->height; y++)
	for (int x = 0; x < out->width; x++)
		out->idata[2 - d][x][y] = (T)in->imageData[y*in->widthStep + x*in->nChannels + d];
}
template <typename T> void KInterface_OpenCV::ConvertListOfArray2DToIplImage(KList1D<KArray2D<T>*> *in, IplImage *out)
{
	if (in->size()>0)
	{
		out = cvCreateImage(cvSize(in->at(0)->width, in->at(0)->height), IPL_DEPTH_8U, in->size());

		for (int d = 0; d < in->size(); d++)
		for (int y = 0; y < in->at(0)->height; y++)
		for (int x = 0; x < in->at(0)->width; x++)
			out->imageData[y*out->widthStep + x*out->nChannels + d] = (char)in->at(d)->idata[x][y];
	}
	else
		ErrorMessage("Error [KInterface_OpenCV::ConvertListOfArrayToIplImage] [in] doesn't have data");
}
template <typename T> void KInterface_OpenCV::ConvertIplImageToListOfArray2D(IplImage *in, KList1D<KArray2D<T>*> *out)
{
	out->clear();
	for (int z = 0; z < in->nChannels; z++)
	{
		out->push_back(new KArray2D<T>);
		out->at(z)->Allocate(in->width, in->height);

		for (int y = 0; y < in->height; y++)
		for (int x = 0; x < in->width; x++)
			out->at(z)->idata[x][y] = (T)in->imageData[y*in->widthStep + x*in->nChannels + z];
	}
}

template <typename T> void KInterface_OpenCV::ReadImage_Grayscale(string &fileName, KArray2D<T> *img)
{
	Mat image;
	image = imread(fileName, CV_LOAD_IMAGE_GRAYSCALE);
	img->Resize(image.cols, image.rows);
	for (int i = 0; i < image.rows; i++)
	for (int j = 0; j < image.cols; j++)
	{
		img->idata[j][i] = (T)image.at<uchar>(i, j);
	}
}
template <typename T> void KInterface_OpenCV::ReadImage_Grayscale(char *fileName, KArray2D<T> *img)
{
	ReadImage_Grayscale(ToString(fileName), img);
}
template <typename T> void KInterface_OpenCV::ReadImage_Grayscale(string &fileName, KArray2D<T> *img, int stx, int sty, int endx, int endy)
{
	Mat image;
	image = imread(fileName, CV_LOAD_IMAGE_GRAYSCALE);
	img->Resize(endx - stx, endy - sty);
	for (int i = sty; i < endy; i++)
	for (int j = stx; j < endx; j++)
	{
		img->idata[j - stx][i - sty] = (T)image.at<uchar>(i, j);
	}
}
template <typename T> void KInterface_OpenCV::ReadImage_Grayscale(char *fileName, KArray2D<T> *img, int stx, int sty, int endx, int endy)
{
	ReadImage_Grayscale(ToString(fileName), img, stx, sty, endx, endy);
}
template <typename T> void KInterface_OpenCV::ReadImage_Color(string &fileName, KArray3D<T> *img)
{
	Mat image;
	image = imread(fileName, CV_LOAD_IMAGE_COLOR);
	img->Resize(3, image.cols, image.rows);
	for (int i = 0; i < image.rows; i++)
	for (int j = 0; j < image.cols; j++)
	{
		Vec3b& rgb = image.at<Vec3b>(i, j);
		img->idata[0][j][i] = (T)rgb[2];
		img->idata[1][j][i] = (T)rgb[1];
		img->idata[2][j][i] = (T)rgb[0];
	}
}
template <typename T> void KInterface_OpenCV::ReadImage_Color(char *fileName, KArray3D<T> *img)
{
	ReadImage_Color(ToString(fileName), img);
}
template <typename T> void KInterface_OpenCV::ReadImage_Color(string &fileName, KArray3D<T> *img, int stx, int sty, int endx, int endy)
{
	Mat image;
	image = imread(fileName, CV_LOAD_IMAGE_COLOR);
	img->Resize(endx - stx, endy - sty);
	for (int i = sty; i < endy; i++)
	for (int j = stx; j < endx; j++)
	{
		Vec3b& rgb = image.at<Vec3b>(i, j);
		img->idata[0][j - stx][i - sty] = (T)rgb[2];
		img->idata[1][j - stx][i - sty] = (T)rgb[1];
		img->idata[2][j - stx][i - sty] = (T)rgb[0];
	}
}
template <typename T> void KInterface_OpenCV::ReadImage_Color(char *fileName, KArray3D<T> *img, int stx, int sty, int endx, int endy)
{
	ReadImage_Color(ToString(fileName), img, stx, sty, endx, endy);
}

#endif