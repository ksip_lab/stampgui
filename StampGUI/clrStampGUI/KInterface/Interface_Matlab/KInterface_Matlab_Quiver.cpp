#include "KInterface_Matlab_Quiver.h"

KInterface_Matlab_Quiver::KInterface_Matlab_Quiver()
{
	numData = 0;

	label_Title = "Untitled";
	label_xAxis = "";
	label_yAxis = "";

	BackgroundColor = KINTERFACE_MATLAB_COLOR_WHITE;
	GridColor = KINTERFACE_MATLAB_COLOR_BLACK;

	haveMeshGridData = false;
	haveContour = false;

	isSetColorMap = false;
	isSetOrientationMode = false;
	isSetXTickLabel = false;
	isSetYTickLabel = false;
	isSetAxisLimit_X = false;
	isSetAxisLimit_Y = false;
	isSetInvertX = false;
	isSetInvertY = true;
	isShowLegend = false;
	isShowGridX = false;
	isShowGridY = false;
	isShowBox = false;
	isLogScaleGridX = false;
	isLogScaleGridY = false;
	isContourShowText = false;
	isContourShading = false;
	isSetNewFigure = false;
}
KInterface_Matlab_Quiver::~KInterface_Matlab_Quiver()
{
	ClearMemory();
}
void KInterface_Matlab_Quiver::ClearData()
{
	meshGridX.FreeMem();
	meshGridY.FreeMem();
	contour.FreeMem();

	for (unsigned int z = 0; z < listDataGX.size(); z++)
	{
		Free<double>(listDataGX.at(z), listWidth[z], listHeight[z]);
		Free<double>(listDataGY.at(z), listWidth[z], listHeight[z]);
	}

	listDataGX.FreeMem();
	listDataGY.FreeMem();
	listWidth.FreeMem();
	listHeight.FreeMem();

	list_DataLabel.FreeMem();
	list_lineStyle.FreeMem();
	list_lineSize.FreeMem();
	list_lineColor.FreeMem();

	haveMeshGridData = false;
	haveContour = false;

	isSetXTickLabel = false;
	isSetYTickLabel = false;
	isSetAxisLimit_X = false;
	isSetAxisLimit_Y = false;
	isSetInvertX = false;
	isSetInvertY = true;

	numData = 0;
}
void KInterface_Matlab_Quiver::ClearMemory()
{
	ClearData();

	label_Title = "";
	label_xAxis = "";
	label_yAxis = "";
	isSetColorMap = false;
	isSetOrientationMode = false;
	isShowLegend = false;
	isShowGridX = false;
	isShowGridY = false;
	isShowBox = false;
	isLogScaleGridX = false;
	isLogScaleGridY = false;
	isContourShowText = false;
	isContourShading = false;
	isSetNewFigure = false;

	list_xTick.FreeMem();
	list_yTick.FreeMem();
	list_xLabel.FreeMem();
	list_yLabel.FreeMem();
}
void KInterface_Matlab_Quiver::Set_ColorMap(string ColorMap)
{
	MapColor = ColorMap;
	isSetColorMap = true;
}
void KInterface_Matlab_Quiver::Set_OrientationMode(bool OrientationMode)
{
	isSetOrientationMode = OrientationMode;
}
void KInterface_Matlab_Quiver::Set_ShowLegend(bool ShowLegend)
{
	isShowLegend = ShowLegend;
}
void KInterface_Matlab_Quiver::Set_ShowGrid(bool ShowGridX, bool ShowGridY)
{
	isShowGridX = ShowGridX;
	isShowGridY = ShowGridY;
}
void KInterface_Matlab_Quiver::Set_ShowBox(bool ShowBox)
{
	isShowBox = ShowBox;
}
void KInterface_Matlab_Quiver::Set_LogScale(bool LogScaleGridX, bool LogScaleGridY)
{
	isLogScaleGridX = LogScaleGridX;
	isLogScaleGridY = LogScaleGridY;
}
void KInterface_Matlab_Quiver::Set_FigureColor(string ColorForBackground, string ColorForGrid)
{
	BackgroundColor = ColorForBackground;
	GridColor = ColorForGrid;
}
void KInterface_Matlab_Quiver::Set_Label_Title(string Label_Title)
{
	label_Title = Label_Title;
}
void KInterface_Matlab_Quiver::Set_Label_xAxis(string Label_xAxis)
{
	label_xAxis = Label_xAxis;
}
void KInterface_Matlab_Quiver::Set_Label_yAxis(string Label_yAxis)
{
	label_yAxis = Label_yAxis;
}
void KInterface_Matlab_Quiver::Set_AxisLimit_X(double xMin, double xMax)
{
	axisLimit_xMin = xMin;
	axisLimit_xMax = xMax;
	if (!isSetAxisLimit_X)
		isSetAxisLimit_X = true;
}
void KInterface_Matlab_Quiver::Set_AxisLimit_Y(double yMin, double yMax)
{
	axisLimit_yMin = yMin;
	axisLimit_yMax = yMax;
	if (!isSetAxisLimit_Y)
		isSetAxisLimit_Y = true;
}
void KInterface_Matlab_Quiver::Set_InvertAxis(bool InvertX, bool InvertY)
{
	isSetInvertX = InvertX;
	isSetInvertY = InvertY;
}
void KInterface_Matlab_Quiver::Set_NewFigure(bool NewFigure)
{
	isSetNewFigure = NewFigure;
}
void KInterface_Matlab_Quiver::SavePlotFile(string mFilePath, long FigureNumber)
{
	if (FigureNumber <= 0)
		ErrorMessage("FigureNumer should more than zero");

	if (GetFileExtension(mFilePath) != ".m")
		ErrorMessage("should be save in [.m] file");

	if (listDataGX.size()!=listDataGY.size())
		ErrorMessage("Error [KInterface_Matlab_Quiver] : number of Gx data not equal to Gy data");

	ofstream	outFile;

	outFile.open((char*)mFilePath.c_str());

	outFile << "clear" << endl;
	if (isSetNewFigure)
		outFile << "figure1 = figure;" << endl << endl;
	else
		outFile << "figure(" << ToString(FigureNumber) << ")" << endl;

	if (isSetColorMap)
		outFile << "colormap(figure(" << ToString(FigureNumber) << ")," << MapColor << ")" << endl;
	outFile << "axes" << ToString(FigureNumber) << "=axes('Parent'," << "figure(" << ToString(FigureNumber) << ")";
	if (isShowGridX)
		outFile << ",'XGrid','on'";
	if (isShowGridY)
		outFile << ",'YGrid','on'";
	if (isLogScaleGridX)
		outFile << ",'XScale','log'";
	if (isLogScaleGridY)
		outFile << ",'YScale','log'";

	outFile << ",'Color'," << ToString(BackgroundColor);
	outFile << ",'XColor'," << ToString(GridColor);
	outFile << ",'YColor'," << ToString(GridColor);

	if (isSetInvertX)
		outFile << ",'XDir','reverse'";
	if (isSetInvertY)
		outFile << ",'YDir','reverse'";

	outFile << ");" << endl;
	if (isShowBox)
		outFile << "box(axes" << ToString(FigureNumber) << ",'on');";
	outFile << "hold(" << "axes" << ToString(FigureNumber) << ",'all');" << endl << endl;

	for (unsigned int z = 0; z < listDataGX.size(); z++)
	{
		outFile << "GX" << ToString(z) << "=[";
		for (int y = 0; y < listHeight.at(z); y++)
		{
			for (int x = 0; x < listWidth.at(z); x++)
			{
				outFile << listDataGX[z][x][y];
				if (x < listWidth.at(z) - 1)
					outFile << " ";
			}
			if (y < listHeight.at(z) - 1)
				outFile << ";";
		}
		outFile << "];" << endl;

		outFile << "GY" << ToString(z) << "=[";
		for (int y = 0; y < listHeight.at(z); y++)
		{
			for (int x = 0; x < listWidth.at(z); x++)
			{
				outFile << listDataGY[z][x][y];
				if (x < listWidth.at(z) - 1)
					outFile << " ";
			}
			if (y < listHeight.at(z) - 1)
				outFile << ";";
		}
		outFile << "];" << endl;
	}

	if (haveMeshGridData)
	{
		outFile << "MX=[";
		for (int y = 0; y < meshGridX.height; y++)
		{
			for (int x = 0; x < meshGridX.width; x++)
			{
				outFile << meshGridX.idata[x][y];
				if (x < meshGridX.width - 1)
					outFile << " ";
			}
			if (y < meshGridX.height - 1)
				outFile << ";";
		}
		outFile << "];" << endl;

		outFile << "MY=[";
		for (int y = 0; y < meshGridY.height; y++)
		{
			for (int x = 0; x < meshGridY.width; x++)
			{
				outFile << meshGridY.idata[x][y];
				if (x < meshGridY.width - 1)
					outFile << " ";
			}
			if (y < meshGridY.height - 1)
				outFile << ";";
		}
		outFile << "];" << endl;
	}

	outFile << "hold(" << "axes" << ToString(FigureNumber) << ",'all');" << endl << endl;

	if (haveContour)
	{
		outFile << "CT=[";
		for (int y = 0; y < contour.height; y++)
		{
			for (int x = 0; x < contour.width; x++)
			{
				outFile << contour.idata[x][y];
				if (x < contour.width - 1)
					outFile << " ";
			}
			if (y < contour.height - 1)
				outFile << ";";
		}
		outFile << "];" << endl;
		if (!haveMeshGridData)
		{
			if (isContourShading)
				outFile << "contourf(CT";
			else
				outFile << "contour(CT";
		}
		else
		{
			if (isContourShading)
				outFile << "contourf(MX,MY,CT";
			else
				outFile << "contour(MX,MY,CT";
		}			

		if (contourLineNumber>0)
			outFile << "," << contourLineNumber;

		if (isContourShowText)
			outFile << ",'ShowText','on'";

		outFile << ")" << endl << endl;
	}

	for (unsigned int z = 0; z < listDataGX.size(); z++)
	{
		if (!haveMeshGridData)
			outFile << "quiver(";
		else
			outFile << "quiver(MX,MY,";

		outFile << "GX" << z << "," << "GY" << z;

		outFile << ",'Color'," << ToString(list_lineColor.at(z));
		outFile << ",'LineStyle'," << ToString(list_lineStyle.at(z));
		outFile << ",'LineWidth'," << ToString(list_lineSize.at(z));
		outFile << ",'DisplayName','" << list_DataLabel.at(z) << "'";

		if (isSetOrientationMode)
			outFile << ",'ShowArrowHead','off','AutoScaleFactor',0.3";

		outFile << ")";

		if (isSetOrientationMode)
		{
			outFile << endl;
			if (!haveMeshGridData)
				outFile << "quiver(";
			else
				outFile << "quiver(MX,MY,";

			outFile << "-GX" << z << "," << "-GY" << z;
			outFile << ",'Color'," << ToString(list_lineColor.at(z));
			outFile << ",'LineStyle'," << ToString(list_lineStyle.at(z));
			outFile << ",'LineWidth'," << ToString(list_lineSize.at(z));
			outFile << ",'DisplayName','" << list_DataLabel.at(z) << "'";
			outFile << ",'ShowArrowHead','off','AutoScaleFactor',0.3";

			outFile << ")";
		}
	}

	outFile << endl;

	if (isSetAxisLimit_X)
	{
		outFile << "xlim(" << "axes" << ToString(FigureNumber) << ",[" << axisLimit_xMin << " " << axisLimit_xMax << "]);" << endl;
		outFile << endl;
	}
	if (isSetAxisLimit_Y)
	{
		outFile << "ylim(" << "axes" << ToString(FigureNumber) << ",[" << axisLimit_yMin << " " << axisLimit_yMax << "]);" << endl;
		outFile << endl;
	}

	outFile << "title(" << "'" << label_Title << "'" << ");" << endl;
	outFile << "xlabel(" << "'" << label_xAxis << "'" << ");" << endl;
	outFile << "ylabel(" << "'" << label_yAxis << "'" << ");" << endl;

	outFile << endl;
	if (isShowLegend)
		outFile << "legend(" << "axes" << ToString(FigureNumber) << ",'show');" << endl << endl;

	if (isSetXTickLabel)
	{
		outFile << "set(gca,'XTick',[";
		for (unsigned int t = 0; t<list_xTick.size(); t++)
		{
			outFile << ToString(list_xTick.at(t));
			if (t<list_xTick.size() - 1)
				outFile << " ";
		}
		outFile << "]);" << endl;

		outFile << "set(gca,'XTickLabel',{";
		for (unsigned int t = 0; t<list_xTick.size(); t++)
		{
			outFile << "'" << ToString(list_xLabel.at(t)) << "'";
			if (t<list_xLabel.size() - 1)
				outFile << ",";
		}
		outFile << "});" << endl;
	}
	if (isSetYTickLabel)
	{
		outFile << "set(gca,'YTick',[";
		for (unsigned int t = 0; t<list_yTick.size(); t++)
		{
			outFile << ToString(list_yTick.at(t));
			if (t<list_yTick.size() - 1)
				outFile << " ";
		}
		outFile << "]);" << endl;

		outFile << "set(gca,'YTickLabel',{";
		for (unsigned int t = 0; t<list_yTick.size(); t++)
		{
			outFile << "'" << ToString(list_yLabel.at(t)) << "'";
			if (t<list_yLabel.size() - 1)
				outFile << ",";
		}
		outFile << "});" << endl;
	}

	outFile.close();
}