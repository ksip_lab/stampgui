// ------------------------------------------------ //
// contain:		Interface for Matlab
//
// developer:	Krisada Phromsuthirak
// ------------------------------------------------ //

#ifndef _KInterface_Matlab_H
#define _KInterface_Matlab_H

#include "KHeader.h"
#include "KInterface_Matlab_PlotLine.h"
#include "KInterface_Matlab_Quiver.h"

class KInterface_Matlab
{
private:

	bool	isShowContour;

public:

	KInterface_Matlab();
	~KInterface_Matlab();

	KInterface_Matlab_PlotLine	PlotLine;
	KInterface_Matlab_Quiver	Quiver;

	void ClearMemory();

};

#endif