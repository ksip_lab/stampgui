// This is the main DLL file.

//#include "stdafx.h"
#include "clrStampGUI.h"
#include <msclr\marshal_cppstd.h>

#include "OCR.h"
#include "OCR.cpp"

using namespace StampOCR;

std::string StampOCRGUI::SystemStringToStdString(System::String^ str)
{
	msclr::interop::marshal_context context;
	std::string standardString = context.marshal_as<std::string>(str);
	return standardString;
}

System::String^ StampOCRGUI::StdStringToSystemString(std::string str)
{
	System::String^ str2 = gcnew System::String(str.c_str());
	return str2;
}

void StampOCRGUI::ListStringToVector(List<System::String^>^ list, std::vector<string> *result)
{
	result->clear();
	//if (list == nullptr) throw gcnew ArgumentNullException(L"list");
	for each (System::String^ elem in list)
	{
		string elem2 = SystemStringToStdString(elem);
		result->push_back(elem2);
	}
}

void StampOCRGUI::VectorStringToList(vector<string> vect, List<System::String^>^ list_tmp)
{
	for (int i = 0; i < vect.size(); i++)
	{
		System::String^ str = StdStringToSystemString(vect[i]);
		list_tmp->Add(str);
		//list_tmp.Add(str);
	}
}

StampOCRGUI::StampOCRGUI()
{
	cppStampOCR = new ClassStampOCR();
	cppStampOCR->CopyParameter(this->dark_val, this->dark_thr, this->box_size, this->num_char, this->feature_sampling_rate, this->training_size, this->training_char_size, this->template_width, this->template_height);
}

void StampOCRGUI::UpdateParameter()
{
	cppStampOCR->CopyParameter(this->dark_val, this->dark_thr, this->box_size, this->num_char, this->feature_sampling_rate, this->training_size, this->training_char_size, this->template_width, this->template_height);
}

void StampOCRGUI::GetActualSerialNumber(System::String^ sn_path, List<System::String^>^ sn_string)
{
	string str = this->SystemStringToStdString(sn_path);
	/*vector<vector<int>> sn_v;
	vector<string> sn_str;*/
	cppStampOCR->GetActualSerialNumber(str, &cppStampOCR->sn_val, &cppStampOCR->sn_str);
	this->VectorStringToList(cppStampOCR->sn_str, sn_string);
}

//void StampOCRGUI::GetActualSerialNumber(System::String^ sn_path, List<List<int>^> sn_value, List<System::String^>^ sn_string)
//{
//	string str = this->SystemStringToStdString(sn_path);
//	/*vector<vector<int>> sn_v;
//	vector<string> sn_str;*/
//	cppStampOCR->GetActualSerialNumber(str, &cppStampOCR->sn_val, &cppStampOCR->sn_str);
//	this->VectorStringToList(cppStampOCR->sn_str, sn_string);
//
//	for (int i = 0;i<sn_va)
//}

void StampOCRGUI::GetFileList(System::String^ folder_path, System::String^ file_extension, List<System::String^>^ train_set)
{
	string path = this->SystemStringToStdString(folder_path);
	string ext = this->SystemStringToStdString(file_extension);
	cppStampOCR->GetFilePath(path, ext, &cppStampOCR->filepath);
	this->VectorStringToList(cppStampOCR->filepath, train_set);
	this->FilePath = train_set;
}

void StampOCRGUI::GetCharacterSize(List<System::String^>^ train_set, int* max_char_width, int* max_char_height)
{
	vector<string> tr_set;
	ListStringToVector(train_set, &tr_set);
	cppStampOCR->GetCharacterSize(tr_set, max_char_width, max_char_height);
}

void StampOCRGUI::GenerateTemplate(List<System::String^>^ train_set, int template_char_width, int template_char_height, System::String^ sav_path)
{
	vector<string> tr_set;
	ListStringToVector(train_set, &tr_set);
	string sav_p = this->SystemStringToStdString(sav_path);
	cppStampOCR->GenerateCharacterTemplate(tr_set, cppStampOCR->sn_val, template_char_width, template_char_height, sav_p, &cppStampOCR->stored_template);
	
	//for (int i = 0; i < cppStampOCR->stored_template.size(); i++)
	//{
	//	List<double>^ tmp_list = gcnew List<double>();
	//	this->VectorToList<double>(cppStampOCR->stored_template.at(i), tmp_list);
	//	//this->VectorToList<double>(cppStampOCR->sn_str, tmp_list);
	//	feature_template.Add(tmp_list);
	//}
}

void StampOCRGUI::GetTemplate(System::String^ template_path)
{
	int template_w, template_h;
	string path = this->SystemStringToStdString(template_path);
	cppStampOCR->stored_template.clear();
	cppStampOCR->GetTemplate(path, &template_w, &template_h, &cppStampOCR->stored_template);
	this->template_width = template_w;
	this->template_height = template_h;
	this->UpdateParameter();
}

void StampOCRGUI::TemplateDownsampling()
{
	cppStampOCR->TemplateDownsampling(&cppStampOCR->stored_template);
}

//void StampOCRGUI::OCRProcess(System::String^ filepath, System::String^ serialnumber, List<int> TL_x, List<int> TL_y, List<int> BR_x, List<int> BR_y, List<double> char_score, double* ocr_score)
//void StampOCRGUI::OCRProcess(System::String^ filepath, System::String^ serialnumber, List<double>^ char_score, double* ocr_score)
//System::String^ StampOCRGUI::OCRProcess(System::String^ filepath, List<double>^ char_score, double* ocr_score)
System::String^ StampOCRGUI::OCRProcess(System::String^ filepath, List<int>^ TL_x, List<int>^ TL_y, List<int>^ BR_x, List<int>^ BR_y, List<double>^ char_score, double* ocr_score)
{
	string fp = this->SystemStringToStdString(filepath);
	string sn;
	vector<double> sc_char;
	vector<KCoor<int>> tl, br;
	cppStampOCR->OCRProcess(fp, &cppStampOCR->stored_template, &tl, &br, &sn, &sc_char, ocr_score);

	for (int i = 0; i < tl.size(); i++)
	{
		TL_x->Add(tl[i].x);
		TL_y->Add(tl[i].y);
		BR_x->Add(br[i].x);
		BR_y->Add(br[i].y);

		char_score->Add(sc_char[i]);
	}

	//serialnumber = "";
	System::String^ sn2 = gcnew System::String(sn.c_str());
	return sn2;
}