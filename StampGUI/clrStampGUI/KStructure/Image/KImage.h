// ------------------------------------------------ //
// contain:		Image Structure
//
// developer:	Krisada Phromsuthirak
// ------------------------------------------------ //

#ifndef _KImage_H
#define _KImage_H

#include "KGImage.h"
#include "KCImage.h"

template <typename T> void GIMAGE_SHOW_MOUSEHANDLER_INFO(int event, int x, int y, int flags, void* param);
template <typename T> void CIMAGE_SHOW_MOUSEHANDLER_INFO(int event, int x, int y, int flags, void* param);

template <typename T> void GIMAGE_SHOW_MOUSEHANDLER_INFO(int event, int x, int y, int flags, void* param)
{
	//===NOTE FOR MOUSE EVENTS AND FLAG===//
	//CV_EVENT_MOUSEMOVE		- Mouse movement
	//CV_EVENT_LBUTTONDOWN		- Left button down
	//CV_EVENT_RBUTTONDOWN		- Right button down
	//CV_EVENT_MBUTTONDOWN		- Middle button down
	//CV_EVENT_LBUTTONUP		- Left button up
	//CV_EVENT_RBUTTONUP		- Right button up
	//CV_EVENT_MBUTTONUP		- Middle button up
	//CV_EVENT_LBUTTONDBLCLK	- Left button double click
	//CV_EVENT_RBUTTONDBLCLK	- Right button double click
	//CV_EVENT_MBUTTONDBLCLK	- Middle button double click

	//CV_EVENT_FLAG_LBUTTON		- Left button pressed
	//CV_EVENT_FLAG_RBUTTON		- Right button pressed
	//CV_EVENT_FLAG_MBUTTON		- Middle button pressed
	//CV_EVENT_FLAG_CTRLKEY		- Control key pressed
	//CV_EVENT_FLAG_SHIFTKEY	- Shift key pressed
	//CV_EVENT_FLAG_ALTKEY		- Alt key pressed
	//====================================//

	KGImage<T> *img = (KGImage<T>*)param;

	if (event == CV_EVENT_MOUSEMOVE)
	{
		if (x >= 0 && x < img->width && y >= 0 && y < img->height)
		{
			cout << "\r(" << x << "," << y << ") : [" << img->idata[x][y] << "]" << setw(30);
		}
	}
	if (event == CV_EVENT_LBUTTONDOWN)
		cout << endl;
}
template <typename T> void CIMAGE_SHOW_MOUSEHANDLER_INFO(int event, int x, int y, int flags, void* param)
{
	//===NOTE FOR MOUSE EVENTS AND FLAG===//
	//CV_EVENT_MOUSEMOVE		- Mouse movement
	//CV_EVENT_LBUTTONDOWN		- Left button down
	//CV_EVENT_RBUTTONDOWN		- Right button down
	//CV_EVENT_MBUTTONDOWN		- Middle button down
	//CV_EVENT_LBUTTONUP		- Left button up
	//CV_EVENT_RBUTTONUP		- Right button up
	//CV_EVENT_MBUTTONUP		- Middle button up
	//CV_EVENT_LBUTTONDBLCLK	- Left button double click
	//CV_EVENT_RBUTTONDBLCLK	- Right button double click
	//CV_EVENT_MBUTTONDBLCLK	- Middle button double click

	//CV_EVENT_FLAG_LBUTTON		- Left button pressed
	//CV_EVENT_FLAG_RBUTTON		- Right button pressed
	//CV_EVENT_FLAG_MBUTTON		- Middle button pressed
	//CV_EVENT_FLAG_CTRLKEY		- Control key pressed
	//CV_EVENT_FLAG_SHIFTKEY	- Shift key pressed
	//CV_EVENT_FLAG_ALTKEY		- Alt key pressed
	//====================================//

	KCImage<T> *img = (KCImage<T>*)param;

	if (event == CV_EVENT_MOUSEMOVE)
	{
		if (x >= 0 && x < img->width && y >= 0 && y < img->height)
		{
			cout << "\r(" << x << "," << y << ") : [" << img->idata[0][x][y] << "][" << img->idata[1][x][y] << "][" << img->idata[2][x][y] << "]" << setw(30);
		}
	}
	if (event == CV_EVENT_LBUTTONDOWN)
		cout << endl;
}

#endif