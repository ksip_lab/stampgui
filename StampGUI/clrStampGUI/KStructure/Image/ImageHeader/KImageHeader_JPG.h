// ------------------------------------------------ //
// contain:		Header of .JPG file
//
// developer:	Krisada Phromsuthirak
// ------------------------------------------------ //

#ifndef _KImageHeader_JPG_H
#define _KImageHeader_JPG_H

#include "KImageHeader.h"

class KImageHeader_JPG : public KImageHeader
{
public:

	int	iQuality;

	KImageHeader_JPG();
	~KImageHeader_JPG();

	friend istream& operator >> (istream& in, KImageHeader_JPG& obj);
	friend ostream& operator << (ostream& out, const KImageHeader_JPG& obj);

};

#endif
