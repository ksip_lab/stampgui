// ------------------------------------------------ //
// contain:		Header of .BMP file
//
// developer:	Krisada Phromsuthirak
// ------------------------------------------------ //

#ifndef _KImageHeader_BMP_H
#define _KImageHeader_BMP_H

#include "KImageHeader.h"

class KImageHeader_BMP : public KImageHeader
{
public:

	int				iPixelPerInch;

	KImageHeader_BMP();
	~KImageHeader_BMP();

	friend istream& operator >> (istream& in, KImageHeader_BMP& obj);
	friend ostream& operator << (ostream& out, const KImageHeader_BMP& obj);

};

#endif
