#include "KImageHeader_BMP.h"

KImageHeader_BMP::KImageHeader_BMP()
{
	iPixelPerInch = 500;
}
KImageHeader_BMP::~KImageHeader_BMP()
{

}
istream& operator>> (istream& in, KImageHeader_BMP& obj)
{
	in >> obj.iPixelPerInch;

	return in;
}
ostream& operator<< (ostream& out, const KImageHeader_BMP& obj)
{
	out << obj.iPixelPerInch << ' ';

	return out;
}