// ------------------------------------------------ //
// contain:		Header of .PNG file
//
// developer:	Krisada Phromsuthirak
// ------------------------------------------------ //

#ifndef _KImageHeader_PNG_H
#define _KImageHeader_PNG_H

#include "KImageHeader.h"

class KImageHeader_PNG : public KImageHeader
{
public:

	int	iCompression;

	KImageHeader_PNG();
	~KImageHeader_PNG();

	friend istream& operator >> (istream& in, KImageHeader_PNG& obj);
	friend ostream& operator << (ostream& out, const KImageHeader_PNG& obj);

};

#endif
