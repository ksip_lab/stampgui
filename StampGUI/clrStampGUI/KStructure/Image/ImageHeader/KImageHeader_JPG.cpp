#include "KImageHeader_JPG.h"

KImageHeader_JPG::KImageHeader_JPG()
{
	iQuality = 95;
}
KImageHeader_JPG::~KImageHeader_JPG()
{

}
istream& operator>> (istream& in, KImageHeader_JPG& obj)
{
	in >> obj.iQuality;

	return in;
}
ostream& operator<< (ostream& out, const KImageHeader_JPG& obj)
{
	out << obj.iQuality << ' ';

	return out;
}