#include "KImageHeader_PNG.h"

KImageHeader_PNG::KImageHeader_PNG()
{
	iCompression = 3;
}
KImageHeader_PNG::~KImageHeader_PNG()
{

}
istream& operator>> (istream& in, KImageHeader_PNG& obj)
{
	in >> obj.iCompression;

	return in;
}
ostream& operator<< (ostream& out, const KImageHeader_PNG& obj)
{
	out << obj.iCompression << ' ';

	return out;
}