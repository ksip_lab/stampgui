#ifndef _KAPI_Feature_H
#define _KAPI_Feature_H

#include "KGeneral.h"
#include "KCoor.h"
#include "KImage.h"
#include "KDraw.h"
#include "KMatrix.h"
#include "KAdjustment.h"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"

#define sl_size 10					//Start search list size
#define ll_size 20					//Last search list size

template <class T> void GetPoint(KGImage<T> *src, vector<KCoor<int>> *list, int contour_color);
template <class T> void KConvexHULL(KGImage<T> *img, vector<KCoor<int>> *contour, vector<KCoor<int>> *Hull, double *peri_hull);
template <class T> void KMinRectBox(KGImage<T> *img, vector<KCoor<int>> *Hull, double *Dmin, double *Dmax, int *m, int *c, double *theta_rect);
template <class T> void KHuMoment(KGImage<T> *img, vector<double> *hu_value, bool IsBinary = true);
template <class T> void GetFeatureValue(KGImage<T> *src, vector<double> *feature);

template <class T> void GetPoint(KGImage<T> *src, vector<KCoor<int>> *list, int contour_color)
{
	vector<KCoor<int>> temp_list, start_list, last_list;
	KCoor<int> temp_point;
	int count = 0;
	//First point and count all point
	for (int j = 0; j<src->height; j++)
	{
		for (int i = 0; i<src->width; i++)
		{
			if (src->idata[i][j] == contour_color)
			{
				if (count == 0)
				{
					temp_point.x = i;
					temp_point.y = j;
					list->push_back(temp_point);
					last_list.push_back(temp_point);
				}
				count++;
			}
		}
	}

	KCoor<int> last_point, last2_point;
	last_point.x = temp_point.x;
	last_point.y = temp_point.y;

	last2_point.x = temp_point.x - 1;
	last2_point.y = temp_point.y - 1;

	if (start_list.size() < sl_size)
		start_list.push_back(temp_point);

	//Get other point
	int loop = 1;
	int fs;
	while (loop == 1)
	{
		fs = 1;
		int num_point = 0;
		do{
			fs += 2;
			for (int j = 0; j<fs; j++)
			{
				for (int i = 0; i <= fs; i++)
				{
					int rx = i - fs / 2, ry = j - fs / 2;
					if ((rx != 0 || ry != 0) && (last_point.x + rx) >= 0 && (last_point.y + ry) >= 0 && (last_point.x + rx)<src->width && (last_point.y + ry)<src->height)
					{
						if (src->idata[last_point.x + rx][last_point.y + ry] == contour_color)
						{
							int k;
							for (k = 0; k < last_list.size(); k++)
							{
								if (last_list.at(k).x == last_point.x + rx && last_list.at(k).y == last_point.y + ry)
									k = last_list.size() * 2;
							}
							if (k == last_list.size())
							{
								num_point++;
								temp_point.x = last_point.x + rx;
								temp_point.y = last_point.y + ry;
								temp_list.push_back(temp_point);
							}
						}
					}
				}
			}
			if (num_point == 0 && fs>20)
			{
				num_point = 1;
				loop = 0;
			}
		} while (num_point == 0);

		if (loop == 1)
		{
			float dist = 99999.0;
			KCoor<int> d_point;
			for (int i = 0; i<num_point; i++)
			{
				int sx = last_point.x - (last2_point.x - last_point.x), sy = last_point.y - (last2_point.y - last_point.y), fx, fy;
				fx = temp_list.at(i).x;
				fy = temp_list.at(i).y;

				double d = sqrt((double)((fx - sx)*(fx - sx) + (fy - sy)*(fy - sy)));
				if (d < dist)
				{
					if ((fx != last_point.x || fy != last_point.y) && (fx != last2_point.x || fy != last2_point.y))
					{
						dist = d;
						d_point.x = fx;
						d_point.y = fy;
					}
				}
			}
			temp_list.clear();
			list->push_back(d_point);
			last_list.push_back(d_point);
			if (last_list.size() >= ll_size)
			{
				last_list.erase(last_list.begin());
			}
			last2_point.x = last_point.x;
			last2_point.y = last_point.y;
			last_point.x = d_point.x;
			last_point.y = d_point.y;
			if (start_list.size() < sl_size)
				start_list.push_back(d_point);
			else
			{
				for (int i = 0; i<sl_size; i++)
				{
					if (d_point.x == start_list.at(i).x && d_point.y == start_list.at(i).y)
						loop = 0;
				}
			}

			if (list->size()>count)
				loop = 0;
		}
	}
}

template <class T> void KConvexHULL(KGImage<T> *img, vector<KCoor<int>> *contour, vector<KCoor<int>> *Hull, double *peri_hull)
{
	vector<cv::Point> con, h;
	for (int i = 0; i < contour->size(); i++)
	{
		cv::Point ptmp;
		ptmp.x = contour->at(i).x;
		ptmp.y = contour->at(i).y;
		con.push_back(ptmp);
	}
	cv::convexHull(Mat(con), h);

	*peri_hull = 0;
	for (int i = 0; i < h.size() - 1; i++)
	{
		KCoor<int> cotmp, cotmp2;
		cotmp.x = h.at(i).x;
		cotmp.y = h.at(i).y;
		cotmp2.x = h.at(i + 1).x;
		cotmp2.y = h.at(i + 1).y;
		KDrawLineP2P<T, int, int>(img, cotmp, cotmp2, 150);
		*peri_hull += FindDistance<int, int>(cotmp, cotmp2);
		if (i == h.size() - 2)
		{
			cotmp.x = h.at(i + 1).x;
			cotmp.y = h.at(i + 1).y;
			cotmp2.x = h.at(0).x;
			cotmp2.y = h.at(0).y;
			KDrawLineP2P<T, int, int>(img, cotmp, cotmp2, 150);
			*peri_hull += FindDistance<int, int>(cotmp, cotmp2);
		}
	}

	for (int i = 0; i < h.size(); i++)
	{
		KCoor<int> tmp;
		tmp.x = h.at(i).x;
		tmp.y = h.at(i).y;
		Hull->push_back(tmp);
	}
}

template <class T> void KMinRectBox(KGImage<T> *img, vector<KCoor<int>> *Hull, double *Dmin, double *Dmax, int *m, int *c, double *theta_rect)
{
	RotatedRect Rect;
	vector<cv::Point> hu;
	for (int i = 0; i < Hull->size(); i++)
	{
		cv::Point tmp;
		tmp.x = Hull->at(i).x;
		tmp.y = Hull->at(i).y;
		hu.push_back(tmp);
	}
	Rect = minAreaRect(hu);

	double theta = Rect.angle;
	double h = Rect.size.height;
	double w = Rect.size.width;

	if (h > w)
	{
		*Dmin = w;
		*Dmax = h;
		*theta_rect = theta;
	}
	else
	{
		*Dmin = h;
		*Dmax = w;
		*theta_rect = theta - 90;
	}

	KCoor<int> pc;
	pc.x = Rect.center.x;
	pc.y = Rect.center.y;

	KCoor<int> ce;
	ce.x = (int)(((double)(h / 2)*cos((theta + 90)*PI / 180) + (double)pc.x) + 0.5);
	ce.y = (int)(((double)(h / 2)*sin((theta + 90)*PI / 180) + (double)pc.y) + 0.5);

	KCoor<int> p1, p2, p3, p4;
	p1.x = (int)(((double)(w / 2)*cos((theta)*PI / 180) + (double)ce.x) + 0.5);
	p1.y = (int)(((double)(w / 2)*sin((theta)*PI / 180) + (double)ce.y) + 0.5);
	p2.x = (int)(((double)(w / 2)*cos((theta + 180)*PI / 180) + (double)ce.x) + 0.5);
	p2.y = (int)(((double)(w / 2)*sin((theta + 180)*PI / 180) + (double)ce.y) + 0.5);
	p3.x = (int)(((double)(h)*cos((theta - 90)*PI / 180) + (double)p1.x) + 0.5);
	p3.y = (int)(((double)(h)*sin((theta - 90)*PI / 180) + (double)p1.y) + 0.5);
	p4.x = (int)(((double)(h)*cos((theta - 90)*PI / 180) + (double)p2.x) + 0.5);
	p4.y = (int)(((double)(h)*sin((theta - 90)*PI / 180) + (double)p2.y) + 0.5);

	KDrawLineP2P<T, int, int>(img, p1, p2, 100);
	KDrawLineP2P<T, int, int>(img, p1, p3, 100);
	KDrawLineP2P<T, int, int>(img, p2, p4, 100);
	KDrawLineP2P<T, int, int>(img, p3, p4, 100);

	if ((pc.x - ce.x) != 0)
		*m = (pc.y - ce.y) / (pc.x - ce.x);
	else
		*m = (pc.y - ce.y) / (pc.x - ce.x + 1);
	*c = pc.y - (*m * pc.x);
}

template <class T> void KHuMoment(KGImage<T> *img, vector<double> *hu_value, bool IsBinary)
{
	KGImage<T> img_tmp;
	img_tmp = *img;
	if (IsBinary)
		img_tmp.Scaling((T)0.0, (T)1.0);

	KCoor<double> centroid;
	centroid.x = 0.0;
	centroid.y = 0.0;
	double sum = 0.0;
	for (int u = 0; u < img_tmp.width; u++)
	{
		for (int v = 0; v < img_tmp.height; v++)
		{
			if (IsBinary)
			{
				if (img_tmp.idata[u][v] == (T)1.0)
				{
					sum++;
					centroid.x += (double)u;
					centroid.y += (double)v;
				}
			}
			else
			{
				sum += (double)img_tmp.idata[u][v];
				centroid.x += (double)img_tmp.idata[u][v] * (double)u;
				centroid.y += (double)img_tmp.idata[u][v] * (double)v;
			}
		}
	}
	centroid /= sum;

	KArray2D<double> central_moment;
	central_moment.Allocate(4, 4);
	for (int p = 0; p < central_moment.width; p++)
	{
		for (int q = 0; q < central_moment.height; q++)
		{
			central_moment.idata[p][q] = 0.0;
			if (p == 0 && q == 0)
			{
				central_moment.idata[p][q] = sum;
			}
			else
			{
				for (int u = 0; u < img_tmp.width; u++)
				{
					for (int v = 0; v < img_tmp.height; v++)
					{
						if (IsBinary)
						{
							if (img_tmp.idata[u][v] == (T)1.0)
							{
								central_moment.idata[p][q] += pow(((double)u - centroid.x), p) * pow(((double)v - centroid.y), q);
							}
						}
						else
						{
							central_moment.idata[p][q] += img_tmp.idata[u][v] * pow(((double)u - centroid.x), p) * pow(((double)v - centroid.y), q);
						}
					}
				}
			}
			//cout << central_moment.idata[p][q] << "\t";
		}
		//cout << endl;
	}
	//cout << endl << endl;

	KArray2D<double> norm_cm;
	norm_cm.Allocate(4, 4);
	for (int p = 0; p < central_moment.width; p++)
	{
		for (int q = 0; q < central_moment.height; q++)
		{
			double gamma = double(p + q + 2.0) / 2.0;
			norm_cm.idata[p][q] = central_moment.idata[p][q] / pow(central_moment.idata[0][0], gamma);
			//cout << norm_cm.idata[p][q] << "\t";
		}
		//cout << endl;
	}
	//cout << endl << endl;
	//Seven Moment; norm_cm.idata[][]
	double val;
	// #1
	val = norm_cm.idata[2][0] + norm_cm.idata[0][2];
	hu_value->push_back(val);
	// #2
	val = pow((norm_cm.idata[2][0] - norm_cm.idata[0][2]), 2) + 4 * pow(norm_cm.idata[1][1], 2);
	hu_value->push_back(val);
	// #3
	val = pow((norm_cm.idata[3][0] - 3 * norm_cm.idata[1][2]), 2) + pow((norm_cm.idata[0][3] - 3 * norm_cm.idata[2][1]), 2);
	hu_value->push_back(val);
	// #4
	val = pow((norm_cm.idata[3][0] + norm_cm.idata[1][2]), 2) + pow((norm_cm.idata[0][3] + norm_cm.idata[2][1]), 2);
	hu_value->push_back(val);
	// #5
	val = (norm_cm.idata[3][0] - 3 * norm_cm.idata[1][2]) * (norm_cm.idata[3][0] + norm_cm.idata[1][2]) * (pow((norm_cm.idata[3][0] + norm_cm.idata[1][2]), 2) - 3 * pow((norm_cm.idata[0][3] + norm_cm.idata[2][1]), 2))
		+ (norm_cm.idata[0][3] - 3 * norm_cm.idata[2][1]) * (norm_cm.idata[0][3] + norm_cm.idata[2][1]) * (pow((norm_cm.idata[0][3] + norm_cm.idata[2][1]), 2) - 3 * pow((norm_cm.idata[3][0] + norm_cm.idata[1][2]), 2));
	hu_value->push_back(val);
	// #6
	val = (norm_cm.idata[2][0] - norm_cm.idata[0][2]) * (pow((norm_cm.idata[3][0] + norm_cm.idata[1][2]), 2) - pow((norm_cm.idata[0][3] + norm_cm.idata[2][1]), 2))
		+ 4 * norm_cm.idata[1][1] * (norm_cm.idata[3][0] + norm_cm.idata[1][2]) * (norm_cm.idata[0][3] + norm_cm.idata[2][1]);
	hu_value->push_back(val);
	// #7
	val = (3 * norm_cm.idata[2][1] - norm_cm.idata[0][3]) * (norm_cm.idata[3][0] + norm_cm.idata[1][2]) * (pow((norm_cm.idata[3][0] + norm_cm.idata[1][2]), 2) - 3 * pow((norm_cm.idata[0][3] + norm_cm.idata[2][1]), 2))
		+ (norm_cm.idata[3][0] - 3 * norm_cm.idata[1][2]) * (norm_cm.idata[0][3] + norm_cm.idata[2][1]) * (pow((norm_cm.idata[0][3] + norm_cm.idata[2][1]), 2) - 3 * pow((norm_cm.idata[3][0] + norm_cm.idata[1][2]), 2));
	hu_value->push_back(val);
}

template <class T> void GetFeatureValue(KGImage<T> *src, vector<double> *feature)
{
	KGImage<T> gimg, edg, edg_n, bw, tmp_src;
	edg_n = *src;

	vector<double> hu_val;
	KHuMoment<double>(&edg_n, &hu_val, false);

	edg_n = *src;
	int area_original = 0;
	for (int i = 0; i < edg_n.width; i++)
	for (int j = 0; j < edg_n.height; j++)
	{
		if (edg_n.idata[i][j] == 255)
			area_original++;
	}

	bw = edg_n;
	KDistanceTransform<T, T>(&edg_n, &edg);
	for (int u = 0; u < edg.width; u++)
	{
		for (int v = 0; v < edg.height; v++)
		{
			if (edg.idata[u][v] != 1)
				edg.idata[u][v] = 0;
			else
				edg.idata[u][v] = 255;
		}
	}
	edg_n = edg;

	vector<con_comp> list_conn;
	KConnectedComponent<T, int>(&edg_n, 255, &list_conn);
	for (int conn_i = 1; conn_i < list_conn.size(); conn_i++)
	{
		KFloodFill<T, int, int>(&edg_n, list_conn.at(conn_i).x, list_conn.at(conn_i).y, 255, 0);
	}
	edg = edg_n;
	Thresholding<T, T, int, int>(&edg, Mean<T>(&edg), 0, 255);
	//edg.Show("", false, true);
	
	gimg = edg;
	bw = edg;

	vector<KCoor<int>> contour;
	KRegionConnected<T, int, int>(&bw, 0, &contour);
	int peri_ori = contour.size();

	vector<KCoor<int>> Hull;
	double peri_hull;
	KConvexHULL<double>(&gimg, &contour, &Hull, &peri_hull);
	//gimg.Show("", false, true);

	double Dmin, Dmax;
	int m_lin, c_lin;
	double theta_rect;
	KMinRectBox<double>(&gimg, &Hull, &Dmin, &Dmax, &m_lin, &c_lin, &theta_rect);
	//gimg.Show("", false, true);

	int y1 = c_lin;
	int y2 = m_lin*(gimg.width - 1) + c_lin;

	/*KCoor<int> pp1, pp2;
	pp1.x = 0;
	pp1.y = y1;
	pp2.x = gimg.width - 1;
	pp2.y = y2;
	KDrawLineP2P<double, int, int>(&gimg, pp1, pp2, 75);*/

	int c1 = -1, c2 = -1;
	for (int i = 0; i < contour.size(); i++)
	{
		if ((abs((int)m_lin*contour.at(i).x + c_lin - contour.at(i).y) <= 2) || (abs((int)(contour.at(i).y - c_lin) / ((double)m_lin + 0.000001) - contour.at(i).x) <= 2))
		{
			if (c1 == -1)
				c1 = i;
			else
			{
				if ((i - c1) % contour.size() >20)
					c2 = i;
			}
		}
	}

	//vector<KCoor<int>> con1, con2; 
	double dist1 = 0.0, dist2 = 0.0;
	for (int i = c1; i < c2; i++)
	{
		dist1 += abs((double)-m_lin*contour.at(i).x + (double)contour.at(i).y - (double)c_lin) / sqrt(m_lin*m_lin + 1);
	}

	for (int i = c2; i < contour.size(); i++)
	{
		dist2 += abs((double)-m_lin*contour.at(i).x + (double)contour.at(i).y - (double)c_lin) / sqrt(m_lin*m_lin + 1);
	}

	for (int i = 0; i < c1; i++)
	{
		dist2 += abs((double)-m_lin*contour.at(i).x + (double)contour.at(i).y - (double)c_lin) / sqrt(m_lin*m_lin + 1);
	}

	//Area of Convex
	double area_convex = 0;
	for (int i = 0; i < Hull.size(); i++)
	{
		int next_i = (i + 1) % (Hull.size());
		double dX = Hull[next_i].x - Hull[i].x;
		double avgY = (Hull[next_i].y + Hull[i].y) / 2;
		area_convex += dX*avgY;  // This is the integration step.
	}
	area_convex = abs(area_convex);
	/*cout << "Area of Original = " << area_original << endl;
	cout << "Area of ConvexHull = " << area_convex << endl;

	cout << "Perimeter of Original = " << peri_ori << endl;
	cout << "Perimeter of ConvexHull = " << (int)peri_hull << endl;*/

	double AC = (double)area_original / area_convex;
	double PC = (double)peri_ori / peri_hull;
	double cir = (double)area_original / (peri_hull*peri_hull);
	double PR = (double)peri_ori / sqrt(area_original);
	double AR = (double)Dmax / Dmin;
	double rec = (double)area_original / (Dmax*Dmin);
	float sum_dist_ratio = dist1 / dist2;
	if (sum_dist_ratio > 1 && sum_dist_ratio != 0)
		sum_dist_ratio = 1 / sum_dist_ratio;

	/*cout << "\nArea Convexity \t\t\t= " << AC << endl;
	cout << "Perimeter Convexity \t\t= " << PC << endl;
	cout << "Circularity \t\t\t= " << cir << endl;
	cout << "Perimeter Ratio \t\t= " << PR << endl;
	cout << "Aspect Ratio \t\t\t= " << AR << endl;
	cout << "Rectangularity \t\t\t= " << rec << endl;
	cout << "Sum Distance Ratio \t\t= " << sum_dist_ratio << endl;*/
	//gimg.Show("", false, true);

	*src = gimg;

	feature->push_back(AC);
	feature->push_back(PC);
	feature->push_back(cir);
	feature->push_back(PR);
	feature->push_back(AR);
	feature->push_back(rec);
	feature->push_back(sum_dist_ratio);

	for (int k = 0; k < hu_val.size(); k++)
		feature->push_back(hu_val[k]);
}

#endif