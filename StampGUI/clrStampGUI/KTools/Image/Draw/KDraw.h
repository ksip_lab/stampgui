// ------------------------------------------------ //
// contain:		Drawing Tools
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KDraw_H
#define _KDraw_H

#include "KGeneral.h"
#include "KFont.h"
#include "KColor.h"
#include "KGImage.h"
#include "KCImage.h"

#define DRAWLINE_ARROW_NONE		0
#define DRAWLINE_ARROW_END		1
#define DRAWLINE_ARROW_START	2
#define DRAWLINE_ARROW_BOTH		3

#define DRAWCROSS_SHAPE_PLUS	0
#define DRAWCROSS_SHAPE_X		1

//===========================================================//
// ----------------- General Drawing Tools ----------------- //
// [0] Draw Line from Point to Point
// [1] Draw Line from Point and Angle[-PI,PI]
// [2] Draw Circle from Center Point and Radius
// [3] Draw DrawRectangular from 2-Point
// [4] Draw Square from Center Point and Radius[-PI,PI]
// [5] Draw Cross from Center Point and Length
// [6] Draw Arc from Focus Point, Radius, AngleStart[-PI,PI] and AngleStop[-PI,PI]
// [7] Draw Grid on Image
// [8] Draw Font on Image
//===========================================================//
template <typename T0,typename T1,typename T2> void KDrawLineP2P(KGImage<T0> *image,KCoor<T1> pst,KCoor<T1> pend,T2 value,int arrow=DRAWLINE_ARROW_NONE,int arrowSize=3);
template <typename T0,typename T1,typename T2> void KDrawLineP2P(KCImage<T0> *image,KCoor<T1> pst,KCoor<T1> pend,T2 value0,T2 value1,T2 value2,int arrow=DRAWLINE_ARROW_NONE,int arrowSize=3);
template <typename T0,typename T1,typename T2> void KDrawLineP2P(KGImage<T0> *image,T1 stx,T1 sty,T1 endx,T1 endy,T2 value,int arrow=DRAWLINE_ARROW_NONE,int arrowSize=3);
template <typename T0,typename T1,typename T2> void KDrawLineP2P(KCImage<T0> *image,T1 stx,T1 sty,T1 endx,T1 endy,T2 value0,T2 value1,T2 value2,int arrow=DRAWLINE_ARROW_NONE,int arrowSize=3);

template <typename T0,typename T1,typename T2,typename T3,typename T4> void KDrawLineANG(KGImage<T0> *image,KCoor<T1> pst,T2 angle,T3 length,T4 value,int arrow=DRAWLINE_ARROW_NONE,int arrowSize=3);
template <typename T0,typename T1,typename T2,typename T3,typename T4> void KDrawLineANG(KCImage<T0> *image,KCoor<T1> pst,T2 angle,T3 length,T4 value0,T4 value1,T4 value2,int arrow=DRAWLINE_ARROW_NONE,int arrowSize=3);
template <typename T0,typename T1,typename T2,typename T3,typename T4> void KDrawLineANG(KGImage<T0> *image,T1 stx,T1 sty,T2 angle,T3 length,T4 value,int arrow=DRAWLINE_ARROW_NONE,int arrowSize=3);
template <typename T0,typename T1,typename T2,typename T3,typename T4> void KDrawLineANG(KCImage<T0> *image,T1 stx,T1 sty,T2 angle,T3 length,T4 value0,T4 value1,T4 value2,int arrow=DRAWLINE_ARROW_NONE,int arrowSize=3);

template <typename T0,typename T1,typename T2,typename T3> void KDrawCircle(KGImage<T0> *image,T1 cx,T1 cy,T2 radius,T3 value);
template <typename T0,typename T1,typename T2,typename T3> void KDrawCircle(KCImage<T0> *image,T1 cx,T1 cy,T2 radius,T3 value0,T3 value1,T3 value2);
template <typename T0,typename T1,typename T2,typename T3> void KDrawCircle(KGImage<T0> *image,KCoor<T1> centerPoint,T2 radius,T3 value);
template <typename T0,typename T1,typename T2,typename T3> void KDrawCircle(KCImage<T0> *image,KCoor<T1> centerPoint,T2 radius,T3 value0,T3 value1,T3 value2);

template <typename T0,typename T1,typename T2> void KDrawRectangular(KGImage<T0> *image,T1 stx,T1 sty,T1 endx,T1 endy,T2 value);
template <typename T0,typename T1,typename T2> void KDrawRectangular(KCImage<T0> *image,T1 stx,T1 sty,T1 endx,T1 endy,T2 value0,T2 value1,T2 value2);
template <typename T0,typename T1,typename T2> void KDrawRectangular(KGImage<T0> *image,KCoor<T1> pst,KCoor<T1> pend,T2 value);
template <typename T0,typename T1,typename T2> void KDrawRectangular(KCImage<T0> *image,KCoor<T1> pst,KCoor<T1> pend,T2 value0,T2 value1,T2 value2);

template <typename T0,typename T1,typename T2,typename T3> void KDrawSquare(KGImage<T0> *image,T1 cx,T1 cy,T2 radius,T3 value);
template <typename T0,typename T1,typename T2,typename T3> void KDrawSquare(KCImage<T0> *image,T1 cx,T1 cy,T2 radius,T3 value0,T3 value1,T3 value2);
template <typename T0,typename T1,typename T2,typename T3> void KDrawSquare(KGImage<T0> *image,KCoor<T1> centerPoint,T2 radius,T3 value);
template <typename T0,typename T1,typename T2,typename T3> void KDrawSquare(KCImage<T0> *image,KCoor<T1> centerPoint,T2 radius,T3 value0,T3 value1,T3 value2);

template <typename T0,typename T1,typename T2,typename T3> void KDrawCross(KGImage<T0> *image,T1 cx,T1 cy,T2 length,T3 value,int shape=DRAWCROSS_SHAPE_PLUS);
template <typename T0,typename T1,typename T2,typename T3> void KDrawCross(KCImage<T0> *image,T1 cx,T1 cy,T2 length,T3 value0,T3 value1,T3 value2,int shape=DRAWCROSS_SHAPE_PLUS);
template <typename T0,typename T1,typename T2,typename T3> void KDrawCross(KGImage<T0> *image,KCoor<T1> centerPoint,T2 length,T3 value,int shape=DRAWCROSS_SHAPE_PLUS);
template <typename T0,typename T1,typename T2,typename T3> void KDrawCross(KCImage<T0> *image,KCoor<T1> centerPoint,T2 length,T3 value0,T3 value1,T3 value2,int shape=DRAWCROSS_SHAPE_PLUS);

template <typename T0,typename T1,typename T2,typename T3,typename T4,typename T5> void KDrawArc(KGImage<T0> *image,T1 xFocus,T1 yFocus,T2 radius,T3 angleStart,T4 angleStop,T5 value);
template <typename T0,typename T1,typename T2,typename T3,typename T4,typename T5> void KDrawArc(KCImage<T0> *image,T1 xFocus,T1 yFocus,T2 radius,T3 angleStart,T4 angleStop,T5 value0,T5 value1,T5 value2);
template <typename T0,typename T1,typename T2,typename T3,typename T4,typename T5> void KDrawArc(KGImage<T0> *image,KCoor<T1> focus,T2 radius,T3 angleStart,T4 angleStop,T5 value);
template <typename T0,typename T1,typename T2,typename T3,typename T4,typename T5> void KDrawArc(KCImage<T0> *image,KCoor<T1> focus,T2 radius,T3 angleStart,T4 angleStop,T5 value0,T5 value1,T5 value2);

template <typename T0,typename T1,typename T2> void KDrawGrid(KGImage<T0> *image,T1 xPitch,T1 yPitch,T2 value);
template <typename T0,typename T1,typename T2> void KDrawGrid(KCImage<T0> *image,T1 xPitch,T1 yPitch,T2 value0,T2 value1,T2 value2);
template <typename T0,typename T1,typename T2> void KDrawGrid(KGImage<T0> *image,T1 sizePitch,T2 value);
template <typename T0,typename T1,typename T2> void KDrawGrid(KCImage<T0> *image,T1 sizePitch,T2 value0,T2 value1,T2 value2);

template <typename T0,typename T1,typename T2> void KDrawFont(KGImage<T0> *image,KCoor<T1> topLeft,const bool KFont[FONT_HEIGHT][FONT_WIDTH],T2 value,bool transparence=true);
template <typename T0,typename T1,typename T2> void KDrawFont(KCImage<T0> *image,KCoor<T1> topLeft,const bool KFont[FONT_HEIGHT][FONT_WIDTH],T2 value0,T2 value1,T2 value2,bool transparence=true);
template <typename T0,typename T1,typename T2> void KDrawFont(KGImage<T0> *image,T1 topLeftX,T1 topLeftY,const bool KFont[FONT_HEIGHT][FONT_WIDTH],T2 value,bool transparence=true);
template <typename T0,typename T1,typename T2> void KDrawFont(KCImage<T0> *image,T1 topLeftX,T1 topLeftY,const bool KFont[FONT_HEIGHT][FONT_WIDTH],T2 value0,T2 value1,T2 value2,bool transparence=true);

template <typename T0,typename T1,typename T2,typename T3> void KDrawFont(KGImage<T0> *image,KCoor<T1> topLeft,T3 font,T2 value,bool transparence=true);
template <typename T0,typename T1,typename T2,typename T3> void KDrawFont(KCImage<T0> *image,KCoor<T1> topLeft,T3 font,T2 value0,T2 value1,T2 value2,bool transparence=true);
template <typename T0,typename T1,typename T2,typename T3> void KDrawFont(KGImage<T0> *image,T1 topLeftX,T1 topLeftY,T3 font,T2 value,bool transparence=true);
template <typename T0,typename T1,typename T2,typename T3> void KDrawFont(KCImage<T0> *image,T1 topLeftX,T1 topLeftY,T3 font,T2 value0,T2 value1,T2 value2,bool transparence=true);

//===========================================================//
template <typename T0,typename T1,typename T2> void KDrawLineP2P(KGImage<T0> *image,KCoor<T1> pst,KCoor<T1> pend,T2 value,int arrow,int arrowSize)
{
	if (!image->isAlloc)
		ErrorMessage("image not allocate");

	if (pst != pend)
	{
		double Length;
		double Angle;
		KCoor<int> plotPoint;

		Length = FindDistance(pst, pend);
		Angle = FindDirection(pst, pend);

		for (int l = 0; l < Length; l++)
		{
			plotPoint.x = (int)((double)pst.x + ((double)l*cos(Angle)));
			plotPoint.y = (int)((double)pst.y + ((double)l*sin(Angle)));
			if (image->IsInBound(plotPoint.x, plotPoint.y))
				image->idata[plotPoint.x][plotPoint.y] = value;

			plotPoint.x = (int)((double)pend.x + ((double)l*cos(Angle + PI)));
			plotPoint.y = (int)((double)pend.y + ((double)l*sin(Angle + PI)));
			if (image->IsInBound(plotPoint.x, plotPoint.y))
				image->idata[plotPoint.x][plotPoint.y] = value;
		}

		if (arrow != DRAWLINE_ARROW_NONE)
		{
			double angle = FindOrientationFull(pst.x, pst.y, pend.x, pend.y);
			if (arrow == DRAWLINE_ARROW_START || arrow == DRAWLINE_ARROW_BOTH)
			{
				KDrawLineANG(image, pst, (double)(angle + (PI / 4)), arrowSize, value, 0, 0);
				KDrawLineANG(image, pst, (double)(angle - (PI / 4)), arrowSize, value, 0, 0);
			}
			if (arrow == DRAWLINE_ARROW_END || arrow == DRAWLINE_ARROW_BOTH)
			{
				KDrawLineANG(image, pend, (double)(angle + (3 * PI / 4)), arrowSize, value, 0, 0);
				KDrawLineANG(image, pend, (double)(angle - (3 * PI / 4)), arrowSize, value, 0, 0);
			}
		}
	}
}
template <typename T0,typename T1,typename T2> void KDrawLineP2P(KCImage<T0> *image,KCoor<T1> pst,KCoor<T1> pend,T2 value0,T2 value1,T2 value2,int arrow,int arrowSize)
{
	if (!image->isAlloc)
		ErrorMessage("image not allocate");

	if (pst != pend)
	{
		double Length;
		double Angle;
		KCoor<int> plotPoint;

		Length = FindDistance(pst, pend);
		Angle = FindDirection(pst, pend);

		for (int l = 0; l < Length; l++)
		{
			plotPoint.x = (int)((double)pst.x + ((double)l*cos(Angle)));
			plotPoint.y = (int)((double)pst.y + ((double)l*sin(Angle)));
			if (image->IsInBound(plotPoint.x, plotPoint.y))
			{
				image->idata[0][plotPoint.x][plotPoint.y] = value0;
				image->idata[1][plotPoint.x][plotPoint.y] = value1;
				image->idata[2][plotPoint.x][plotPoint.y] = value2;
			}

			plotPoint.x = (int)((double)pend.x + ((double)l*cos(Angle + PI)));
			plotPoint.y = (int)((double)pend.y + ((double)l*sin(Angle + PI)));
			if (image->IsInBound(plotPoint.x, plotPoint.y))
			{
				image->idata[0][plotPoint.x][plotPoint.y] = value0;
				image->idata[1][plotPoint.x][plotPoint.y] = value1;
				image->idata[2][plotPoint.x][plotPoint.y] = value2;
			}
		}

		if (arrow != DRAWLINE_ARROW_NONE)
		{
			double angle = FindOrientationFull(pst.x, pst.y, pend.x, pend.y);
			if (arrow == DRAWLINE_ARROW_START || arrow == DRAWLINE_ARROW_BOTH)
			{
				KDrawLineANG(image, pst, (double)(angle + (PI / 4)), arrowSize, value0, value1, value2, 0, 0);
				KDrawLineANG(image, pst, (double)(angle - (PI / 4)), arrowSize, value0, value1, value2, 0, 0);
			}
			if (arrow == DRAWLINE_ARROW_END || arrow == DRAWLINE_ARROW_BOTH)
			{
				KDrawLineANG(image, pend, (double)(angle + (3 * PI / 4)), arrowSize, value0, value1, value2, 0, 0);
				KDrawLineANG(image, pend, (double)(angle - (3 * PI / 4)), arrowSize, value0, value1, value2, 0, 0);
			}
		}
	}
}
template <typename T0,typename T1,typename T2> void KDrawLineP2P(KGImage<T0> *image,T1 stx,T1 sty,T1 endx,T1 endy,T2 value,int arrow,int arrowSize)
{
	KCoor<T1> pst,pend;
	pst.Set(stx,sty);
	pend.Set(endx,endy);
	KDrawLineP2P(image,pst,pend,value,arrow,arrowSize);
}
template <typename T0,typename T1,typename T2> void KDrawLineP2P(KCImage<T0> *image,T1 stx,T1 sty,T1 endx,T1 endy,T2 value0,T2 value1,T2 value2,int arrow,int arrowSize)
{
	KCoor<T1> pst,pend;
	pst.Set(stx,sty);
	pend.Set(endx,endy);
	KDrawLineP2P(image,pst,pend,value0,value1,value2,arrow,arrowSize);
}
template <typename T0,typename T1,typename T2,typename T3,typename T4> void KDrawLineANG(KGImage<T0> *image,KCoor<T1> pst,T2 angle,T3 length,T4 value,int arrow,int arrowSize)
{
	KCoor<T1> pend;
	pend.x = (T1)( ((double)length*cos((double)angle)+(double)pst.x));
	pend.y = (T1)( ((double)length*sin((double)angle)+(double)pst.y));
	KDrawLineP2P(image,pst,pend,value,arrow,arrowSize);
}
template <typename T0,typename T1,typename T2,typename T3,typename T4> void KDrawLineANG(KCImage<T0> *image,KCoor<T1> pst,T2 angle,T3 length,T4 value0,T4 value1,T4 value2,int arrow,int arrowSize)
{
	KCoor<T1> pend;
	pend.x = (T1)( ((double)length*cos((double)angle)+(double)pst.x));
	pend.y = (T1)( ((double)length*sin((double)angle)+(double)pst.y));
	KDrawLineP2P(image,pst,pend,value0,value1,value2,arrow,arrowSize);
}
template <typename T0,typename T1,typename T2,typename T3,typename T4> void KDrawLineANG(KGImage<T0> *image,T1 stx,T1 sty,T2 angle,T3 length,T4 value,int arrow,int arrowSize)
{
	KCoor<T1> pst;
	pst.Set(stx,sty);
	KDrawLineANG(image,pst,angle,length,value,arrow,arrowSize);
}
template <typename T0,typename T1,typename T2,typename T3,typename T4> void KDrawLineANG(KCImage<T0> *image,T1 stx,T1 sty,T2 angle,T3 length,T4 value0,T4 value1,T4 value2,int arrow,int arrowSize)
{
	KCoor<T1> pst;
	pst.Set(stx,sty);
	KDrawLineANG(image,pst,angle,length,value0,value1,value2,arrow,arrowSize);
}
template <typename T0,typename T1,typename T2,typename T3> void KDrawCircle(KGImage<T0> *image,T1 cx,T1 cy,T2 radius,T3 value)
{
	if(!image->isAlloc)
		ErrorMessage("image not allocate");

	// Plot circle by midpoint generating algorithm (Bresenham's idea,1977)
	int x,y,p;
	x = 0;
	y = (int)radius;
	p = (int)(1-(int)radius); // (x0=0, y0=R)
	//plot initial circle point to all circular quadrant
	if(image->IsInBound((int)cx+x,(int)cy+y))	image->Set((int)cx+x,(int)cy+y,(T0)value); //Q1
	if(image->IsInBound((int)cx-x,(int)cy+y))	image->Set((int)cx-x,(int)cy+y,(T0)value); //Q4
	if(image->IsInBound((int)cx+x,(int)cy-y))	image->Set((int)cx+x,(int)cy-y,(T0)value); //Q8
	if(image->IsInBound((int)cx-x,(int)cy-y))	image->Set((int)cx-x,(int)cy-y,(T0)value); //Q5
	if(image->IsInBound((int)cx+y,(int)cy+x))	image->Set((int)cx+y,(int)cy+x,(T0)value); //Q2
	if(image->IsInBound((int)cx-y,(int)cy+x))	image->Set((int)cx-y,(int)cy+x,(T0)value); //Q3
	if(image->IsInBound((int)cx+y,(int)cy-x))	image->Set((int)cx+y,(int)cy-x,(T0)value); //Q7
	if(image->IsInBound((int)cx-y,(int)cy-x))	image->Set((int)cx-y,(int)cy-x,(T0)value); //Q6
	// generate data in 1st circular quadrant
	while(x<y)
	{															   
		x++;
		if(p<0)
		{
			p += (x<<1);
			p++;
		}
		else
		{
			y--;
			p += ((x-y)<<1);
			p++;
		}
		//plot circle point to all circular quadrant
		if (image->IsInBound((int)cx + x, (int)cy + y))	image->Set((int)cx + x, (int)cy + y, (T0)value);
		if (image->IsInBound((int)cx - x, (int)cy + y))	image->Set((int)cx - x, (int)cy + y, (T0)value);
		if (image->IsInBound((int)cx + x, (int)cy - y))	image->Set((int)cx + x, (int)cy - y, (T0)value);
		if (image->IsInBound((int)cx - x, (int)cy - y))	image->Set((int)cx - x, (int)cy - y, (T0)value);
		if (image->IsInBound((int)cx + y, (int)cy + x))	image->Set((int)cx + y, (int)cy + x, (T0)value);
		if (image->IsInBound((int)cx - y, (int)cy + x))	image->Set((int)cx - y, (int)cy + x, (T0)value);
		if (image->IsInBound((int)cx + y, (int)cy - x))	image->Set((int)cx + y, (int)cy - x, (T0)value);
		if (image->IsInBound((int)cx - y, (int)cy - x))	image->Set((int)cx - y, (int)cy - x, (T0)value);
	}
}
template <typename T0,typename T1,typename T2,typename T3> void KDrawCircle(KCImage<T0> *image,T1 cx,T1 cy,T2 radius,T3 value0,T3 value1,T3 value2)
{
	if(!image->isAlloc)
		ErrorMessage("image not allocate");

	// Plot circle by midpoint generating algorithm (Bresenham's idea,1977)
	int x,y,p;
	x = 0;
	y = (int)radius;
	p = (int)(1-(int)radius); // (x0=0, y0=R)
	//plot initial circle point to all circular quadrant
	if (image->IsInBound((int)cx + x, (int)cy + y))	image->Set((int)cx + x, (int)cy + y, (T0)value0, (T0)value1, (T0)value2); //Q1
	if (image->IsInBound((int)cx - x, (int)cy + y))	image->Set((int)cx - x, (int)cy + y, (T0)value0, (T0)value1, (T0)value2); //Q4
	if (image->IsInBound((int)cx + x, (int)cy - y))	image->Set((int)cx + x, (int)cy - y, (T0)value0, (T0)value1, (T0)value2); //Q8
	if (image->IsInBound((int)cx - x, (int)cy - y))	image->Set((int)cx - x, (int)cy - y, (T0)value0, (T0)value1, (T0)value2); //Q5
	if (image->IsInBound((int)cx + y, (int)cy + x))	image->Set((int)cx + y, (int)cy + x, (T0)value0, (T0)value1, (T0)value2); //Q2
	if (image->IsInBound((int)cx - y, (int)cy + x))	image->Set((int)cx - y, (int)cy + x, (T0)value0, (T0)value1, (T0)value2); //Q3
	if (image->IsInBound((int)cx + y, (int)cy - x))	image->Set((int)cx + y, (int)cy - x, (T0)value0, (T0)value1, (T0)value2); //Q7
	if (image->IsInBound((int)cx - y, (int)cy - x))	image->Set((int)cx - y, (int)cy - x, (T0)value0, (T0)value1, (T0)value2); //Q6
	// generate data in 1st circular quadrant
	while(x<y)
	{															   
		x++;
		if(p<0)
		{
			p += (x<<1);
			p++;
		}
		else
		{
			y--;
			p += ((x-y)<<1);
			p++;
		}
		//plot circle point to all circular quadrant
		if (image->IsInBound((int)cx + x, (int)cy + y))	image->Set((int)cx + x, (int)cy + y, (T0)value0, (T0)value1, (T0)value2);
		if (image->IsInBound((int)cx - x, (int)cy + y))	image->Set((int)cx - x, (int)cy + y, (T0)value0, (T0)value1, (T0)value2);
		if (image->IsInBound((int)cx + x, (int)cy - y))	image->Set((int)cx + x, (int)cy - y, (T0)value0, (T0)value1, (T0)value2);
		if (image->IsInBound((int)cx - x, (int)cy - y))	image->Set((int)cx - x, (int)cy - y, (T0)value0, (T0)value1, (T0)value2);
		if (image->IsInBound((int)cx + y, (int)cy + x))	image->Set((int)cx + y, (int)cy + x, (T0)value0, (T0)value1, (T0)value2);
		if (image->IsInBound((int)cx - y, (int)cy + x))	image->Set((int)cx - y, (int)cy + x, (T0)value0, (T0)value1, (T0)value2);
		if (image->IsInBound((int)cx + y, (int)cy - x))	image->Set((int)cx + y, (int)cy - x, (T0)value0, (T0)value1, (T0)value2);
		if (image->IsInBound((int)cx - y, (int)cy - x))	image->Set((int)cx - y, (int)cy - x, (T0)value0, (T0)value1, (T0)value2);
	}
}
template <typename T0,typename T1,typename T2,typename T3> void KDrawCircle(KGImage<T0> *image,KCoor<T1> centerPoint,T2 radius,T3 value)
{
	KDrawCircle(image,centerPoint.x,centerPoint.y,radius,(T0)value);
}
template <typename T0,typename T1,typename T2,typename T3> void KDrawCircle(KCImage<T0> *image,KCoor<T1> centerPoint,T2 radius,T3 value0,T3 value1,T3 value2)
{
	KDrawCircle(image,centerPoint.x,centerPoint.y,radius,(T0)value0,(T0)value1,(T0)value2);
}
template <typename T0,typename T1,typename T2> void KDrawRectangular(KGImage<T0> *image,T1 stx,T1 sty,T1 endx,T1 endy,T2 value)
{
	if(!image->isAlloc)
		ErrorMessage("image not allocate");

	if(endx<sty&&endy<sty)
		ErrorMessage("drawing KCoordinate error");

	for(int i=0;i<=((int)endx-(int)stx);i++)
	{
		if (image->IsInBound((int)stx + i, (int)sty))
			image->idata[(int)stx+i][(int)sty]=(T0)value;
		if (image->IsInBound((int)stx + i, (int)sty + ((int)endy - (int)sty)))
			image->idata[(int)stx+i][(int)sty+((int)endy-(int)sty)]=(T0)value;
	}
	for(int j=0;j<((int)endy-(int)sty);j++)
	{
		if (image->IsInBound((int)stx, (int)sty + j))
			image->idata[(int)stx][(int)sty+j]=(T0)value;
		if (image->IsInBound((int)stx + ((int)endx - (int)stx), (int)sty + j))
			image->idata[(int)stx+((int)endx-(int)stx)][(int)sty+j]=(T0)value;
	}
}
template <typename T0,typename T1,typename T2> void KDrawRectangular(KCImage<T0> *image,T1 stx,T1 sty,T1 endx,T1 endy,T2 value0,T2 value1,T2 value2)
{
	if(!image->isAlloc)
		ErrorMessage("image not allocate");

	if(endx<sty&&endy<sty)
		ErrorMessage("drawing KCoordinate error");

	for(int i=0;i<=((int)endx-(int)stx);i++)
	{
		if (image->IsInBound((int)stx + i, (int)sty))
		{
			image->idata[0][(int)stx+i][(int)sty]=(T0)value0;
			image->idata[1][(int)stx+i][(int)sty]=(T0)value1;
			image->idata[2][(int)stx+i][(int)sty]=(T0)value2;
		}
		if (image->IsInBound((int)stx + i, (int)sty + ((int)endy - (int)sty)))
		{
			image->idata[0][(int)stx+i][(int)sty+((int)endy-(int)sty)]=(T0)value0;
			image->idata[1][(int)stx+i][(int)sty+((int)endy-(int)sty)]=(T0)value1;
			image->idata[2][(int)stx+i][(int)sty+((int)endy-(int)sty)]=(T0)value2;
		}
	}
	for(int j=0;j<((int)endy-(int)sty);j++)
	{
		if (image->IsInBound((int)stx, (int)sty + j))
		{
			image->idata[0][(int)stx][(int)sty+j]=(T0)value0;
			image->idata[1][(int)stx][(int)sty+j]=(T0)value1;
			image->idata[2][(int)stx][(int)sty+j]=(T0)value2;
		}
		if (image->IsInBound((int)stx + ((int)endx - (int)stx), (int)sty + j))
		{
			image->idata[0][(int)stx+((int)endx-(int)stx)][(int)sty+j]=(T0)value0;
			image->idata[1][(int)stx+((int)endx-(int)stx)][(int)sty+j]=(T0)value1;
			image->idata[2][(int)stx+((int)endx-(int)stx)][(int)sty+j]=(T0)value2;
		}
	}
}
template <typename T0,typename T1,typename T2> void KDrawRectangular(KGImage<T0> *image,KCoor<T1> pst,KCoor<T1> pend,T2 value)
{
	KDrawRectangular(image,pst.x,pst.y,pend.x,pend.y,value);
}
template <typename T0,typename T1,typename T2> void KDrawRectangular(KCImage<T0> *image,KCoor<T1> pst,KCoor<T1> pend,T2 value0,T2 value1,T2 value2)
{
	KDrawRectangular(image,pst.x,pst.y,pend.x,pend.y,value0,value1,value2);
}
template <typename T0,typename T1,typename T2,typename T3> void KDrawSquare(KGImage<T0> *image,T1 cx,T1 cy,T2 radius,T3 value)
{
	KDrawRectangular(image,((float)cx-(float)radius),((float)cy-(float)radius),((float)cx+(float)radius),((float)cy+(float)radius),value);
}
template <typename T0,typename T1,typename T2,typename T3> void KDrawSquare(KCImage<T0> *image,T1 cx,T1 cy,T2 radius,T3 value0,T3 value1,T3 value2)
{
	KDrawRectangular(image,((float)cx-(float)radius),((float)cy-(float)radius),((float)cx+(float)radius),((float)cy+(float)radius),value0,value1,value2);
}
template <typename T0,typename T1,typename T2,typename T3> void KDrawSquare(KGImage<T0> *image,KCoor<T1> centerPoint,T2 radius,T3 value)
{
	KCoor<float> pst,pend;
	pst.x	= (float)centerPoint.x - (float)radius;
	pst.y	= (float)centerPoint.y - (float)radius;
	pend.x	= (float)centerPoint.x + (float)radius;
	pend.y	= (float)centerPoint.y + (float)radius;
	KDrawRectangular(image,pst,pend,value);
}
template <typename T0,typename T1,typename T2,typename T3> void KDrawSquare(KCImage<T0> *image,KCoor<T1> centerPoint,T2 radius,T3 value0,T3 value1,T3 value2)
{
	KCoor<float> pst,pend;
	pst.x	= (float)centerPoint.x - (float)radius;
	pst.y	= (float)centerPoint.y - (float)radius;
	pend.x	= (float)centerPoint.x + (float)radius;
	pend.y	= (float)centerPoint.y + (float)radius;
	KDrawRectangular(image,pst,pend,value0,value1,value2);
}
template <typename T0,typename T1,typename T2,typename T3> void KDrawCross(KGImage<T0> *image,T1 cx,T1 cy,T2 length,T3 value,int shape)
{
	if(shape==DRAWCROSS_SHAPE_PLUS)
	{
		KDrawLineANG(image, cx, cy, (float)(0), length, value, DRAWLINE_ARROW_NONE);
		KDrawLineANG(image, cx, cy, (float)(PI / 2), length, value, DRAWLINE_ARROW_NONE);
		KDrawLineANG(image, cx, cy, (float)(PI), length, value, DRAWLINE_ARROW_NONE);
		KDrawLineANG(image, cx, cy, (float)(3 * PI / 2), length, value, DRAWLINE_ARROW_NONE);
	}
	if(shape==DRAWCROSS_SHAPE_X)
	{
		KDrawLineANG(image, cx, cy, (float)(PI / 4), length, value, DRAWLINE_ARROW_NONE);
		KDrawLineANG(image, cx, cy, (float)(3 * PI / 4), length, value, DRAWLINE_ARROW_NONE);
		KDrawLineANG(image, cx, cy, (float)(-PI / 4), length, value, DRAWLINE_ARROW_NONE);
		KDrawLineANG(image, cx, cy, (float)(-3 * PI / 4), length, value, DRAWLINE_ARROW_NONE);
	}
}
template <typename T0,typename T1,typename T2,typename T3> void KDrawCross(KCImage<T0> *image,T1 cx,T1 cy,T2 length,T3 value0,T3 value1,T3 value2,int shape)
{
	if(shape==DRAWCROSS_SHAPE_PLUS)
	{
		KDrawLineANG(image, cx, cy, (float)(0), length, value0, value1, value2, DRAWLINE_ARROW_NONE);
		KDrawLineANG(image, cx, cy, (float)(PI / 2), length, value0, value1, value2, DRAWLINE_ARROW_NONE);
		KDrawLineANG(image, cx, cy, (float)(PI), length, value0, value1, value2, DRAWLINE_ARROW_NONE);
		KDrawLineANG(image, cx, cy, (float)(3 * PI / 2), length, value0, value1, value2, DRAWLINE_ARROW_NONE);
	}
	if(shape==DRAWCROSS_SHAPE_X)
	{
		KDrawLineANG(image, cx, cy, (float)(PI / 4), length, value0, value1, value2, DRAWLINE_ARROW_NONE);
		KDrawLineANG(image, cx, cy, (float)(3 * PI / 4), length, value0, value1, value2, DRAWLINE_ARROW_NONE);
		KDrawLineANG(image, cx, cy, (float)(-PI / 4), length, value0, value1, value2, DRAWLINE_ARROW_NONE);
		KDrawLineANG(image, cx, cy, (float)(-3 * PI / 4), length, value0, value1, value2, DRAWLINE_ARROW_NONE);
	}
}
template <typename T0,typename T1,typename T2,typename T3> void KDrawCross(KGImage<T0> *image,KCoor<T1> centerPoint,T2 length,T3 value,int shape)
{
	KDrawCross(image,centerPoint.x,centerPoint.y,length,value,shape);
}
template <typename T0,typename T1,typename T2,typename T3> void KDrawCross(KCImage<T0> *image,KCoor<T1> centerPoint,T2 length,T3 value0,T3 value1,T3 value2,int shape)
{
	KDrawCross(image,centerPoint.x,centerPoint.y,length,value0,value1,value2,shape);
}
template <typename T0,typename T1,typename T2,typename T3,typename T4,typename T5> void KDrawArc(KGImage<T0> *image,T1 xFocus,T1 yFocus,T2 radius,T3 angleStart,T4 angleStop,T5 value)
{
	KCoor<T1> focus;
	focus.x	= xFocus;
	focus.y = yFocus;
	KDrawArc(image,focus,radius,angleStart,angleStop,value);
}
template <typename T0,typename T1,typename T2,typename T3,typename T4,typename T5> void KDrawArc(KCImage<T0> *image,T1 xFocus,T1 yFocus,T2 radius,T3 angleStart,T4 angleStop,T5 value0,T5 value1,T5 value2)
{
	KCoor<T1> focus;
	focus.x	= xFocus;
	focus.y = yFocus;
	KDrawArc(image,focus,radius,angleStart,angleStop,value0,value1,value2);
}
template <typename T0,typename T1,typename T2,typename T3,typename T4,typename T5> void KDrawArc(KGImage<T0> *image,KCoor<T1> focus,T2 radius,T3 angleStart,T4 angleStop,T5 value)
{
	double			segments;
	double			real_segments;
	double			theta;
	double			tangetial_factor;
	double			radial_factor;
	KCoor<double>	p1,p2;
	KCoor<double>	ip1,ip2;

	segments			= (double)500;	//coarse --> fine drawing
	real_segments		= (double)(abs((double)angleStop-(double)angleStart) / (2*PI)*(double)segments) + 1;
	theta				= ((double)angleStop-(double)angleStart) / (double)(real_segments);
	tangetial_factor	= tan(theta);
	radial_factor		= 1 - cos(theta);

	p1.x = (double)focus.x + (double)radius * cos((double)angleStart);
	p1.y = (double)focus.y + (double)radius * sin((double)angleStart);

	for(double ii = 0; ii < real_segments + 1; ii++)
	{
		p2 = p1;

		double tx = -(p1.y - (double)focus.y);
		double ty = p1.x - (double)focus.x;

		p1.x += tx * tangetial_factor;
		p1.y += ty * tangetial_factor;

		double rx = (double)focus.x - p1.x;
		double ry = (double)focus.y - p1.y;

		p1.x += rx * radial_factor;
		p1.y += ry * radial_factor;

		ip1.x	= (double)p1.x;
		ip1.y	= (double)p1.y;
		ip2.x	= (double)p2.x;
		ip2.y	= (double)p2.y;
		KDrawLineP2P(image,ip1,ip2,value,DRAWLINE_ARROW_NONE);
	}
}
template <typename T0,typename T1,typename T2,typename T3,typename T4,typename T5> void KDrawArc(KCImage<T0> *image,KCoor<T1> focus,T2 radius,T3 angleStart,T4 angleStop,T5 value0,T5 value1,T5 value2)
{
	double			segments;
	double			real_segments;
	double			theta;
	double			tangetial_factor;
	double			radial_factor;
	KCoor<double>	p1,p2;
	KCoor<double>	ip1,ip2;

	segments			= (double)500;	//coarse --> fine drawing
	real_segments		= (double)(abs((double)angleStop-(double)angleStart) / (2*PI)*(double)segments) + 1;
	theta				= ((double)angleStop-(double)angleStart) / (double)(real_segments);
	tangetial_factor	= tan(theta);
	radial_factor		= 1 - cos(theta);

	p1.x = (double)focus.x + (double)radius * cos((double)angleStart);
	p1.y = (double)focus.y + (double)radius * sin((double)angleStart);

	for(double ii = 0; ii < real_segments + 1; ii++)
	{
		p2 = p1;

		double tx = -(p1.y - (double)focus.y);
		double ty = p1.x - (double)focus.x;

		p1.x += tx * tangetial_factor;
		p1.y += ty * tangetial_factor;

		double rx = (double)focus.x - p1.x;
		double ry = (double)focus.y - p1.y;

		p1.x += rx * radial_factor;
		p1.y += ry * radial_factor;

		ip1.x	= (double)p1.x;
		ip1.y	= (double)p1.y;
		ip2.x	= (double)p2.x;
		ip2.y	= (double)p2.y;
		KDrawLineP2P(image,ip1,ip2,value0,value1,value2,DRAWLINE_ARROW_NONE);
	}
}
template <typename T0,typename T1,typename T2> void KDrawGrid(KGImage<T0> *image,T1 xPitch,T1 yPitch,T2 value)
{
	if(xPitch>0)
		for(int x=(int)xPitch;x<image->Width;x+=(int)xPitch)
			for(int y=0;y<image->Height;y+=2)
				if(image->IsInBound(x,y))
					image->idata[x][y]=(T0)value;
	if(yPitch>0)
		for(int y=(int)yPitch;y<image->Height;y+=(int)yPitch)
			for(int x=0;x<image->Width;x+=2)
				if(image->IsInBound(x,y))
					image->idata[x][y]=(T0)value;
}
template <typename T0,typename T1,typename T2> void KDrawGrid(KCImage<T0> *image,T1 xPitch,T1 yPitch,T2 value0,T2 value1,T2 value2)
{
	if(xPitch>0)
	{
		for(int x=(int)xPitch;x<image->width;x+=(int)xPitch)
		for(int y=0;y<image->height;y+=2)
		{
			if(image->IsInBound(x,y))
			{
				image->idata[0][x][y]=(T0)value0;
				image->idata[1][x][y]=(T0)value1;
				image->idata[2][x][y]=(T0)value2;
			}
		}
	}
	if(yPitch>0)
	{
		for(int y=(int)yPitch;y<image->height;y+=(int)yPitch)
		for(int x=0;x<image->width;x+=2)
		{
			if(image->IsInBound(x,y))
			{
				image->idata[0][x][y]=(T0)value0;
				image->idata[1][x][y]=(T0)value1;
				image->idata[2][x][y]=(T0)value2;
			}
		}
	}
}
template <typename T0,typename T1,typename T2> void KDrawGrid(KGImage<T0> *image,T1 sizePitch,T2 value)
{
	KDrawGrid(image,sizePitch,sizePitch,value);
}
template <typename T0,typename T1,typename T2> void KDrawGrid(KCImage<T0> *image,T1 sizePitch,T2 value0,T2 value1,T2 value2)
{
	KDrawGrid(image,sizePitch,sizePitch,value0,value1,value2);
}
template <typename T0,typename T1,typename T2> void KDrawFont(KGImage<T0> *image,KCoor<T1> topLeft,const bool KFont[FONT_HEIGHT][FONT_WIDTH],T2 value,bool transparence)
{
	for(int y=0; y<FONT_HEIGHT; y++)
	for(int x=0; x<FONT_WIDTH ; x++)
	{
		int xx = (int)topLeft.x + x;
		int yy = (int)topLeft.y + y;
		if(image->IsInBound(xx,yy))
		{
			if(KFont[y][x])
				image->idata[xx][yy] = (T0)(value);
			else if(!transparence)
				image->idata[xx][yy] = (T0)0;
		}	
	}
}
template <typename T0,typename T1,typename T2> void KDrawFont(KCImage<T0> *image,KCoor<T1> topLeft,const bool KFont[FONT_HEIGHT][FONT_WIDTH],T2 value0,T2 value1,T2 value2,bool transparence)
{
	for(int y=0; y<FONT_HEIGHT; y++)
	for(int x=0; x<FONT_WIDTH ; x++)
	{
		int xx = (int)topLeft.x + x;
		int yy = (int)topLeft.y + y;
		if(image->IsInBound(xx,yy))
		{
			if(KFont[y][x])
			{
				image->idata[0][xx][yy] = (T0)(value0);
				image->idata[1][xx][yy] = (T0)(value1);
				image->idata[2][xx][yy] = (T0)(value2);
			}
			else if(!transparence)
			{
				image->idata[0][xx][yy] = (T0)0;
				image->idata[1][xx][yy] = (T0)0;
				image->idata[2][xx][yy] = (T0)0;
			}
		}	
	}
}
template <typename T0,typename T1,typename T2> void KDrawFont(KGImage<T0> *image,T1 topLeftX,T1 topLeftY,const bool KFont[FONT_HEIGHT][FONT_WIDTH],T2 value,bool transparence)
{
	KCoor<T1> topLeft;
	topLeft.x = topLeftX;
	topLeft.y = topLeftY;
	KDrawFont(image,topLeft,KFont,value,transparence);
}
template <typename T0,typename T1,typename T2> void DrawFont(KCImage<T0> *image,T1 topLeftX,T1 topLeftY,const bool KFont[FONT_HEIGHT][FONT_WIDTH],T2 value0,T2 value1,T2 value2,bool transparence)
{
	KCoor<T1> topLeft;
	topLeft.x = topLeftX;
	topLeft.y = topLeftY;
	KDrawFont(image,topLeft,KFont,value0,value1,value2,transparence);
}
template <typename T0,typename T1,typename T2,typename T3> void KDrawFont(KGImage<T0> *image,KCoor<T1> topLeft,T3 font,T2 value,bool transparence)
{
	string			text2Draw;
	ostringstream	convert2String;
	int				charSpace;
	KCoor<T1>		pos;
	char			c;

	convert2String<<font;
	text2Draw		= convert2String.str();
	pos				= topLeft;
	if(!transparence)
		charSpace	= 0;
	else
		charSpace	= 1;

	for(long long i=0; i<(long long)text2Draw.length();i++)
	{
		c = text2Draw[(int)i];
		switch(c)
		{
		case 'a': case 'A':KDrawFont(image,pos,KFontA,value,transparence); break;
		case 'b': case 'B':KDrawFont(image,pos,KFontB,value,transparence); break;
		case 'c': case 'C':KDrawFont(image,pos,KFontC,value,transparence); break;
		case 'd': case 'D':KDrawFont(image,pos,KFontD,value,transparence); break;
		case 'e': case 'E':KDrawFont(image,pos,KFontE,value,transparence); break;
		case 'f': case 'F':KDrawFont(image,pos,KFontF,value,transparence); break;
		case 'g': case 'G':KDrawFont(image,pos,KFontG,value,transparence); break;
		case 'h': case 'H':KDrawFont(image,pos,KFontH,value,transparence); break;
		case 'i': case 'I':KDrawFont(image,pos,KFontI,value,transparence); break;
		case 'j': case 'J':KDrawFont(image,pos,KFontJ,value,transparence); break;
		case 'k': case 'K':KDrawFont(image,pos,KFontK,value,transparence); break;
		case 'l': case 'L':KDrawFont(image,pos,KFontL,value,transparence); break;
		case 'm': case 'M':KDrawFont(image,pos,KFontM,value,transparence); break;
		case 'n': case 'N':KDrawFont(image,pos,KFontN,value,transparence); break;
		case 'o': case 'O':KDrawFont(image,pos,KFontO,value,transparence); break;
		case 'p': case 'P':KDrawFont(image,pos,KFontP,value,transparence); break;
		case 'q': case 'Q':KDrawFont(image,pos,KFontQ,value,transparence); break;
		case 'r': case 'R':KDrawFont(image,pos,KFontR,value,transparence); break;
		case 's': case 'S':KDrawFont(image,pos,KFontS,value,transparence); break;
		case 't': case 'T':KDrawFont(image,pos,KFontT,value,transparence); break;
		case 'u': case 'U':KDrawFont(image,pos,KFontU,value,transparence); break;
		case 'v': case 'V':KDrawFont(image,pos,KFontV,value,transparence); break;
		case 'w': case 'W':KDrawFont(image,pos,KFontW,value,transparence); break;
		case 'x': case 'X':KDrawFont(image,pos,KFontX,value,transparence); break;
		case 'y': case 'Y':KDrawFont(image,pos,KFontY,value,transparence); break;
		case 'z': case 'Z':KDrawFont(image,pos,KFontZ,value,transparence); break;

		case ' ' :KDrawFont(image,pos,KFontSPACE,value,transparence); break;
		case '.' :KDrawFont(image,pos,KFontDOT,value,transparence); break;
		case ':' :KDrawFont(image,pos,KFontCOlON,value,transparence); break;
		case '-' :KDrawFont(image,pos,KFont_,value,transparence); break;
		case '0' :KDrawFont(image,pos,KFont0,value,transparence); break;
		case '1' :KDrawFont(image,pos,KFont1,value,transparence); break;
		case '2' :KDrawFont(image,pos,KFont2,value,transparence); break;
		case '3' :KDrawFont(image,pos,KFont3,value,transparence); break;
		case '4' :KDrawFont(image,pos,KFont4,value,transparence); break;
		case '5' :KDrawFont(image,pos,KFont5,value,transparence); break;
		case '6' :KDrawFont(image,pos,KFont6,value,transparence); break;
		case '7' :KDrawFont(image,pos,KFont7,value,transparence); break;
		case '8' :KDrawFont(image,pos,KFont8,value,transparence); break;
		case '9' :KDrawFont(image,pos,KFont9,value,transparence); break;
		}
		pos.x += (T1)(FONT_WIDTH+charSpace);
	}	
}
template <typename T0,typename T1,typename T2,typename T3> void KDrawFont(KCImage<T0> *image,KCoor<T1> topLeft,T3 font,T2 value0,T2 value1,T2 value2,bool transparence)
{
	string			text2Draw;
	ostringstream	convert2String;
	int				charSpace;
	KCoor<T1>		pos;
	char			c;

	convert2String<<font;
	text2Draw		= convert2String.str();
	pos				= topLeft;
	if(!transparence)
		charSpace	= 0;
	else
		charSpace	= 1;

	for(long long i=0; i<(long long)text2Draw.length();i++)
	{
		c = text2Draw[(int)i];
		switch(c)
		{
		case 'a': case 'A':KDrawFont(image,pos,KFontA,value0,value1,value2,transparence); break;
		case 'b': case 'B':KDrawFont(image,pos,KFontB,value0,value1,value2,transparence); break;
		case 'c': case 'C':KDrawFont(image,pos,KFontC,value0,value1,value2,transparence); break;
		case 'd': case 'D':KDrawFont(image,pos,KFontD,value0,value1,value2,transparence); break;
		case 'e': case 'E':KDrawFont(image,pos,KFontE,value0,value1,value2,transparence); break;
		case 'f': case 'F':KDrawFont(image,pos,KFontF,value0,value1,value2,transparence); break;
		case 'g': case 'G':KDrawFont(image,pos,KFontG,value0,value1,value2,transparence); break;
		case 'h': case 'H':KDrawFont(image,pos,KFontH,value0,value1,value2,transparence); break;
		case 'i': case 'I':KDrawFont(image,pos,KFontI,value0,value1,value2,transparence); break;
		case 'j': case 'J':KDrawFont(image,pos,KFontJ,value0,value1,value2,transparence); break;
		case 'k': case 'K':KDrawFont(image,pos,KFontK,value0,value1,value2,transparence); break;
		case 'l': case 'L':KDrawFont(image,pos,KFontL,value0,value1,value2,transparence); break;
		case 'm': case 'M':KDrawFont(image,pos,KFontM,value0,value1,value2,transparence); break;
		case 'n': case 'N':KDrawFont(image,pos,KFontN,value0,value1,value2,transparence); break;
		case 'o': case 'O':KDrawFont(image,pos,KFontO,value0,value1,value2,transparence); break;
		case 'p': case 'P':KDrawFont(image,pos,KFontP,value0,value1,value2,transparence); break;
		case 'q': case 'Q':KDrawFont(image,pos,KFontQ,value0,value1,value2,transparence); break;
		case 'r': case 'R':KDrawFont(image,pos,KFontR,value0,value1,value2,transparence); break;
		case 's': case 'S':KDrawFont(image,pos,KFontS,value0,value1,value2,transparence); break;
		case 't': case 'T':KDrawFont(image,pos,KFontT,value0,value1,value2,transparence); break;
		case 'u': case 'U':KDrawFont(image,pos,KFontU,value0,value1,value2,transparence); break;
		case 'v': case 'V':KDrawFont(image,pos,KFontV,value0,value1,value2,transparence); break;
		case 'w': case 'W':KDrawFont(image,pos,KFontW,value0,value1,value2,transparence); break;
		case 'x': case 'X':KDrawFont(image,pos,KFontX,value0,value1,value2,transparence); break;
		case 'y': case 'Y':KDrawFont(image,pos,KFontY,value0,value1,value2,transparence); break;
		case 'z': case 'Z':KDrawFont(image,pos,KFontZ,value0,value1,value2,transparence); break;

		case ' ' :KDrawFont(image,pos,KFontSPACE,value0,value1,value2,transparence); break;
		case '.' :KDrawFont(image,pos,KFontDOT,value0,value1,value2,transparence); break;
		case ':' :KDrawFont(image,pos,KFontCOlON,value0,value1,value2,transparence); break;
		case '-' :KDrawFont(image,pos,KFont_,value0,value1,value2,transparence); break;
		case '0' :KDrawFont(image,pos,KFont0,value0,value1,value2,transparence); break;
		case '1' :KDrawFont(image,pos,KFont1,value0,value1,value2,transparence); break;
		case '2' :KDrawFont(image,pos,KFont2,value0,value1,value2,transparence); break;
		case '3' :KDrawFont(image,pos,KFont3,value0,value1,value2,transparence); break;
		case '4' :KDrawFont(image,pos,KFont4,value0,value1,value2,transparence); break;
		case '5' :KDrawFont(image,pos,KFont5,value0,value1,value2,transparence); break;
		case '6' :KDrawFont(image,pos,KFont6,value0,value1,value2,transparence); break;
		case '7' :KDrawFont(image,pos,KFont7,value0,value1,value2,transparence); break;
		case '8' :KDrawFont(image,pos,KFont8,value0,value1,value2,transparence); break;
		case '9' :KDrawFont(image,pos,KFont9,value0,value1,value2,transparence); break;
		}
		pos.x += (T1)(FONT_WIDTH+charSpace);
	}	
}
template <typename T0,typename T1,typename T2,typename T3> void KDrawFont(KGImage<T0> *image,T1 topLeftX,T1 topLeftY,T3 font,T2 value,bool transparence)
{
	KCoor<T1> topLeft;
	topLeft.x = topLeftX;
	topLeft.y = topLeftY;
	KDrawFont(image,topLeft,font,value,transparence);
}
template <typename T0,typename T1,typename T2,typename T3> void KDrawFont(KCImage<T0> *image,T1 topLeftX,T1 topLeftY,T3 font,T2 value0,T2 value1,T2 value2,bool transparence)
{
	KCoor<T1> topLeft;
	topLeft.x = topLeftX;
	topLeft.y = topLeftY;
	KDrawFont(image,topLeft,font,value0,value1,value2,transparence);
}

#endif