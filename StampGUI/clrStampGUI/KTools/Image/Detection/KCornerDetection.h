// ------------------------------------------------ //
// contain:		Corner Detection Tools
//
// developer:	Krisada Phromsuthirak
// ------------------------------------------------ //

#ifndef _KCornerDetection_H
#define _KCornerDetection_H

#include "KGeneral.h"
#include "KConvolution.h"

template <typename T1, typename T2> void KHarrisCornersResponse(KArray2D<T1> *in, KArray2D<T1> *out, KArray2D<T2> *filterDerivativeHor, KArray2D<T2> *filterDerivativeVer, KArray2D<T2> *filterComponentBlur, double steering = 0.05);
template <typename T1, typename T2> void KHarrisCornersResponse(KArray2D<T1> &in, KArray2D<T1> &out, KArray2D<T2> &filterDerivativeHor, KArray2D<T2> &filterDerivativeVer, KArray2D<T2> &filterComponentBlur, double steering = 0.05);
template <typename T> void KHarrisCornersResponse(KArray2D<T> *in, KArray2D<T> *out, double steering = 0.05);
template <typename T> void KHarrisCornersResponse(KArray2D<T> &in, KArray2D<T> &out, double steering = 0.05);

template <typename T1, typename T2> void KHarrisCornersResponse(KArray2D<T1> *in, KArray2D<T1> *out, KArray2D<T2> *filterDerivativeHor, KArray2D<T2> *filterDerivativeVer, KArray2D<T2> *filterComponentBlur, double steering)
{
	KHarrisCornersResponse(*in, *out, *filterDerivativeHor, *filterDerivativeVer, *filterComponentBlur, steering);
}
template <typename T1, typename T2> void KHarrisCornersResponse(KArray2D<T1> &in, KArray2D<T1> &out, KArray2D<T2> &filterDerivativeHor, KArray2D<T2> &filterDerivativeVer, KArray2D<T2> &filterComponentBlur, double steering)
{
	KArray2D<double> tmp, gx, gy;
	KArray2D<double> a, b, c;

	out.Resize(in.width, in.height);
	tmp.Allocate(in.width, in.height);
	for (int y = 0; y < in.height; y++)
	for (int x = 0; x < in.width; x++)
		tmp.idata[x][y] = (double)in.idata[x][y];

	gx = gy = tmp;
	tmp.FreeMem();

	KConvolution2D(&gx, &filterDerivativeHor);
	KConvolution2D(&gy, &filterDerivativeVer);

	a.Allocate(in.width, in.height);
	b.Allocate(in.width, in.height);
	c.Allocate(in.width, in.height);
	for (int y = 0; y < in.height; y++)
	for (int x = 0; x < in.width; x++)
	{
		a.idata[x][y] = (double)(gx.idata[x][y] * gx.idata[x][y]);
		b.idata[x][y] = (double)(gy.idata[x][y] * gy.idata[x][y]);
		c.idata[x][y] = (double)(gx.idata[x][y] * gy.idata[x][y]);
	}
	gx.FreeMem();
	gy.FreeMem();

	KConvolution2D(&a, &filterComponentBlur);
	KConvolution2D(&b, &filterComponentBlur);
	KConvolution2D(&c, &filterComponentBlur);

	for (int y = 0; y < out.height; y++)
	for (int x = 0; x < out.width; x++)
		out.idata[x][y] = (T)(((double)(a.idata[x][y] * b.idata[x][y]) - (double)(c.idata[x][y] * c.idata[x][y])) - (double)((steering)*(double)Pow((a.idata[x][y] + b.idata[x][y]), 2.0)));

	a.FreeMem();
	b.FreeMem();
	c.FreeMem();
}
template <typename T> void KHarrisCornersResponse(KArray2D<T> *in, KArray2D<T> *out, double steering)
{
	KHarrisCornersResponse(*in, *out, steering);
}
template <typename T> void KHarrisCornersResponse(KArray2D<T> &in, KArray2D<T> &out, double steering)
{
	KArray2D<double> tmp, gx, gy;
	KArray2D<double> a, b, c;
	KArray2D<double> fGx, fGy, fCsmooth;

	fGx.Allocate(3, 3);
	fGy.Allocate(3, 3);
	fCsmooth.Allocate(7, 7);

	fGx.idata[0][0] = -1;	fGx.idata[1][0] = 0;	fGx.idata[2][0] = 1;
	fGx.idata[0][1] = -2;	fGx.idata[1][1] = 0;	fGx.idata[2][1] = 2;
	fGx.idata[0][2] = -1;	fGx.idata[1][2] = 0;	fGx.idata[2][2] = 1;

	fGy.idata[0][0] = -1;	fGy.idata[1][0] = -2;	fGy.idata[2][0] = -1;
	fGy.idata[0][1] = 0;	fGy.idata[1][1] = 0;	fGy.idata[2][1] = 0;
	fGy.idata[0][2] = 1;	fGy.idata[1][2] = 2;	fGy.idata[2][2] = 1;

	KGaussian(fCsmooth, 1.0, KERNEL_MODE_CENTER);

	out.Resize(in.width, in.height);
	tmp.Allocate(in.width, in.height);
	for (int y = 0; y < in.height; y++)
	for (int x = 0; x < in.width; x++)
		tmp.idata[x][y] = (double)in.idata[x][y];

	gx = gy = tmp;
	tmp.FreeMem();

	KConvolution2D(&gx, &fGx);
	KConvolution2D(&gy, &fGy);

	a.Allocate(in.width, in.height);
	b.Allocate(in.width, in.height);
	c.Allocate(in.width, in.height);
	for (int y = 0; y < in.height; y++)
	for (int x = 0; x < in.width; x++)
	{
		a.idata[x][y] = (double)(gx.idata[x][y] * gx.idata[x][y]);
		b.idata[x][y] = (double)(gy.idata[x][y] * gy.idata[x][y]);
		c.idata[x][y] = (double)(gx.idata[x][y] * gy.idata[x][y]);
	}
	gx.FreeMem();
	gy.FreeMem();

	KConvolution2D(&a, &fCsmooth);
	KConvolution2D(&b, &fCsmooth);
	KConvolution2D(&c, &fCsmooth);

	for (int y = 0; y < out.height; y++)
	for (int x = 0; x < out.width; x++)
		out.idata[x][y] = (T)(((double)(a.idata[x][y] * b.idata[x][y]) - (double)(c.idata[x][y] * c.idata[x][y])) - (double)((steering)*(double)Pow((a.idata[x][y] + b.idata[x][y]), 2.0)));

	a.FreeMem();
	b.FreeMem();
	c.FreeMem();

	fGx.FreeMem();
	fGy.FreeMem();
	fCsmooth.FreeMem();
}

#endif