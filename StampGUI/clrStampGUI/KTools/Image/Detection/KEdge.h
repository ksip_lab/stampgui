#ifndef _KEdge_H
#define _KEdge_H

#include "KGeneral.h"
#include "KConvolution.h"
#include "KKernel.h"
#include "KImage.h"
#include <typeinfo> 

#define sobel_mode_norm1 1
#define sobel_mode_norm2 2

template <typename T1> void KEdgeSobel(KArray2D<T1> *in, KArray2D<T1> *out_x, KArray2D<T1> *out_y);
template <typename T1> void KEdgeSobel(KArray2D<T1> *in, KArray2D<T1> *out, int mode);
template <typename T1, typename T2> void KEdgeSobel(KArray2D<T1> *in, KArray2D<T1> *out_x, KArray2D<T1> *out_y, KArray2D<T2> *f_x, KArray2D<T2> *f_y);
template <typename T1> void KEdgeCanny(KArray2D<T1> *in, KArray2D<T1> *out, double threshold1, double threshold2);
template <typename T1> void KEdgeCannyV2(KArray2D<T1> *in, KArray2D<T1> *out);
template <typename T1, typename T2> void KEdgeCannyV2(KArray2D<T1> *in, KArray2D<T1> *out, T2 threshold1, T2 threshold2);
template <typename T1> void KEdgeDeriche(KArray2D<T1> *in, double alpha, KArray2D<T1> *out);

template <typename T1> void KEdgeSobel(KArray2D<T1> *in, KArray2D<T1> *out_x, KArray2D<T1> *out_y)
{
	int sobelhor[3][3] = { -1, -2, -1,
		0, 0, 0,
		1, 2, 1 };

	int sobelver[3][3] = { -1, 0, 1,
		-2, 0, 2,
		-1, 0, 1 };

	KArray2D<int> filter;
	filter.Allocate(3, 3);
	for (int i = 0; i<3; i++)
	for (int j = 0; j<3; j++)
		filter.idata[i][j] = sobelver[i][j];
	*out_x = *in;
	KConvolution2D<T1, int>(out_x, &filter);

	for (int i = 0; i<3; i++)
	for (int j = 0; j<3; j++)
		filter.idata[i][j] = sobelhor[i][j];
	*out_y = *in;
	KConvolution2D<T1, int>(out_y, &filter);

	filter.FreeMem();
}

template <typename T1, typename T2> void KEdgeSobel(KArray2D<T1> *in, KArray2D<T1> *out_x, KArray2D<T1> *out_y, KArray2D<T2> *f_x, KArray2D<T2> *f_y)
{
	*out_x = *in;
	KConvolution2D<T1, int>(out_x, f_x);

	*out_y = *in;
	KConvolution2D<T1, int>(out_y, f_y);
}

template <typename T1> void KEdgeSobel(KArray2D<T1> *in, KArray2D<T1> *out, int mode)
{
	// mode = 2 -> use norm 2 (Euclidean Distance)
	// Other mode -> use norm 1 (Absolute Summable, aka Manhattan Distance)
	KArray2D<T1> out_x, out_y;
	KEdgeSobel<T1>(in, &out_x, &out_y);
	
	out->Allocate(out_x.width, out_x.height);
	for (int i = 0; i < out_x.width; i++)
	{
		for (int j = 0; j < out_x.height; j++)
		{
			if (mode == 2)
			{
				out->idata[i][j] = (T1)sqrt(pow(out_x.idata[i][j], 2) + pow(out_y.idata[i][j], 2));
			}
			else
			{
				out->idata[i][j] = (T1)abs(out_x.idata[i][j]) + abs(out_y.idata[i][j]);
			}
		}
	}

	out_x.FreeMem();
	out_y.FreeMem();
}

template <typename T1> void KEdgeCanny(KArray2D<T1> *in, KArray2D<T1> *out, double threshold1, double threshold2)
{
	Mat dst_gray,edge,src_gray;
	src_gray = Mat(Size(in->width, in->height), CV_8U);

	for (int i = 0; i < in->width; i++)
	{
		for (int j = 0; j < in->height; j++)
		{
			src_gray.at<uchar>(j, i) = in->idata[i][j];
		}
	}

	blur(src_gray, dst_gray, Size(3, 3));
	Canny(dst_gray, edge, threshold1, threshold2);

	out->Allocate(in->width, in->height);

	for (int i = 0; i < in->width; i++)
	{
		for (int j = 0; j < in->height; j++)
		{
			out->idata[i][j] = (T1)edge.at<uchar>(j, i);
		}
	}
}

template <typename T1> void KEdgeCannyV2(KArray2D<T1> *in, KArray2D<T1> *out)
{
	// Apply Gaussian filter to smooth the image in order to remove the noise
	KArray2D<double> gauss_mask;
	double sigma = 1.4;
	gauss_mask.Allocate(5, 5);
	KGaussian<double>(&gauss_mask, sigma, sigma);
	KArray2D<T1> B;
	B = *in;
	KConvolution2D<T1, double>(&B, &gauss_mask);
	
	//Find the intensity gradients of the image
	KArray2D<double> gx, gy, G, T;
	KEdgeSobel<T1>(&B, &gx, &gy);
	G.Allocate(gx.width, gx.height);
	T.Allocate(gx.width, gx.height);
	for (int u = 0; u < gx.width; u++)
	{
		for (int v = 0; v < gx.height; v++)
		{
			G.idata[u][v] = sqrt(pow(gx.idata[u][v], 2) + pow(gy.idata[u][v], 2));
			T.idata[u][v] = atan2(gy.idata[u][v], gx.idata[u][v]) * 180 / PI;
			if (T.idata[u][v] < 0.0)
				T.idata[u][v] += 180.0;

			if (T.idata[u][v] >= 0.0 && T.idata[u][v] <= 22.5)
				T.idata[u][v] = 0.0;
			else if (T.idata[u][v] > 22.5 && T.idata[u][v] <= 67.5)
				T.idata[u][v] = 45.0;
			else if (T.idata[u][v] > 67.5 && T.idata[u][v] <= 112.5)
				T.idata[u][v] = 90.0;
			else if (T.idata[u][v] > 112.5 && T.idata[u][v] <= 157.5)
				T.idata[u][v] = 135.0;
			else// if (T.idata[u][v] > 157.5 && T.idata[u][v] <= 180.0)
				T.idata[u][v] = 0.0;
		}
	}

	//Apply non-maximum suppression to get rid of spurious response to edge detection
	out->Allocate(G.width, G.height);
	for (int u = 0; u < G.width; u++)
	for (int v = 0; v < G.height; v++)
	{
		out->idata[u][v] = (T1)0.0;
		int uu, vv;
		if (T.idata[u][v] == 0.0)
		{
			uu = 1;
			vv = 0;
		}
		else if(T.idata[u][v] == 45.0)
		{
			uu = 1;
			vv = -1;
		}
		else if (T.idata[u][v] == 90.0)
		{
			uu = 0;
			vv = -1;
		}
		else
		{
			uu = -1;
			vv = -1;
		}

		vector<double> g_val;
		g_val.push_back(G.idata[u][v]);
		if (G.IsInBound(u + vv, v - uu))
			g_val.push_back(G.idata[u + vv][v - uu]);
		if (G.IsInBound(u - vv, v + uu))
			g_val.push_back(G.idata[u - vv][v + uu]);
		if (g_val.size() > 1)
		{
			int max_idx = 0;
			for (int i = 1; i < g_val.size(); i++)
			{
				if (g_val.at(max_idx) < g_val.at(i))
					max_idx = i;
			}

			if (max_idx == 0)
				out->idata[u][v] = (T1)G.idata[u][v];
		}
	}

	/*KGImage<double> tmp_img;
	tmp_img = *out;
	tmp_img.Scaling();
	tmp_img.Save("D:\\canny.bmp");*/
	////tmp_img.Show("Out", true, true);
}

template <typename T1, typename T2> void KEdgeCannyV2(KArray2D<T1> *in, KArray2D<T1> *out, T2 threshold1, T2 threshold2)
{
	int max_intensity = 255;
	KEdgeCannyV2<T1>(in, out);
	for (int u = 0; u < out->width; u++)
	for (int v = 0; v < out->height; v++)
	{
		if (out->idata[u][v] >= (T1)threshold2)
			out->idata[u][v] = (T1)max_intensity;
		else if (out->idata[u][v] >= (T1)threshold1)
			out->idata[u][v] = (T1)max_intensity / 2;
		else
			out->idata[u][v] = (T1)0;
	}

	KQueue<KCoor<int>>	tmpQ;
	KCoor<int>			tmpCoor1, tmpCoor2;
	for (int u = 0; u < out->width; u++)
	for (int v = 0; v < out->height; v++)
	{
		if (out->idata[u][v] == (T1)max_intensity)
		{
			for (int uu = -1; uu <= 1; uu++)
			for (int vv = -1; vv <= 1; vv++)
			{
				if (out->IsInBound(u + uu, v + vv))
				{
					if (out->idata[u + uu][v + vv] == (T1)max_intensity / 2)
					{
						tmpCoor1.x = u + uu;
						tmpCoor1.y = v + vv;
						tmpQ.Enqueue(tmpCoor1);
						while (!tmpQ.empty())
						{
							tmpCoor1 = tmpQ.Dequeue();
							if (out->IsInBound(tmpCoor1.x, tmpCoor1.y) && out->idata[tmpCoor1.x][tmpCoor1.y] == (T1)max_intensity / 2)
							{
								out->idata[tmpCoor1.x][tmpCoor1.y] = (T1)max_intensity;
								tmpCoor2.x = tmpCoor1.x + 1;		tmpCoor2.y = tmpCoor1.y;			tmpQ.Enqueue(tmpCoor2);
								tmpCoor2.x = tmpCoor1.x;			tmpCoor2.y = tmpCoor1.y + 1;		tmpQ.Enqueue(tmpCoor2);
								tmpCoor2.x = tmpCoor1.x - 1;		tmpCoor2.y = tmpCoor1.y;			tmpQ.Enqueue(tmpCoor2);
								tmpCoor2.x = tmpCoor1.x;			tmpCoor2.y = tmpCoor1.y - 1;		tmpQ.Enqueue(tmpCoor2);

								tmpCoor2.x = tmpCoor1.x + 1;	tmpCoor2.y = tmpCoor1.y + 1;		tmpQ.Enqueue(tmpCoor2);
								tmpCoor2.x = tmpCoor1.x + 1;	tmpCoor2.y = tmpCoor1.y - 1;		tmpQ.Enqueue(tmpCoor2);
								tmpCoor2.x = tmpCoor1.x - 1;	tmpCoor2.y = tmpCoor1.y + 1;		tmpQ.Enqueue(tmpCoor2);
								tmpCoor2.x = tmpCoor1.x - 1;	tmpCoor2.y = tmpCoor1.y - 1;		tmpQ.Enqueue(tmpCoor2);
							}
						}
					}
				}
			}
		}
	}

	for (int u = 0; u < out->width; u++)
	for (int v = 0; v < out->height; v++)
	{
		if (out->idata[u][v] == (T1)max_intensity / 2)
			out->idata[u][v] = (T1)0;
	}

	/*KGImage<double> tmp_img;
	tmp_img = *out;
	tmp_img.Scaling();
	tmp_img.Save("D:\\canny4.bmp");*/
}

template <typename T1> void KEdgeDeriche(KArray2D<T1> *in, double alpha, KArray2D<T1> *out)
{
	////Deriche filter coefficients
	//double k, a1, a2, a3, a4, a5, a6, a7, a8, b1, b2, c1, c2;
	//KArray2D<T1> B;
	//
	//// +++++++++++++++++++++++++++ Smoothing
	//k = ((1 - exp(-alpha))*(1 - exp(-alpha))) / (1 + (2 * alpha*exp(-alpha)) - exp(-2 * alpha));

	//a1 = k;
	//a2 = k*exp(-alpha)*(alpha - 1);
	//a3 = k*exp(-alpha)*(alpha + 1);
	//a4 = -k*exp(-2 * alpha);

	//a5 = k;
	//a6 = k*exp(-alpha)*(alpha - 1);
	//a7 = k*exp(-alpha)*(alpha + 1);
	//a8 = -k*exp(-2 * alpha);

	//b1 = 2 * exp(-alpha);
	//b2 = -exp(-2 * alpha);

	//c1 = 1;
	//c2 = 1;
	//
	//KArray2D<T1> y1, y2, theta;
	//theta.Allocate(in->width, in->height);

	//// Horizontal
	//y1.Allocate(in->width, in->height);
	//y2.Allocate(in->width, in->height);
	//for (int v = 0; v < in->height; v++)
	//{
	//	for (int u = 0; u < in->width; u++)
	//	{
	//		y1.idata[u][v] = (T1)null_val;
	//		y2.idata[u][v] = (T1)null_val;
	//	}
	//}

	//for (int v = 0; v < in->height; v++)
	//{
	//	for (int u = 0; u < in->width; u++)
	//	{
	//		y1.idata[u][v] = 0;
	//		if (in->IsInBound(u, v))
	//			y1.idata[u][v] += a1*in->idata[u][v];

	//		if (in->IsInBound(u - 1, v))
	//			y1.idata[u][v] += a2*in->idata[u - 1][v];

	//		if (y1.IsInBound(u-1,v))
	//		if (y1.idata[u - 1][v] != (T1)null_val)
	//			y1.idata[u][v] += b1*y1.idata[u - 1][v];

	//		if (y1.IsInBound(u - 2, v))
	//		if (y1.idata[u - 2][v] != (T1)null_val)
	//			y1.idata[u][v] += b2*y1.idata[u - 2][v];
	//	}
	//}

	//for (int v = 0; v < in->height; v++)
	//{
	//	for (int u = in->width - 1; u >= 0; u--)
	//	{
	//		y2.idata[u][v] = 0;
	//		if (in->IsInBound(u + 1, v))
	//			y2.idata[u][v] += a3*in->idata[u + 1][v];

	//		if (in->IsInBound(u + 2, v))
	//			y2.idata[u][v] += a4*in->idata[u + 2][v];

	//		if (y2.IsInBound(u + 1, v))
	//		if (y2.idata[u + 1][v] != (T1)null_val)
	//			y2.idata[u][v] += b1*y2.idata[u + 1][v];

	//		if (y2.IsInBound(u + 2, v))
	//		if (y2.idata[u + 2][v] != (T1)null_val)
	//			y2.idata[u][v] += b2*y2.idata[u + 2][v];

	//		theta.idata[u][v] = c1*(y1.idata[u][v] + y2.idata[u][v]);
	//	}
	//}

	//// Vertical
	//y1.FreeMem();
	//y2.FreeMem();
	//y1.Allocate(in->width, in->height);
	//y2.Allocate(in->width, in->height);
	//for (int v = 0; v < in->height; v++)
	//{
	//	for (int u = 0; u < in->width; u++)
	//	{
	//		y1.idata[u][v] = (T1)null_val;
	//		y2.idata[u][v] = (T1)null_val;
	//	}
	//}

	//for (int u = 0; u < in->width; u++)
	//{
	//	for (int v = 0; v < in->height; v++)
	//	{
	//		y1.idata[u][v] = 0;
	//		if (theta.IsInBound(u, v))
	//			y1.idata[u][v] += a5*theta.idata[u][v];

	//		if (theta.IsInBound(u, v - 1))
	//			y1.idata[u][v] += a6*theta.idata[u][v - 1];

	//		if (y1.IsInBound(u, v - 1))
	//		if (y1.idata[u][v - 1] != (T1)null_val)
	//			y1.idata[u][v] += b1*y1.idata[u][v - 1];

	//		if (y1.IsInBound(u, v - 2))
	//		if (y1.idata[u][v - 2] != (T1)null_val)
	//			y1.idata[u][v] += b2*y1.idata[u][v - 2];
	//	}
	//}

	//for (int u = 0; u < in->width; u++)
	//{
	//	for (int v = in->height - 1; v >= 0; v--)
	//	{
	//		y2.idata[u][v] = 0;
	//		if (theta.IsInBound(u, v + 1))
	//			y2.idata[u][v] += a7*theta.idata[u][v + 1];

	//		if (theta.IsInBound(u, v + 2))
	//			y2.idata[u][v] += a8*theta.idata[u][v + 2];

	//		if (y2.IsInBound(u, v + 1))
	//		if (y2.idata[u][v + 1] != (T1)null_val)
	//			y2.idata[u][v] += b1*y2.idata[u][v + 1];

	//		if (y2.IsInBound(u, v + 2))
	//		if (y2.idata[u][v + 2] != (T1)null_val)
	//			y2.idata[u][v] += b2*y2.idata[u][v + 2];

	//		theta.idata[u][v] = c2*(y1.idata[u][v] + y2.idata[u][v]);
	//	}
	//}
	//B = theta;
	///*KGImage<double> tmp_img;
	//tmp_img = theta;
	//tmp_img.Scaling();
	//tmp_img.Save("D:\\smooth.bmp");*/

	////Find the intensity gradients of the image
	//KArray2D<double> gx, gy, G, T;
	//KEdgeSobel<T1>(&B, &gx, &gy);
	//G.Allocate(gx.width, gx.height);
	//T.Allocate(gx.width, gx.height);
	//for (int u = 0; u < gx.width; u++)
	//{
	//	for (int v = 0; v < gx.height; v++)
	//	{
	//		G.idata[u][v] = sqrt(pow(gx.idata[u][v], 2) + pow(gy.idata[u][v], 2));
	//		T.idata[u][v] = atan2(gy.idata[u][v], gx.idata[u][v]) * 180 / PI;
	//		if (T.idata[u][v] < 0.0)
	//			T.idata[u][v] += 180.0;

	//		if (T.idata[u][v] >= 0.0 && T.idata[u][v] <= 22.5)
	//			T.idata[u][v] = 0.0;
	//		else if (T.idata[u][v] > 22.5 && T.idata[u][v] <= 67.5)
	//			T.idata[u][v] = 45.0;
	//		else if (T.idata[u][v] > 67.5 && T.idata[u][v] <= 112.5)
	//			T.idata[u][v] = 90.0;
	//		else if (T.idata[u][v] > 112.5 && T.idata[u][v] <= 157.5)
	//			T.idata[u][v] = 135.0;
	//		else// if (T.idata[u][v] > 157.5 && T.idata[u][v] <= 180.0)
	//			T.idata[u][v] = 0.0;
	//	}
	//}

	////Apply non-maximum suppression to get rid of spurious response to edge detection
	//out->Allocate(G.width, G.height);
	//for (int u = 0; u < G.width; u++)
	//for (int v = 0; v < G.height; v++)
	//{
	//	out->idata[u][v] = (T1)0.0;
	//	int uu, vv;
	//	if (T.idata[u][v] == 0.0)
	//	{
	//		uu = 1;
	//		vv = 0;
	//	}
	//	else if (T.idata[u][v] == 45.0)
	//	{
	//		uu = 1;
	//		vv = -1;
	//	}
	//	else if (T.idata[u][v] == 90.0)
	//	{
	//		uu = 0;
	//		vv = -1;
	//	}
	//	else
	//	{
	//		uu = -1;
	//		vv = -1;
	//	}

	//	vector<double> g_val;
	//	g_val.push_back(G.idata[u][v]);
	//	if (G.IsInBound(u + vv, v - uu))
	//		g_val.push_back(G.idata[u + vv][v - uu]);
	//	if (G.IsInBound(u - vv, v + uu))
	//		g_val.push_back(G.idata[u - vv][v + uu]);
	//	if (g_val.size() > 1)
	//	{
	//		int max_idx = 0;
	//		for (int i = 1; i < g_val.size(); i++)
	//		{
	//			if (g_val.at(max_idx) < g_val.at(i))
	//				max_idx = i;
	//		}

	//		if (max_idx == 0)
	//			out->idata[u][v] = (T1)G.idata[u][v];
	//	}
	//}

	//// +++++++++++++++++++++++++++ Calculation of magnitude and gradient direction
	////KArray2D<double> gx, gy, G, T;
	////// Gx
	////k = ((1 - exp(-alpha))*(1 - exp(-alpha))) / (1 + (2 * alpha*exp(-alpha)) - exp(-2 * alpha));

	////a1 = 0;
	////a2 = 1;
	////a3 = -1;
	////a4 = 0;

	////a5 = k;
	////a6 = k*exp(-alpha)*(alpha - 1);
	////a7 = k*exp(-alpha)*(alpha + 1);
	////a8 = -k*exp(-2 * alpha);

	////b1 = 2 * exp(-alpha);
	////b2 = -exp(-2 * alpha);

	////c1 = -((1 - exp(-alpha))*(1 - exp(-alpha)));
	////c2 = 1;

	////theta.FreeMem();
	////theta.Allocate(in->width, in->height);

	////// Horizontal
	////y1.FreeMem();
	////y2.FreeMem();
	////y1.Allocate(in->width, in->height);
	////y2.Allocate(in->width, in->height);
	////for (int v = 0; v < in->height; v++)
	////{
	////	for (int u = 0; u < in->width; u++)
	////	{
	////		y1.idata[u][v] = (T1)null_val;
	////		y2.idata[u][v] = (T1)null_val;
	////	}
	////}

	////for (int v = 0; v < in->height; v++)
	////{
	////	for (int u = 0; u < in->width; u++)
	////	{
	////		y1.idata[u][v] = 0;
	////		if (in->IsInBound(u, v))
	////			y1.idata[u][v] += a1*in->idata[u][v];

	////		if (in->IsInBound(u - 1, v))
	////			y1.idata[u][v] += a2*in->idata[u - 1][v];

	////		if (y1.IsInBound(u - 1, v))
	////		if (y1.idata[u - 1][v] != (T1)null_val)
	////			y1.idata[u][v] += b1*y1.idata[u - 1][v];

	////		if (y1.IsInBound(u - 2, v))
	////		if (y1.idata[u - 2][v] != (T1)null_val)
	////			y1.idata[u][v] += b2*y1.idata[u - 2][v];
	////	}
	////}

	////for (int v = 0; v < in->height; v++)
	////{
	////	for (int u = in->width - 1; u >= 0; u--)
	////	{
	////		y2.idata[u][v] = 0;
	////		if (in->IsInBound(u + 1, v))
	////			y2.idata[u][v] += a3*in->idata[u + 1][v];

	////		if (in->IsInBound(u + 2, v))
	////			y2.idata[u][v] += a4*in->idata[u + 2][v];

	////		if (y2.IsInBound(u + 1, v))
	////		if (y2.idata[u + 1][v] != (T1)null_val)
	////			y2.idata[u][v] += b1*y2.idata[u + 1][v];

	////		if (y2.IsInBound(u + 2, v))
	////		if (y2.idata[u + 2][v] != (T1)null_val)
	////			y2.idata[u][v] += b2*y2.idata[u + 2][v];

	////		theta.idata[u][v] = c1*(y1.idata[u][v] + y2.idata[u][v]);
	////	}
	////}

	////// Vertical
	////y1.FreeMem();
	////y2.FreeMem();
	////y1.Allocate(in->width, in->height);
	////y2.Allocate(in->width, in->height);
	////for (int v = 0; v < in->height; v++)
	////{
	////	for (int u = 0; u < in->width; u++)
	////	{
	////		y1.idata[u][v] = (T1)null_val;
	////		y2.idata[u][v] = (T1)null_val;
	////	}
	////}

	////for (int u = 0; u < in->width; u++)
	////{
	////	for (int v = 0; v < in->height; v++)
	////	{
	////		y1.idata[u][v] = 0;
	////		if (theta.IsInBound(u, v))
	////			y1.idata[u][v] += a5*theta.idata[u][v];

	////		if (theta.IsInBound(u, v - 1))
	////			y1.idata[u][v] += a6*theta.idata[u][v - 1];

	////		if (y1.IsInBound(u, v - 1))
	////		if (y1.idata[u][v - 1] != (T1)null_val)
	////			y1.idata[u][v] += b1*y1.idata[u][v - 1];

	////		if (y1.IsInBound(u, v - 2))
	////		if (y1.idata[u][v - 2] != (T1)null_val)
	////			y1.idata[u][v] += b2*y1.idata[u][v - 2];
	////	}
	////}

	////for (int u = 0; u < in->width; u++)
	////{
	////	for (int v = 0; v < in->height; v++)
	////	{
	////		y2.idata[u][v] = 0;
	////		if (theta.IsInBound(u, v + 1))
	////			y2.idata[u][v] += a7*theta.idata[u][v + 1];

	////		if (theta.IsInBound(u, v + 2))
	////			y2.idata[u][v] += a8*theta.idata[u][v + 2];

	////		if (y2.IsInBound(u, v + 1))
	////		if (y2.idata[u][v + 1] != (T1)null_val)
	////			y2.idata[u][v] += b1*y2.idata[u][v + 1];

	////		if (y2.IsInBound(u, v + 2))
	////		if (y2.idata[u][v + 2] != (T1)null_val)
	////			y2.idata[u][v] += b2*y2.idata[u][v + 2];

	////		theta.idata[u][v] = c2*(y1.idata[u][v] + y2.idata[u][v]);
	////	}
	////}
	////gx = theta;

	////// Gy
	////k = ((1 - exp(-alpha))*(1 - exp(-alpha))) / (1 + (2 * alpha*exp(-alpha)) - exp(-2 * alpha));

	////a1 = k;
	////a2 = k*exp(-alpha)*(alpha - 1);
	////a3 = k*exp(-alpha)*(alpha + 1);
	////a4 = -k*exp(-2 * alpha);

	////a5 = 0;
	////a6 = 1;
	////a7 = -1;
	////a8 = 0;

	////b1 = 2 * exp(-alpha);
	////b2 = -exp(-2 * alpha);

	////c1 = 1;
	////c2 = -((1 - exp(-alpha))*(1 - exp(-alpha)));

	////theta.FreeMem();
	////theta.Allocate(in->width, in->height);

	////// Horizontal
	////y1.FreeMem();
	////y2.FreeMem();
	////y1.Allocate(in->width, in->height);
	////y2.Allocate(in->width, in->height);
	////for (int v = 0; v < in->height; v++)
	////{
	////	for (int u = 0; u < in->width; u++)
	////	{
	////		y1.idata[u][v] = (T1)null_val;
	////		y2.idata[u][v] = (T1)null_val;
	////	}
	////}

	////for (int v = 0; v < in->height; v++)
	////{
	////	for (int u = 0; u < in->width; u++)
	////	{
	////		y1.idata[u][v] = 0;
	////		if (in->IsInBound(u, v))
	////			y1.idata[u][v] += a1*in->idata[u][v];

	////		if (in->IsInBound(u - 1, v))
	////			y1.idata[u][v] += a2*in->idata[u - 1][v];

	////		if (y1.IsInBound(u - 1, v))
	////		if (y1.idata[u - 1][v] != (T1)null_val)
	////			y1.idata[u][v] += b1*y1.idata[u - 1][v];

	////		if (y1.IsInBound(u - 2, v))
	////		if (y1.idata[u - 2][v] != (T1)null_val)
	////			y1.idata[u][v] += b2*y1.idata[u - 2][v];
	////	}
	////}

	////for (int v = 0; v < in->height; v++)
	////{
	////	for (int u = in->width - 1; u >= 0; u--)
	////	{
	////		y2.idata[u][v] = 0;
	////		if (in->IsInBound(u + 1, v))
	////			y2.idata[u][v] += a3*in->idata[u + 1][v];

	////		if (in->IsInBound(u + 2, v))
	////			y2.idata[u][v] += a4*in->idata[u + 2][v];

	////		if (y2.IsInBound(u + 1, v))
	////		if (y2.idata[u + 1][v] != (T1)null_val)
	////			y2.idata[u][v] += b1*y2.idata[u + 1][v];

	////		if (y2.IsInBound(u + 2, v))
	////		if (y2.idata[u + 2][v] != (T1)null_val)
	////			y2.idata[u][v] += b2*y2.idata[u + 2][v];

	////		theta.idata[u][v] = c1*(y1.idata[u][v] + y2.idata[u][v]);
	////	}
	////}

	////// Vertical
	////y1.FreeMem();
	////y2.FreeMem();
	////y1.Allocate(in->width, in->height);
	////y2.Allocate(in->width, in->height);
	////for (int v = 0; v < in->height; v++)
	////{
	////	for (int u = 0; u < in->width; u++)
	////	{
	////		y1.idata[u][v] = (T1)null_val;
	////		y2.idata[u][v] = (T1)null_val;
	////	}
	////}

	////for (int u = 0; u < in->width; u++)
	////{
	////	for (int v = 0; v < in->height; v++)
	////	{
	////		y1.idata[u][v] = 0;
	////		if (theta.IsInBound(u, v))
	////			y1.idata[u][v] += a5*theta.idata[u][v];

	////		if (theta.IsInBound(u, v - 1))
	////			y1.idata[u][v] += a6*theta.idata[u][v - 1];

	////		if (y1.IsInBound(u, v - 1))
	////		if (y1.idata[u][v - 1] != (T1)null_val)
	////			y1.idata[u][v] += b1*y1.idata[u][v - 1];

	////		if (y1.IsInBound(u, v - 2))
	////		if (y1.idata[u][v - 2] != (T1)null_val)
	////			y1.idata[u][v] += b2*y1.idata[u][v - 2];
	////	}
	////}

	////for (int u = 0; u < in->width; u++)
	////{
	////	for (int v = 0; v < in->height; v++)
	////	{
	////		y2.idata[u][v] = 0;
	////		if (theta.IsInBound(u, v + 1))
	////			y2.idata[u][v] += a7*theta.idata[u][v + 1];

	////		if (theta.IsInBound(u, v + 2))
	////			y2.idata[u][v] += a8*theta.idata[u][v + 2];

	////		if (y2.IsInBound(u, v + 1))
	////		if (y2.idata[u][v + 1] != (T1)null_val)
	////			y2.idata[u][v] += b1*y2.idata[u][v + 1];

	////		if (y2.IsInBound(u, v + 2))
	////		if (y2.idata[u][v + 2] != (T1)null_val)
	////			y2.idata[u][v] += b2*y2.idata[u][v + 2];

	////		theta.idata[u][v] = c2*(y1.idata[u][v] + y2.idata[u][v]);
	////	}
	////}
	////gy = theta;

	////tmp_img = gx;
	//////tmp_img.Scaling();
	////tmp_img.Save("D:\\gx.bmp");
	////tmp_img = gy;
	//////tmp_img.Scaling();
	////tmp_img.Save("D:\\gy.bmp");

	////G.Allocate(gx.width, gx.height);
	////T.Allocate(gx.width, gx.height);
	////for (int u = 0; u < gx.width; u++)
	////{
	////	for (int v = 0; v < gx.height; v++)
	////	{
	////		G.idata[u][v] = sqrt(pow(gx.idata[u][v], 2) + pow(gy.idata[u][v], 2));
	////		T.idata[u][v] = atan2(gy.idata[u][v], gx.idata[u][v]) * 180 / PI;
	////		if (T.idata[u][v] < 0.0)
	////			T.idata[u][v] += 180.0;

	////		if (T.idata[u][v] >= 0.0 && T.idata[u][v] <= 22.5)
	////			T.idata[u][v] = 0.0;
	////		else if (T.idata[u][v] > 22.5 && T.idata[u][v] <= 67.5)
	////			T.idata[u][v] = 45.0;
	////		else if (T.idata[u][v] > 67.5 && T.idata[u][v] <= 112.5)
	////			T.idata[u][v] = 90.0;
	////		else if (T.idata[u][v] > 112.5 && T.idata[u][v] <= 157.5)
	////			T.idata[u][v] = 135.0;
	////		else// if (T.idata[u][v] > 157.5 && T.idata[u][v] <= 180.0)
	////			T.idata[u][v] = 0.0;
	////	}
	////}

	////tmp_img = G;
	////tmp_img.Scaling();
	////tmp_img.Save("D:\\Grad.bmp");
	////tmp_img = T;
	////tmp_img.Scaling();
	////tmp_img.Save("D:\\Theta.bmp");
}

#endif