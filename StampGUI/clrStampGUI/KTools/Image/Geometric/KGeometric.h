// ------------------------------------------------ //
// contain:		Geometric Tools
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KGeometric_H
#define _KGeometric_H

#include "KGeneral.h"
#include "KInterface\Interface_OpenCV\KInterface_OpenCV.h"

//===========================================//
// ------------ Geometric Tools ------------ //
// [0] Rotate (Array2D)
// [1] Flip (Array2D)
//===========================================//

#define FLIP_MODE_HORIZONTAL			0
#define FLIP_MODE_VERTICAL				1

#define ROTATE_MODE_90					0
#define ROTATE_MODE_180					1
#define ROTATE_MODE_270					2

#define ROTATE_INTERPOLATE_NEAREST		0

//===========================================//
template <typename T> void KRotate(KArray2D<T> *in, char mode);
template <typename T> void KRotate(KArray2D<T> *in, double direction, char interpolate, T backgroundValue = NULL);
template <typename T> void KFlip(KArray2D<T> *in, int mode);
template <typename T> void KImageResize(KArray2D<T> *in, int new_x, int new_y);
template <typename T> void KImageResize(KArray2D<T> *in, double new_size);
template <typename T> void KImageResize(KArray3D<T> *in, int new_x, int new_y);
template <typename T> void KImageResize(KArray3D<T> *in, double new_size);
template <typename T> void KArray3DSplitter(KArray3D<T> *in, KArray2D<T> *r, KArray2D<T> *g, KArray2D<T> *b);
template <typename T> void KArray3DMerger(KArray2D<T> *r, KArray2D<T> *g, KArray2D<T> *b, KArray3D<T> *out);
//===========================================//

template <typename T> void KRotate(KArray2D<T> *in, char mode)
{
	if (mode == ROTATE_MODE_90)
	{
		KArray2D<T> tmp;
		tmp = *in;
		in->Resize(tmp.height, tmp.width);
		for (int y = 0, y_end = (tmp.height - 1); y < tmp.height; y++, y_end--)
		for (int x = 0; x < tmp.width; x++)
			in->idata[y_end][x] = tmp.idata[x][y];
		tmp.FreeMem();
	}
	else if (mode == ROTATE_MODE_180)
	{
		for (int y = 0, y_flip = (in->height - 1); y < in->height>>1; y++, y_flip--)
		for (int x = 0, x_flip = (in->width - 1); x < in->width; x++, x_flip--)
			Swap(in->idata[x][y],in->idata[x_flip][y_flip]);
	}
	else if (mode == ROTATE_MODE_270)
	{
		KArray2D<T> tmp;
		tmp = *in;
		in->Resize(tmp.height, tmp.width);
		for (int y = 0; y < tmp.height; y++)
		for (int x = 0, x_end = (tmp.width - 1); x < tmp.width; x++, x_end--)
			in->idata[y][x_end] = tmp.idata[x][y];
		tmp.FreeMem();
	}
	else
		ErrorMessage("Rotate mode error");
}
template <typename T> void KRotate(KArray2D<T> *in, double direction, char interpolate, T backgroundValue)
{
	KArray2D<T> tmp;
	double x_new, y_new;
	int cx, cy;

	tmp = *in;
	cx = in->width >> 1;
	cy = in->height >> 1;	

	for (int y = 0; y < in->height; y++)
	for (int x = 0; x < in->width; x++)
	{
		x_new = ((double)x - cx)*cos(direction) + ((double)y - cy)*sin(direction) + cx;
		y_new = ((double)y - cy)*cos(direction) - ((double)x - cx)*sin(direction) + cy;

		if (interpolate == ROTATE_INTERPOLATE_NEAREST)
		{
			if (tmp.IsInBound((int)x_new, (int)y_new))
				in->idata[x][y] = (T)tmp.idata[(int)x_new][(int)y_new];
			else
				in->idata[x][y] = backgroundValue;
		}
		else
			ErrorMessage("Error [KRotate]: interpolate method wrong");
	}

	tmp.FreeMem();
}
template <typename T> void KFlip(KArray2D<T> *in, int mode)
{
	if (mode == FLIP_MODE_HORIZONTAL)
	{
		for (int y = 0; y < in->height; y++)
		for (int x = 0, x_end = (in->width - 1); x < (in->width >> 1); x++, x_end--)
			Swap(in->idata[x][y], in->idata[x_end][y]);
	}
	else if (mode == FLIP_MODE_VERTICAL)
	{
		for (int y = 0, y_end = (in->height - 1); y < (in->height >> 1); y++, y_end--)
		for (int x = 0; x < in->width; x++)
			Swap(in->idata[x][y], in->idata[x][y_end]);
	}
	else
		ErrorMessage("Flip mode error");
}

template <typename T> void KImageResize(KArray2D<T> *in, int new_width, int new_height)
{
	Mat src,dst;
	KInterface_OpenCV k_cv;
	k_cv.IOpenCV_ConvertKArray2DToMat<T>(in, &src);

	resize(src, dst, Size(new_width, new_height));
	k_cv.IOpenCV_ConvertMatToKArray2D<T>(&dst, in);
}

template <typename T> void KImageResize(KArray2D<T> *in, double new_size)
{
	KImageResize(in, in->width*new_size, in->height*new_size);
}

template <typename T> void KImageResize(KArray3D<T> *in, int new_width, int new_height)
{
	KArray2D<T> r, g, b;
	KArray3DSplitter<T>(in, &r, &g, &b);

	Mat src, dst;
	KInterface_OpenCV k_cv;
	k_cv.IOpenCV_ConvertKArray2DToMat<T>(&r, &src);
	resize(src, dst, Size(new_width, new_height));
	k_cv.IOpenCV_ConvertMatToKArray2D<T>(&dst, &r);

	k_cv.IOpenCV_ConvertKArray2DToMat<T>(&g, &src);
	resize(src, dst, Size(new_width, new_height));
	k_cv.IOpenCV_ConvertMatToKArray2D<T>(&dst, &g);

	k_cv.IOpenCV_ConvertKArray2DToMat<T>(&b, &src);
	resize(src, dst, Size(new_width, new_height));
	k_cv.IOpenCV_ConvertMatToKArray2D<T>(&dst, &b);

	KArray3DMerger<T>(&r, &g, &b, in);
}

template <typename T> void KImageResize(KArray3D<T> *in, double new_size)
{
	KImageResize(in, in->width*new_size, in->height*new_size);
}

template <typename T> void KArray3DSplitter(KArray3D<T> *in, KArray2D<T> *r, KArray2D<T> *g, KArray2D<T> *b)
{
	r->Allocate(in->width, in->height);
	g->Allocate(in->width, in->height);
	b->Allocate(in->width, in->height);
	for (int z = 0; z < in->depth; z++)
	{
		for (int x = 0; x < in->width; x++)
		{
			for (int y = 0; y < in->height; y++)
			{
				if (z == 0)
					r->idata[x][y] = in->idata[z][x][y];
				else if (z==1)
					g->idata[x][y] = in->idata[z][x][y];
				else
					b->idata[x][y] = in->idata[z][x][y];
			}
		}
	}
}

template <typename T> void KArray3DMerger(KArray2D<T> *r, KArray2D<T> *g, KArray2D<T> *b, KArray3D<T> *out)
{
	out->Allocate(3, r->width, r->height);
	for (int z = 0; z < 3; z++)
	{
		for (int x = 0; x < r->width; x++)
		{
			for (int y = 0; y < r->height; y++)
			{
				if (z == 0)
					out->idata[z][x][y] = r->idata[x][y];
				else if (z == 1)
					out->idata[z][x][y] = g->idata[x][y];
				else
					out->idata[z][x][y] = b->idata[x][y];
			}
		}
	}
}

#endif