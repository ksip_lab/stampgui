#ifndef _KMorphologyElement_H
#define _KMorphologyElement_H

#include "KGeneral.h"

template <typename T1> void KElementDisk(KArray2D<T1> *mask, int size);
template <typename T1> void KElementRect(KArray2D<T1> *mask, int size);

template <typename T1> void KElementDisk(KArray2D<T1> *mask, int size)
{
	if (size % 2 == 0)
		cout << "Error! Please use size in Odd number";
	else
	{
		mask->Allocate(size, size);

		for (int i = 0; i <= size/2; i++)
		{
			for (int j = -i; j <= i; j++)
			{
				mask->idata[i][(size / 2) + j] = 1;
				mask->idata[size - i - 1][(size / 2) + j] = 1;
			}
		}

	}
}

template <typename T1> void KElementRect(KArray2D<T1> *mask, int size)
{
	if (size % 2 == 0)
		cout << "Error! Please use size in Odd number";
	else
	{
		mask->FreeMem();
		mask->Allocate(size, size);

		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < size; j++)
			{
				mask->idata[i][j] = 1;
			}
		}

	}
}

#endif