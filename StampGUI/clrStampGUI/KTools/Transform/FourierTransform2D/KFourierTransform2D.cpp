#include "KFourierTransform2D.h"

void KFourierTransform2D::CreateFourierTable(KGImage<double> *TableOrientation,KGImage<double> *TableFrequency)
{
	double	tmpOF;
	int		W_2,H_2;
	W_2		= dataWidth>>1;
	H_2		= dataHeight>>1;
	for(int y=0;y<dataHeight;y++)
	for(int x=0;x<dataWidth;x++)
	{
		tmpOF							= FindOrientationHalf(W_2,H_2,x,y)-PI_2;
		if(tmpOF<0)
			tmpOF						= tmpOF+PI;
		TableOrientation->idata[x][y]	= tmpOF;
		TableFrequency->idata[x][y]		= RSS( (x-W_2),(y-H_2) );
	}
}
KFourierTransform2D::KFourierTransform2D()
{
	isInitial		= false;
}
KFourierTransform2D::~KFourierTransform2D()
{
	ClearMemory();
}
void KFourierTransform2D::Initialization(int DataWidth,int DataHeight,char Component,bool IsShiftToCenter)
{
	dataWidth		= DataWidth;
	dataHeight		= DataHeight;
	component		= Component;
	isShiftToCenter	= IsShiftToCenter;

	tempColComp1.Allocate(dataWidth);
	tempColComp2.Allocate(dataWidth);
	tempRowComp1.Allocate(dataHeight);
	tempRowComp2.Allocate(dataHeight);

	transformCol.Initialization(dataWidth,FOURIERTRANSFORM_COMPONENT_COMPLEX,isShiftToCenter);
	transformRow.Initialization(dataHeight,FOURIERTRANSFORM_COMPONENT_COMPLEX,isShiftToCenter);

	tableOrientation.Allocate(dataWidth,dataHeight);
	tableFrequency.Allocate(dataWidth,dataHeight);

	CreateFourierTable(&tableOrientation,&tableFrequency);

	isInitial		= true;
}
void KFourierTransform2D::ClearMemory()
{
	if(isInitial)
	{
		tempColComp1.FreeMem();
		tempColComp2.FreeMem();
		tempRowComp1.FreeMem();
		tempRowComp2.FreeMem();

		transformCol.ClearMemory();
		transformRow.ClearMemory();

		tableOrientation.FreeMem();
		tableFrequency.FreeMem();

		isInitial	= false;
	}
}
void KFourierTransform2D::ForwardTransform(KArray2D<double> *Comp1,KArray2D<double> *Comp2,KArray1D<double> *SpatialWindowRow,KArray1D<double> *SpatialWindowCol)
{
	if(isInitial)
	{
		if(!Comp1->IsSameSize(dataWidth,dataHeight))
			ErrorMessage("[Comp1] size doesn't match to Initialization parameter");
		if(!Comp2->IsSameSize(dataWidth,dataHeight))
			ErrorMessage("[Comp2] size doesn't match to Initialization parameter");
		if(SpatialWindowRow!=NULL)
			if(!SpatialWindowRow->IsSameSize(dataHeight))
				ErrorMessage("[SpatialWindowRow] size doesn't match to Initialization parameter");
		if(SpatialWindowCol!=NULL)
			if(!SpatialWindowCol->IsSameSize(dataWidth))
				ErrorMessage("[SpatialWindowCol] size doesn't match to Initialization parameter");

		for(int j=0;j<Comp1->height;j++)
		{
			for(int i=0;i<Comp1->width;i++)
			{
				tempColComp1.idata[i] = Comp1->idata[i][j];
				tempColComp2.idata[i] = Comp2->idata[i][j];
			}
			transformCol.ForwardTransform(&tempColComp1,&tempColComp2,SpatialWindowCol);
			for(int i=0;i<Comp1->width;i++)
			{
				Comp1->idata[i][j] = tempColComp1.idata[i];
				Comp2->idata[i][j] = tempColComp2.idata[i];
			}
		}
		for(int i=0;i<Comp1->width;i++)
		{
			for(int j=0;j<Comp1->height;j++)
			{
				tempRowComp1.idata[j] = Comp1->idata[i][j];
				tempRowComp2.idata[j] = Comp2->idata[i][j];
			}
			transformRow.ForwardTransform(&tempRowComp1,&tempRowComp2,SpatialWindowRow);
			for(int j=0;j<Comp1->height;j++)
			{
				if(component==FOURIERTRANSFORM_COMPONENT_COMPLEX)
				{
					Comp1->idata[i][j] = tempRowComp1.idata[j];
					Comp2->idata[i][j] = tempRowComp2.idata[j];
				}
				else if(component==FOURIERTRANSFORM_COMPONENT_VECTOR)
				{
					Comp1->idata[i][j] = RSS(tempRowComp1.idata[j],tempRowComp2.idata[j]);
					Comp2->idata[i][j] = atan2(tempRowComp2.idata[j],tempRowComp1.idata[j]);
				}
				else
					ErrorMessage("Fourier Transform component mode Error");
			}
		}
	}
	else
		ErrorMessage("Should be Initialization before using <ForwardTransform>");

	
}
void KFourierTransform2D::InverseTransform(KArray2D<double> *Comp1,KArray2D<double> *Comp2)
{
	if(isInitial)
	{
		if(!Comp1->IsSameSize(dataWidth,dataHeight))
			ErrorMessage("[Comp1] size doesn't match to Initialization parameter");
		if(!Comp2->IsSameSize(dataWidth,dataHeight))
			ErrorMessage("[Comp2] size doesn't match to Initialization parameter");

		for(int j=0;j<Comp1->height;j++)
		{
			for(int i=0;i<Comp1->width;i++)
			{
				if(component==FOURIERTRANSFORM_COMPONENT_COMPLEX)
				{
					tempColComp1.idata[i]	= Comp1->idata[i][j];
					tempColComp2.idata[i]	= Comp2->idata[i][j];
				}
				else if(component==FOURIERTRANSFORM_COMPONENT_VECTOR)
				{
					tempColComp1.idata[i]	= Comp1->idata[i][j]*cos(Comp2->idata[i][j]);
					tempColComp2.idata[i]	= Comp1->idata[i][j]*sin(Comp2->idata[i][j]);
				}
				else
					ErrorMessage("Fourier Transform component mode Error");
			}
			transformCol.InverseTransform(&tempColComp1,&tempColComp2);
			for(int i=0;i<Comp1->width;i++)
			{
				Comp1->idata[i][j] = tempColComp1.idata[i];
				Comp2->idata[i][j] = tempColComp2.idata[i];
			}
		}
		for(int i=0;i<Comp1->width;i++)
		{
			for(int j=0;j<Comp1->height;j++)
			{
				tempRowComp1.idata[j] = Comp1->idata[i][j];
				tempRowComp2.idata[j] = Comp2->idata[i][j];
			}
			transformRow.InverseTransform(&tempRowComp1,&tempRowComp2);
			for(int j=0;j<Comp1->height;j++)
			{
				Comp1->idata[i][j] = tempRowComp1.idata[j];
				Comp2->idata[i][j] = tempRowComp2.idata[j];
			}
		}
	}
	else
		ErrorMessage("Should be Initialization before using <InverseTransform>");
}