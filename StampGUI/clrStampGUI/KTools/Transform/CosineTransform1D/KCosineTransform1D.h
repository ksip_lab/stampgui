// ------------------------------------------------ //
// contain:		Cosine Transform 1-Deimension
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KCosineTransform1D_H
#define _KCosineTransform1D_H

#include "KGeneral.h"

#define COSINETRANSFORM_TYPE_1		1
#define COSINETRANSFORM_TYPE_2		2
#define COSINETRANSFORM_TYPE_3		3
#define COSINETRANSFORM_TYPE_4		4

class KCosineTransform1D
{
private:
	
	bool				isInitial;
	int					dataSize;
	char				type;

	KArray1D<double>	tempData;
	double				PI_DATASIZE;
	double				PI_2DATASIZE;
	double				SQRT1_DATASIZE;
	double				SQRT2_DATASIZE;

public:

	KCosineTransform1D();
	~KCosineTransform1D();

	void Initialization(int DataSize, char Type = COSINETRANSFORM_TYPE_2);
	void ClearMemory();
	void ForwardTransform(KArray1D<double> *data, KArray1D<double> *SpatialWindow = NULL);
	void InverseTransform(KArray1D<double> *data);

};

#endif