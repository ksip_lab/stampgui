// ------------------------------------------------ //
// contain:		Cosine Transform 2-Deimension
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KCosineTransform2D_H
#define _KCosineTransform2D_H

#include "KImage.h"
#include "KCosineTransform1D.h"

class KCosineTransform2D
{
private:

	bool					isInitial;
	char					type;
	int						dataWidth;
	int						dataHeight;

	KArray1D<double>		tempColComp;
	KArray1D<double>		tempRowComp;
	KCosineTransform1D		transformCol;
	KCosineTransform1D		transformRow;

	void CreateCosineTable(KGImage<double> *TableOrientation, KGImage<double> *TableFrequency);

public:

	KCosineTransform2D();
	~KCosineTransform2D();

	KGImage<double>			tableOrientation;
	KGImage<double>			tableFrequency;

	void Initialization(int DataWidth, int DataHeight, char Type = COSINETRANSFORM_TYPE_4);
	void ClearMemory();
	void ForwardTransform(KArray2D<double> *data, KArray1D<double> *SpatialWindowRow = NULL, KArray1D<double> *SpatialWindowCol = NULL);
	void InverseTransform(KArray2D<double> *data);

};

#endif