#include "KFourierTransform2D_Res.h"

void KFourierTransform2D_Res::CreateFourierTable(KGImage<double> *TableOrientation,KGImage<double> *TableFrequency)
{
	if(TableOrientation->width!=blocksizeInner||TableOrientation->height!=blocksizeInner)
		ErrorMessage("[TableOrientation] size error");
	if(TableFrequency->width!=blocksizeInner||TableFrequency->height!=blocksizeInner)
		ErrorMessage("[TableFrequency] size error");

	double tmpOF;
	for(int y=0;y<blocksizeInner;y++)
	for(int x=0;x<blocksizeInner;x++)
	{
		tmpOF							= FindOrientationHalf(inner_2,inner_2,x,y)-PI_2;
		if(tmpOF<0)
			tmpOF						= tmpOF+PI;
		TableOrientation->idata[x][y]	= tmpOF;
		TableFrequency->idata[x][y]		= RSS( (x-inner_2),(y-inner_2) );
	}
}
void KFourierTransform2D_Res::ReOrderToSubband(KGImage<double> *ImgComp1,KGImage<double> *ImgComp2)
{
	int xBlockPos,yBlockPos;
	int xSpatialPos,ySpatialPos;
	int n=0;

	for(int y=0;y<outputHeight;y++)
	for(int x=0;x<outputWidth;x++)
	{
		xBlockPos	= x%this->width;
		yBlockPos	= y%this->height;
		xSpatialPos	= (int)floor((double)(x/this->width));
		ySpatialPos	= (int)floor((double)(y/this->height));

		this->idata[xBlockPos][yBlockPos].setNode(n,xSpatialPos,ySpatialPos,&(ImgComp1->idata[x][y]),&(ImgComp2->idata[x][y]));
		n++;
	}
}
void KFourierTransform2D_Res::BlockToPixel(int block_X,int block_Y,int &pixel_X,int &pixel_Y)
{
	if(isInitial)
	{
		pixel_X = (int)(block_X*shiftsize);
		pixel_Y = (int)(block_Y*shiftsize);
	}
	else
		ErrorMessage("Should be Initialization before using <BlockToPixel>");
}
void KFourierTransform2D_Res::PixelToBlock(int pixel_X,int pixel_Y,int &block_X,int &block_Y)
{
	if(isInitial)
	{
		block_X	= (int)(pixel_X/shiftsize);
		block_Y	= (int)(pixel_Y/shiftsize);
	}
	else
		ErrorMessage("Should be Initialization before using <PixelToBlock>");
}
KFourierTransform2D_Res::KFourierTransform2D_Res()
{
	isInitial = false;
}
KFourierTransform2D_Res::~KFourierTransform2D_Res()
{
	ClearMemory();
}
void KFourierTransform2D_Res::Initialization(int DataWidth,int DataHeight,int BlocksizeInner,int BlocksizeOuter,int ShiftSize,char Component,char Padding,double PaddingValue,bool IsShiftToCenter)
{
	if(BlocksizeInner<0)
		ErrorMessage("[BlocksizeInner] should larger than 0");
	if(BlocksizeOuter<0)
		ErrorMessage("[BlocksizeOuter] should larger than 0");
	if(ShiftSize<0)
		ErrorMessage("[ShiftSize] should larger than 0");
	if(BlocksizeInner>BlocksizeOuter)
		ErrorMessage("[BlocksizeInner] should smaller than BlocksizeOuter");
	if(ShiftSize>BlocksizeInner)
		ErrorMessage("[ShiftSize] should smaller than BlocksizeInner");
	if(DataWidth%ShiftSize!=0 || DataHeight%ShiftSize!=0)
		ErrorMessage("[ShiftSize] should modulate by DataWidth & DataHeight to zero");

	dataWidth		= DataWidth;
	dataHeight		= DataHeight;
	blocksizeInner	= BlocksizeInner;
	blocksizeOuter	= BlocksizeOuter;
	shiftsize		= ShiftSize;
	component		= Component;
	isShiftToCenter	= IsShiftToCenter;
	padding			= Padding;
	paddingValue	= PaddingValue;

	pointStart		= (int)((blocksizeOuter - blocksizeInner)>>1);
	pointEnd		= (int)((blocksizeOuter + blocksizeInner)>>1);

	pointOvlpStart	= (int)((blocksizeOuter - shiftsize)>>1);
	pointOvlpEnd	= (int)((blocksizeOuter + shiftsize)>>1);

	outputWidth		= (int)( blocksizeInner*(dataWidth/shiftsize) );
	outputHeight	= (int)( blocksizeInner*(dataHeight/shiftsize) );

	subW			= dataWidth/shiftsize;
	subH			= dataHeight/shiftsize;

	inner_2			= blocksizeInner>>1;
	outer_2			= blocksizeOuter>>1;
	shift_2			= shiftsize>>1;

	tmp1DRowComp1.Allocate(blocksizeOuter);
	tmp1DRowComp2.Allocate(blocksizeOuter);
	tmp1DColComp1.Allocate(blocksizeOuter);
	tmp1DColComp2.Allocate(blocksizeOuter);

	tmp2DRowComp1.Allocate(blocksizeOuter,dataHeight);
	tmp2DRowComp2.Allocate(blocksizeOuter,dataHeight);

	tmp2DInComp1.Allocate(dataWidth,dataHeight);
	tmp2DInComp2.Allocate(dataWidth,dataHeight);

	inv1DRowComp1.Allocate(blocksizeOuter);
	inv1DRowComp2.Allocate(blocksizeOuter);
	inv1DColComp1.Allocate(blocksizeOuter);
	inv1DColComp2.Allocate(blocksizeOuter);

	inv2DRowComp1.Allocate(blocksizeOuter,(int)(blocksizeOuter*(dataHeight/shiftsize)));
	inv2DRowComp2.Allocate(blocksizeOuter,(int)(blocksizeOuter*(dataHeight/shiftsize)));

	inv2DInComp1.Allocate(outputWidth,outputHeight);
	inv2DInComp2.Allocate(outputWidth,outputHeight);

	transformRow.Initialization(blocksizeOuter,FOURIERTRANSFORM_COMPONENT_COMPLEX,isShiftToCenter);
	transformCol.Initialization(blocksizeOuter,FOURIERTRANSFORM_COMPONENT_COMPLEX,isShiftToCenter);

	tableOrientation.Allocate(blocksizeInner,blocksizeInner);
	tableFrequency.Allocate(blocksizeInner,blocksizeInner);

	Spatial1.Allocate(dataWidth,dataHeight);
	Spatial2.Allocate(dataWidth,dataHeight);
	Comp1.Allocate(outputWidth,outputHeight);
	Comp2.Allocate(outputWidth,outputHeight);

	CreateFourierTable(&tableOrientation,&tableFrequency);
	this->Allocate(blocksizeInner,blocksizeInner);
	for(int v=0;v<blocksizeInner;v++)
		for(int u=0;u<blocksizeInner;u++)
			this->idata[u][v].Initialization(u,v,subW,subH,tableFrequency.idata[u][v],tableOrientation.idata[u][v]);
	ReOrderToSubband(&Comp1,&Comp2);

	isInitial		= true;
}
void KFourierTransform2D_Res::ClearMemory()
{
	if(isInitial)
	{
		Comp1.FreeMem();
		Comp2.FreeMem();

		transformRow.ClearMemory();
		transformCol.ClearMemory();

		tmp1DRowComp1.FreeMem();
		tmp1DRowComp2.FreeMem();
		tmp1DColComp1.FreeMem();
		tmp1DColComp2.FreeMem();

		tmp2DRowComp1.FreeMem();
		tmp2DRowComp2.FreeMem();

		tmp2DInComp1.FreeMem();
		tmp2DInComp2.FreeMem();

		inv1DRowComp1.FreeMem();
		inv1DRowComp2.FreeMem();
		inv1DColComp1.FreeMem();
		inv1DColComp2.FreeMem();

		inv2DRowComp1.FreeMem();
		inv2DRowComp2.FreeMem();

		inv2DInComp1.FreeMem();
		inv2DInComp2.FreeMem();

		tableOrientation.FreeMem();
		tableFrequency.FreeMem();

		Spatial1.FreeMem();
		Spatial2.FreeMem();
		Comp1.FreeMem();
		Comp2.FreeMem();

		for(int v=0;v<blocksizeInner;v++)
			for(int u=0;u<blocksizeInner;u++)
				this->idata[u][v].ClearMemory();
		this->FreeMem();

		isInitial	= false;
	}
}
void KFourierTransform2D_Res::LoadSpatialImage(KArray2D<float> *imgSpatial1,KArray2D<float> *imgSpatial2)
{
	if(!imgSpatial1->IsSameSize(dataWidth,dataHeight))
		ErrorMessage("[imgSpatial1] size doesn't match to Initialization parameter");
	if(!imgSpatial2->IsSameSize(dataWidth,dataHeight))
		ErrorMessage("[imgSpatial2] size doesn't match to Initialization parameter");
	for(int y=0;y<dataHeight;y++)
	for(int x=0;x<dataWidth;x++)
	{
		Spatial1.idata[x][y]	= (double)imgSpatial1->idata[x][y];
		Spatial2.idata[x][y]	= (double)imgSpatial2->idata[x][y];
	}
}
void KFourierTransform2D_Res::LoadSpatialImage(KArray2D<double> *imgSpatial1,KArray2D<double> *imgSpatial2)
{
	if(!imgSpatial1->IsSameSize(dataWidth,dataHeight))
		ErrorMessage("[imgSpatial1] size doesn't match to Initialization parameter");
	if(!imgSpatial2->IsSameSize(dataWidth,dataHeight))
		ErrorMessage("[imgSpatial2] size doesn't match to Initialization parameter");
	for(int y=0;y<dataHeight;y++)
	for(int x=0;x<dataWidth;x++)
	{
		Spatial1.idata[x][y]	= (double)imgSpatial1->idata[x][y];
		Spatial2.idata[x][y]	= (double)imgSpatial2->idata[x][y];
	}
}
void KFourierTransform2D_Res::LoadSpatialImage(KArray2D<int> *imgSpatial1,KArray2D<int> *imgSpatial2)
{
	if(!imgSpatial1->IsSameSize(dataWidth,dataHeight))
		ErrorMessage("[imgSpatial1] size doesn't match to Initialization parameter");
	if(!imgSpatial2->IsSameSize(dataWidth,dataHeight))
		ErrorMessage("[imgSpatial2] size doesn't match to Initialization parameter");
	for(int y=0;y<dataHeight;y++)
	for(int x=0;x<dataWidth;x++)
	{
		Spatial1.idata[x][y]	= (double)imgSpatial1->idata[x][y];
		Spatial2.idata[x][y]	= (double)imgSpatial2->idata[x][y];
	}
}
void KFourierTransform2D_Res::LoadSpatialImage(KArray2D<float> *imgSpatial)
{
	if(!imgSpatial->IsSameSize(dataWidth,dataHeight))
		ErrorMessage("[imgSpatial] size doesn't match to Initialization parameter");
	for(int y=0;y<dataHeight;y++)
	for(int x=0;x<dataWidth;x++)
	{
		Spatial1.idata[x][y]	= (double)imgSpatial->idata[x][y];
		Spatial2.idata[x][y]	= (double)0;
	}
}
void KFourierTransform2D_Res::LoadSpatialImage(KArray2D<double> *imgSpatial)
{
	if(!imgSpatial->IsSameSize(dataWidth,dataHeight))
		ErrorMessage("[imgSpatial] size doesn't match to Initialization parameter");
	for(int y=0;y<dataHeight;y++)
	for(int x=0;x<dataWidth;x++)
	{
		Spatial1.idata[x][y]	= (double)imgSpatial->idata[x][y];
		Spatial2.idata[x][y]	= (double)0;
	}
}
void KFourierTransform2D_Res::LoadSpatialImage(KArray2D<int> *imgSpatial)
{
	if(!imgSpatial->IsSameSize(dataWidth,dataHeight))
		ErrorMessage("[imgSpatial] size doesn't match to Initialization parameter");
	for(int y=0;y<dataHeight;y++)
	for(int x=0;x<dataWidth;x++)
	{
		Spatial1.idata[x][y]	= (double)imgSpatial->idata[x][y];
		Spatial2.idata[x][y]	= (double)0;
	}
}
void KFourierTransform2D_Res::ForwardTransform(KArray2D<int> *ROI,KArray1D<double> *SpatialWindowRow,KArray1D<double> *SpatialWindowCol)
{
	if(isInitial)
	{
		if(ROI!=NULL)
			if(!ROI->IsSameSize(dataWidth,dataHeight))
				ErrorMessage("[ROI] size doesn't match to Initialization parameter");
		if(SpatialWindowRow!=NULL)
			if(!SpatialWindowRow->IsSameSize(blocksizeOuter))
				ErrorMessage("[SpatialWindowRow] size doesn't match to Initialization parameter");
		if(SpatialWindowCol!=NULL)
			if(!SpatialWindowCol->IsSameSize(blocksizeOuter))
				ErrorMessage("[SpatialWindowCol] size doesn't match to Initialization parameter");

		for(int y=0;y<dataHeight;y++)
		for(int x=0;x<dataWidth;x++)
		{
			tmp2DInComp1.idata[x][y]	= Spatial1.idata[x][y];
			tmp2DInComp2.idata[x][y]	= Spatial2.idata[x][y];
		}

		for(int xOverlap=shift_2,xOutput=0;	xOverlap<dataWidth;	xOverlap+=shiftsize,xOutput+=blocksizeInner)
		{
			for(int yPixel=0;	yPixel<dataHeight;	yPixel++)
			{
				// =================================================== //
				// get spatial data, insert to 1D array (for each row) //
				for(int xComp=xOverlap-(outer_2),xTmpRow=0;	xTmpRow<blocksizeOuter;		xComp++,xTmpRow++)
				{
					if(xComp>=0&&xComp<dataWidth)
					{
						tmp1DRowComp1.idata[xTmpRow]	= tmp2DInComp1.idata[xComp][yPixel];
						tmp1DRowComp2.idata[xTmpRow]	= tmp2DInComp2.idata[xComp][yPixel];
					}
					else
					{
						if(padding==FOURIERTRANSFORM_PADDING_MIRROR)
						{
							if(xComp<0)
							{
								tmp1DRowComp1.idata[xTmpRow]	= tmp2DInComp1.idata[abs(xComp)][yPixel];
								tmp1DRowComp2.idata[xTmpRow]	= tmp2DInComp2.idata[abs(xComp)][yPixel];
							}
							else if(xComp>=dataWidth)
							{
								tmp1DRowComp1.idata[xTmpRow]	= tmp2DInComp1.idata[int( dataWidth - (xComp-dataWidth)-1 )][yPixel];
								tmp1DRowComp2.idata[xTmpRow]	= tmp2DInComp2.idata[int( dataWidth - (xComp-dataWidth)-1 )][yPixel];
							}
							else
							{
								tmp1DRowComp1.idata[xTmpRow]	= 0;
								tmp1DRowComp2.idata[xTmpRow]	= 0;
							}
						}
						else if(padding==FOURIERTRANSFORM_PADDING_VALUE)
						{
							tmp1DRowComp1.idata[xTmpRow]	= paddingValue;
							tmp1DRowComp2.idata[xTmpRow]	= paddingValue;
						}
						else
						{
							tmp1DRowComp1.idata[xTmpRow]	= 0;
							tmp1DRowComp2.idata[xTmpRow]	= 0;
						}
					}
				}
				// =================================================== //

				// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //
				// transform 1D array by fourier transform 1D //
				// tmp1DRow size ( blocksizeOuter )
				transformRow.ForwardTransform(&tmp1DRowComp1, &tmp1DRowComp2, SpatialWindowRow);
				// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //

				// ========================================================== //
				// get transform(row) data, insert to 2D array (for each row) //
				// tmp2DRow size ( blocksizeOuter ,dataHeight )               //
				for(int xTmpRow=0;	xTmpRow<blocksizeOuter;	xTmpRow++)
				{
					tmp2DRowComp1.idata[xTmpRow][yPixel]	= tmp1DRowComp1.idata[xTmpRow];
					tmp2DRowComp2.idata[xTmpRow][yPixel]	= tmp1DRowComp2.idata[xTmpRow];
				}
				// ========================================================== //
			}
			for(int xPoint=pointStart,xOut=xOutput;	xPoint<pointEnd;	xPoint++,xOut++)
			{
				for(int yOverlap=shift_2,yOutput=0;	yOverlap<dataHeight;	yOverlap+=shiftsize,yOutput+=blocksizeInner)
				{
					// ============================================================== //
					// get col data(from tmp2DRow), insert to 1D array (for each col) //
					for(int yComp=yOverlap-(outer_2),yTmpCol=0;	yTmpCol<blocksizeOuter;	yComp++,yTmpCol++)
					{
						if(yComp>=0&&yComp<dataHeight)
						{
							tmp1DColComp1.idata[yTmpCol]	= tmp2DRowComp1.idata[xPoint][yComp];
							tmp1DColComp2.idata[yTmpCol]	= tmp2DRowComp2.idata[xPoint][yComp];
						}
						else
						{
							if(padding==FOURIERTRANSFORM_PADDING_MIRROR)
							{
								if(yComp<0)
								{
									tmp1DColComp1.idata[yTmpCol]	= tmp2DRowComp1.idata[xPoint][abs(yComp)];
									tmp1DColComp2.idata[yTmpCol]	= tmp2DRowComp2.idata[xPoint][abs(yComp)];
								}
								else if(yComp>=dataHeight)
								{
									tmp1DColComp1.idata[yTmpCol]	= tmp2DRowComp1.idata[xPoint][int( dataHeight - (yComp-dataHeight)-1 )];
									tmp1DColComp2.idata[yTmpCol]	= tmp2DRowComp2.idata[xPoint][int( dataHeight - (yComp-dataHeight)-1 )];
								}
								else
								{
									tmp1DColComp1.idata[yTmpCol]	= 0;
									tmp1DColComp2.idata[yTmpCol]	= 0;
								}
							}
							else if(padding==FOURIERTRANSFORM_PADDING_VALUE)
							{
								tmp1DColComp1.idata[yTmpCol]	= paddingValue;
								tmp1DColComp2.idata[yTmpCol]	= paddingValue;
							}
							else
							{
								tmp1DColComp1.idata[yTmpCol]	= 0;
								tmp1DColComp2.idata[yTmpCol]	= 0;
							}
						}
					}
					// ============================================================== //

					// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //
					// transform 1D array by fourier transform 1D //
					// tmp1DCol size ( blocksizeOuter )
					transformCol.ForwardTransform(&tmp1DColComp1, &tmp1DColComp2, SpatialWindowCol);
					// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //

					// ================================================================================================= //
					// get transform(col) data, insert to 2D array (OUTPUT)	                                             //
					// tmp2DOut size ( blocksizeInner*(dataWidth/overlapsize) ,blocksizeInner*(dataHeight/overlapsize) ) //
					for(int yTmpCol=pointStart,yOut=yOutput;	yTmpCol<pointEnd;	yTmpCol++,yOut++)
					{
						if(xOut>=0&&xOut<outputWidth&&yOut>=0&&yOut<outputHeight)
						{
							if(ROI!=NULL)
							{
								if (ROI->idata[xOverlap][yOverlap] != 0)
								{
									if(component==FOURIERTRANSFORM_COMPONENT_COMPLEX)
									{
										Comp1.idata[xOut][yOut]	= tmp1DColComp1.idata[yTmpCol];
										Comp2.idata[xOut][yOut]	= tmp1DColComp2.idata[yTmpCol];
									}
									else if(component==FOURIERTRANSFORM_COMPONENT_VECTOR)
									{
										Comp1.idata[xOut][yOut]	= RSS(tmp1DColComp1.idata[yTmpCol],tmp1DColComp2.idata[yTmpCol]);
										Comp2.idata[xOut][yOut]	= atan2(tmp1DColComp2.idata[yTmpCol],tmp1DColComp1.idata[yTmpCol]);
									}
									else
										ErrorMessage("Fourier Transform component mode Error");
								}
								else
								{
									Comp1.idata[xOut][yOut]	= 0;
									Comp2.idata[xOut][yOut]	= 0;
								}
							}
							else
							{
								if(component==FOURIERTRANSFORM_COMPONENT_COMPLEX)
								{
									Comp1.idata[xOut][yOut]	= tmp1DColComp1.idata[yTmpCol];
									Comp2.idata[xOut][yOut]	= tmp1DColComp2.idata[yTmpCol];
								}
								else if(component==FOURIERTRANSFORM_COMPONENT_VECTOR)
								{
									Comp1.idata[xOut][yOut]	= RSS(tmp1DColComp1.idata[yTmpCol],tmp1DColComp2.idata[yTmpCol]);
									Comp2.idata[xOut][yOut]	= atan2(tmp1DColComp2.idata[yTmpCol],tmp1DColComp1.idata[yTmpCol]);
								}
								else
									ErrorMessage("Fourier Transform component mode Error");
							}
						}
					}
					// ================================================================================================= //
				}
			}
		}
	}
	else
		ErrorMessage("Should be Initialization before using <ForwardTransform>");
}
void KFourierTransform2D_Res::InverseTransform(KArray2D<int> *ROI)
{
	if(isInitial)
	{
		if(ROI!=NULL)
			if(!ROI->IsSameSize(dataWidth,dataHeight))
				ErrorMessage("[ROI] size doesn't match to Initialization parameter");

		for(int y=0;y<outputHeight;y++)
		for(int x=0;x<outputWidth;x++)
		{
			if(component==FOURIERTRANSFORM_COMPONENT_COMPLEX)
			{
				inv2DInComp1.idata[x][y]	= Comp1.idata[x][y];
				inv2DInComp2.idata[x][y]	= Comp2.idata[x][y];
			}
			else if(component==FOURIERTRANSFORM_COMPONENT_VECTOR)
			{
				inv2DInComp1.idata[x][y]	= Comp1.idata[x][y]*cos(Comp2.idata[x][y]);
				inv2DInComp2.idata[x][y]	= Comp1.idata[x][y]*sin(Comp2.idata[x][y]);
			}
			else
				ErrorMessage("Fourier Transform component mode Error");
		}

		for(int sx=0,xJump=0;	sx<outputWidth;		sx+=blocksizeInner,xJump+=shiftsize)
		{
			for(int yPixel=0;	yPixel<outputHeight;	yPixel++)
			{
				// ============================================//
				// get data, insert to 1D array (for each row) //
				for(int xArray=0;	xArray<blocksizeOuter;	xArray++)
				{
					if(xArray>=pointStart&&xArray<pointEnd)
					{
						inv1DRowComp1.idata[xArray]	= inv2DInComp1.idata[sx+(xArray-pointStart)][yPixel];
						inv1DRowComp2.idata[xArray]	= inv2DInComp2.idata[sx+(xArray-pointStart)][yPixel];
					}
					else
					{
						inv1DRowComp1.idata[xArray]	= 0;
						inv1DRowComp2.idata[xArray]	= 0;
					}
				}
				// =================================================== //

				// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //
				// transform 1D array by fourier transform 1D //
				transformRow.InverseTransform(&inv1DRowComp1,&inv1DRowComp2);
				// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //

				// ========================================================== //
				// get transform(row) data, insert to 2D array (for each row) //
				for(int xTmpRow=0;	xTmpRow<blocksizeOuter;	xTmpRow++)
				{
					inv2DRowComp1.idata[xTmpRow][yPixel]	= inv1DRowComp1.idata[xTmpRow];
					inv2DRowComp2.idata[xTmpRow][yPixel]	= inv1DRowComp2.idata[xTmpRow];
				}
				// ========================================================== //
			}
			for(int xPoint=pointOvlpStart,xOut=xJump;	xPoint<pointEnd;	xPoint++,xOut++)
			{
				for(int yJump=0,yOverlap=0;	yJump<outputHeight;	yJump+=blocksizeInner,yOverlap+=shiftsize)
				{
					// ============================================================== //
					// get col data(from inv2DRow), insert to 1D array (for each col) //
					for(int yArray=0;	yArray<blocksizeOuter;	yArray++)
					{
						if(yArray>=pointStart&&yArray<pointEnd)
						{
							inv1DColComp1.idata[yArray]	= inv2DRowComp1.idata[xPoint][yJump+(yArray-pointStart)];
							inv1DColComp2.idata[yArray]	= inv2DRowComp2.idata[xPoint][yJump+(yArray-pointStart)];
						}
						else
						{
							inv1DColComp1.idata[yArray]	= 0;
							inv1DColComp2.idata[yArray]	= 0;
						}
					}
					// ============================================================== //

					// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //
					// transform 1D array by fourier transform 1D //
					transformCol.InverseTransform(&inv1DColComp1,&inv1DColComp2);
					// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //

					// ==================================================== //
					// get transform(col) data, insert to 2D array (OUTPUT) //
					for(int yArray=pointOvlpStart,yOut=yOverlap;	yArray<pointOvlpEnd;	yArray++,yOut++)
					{
						if(xOut>=0 && xOut<dataWidth && yOut>=0 && yOut<dataHeight)
						{
							if(ROI!=NULL)
							{
								if (ROI->idata[xOut][yOut] != 0)
								{
									Spatial1.idata[xOut][yOut]	= inv1DColComp1.idata[yArray];
									Spatial2.idata[xOut][yOut]	= inv1DColComp2.idata[yArray];
								}
								else
								{
									Spatial1.idata[xOut][yOut]	= 0;
									Spatial2.idata[xOut][yOut]	= 0;
								}
							}
							else
							{
								Spatial1.idata[xOut][yOut]	= inv1DColComp1.idata[yArray];
								Spatial2.idata[xOut][yOut]	= inv1DColComp2.idata[yArray];
							}
						}
					}
					// ==================================================== //
				}
			}
		}
	}
	else
		ErrorMessage("Should be Initialization before using <InverseTransform>");
}
void KFourierTransform2D_Res::Plot(KGImage<double> *imgComp1, KGImage<double> *imgComp2)
{
	if (!imgComp1->IsSameSize(outputWidth, outputHeight))
		imgComp1->Resize(outputWidth, outputHeight);
	if (!imgComp2->IsSameSize(outputWidth, outputHeight))
		imgComp2->Resize(outputWidth, outputHeight);

	for(int y=0,v=0;v<blocksizeInner;y+=subH,v++)
	for(int x=0,u=0;u<blocksizeInner;x+=subW,u++)
	{
		if(u==blocksizeInner>>1 && v==blocksizeInner>>1)
		{
			for(int py=y,yy=0;py<y+subH;py++,yy++)
			for(int px=x,xx=0;px<x+subW;px++,xx++)
			{
				imgComp1->idata[px][py] = 0;
				imgComp2->idata[px][py] = this->idata[u][v].getComp2(xx, yy);
			}
		}
		else
		{
			for(int py=y,yy=0;py<y+subH;py++,yy++)
			for(int px=x,xx=0;px<x+subW;px++,xx++)
			{
				imgComp1->idata[px][py] = this->idata[u][v].getComp1(xx, yy);
				imgComp2->idata[px][py] = this->idata[u][v].getComp2(xx, yy);
			}
		}		
	}
}