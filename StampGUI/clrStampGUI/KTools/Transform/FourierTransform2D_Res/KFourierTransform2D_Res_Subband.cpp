#include "KFourierTransform2D_Res_Subband.h"

KFourierTransform2D_Res_Subband::KFourierTransform2D_Res_Subband()
{
	isAlloc		= false;
}
KFourierTransform2D_Res_Subband::~KFourierTransform2D_Res_Subband()
{
	ClearMemory();
}
void KFourierTransform2D_Res_Subband::Initialization(int U,int V,int &SubW,int &SubH,double &SubFrequency,double &SubAngular)
{
	u			= U;
	v			= V;
	subW		= &SubW;
	subH		= &SubH;
	subFrq		= &SubFrequency;
	subAng		= &SubAngular;

	this->Allocate(SubW,SubH);

	isAlloc		= true;
}
void KFourierTransform2D_Res_Subband::ClearMemory()
{
	if(isAlloc)
	{
		subFrq		= NULL;
		subAng		= NULL;
		subW		= NULL;
		subH		= NULL;

		for(int y=0;y<this->height;y++)
			for(int x=0;x<this->width;x++)
				this->idata[x][y].ClearMemory();
		this->FreeMem();

		isAlloc		= false;
	}
}
int KFourierTransform2D_Res_Subband::getSubW()
{
	if(isAlloc)
		return *subW;
	else
	{
		ErrorMessage("sub-band didn't Initialize");
		return 0;
	}
}
int KFourierTransform2D_Res_Subband::getSubH()
{
	if(isAlloc)
		return *subH;
	else
	{
		ErrorMessage("sub-band didn't Initialize");
		return 0;
	}
}
double KFourierTransform2D_Res_Subband::getSubFrq()
{
	if(isAlloc)
		return *subFrq;
	else
	{
		ErrorMessage("sub-band didn't Initialize");
		return 0;
	}
}
double KFourierTransform2D_Res_Subband::getSubAng()
{
	if(isAlloc)
		return *subAng;
	else
	{
		ErrorMessage("sub-band didn't Initialize");
		return 0;
	}
}
double KFourierTransform2D_Res_Subband::getComp1(int x,int y)
{
	if(isAlloc)
		return this->idata[x][y].getComp1();
	else
	{
		ErrorMessage("sub-band didn't Initialize");
		return 0;
	}
}
double KFourierTransform2D_Res_Subband::getComp2(int x,int y)
{
	if(isAlloc)
		return this->idata[x][y].getComp2();
	else
	{
		ErrorMessage("sub-band didn't Initialize");
		return 0;
	}
}
void KFourierTransform2D_Res_Subband::setComp1(int x, int y, double Comp1)
{
	if (isAlloc)
		this->idata[x][y].setComp1(Comp1);
	else
		ErrorMessage("sub-band didn't Initialize");
}
void KFourierTransform2D_Res_Subband::setComp2(int x, int y, double Comp2)
{
	if (isAlloc)
		this->idata[x][y].setComp2(Comp2);
	else
		ErrorMessage("sub-band didn't Initialize");
}
void KFourierTransform2D_Res_Subband::getImageComp1(KGImage<double> *imgComp1)
{
	if(isAlloc)
	{
		if (!imgComp1->IsSameSize(getSubW(), getSubH()))
			imgComp1->Resize(getSubW(), getSubH());
		for(int y=0;y<getSubH();y++)
			for(int x=0;x<getSubW();x++)
				imgComp1->idata[x][y] = getComp1(x, y);
	}		
	else
		ErrorMessage("sub-band didn't Initialize");
}
void KFourierTransform2D_Res_Subband::getImageComp2(KGImage<double> *imgComp2)
{
	if(isAlloc)
	{
		if (!imgComp2->IsSameSize(getSubW(), getSubH()))
			imgComp2->Resize(getSubW(), getSubH());
		for(int y=0;y<getSubH();y++)
			for(int x=0;x<getSubW();x++)
				imgComp2->idata[x][y] = getComp2(x, y);
	}		
	else
		ErrorMessage("sub-band didn't Initialize");
}
void KFourierTransform2D_Res_Subband::setNode(long Label,int X,int Y,double *Comp1,double *Comp2)
{
	if(isAlloc)
		this->idata[X][Y].Initialization(Label,u,v,X,Y,subFrq,subAng,Comp1,Comp2);
	else
		ErrorMessage("sub-band didn't Initialize");
}