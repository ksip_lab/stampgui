#ifndef _KFourierTransform2D_Res_Node_H
#define _KFourierTransform2D_Res_Node_H

#include "KHeader.h"
#include "KBasic.h"

class KFourierTransform2D_Res_Node
{
private:
	bool	isAlloc;

public:
	KFourierTransform2D_Res_Node();
	~KFourierTransform2D_Res_Node();

	long	label;
	int		u,v,x,y;
	double	*frq;
	double	*ang;
	double	*comp1;
	double	*comp2;

	void	Initialization(long Label,int U,int V,int X,int Y,double *Frequency,double *Angular,double *Comp1,double *Comp2);
	void	ClearMemory();

	double	getAng();
	double	getFrq();
	double	getComp1();
	double	getComp2();

	void	setComp1(double Comp1);
	void	setComp2(double Comp2);

	void	report();
};

#endif