#include "KFourierTransform1D.h"

void KFourierTransform1D::Scamble(KArray1D<double> *Comp1,KArray1D<double> *Comp2,bool isForward,KArray1D<double> *SpatialWindow)
{
	int i,j,m;
	if (SpatialWindow != NULL && isForward)
	{
		for (i = 0; i < Comp1->length; i++)
		{
			Comp1->idata[i] = Comp1->idata[i] * SpatialWindow->idata[i];
			Comp2->idata[i] = Comp2->idata[i] * SpatialWindow->idata[i];
		}
	}

	if(component==FOURIERTRANSFORM_COMPONENT_VECTOR && !isForward)
	{
		double tmp1,tmp2;
		for(i=0;i<Comp1->length;i++)
		{
			tmp1			= Comp1->idata[i]*cos(Comp2->idata[i]);
			tmp2			= Comp1->idata[i]*sin(Comp2->idata[i]);
			Comp1->idata[i]	= tmp1;
			Comp2->idata[i]	= tmp2;
		}
	}

	for(i=0,j=0;i<Comp1->length;i++)
	{
		if(i>j)
		{
			Swap(Comp1->idata[i],Comp1->idata[j]);
			Swap(Comp2->idata[i],Comp2->idata[j]);
		}
		m=Comp1->length>>1;
		while((j>=m)&(m>=2))
		{
			j-=m;
			m=m>>1;
		}
		j+=m;
	}
}
void KFourierTransform1D::Butterfly(KArray1D<double> *Comp1,KArray1D<double> *Comp2,bool isForward)
{
	double	angle;					//polar angle
	double	w_re,wp_re,temp_re;		//intermediate complex number
	double	w_im,wp_im,temp_im;		//intermediate complex number
	double	wtemp;					//temporary storage for w_re
	int		i,j,k,offset;			//loop variable
	int		N,half_N;				//storage for power of 2
	int		dir;					//transform mode
	int		Log2N;					//x of 2^x=size

	if(isForward)
		dir = 1;
	else
		dir = -1;

	Log2N = PowOf2(Comp1->length);
	N=1;
	for(k=0;k<Log2N;k++)
	{
		half_N	= N;
		N		<<= 1;
		angle	= (double)(-2.0*PI/N*dir);
		wtemp	= (double)sin(0.5*angle);
		wp_re	= (double)(-2.0*wtemp*wtemp);
		wp_im	= (double)sin(angle);
		w_re	= 1.0;
		w_im	= 0.0;
		for(offset=0;offset<half_N;offset++)
		{
			for(i=offset;i<Comp1->length;i+=N)
			{
				j				= i+half_N;
				temp_re			= (w_re*Comp1->idata[j])-(w_im*Comp2->idata[j]);
				temp_im			= (w_im*Comp1->idata[j])+(w_re*Comp2->idata[j]);
				Comp1->idata[j]	= Comp1->idata[i]-temp_re;
				Comp2->idata[j]	= Comp2->idata[i]-temp_im;
				Comp1->idata[i]	+= temp_re;
				Comp2->idata[i]	+= temp_im;
			}
			wtemp	= w_re;
			w_re	= wtemp*wp_re-w_im*wp_im+w_re;
			w_im	= w_im*wp_re+wtemp*wp_im+w_im;
		}
	}
	if(!isForward)
	{
		for(i=0;i<Comp1->length;i++)
		{
			Comp1->idata[i]/=Comp1->length;
			Comp2->idata[i]/=Comp2->length;
		}
	}
	if(component==FOURIERTRANSFORM_COMPONENT_VECTOR && isForward)
	{
		double tmp1,tmp2;
		for(i=0;i<Comp1->length;i++)
		{
			tmp1			= RSS(Comp1->idata[i],Comp2->idata[i]);
			tmp2			= atan2(Comp2->idata[i],Comp1->idata[i]);
			Comp1->idata[i]	= tmp1;
			Comp2->idata[i]	= tmp2;
		}
	}
}
KFourierTransform1D::KFourierTransform1D()
{
	isInitial		= false;
}
KFourierTransform1D::~KFourierTransform1D()
{
	ClearMemory();
}
void KFourierTransform1D::Initialization(int DataSize,char Component,bool IsShiftToCenter)
{
	dataSize		= DataSize;
	component		= Component;
	isShiftToCenter	= IsShiftToCenter;

	tempRe.Allocate(dataSize);
	tempIm.Allocate(dataSize);

	isInitial		= true;
}
void KFourierTransform1D::ClearMemory()
{
	if(isInitial)
	{
		tempRe.FreeMem();
		tempIm.FreeMem();

		isInitial	= false;
	}
}
void KFourierTransform1D::ForwardTransform(KArray1D<double> *Comp1,KArray1D<double> *Comp2,KArray1D<double> *SpatialWindow)
{
	if(isInitial)
	{
		if(Comp1->length!=dataSize)
			ErrorMessage("[Comp1] size doesn't match to Initialization parameter");
		if(Comp2->length!=dataSize)
			ErrorMessage("[Comp2] size doesn't match to Initialization parameter");
		if(SpatialWindow!=NULL)
			if(SpatialWindow->length!=dataSize)
				ErrorMessage("[SpatialWindow] size doesn't match to Initialization parameter");

		if(IsPowOf2(Comp1->length))
		{
			Scamble(Comp1,Comp2,true,SpatialWindow);
			Butterfly(Comp1,Comp2,true);
		}
		else
		{
			double sum_re,sum_im;
			for(int z=0;z<Comp1->length;z++)
			{
				if(SpatialWindow!=NULL)
				{
					tempRe.idata[z]	= Comp1->idata[z] * SpatialWindow->idata[z];
					tempIm.idata[z]	= Comp2->idata[z] * SpatialWindow->idata[z];
				}
				else
				{
					tempRe.idata[z]	= Comp1->idata[z];
					tempIm.idata[z]	= Comp2->idata[z];
				}
			}
			for(int u=0;u<Comp1->length;u++)
			{
				sum_re = 0;
				sum_im = 0;
				for(int x=0;x<Comp1->length;x++)
				{
					sum_re += tempRe.idata[x]*cos(2*PI * x * u / Comp1->length) + tempIm.idata[x]*sin(2*PI * x * u / Comp1->length);
					sum_im += -tempRe.idata[x]*sin(2*PI * x * u / Comp1->length) + tempIm.idata[x]*cos(2*PI * x * u / Comp1->length);
				}

				if(component==FOURIERTRANSFORM_COMPONENT_COMPLEX)
				{
					Comp1->idata[u] = sum_re;
					Comp2->idata[u] = sum_im;
				}
				else if(component==FOURIERTRANSFORM_COMPONENT_VECTOR)
				{
					Comp1->idata[u]	= RSS(sum_re,sum_im);
					Comp2->idata[u]	= atan2(sum_im,sum_re);
				}
				else
					ErrorMessage("Fourier Transform component mode Error");
			}
		}
		if(isShiftToCenter)
		{
			for(int z=0;z<Comp1->length>>1;z++)
			{
				Swap(Comp1->idata[z],Comp1->idata[z+(Comp1->length>>1)]);
				Swap(Comp2->idata[z],Comp2->idata[z+(Comp2->length>>1)]);
			}
		}
	}
	else
		ErrorMessage("Should be Initialization before using <ForwardTransform>");
}
void KFourierTransform1D::InverseTransform(KArray1D<double> *Comp1,KArray1D<double> *Comp2)
{
	if(isInitial)
	{
		if(Comp1->length!=dataSize)
			ErrorMessage("[Comp1] size doesn't match to Initilization parameter");
		if(Comp2->length!=dataSize)
			ErrorMessage("[Comp2] size doesn't match to Initilization parameter");

		if(isShiftToCenter)
		{
			for(int z=0;z<Comp1->length>>1;z++)
			{
				Swap(Comp1->idata[z],Comp1->idata[z+(Comp1->length>>1)]);
				Swap(Comp2->idata[z],Comp2->idata[z+(Comp2->length>>1)]);
			}
		}
		if(IsPowOf2(Comp1->length) && IsPowOf2(Comp2->length))
		{
			Scamble(Comp1,Comp2,false);
			Butterfly(Comp1,Comp2,false);
		}
		else
		{
			double sum_re,sum_im;
			for(int z=0;z<Comp1->length;z++)
			{
				if(component==FOURIERTRANSFORM_COMPONENT_COMPLEX)
				{
					tempRe.idata[z]	= Comp1->idata[z];
					tempIm.idata[z]	= Comp2->idata[z];
				}
				else if(component==FOURIERTRANSFORM_COMPONENT_VECTOR)
				{
					tempRe.idata[z]	= Comp1->idata[z]*cos(Comp2->idata[z]);
					tempIm.idata[z]	= Comp1->idata[z]*sin(Comp2->idata[z]);
				}
				else
					ErrorMessage("Fourier Transform component mode Error");
			}
			for(int u=0;u<Comp1->length;u++)
			{
				sum_re = 0;
				sum_im = 0;
				for(int x=0;x<Comp1->length;x++)
				{
					sum_re += tempRe.idata[x]*cos(2*PI * x * u / Comp1->length) + tempIm.idata[x]*-sin(2*PI * x * u / Comp1->length);
					sum_im += tempRe.idata[x]*sin(2*PI * x * u / Comp2->length) + tempIm.idata[x]*cos(2*PI * x * u / Comp2->length);
				}
				Comp1->idata[u] = sum_re / (Comp1->length);
				Comp2->idata[u] = sum_im / (Comp2->length);
			}
		}
	}
	else
		ErrorMessage("Should be Initialization before using <InverseTransform>");
}