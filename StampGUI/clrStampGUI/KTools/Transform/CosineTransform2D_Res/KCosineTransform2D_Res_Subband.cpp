#include "KCosineTransform2D_Res_Subband.h"

KCosineTransform2D_Res_Subband::KCosineTransform2D_Res_Subband()
{
	isAlloc		= false;
}
KCosineTransform2D_Res_Subband::~KCosineTransform2D_Res_Subband()
{
	ClearMemory();
}
void KCosineTransform2D_Res_Subband::Initialization(int U, int V, int &SubW, int &SubH, double &SubFrequency, double &SubAngular)
{
	u			= U;
	v			= V;
	subW		= &SubW;
	subH		= &SubH;
	subFrq		= &SubFrequency;
	subAng		= &SubAngular;

	this->Allocate(SubW,SubH);

	isAlloc		= true;
}
void KCosineTransform2D_Res_Subband::ClearMemory()
{
	if(isAlloc)
	{
		subFrq		= NULL;
		subAng		= NULL;
		subW		= NULL;
		subH		= NULL;

		for(int y=0;y<this->height;y++)
			for(int x=0;x<this->width;x++)
				this->idata[x][y].ClearMemory();
		this->FreeMem();

		isAlloc		= false;
	}
}
int KCosineTransform2D_Res_Subband::getSubW()
{
	if(isAlloc)
		return *subW;
	else
	{
		ErrorMessage("sub-band didn't Initialize");
		return 0;
	}
}
int KCosineTransform2D_Res_Subband::getSubH()
{
	if(isAlloc)
		return *subH;
	else
	{
		ErrorMessage("sub-band didn't Initialize");
		return 0;
	}
}
double KCosineTransform2D_Res_Subband::getSubFrq()
{
	if(isAlloc)
		return *subFrq;
	else
	{
		ErrorMessage("sub-band didn't Initialize");
		return 0;
	}
}
double KCosineTransform2D_Res_Subband::getSubAng()
{
	if(isAlloc)
		return *subAng;
	else
	{
		ErrorMessage("sub-band didn't Initialize");
		return 0;
	}
}
double KCosineTransform2D_Res_Subband::getMag(int x, int y)
{
	if(isAlloc)
		return this->idata[x][y].getMag();
	else
	{
		ErrorMessage("sub-band didn't Initialize");
		return 0;
	}
}
void KCosineTransform2D_Res_Subband::setMag(int x, int y, double Mag)
{
	if (isAlloc)
		this->idata[x][y].setMag(Mag);
	else
		ErrorMessage("sub-band didn't Initialize");
}
void KCosineTransform2D_Res_Subband::getImageMag(KGImage<double> *imgMagnitude)
{
	if(isAlloc)
	{
		if(!imgMagnitude->IsSameSize(getSubW(),getSubH()))
			imgMagnitude->Resize(getSubW(),getSubH());
		for(int y=0;y<getSubH();y++)
			for(int x=0;x<getSubW();x++)
				imgMagnitude->idata[x][y] = getMag(x,y);
	}		
	else
		ErrorMessage("sub-band didn't Initialize");
}
void KCosineTransform2D_Res_Subband::setNode(long Label, int X, int Y, double *Magnitude)
{
	if (isAlloc)
		this->idata[X][Y].Initialization(Label, u, v, X, Y, subFrq, subAng, Magnitude);
	else
		ErrorMessage("sub-band didn't Initialize");
}