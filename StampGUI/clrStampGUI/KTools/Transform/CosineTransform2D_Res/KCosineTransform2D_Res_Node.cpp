#include "KCosineTransform2D_Res_Node.h"

KCosineTransform2D_Res_Node::KCosineTransform2D_Res_Node()
{
	isAlloc		= false;
}
KCosineTransform2D_Res_Node::~KCosineTransform2D_Res_Node()
{
	ClearMemory();
}
void KCosineTransform2D_Res_Node::Initialization(long Label, int U, int V, int X, int Y, double *Frequency, double *Angular, double *Magnitude)
{
	label		= Label;
	u			= U;
	v			= V;
	x			= X;
	y			= Y;
	frq			= Frequency;
	ang			= Angular;
	mag			= Magnitude;

	isAlloc		= true;
}
void KCosineTransform2D_Res_Node::ClearMemory()
{
	if(isAlloc)
	{
		ang			= NULL;
		frq			= NULL;
		mag			= NULL;

		isAlloc		= false;
	}
}
double KCosineTransform2D_Res_Node::getAng()
{
	if(isAlloc)
		return *ang;
	else
	{
		ErrorMessage("node didn't Initialize");
		return 0;
	}
}
double KCosineTransform2D_Res_Node::getFrq()
{
	if(isAlloc)
		return *frq;
	else
	{
		ErrorMessage("node didn't Initialize");
		return 0;
	}	
}
double KCosineTransform2D_Res_Node::getMag()
{
	if(isAlloc)
		return *mag;
	else
	{
		ErrorMessage("node didn't Initialize");
		return 0;
	}
}
void KCosineTransform2D_Res_Node::setMag(double Mag)
{
	if (isAlloc)
		*mag = Mag;
	else
		ErrorMessage("node didn't Initialize");
}
void KCosineTransform2D_Res_Node::report()
{
	cout << "node label   : " << label << endl;
	cout << "position(u,v): " << "(" << u << "," << v << ")" << endl;
	cout << "position(x,y): " << "(" << x << "," << y << ")" << endl;
	cout << "frequency    : " << getFrq() << endl;
	cout << "angular      : " << getAng() << endl;
	cout << "magnitude    : " << getMag() << endl;
}