#include "KCosineTransform2D_Res.h"

void KCosineTransform2D_Res::CreateCosineTable(KGImage<double> *TableOrientation,KGImage<double> *TableFrequency)
{
	if(TableOrientation->width!=blocksizeInner||TableOrientation->height!=blocksizeInner)
		ErrorMessage("[TableOrientation] size error");
	if(TableFrequency->width!=blocksizeInner||TableFrequency->height!=blocksizeInner)
		ErrorMessage("[TableFrequency] size error");

	double	tmpOF;
	for (int y = 0; y < blocksizeInner; y++)
	for (int x = 0; x < blocksizeInner; x++)
	{
		tmpOF = FindOrientationHalf(0, 0, x, y) - PI_2;
		if (tmpOF<0)
			tmpOF = tmpOF + PI;

		TableOrientation->idata[x][y] = tmpOF;
		TableFrequency->idata[x][y] = RSS(x, y);
	}
}
void KCosineTransform2D_Res::ReOrderToSubband(KGImage<double> *ImgDct)
{
	int xBlockPos,yBlockPos;
	int xSpatialPos,ySpatialPos;
	int n=0;

	for(int y=0;y<outputHeight;y++)
	for(int x=0;x<outputWidth;x++)
	{
		xBlockPos	= x%this->width;
		yBlockPos	= y%this->height;
		xSpatialPos	= (int)floor((double)(x/this->width));
		ySpatialPos	= (int)floor((double)(y/this->height));

		this->idata[xBlockPos][yBlockPos].setNode(n, xSpatialPos, ySpatialPos, &(ImgDct->idata[x][y]));
		n++;
	}
}
void KCosineTransform2D_Res::BlockToPixel(int block_X, int block_Y, int &pixel_X, int &pixel_Y)
{
	if(isInitial)
	{
		pixel_X = (int)(block_X*shiftsize);
		pixel_Y = (int)(block_Y*shiftsize);
	}
	else
		ErrorMessage("Should be Initialization before using <BlockToPixel>");
}
void KCosineTransform2D_Res::PixelToBlock(int pixel_X, int pixel_Y, int &block_X, int &block_Y)
{
	if(isInitial)
	{
		block_X	= (int)(pixel_X/shiftsize);
		block_Y	= (int)(pixel_Y/shiftsize);
	}
	else
		ErrorMessage("Should be Initialization before using <PixelToBlock>");
}
KCosineTransform2D_Res::KCosineTransform2D_Res()
{
	isInitial = false;
}
KCosineTransform2D_Res::~KCosineTransform2D_Res()
{
	ClearMemory();
}
void KCosineTransform2D_Res::Initialization(int DataWidth, int DataHeight, int BlocksizeInner, int BlocksizeOuter, int ShiftSize, char Type, char Padding, double PaddingValue)
{
	if(BlocksizeInner<0)
		ErrorMessage("[BlocksizeInner] should larger than 0");
	if(BlocksizeOuter<0)
		ErrorMessage("[BlocksizeOuter] should larger than 0");
	if(ShiftSize<0)
		ErrorMessage("[ShiftSize] should larger than 0");
	if(BlocksizeInner>BlocksizeOuter)
		ErrorMessage("[BlocksizeInner] should smaller than BlocksizeOuter");
	if(ShiftSize>BlocksizeInner)
		ErrorMessage("[ShiftSize] should smaller than BlocksizeInner");
	if(DataWidth%ShiftSize!=0 || DataHeight%ShiftSize!=0)
		ErrorMessage("[ShiftSize] should modulate by DataWidth & DataHeight to zero");

	dataWidth		= DataWidth;
	dataHeight		= DataHeight;
	blocksizeInner	= BlocksizeInner;
	blocksizeOuter	= BlocksizeOuter;
	shiftsize		= ShiftSize;
	type			= Type;
	padding			= Padding;
	paddingValue	= PaddingValue;

	pointStart		= (int)((blocksizeOuter - blocksizeInner)>>1);
	pointEnd		= (int)((blocksizeOuter + blocksizeInner)>>1);

	pointOvlpStart	= (int)((blocksizeOuter - shiftsize)>>1);
	pointOvlpEnd	= (int)((blocksizeOuter + shiftsize)>>1);

	outputWidth		= (int)( blocksizeInner*(dataWidth/shiftsize) );
	outputHeight	= (int)( blocksizeInner*(dataHeight/shiftsize) );

	subW			= dataWidth/shiftsize;
	subH			= dataHeight/shiftsize;

	inner_2			= blocksizeInner>>1;
	outer_2			= blocksizeOuter>>1;
	shift_2			= shiftsize>>1;

	tmp1DRowComp.Allocate(blocksizeOuter);
	tmp1DColComp.Allocate(blocksizeOuter);

	tmp2DRowComp.Allocate(blocksizeOuter,dataHeight);

	tmp2DInComp.Allocate(dataWidth,dataHeight);

	inv1DRowComp.Allocate(blocksizeOuter);
	inv1DColComp.Allocate(blocksizeOuter);

	inv2DRowComp.Allocate(blocksizeOuter,(int)(blocksizeOuter*(dataHeight/shiftsize)));

	inv2DInComp.Allocate(outputWidth,outputHeight);

	transformRow.Initialization(blocksizeOuter, type);
	transformCol.Initialization(blocksizeOuter, type);

	tableOrientation.Allocate(blocksizeInner,blocksizeInner);
	tableFrequency.Allocate(blocksizeInner,blocksizeInner);

	Spatial.Allocate(dataWidth,dataHeight);
	Dct.Allocate(outputWidth,outputHeight);

	CreateCosineTable(&tableOrientation,&tableFrequency);
	this->Allocate(blocksizeInner,blocksizeInner);
	for(int v=0;v<blocksizeInner;v++)
		for(int u=0;u<blocksizeInner;u++)
			this->idata[u][v].Initialization(u,v,subW,subH,tableFrequency.idata[u][v],tableOrientation.idata[u][v]);
	ReOrderToSubband(&Dct);

	isInitial		= true;
}
void KCosineTransform2D_Res::ClearMemory()
{
	if(isInitial)
	{
		transformRow.ClearMemory();
		transformCol.ClearMemory();

		tmp1DRowComp.FreeMem();
		tmp1DColComp.FreeMem();

		tmp2DRowComp.FreeMem();

		tmp2DInComp.FreeMem();

		inv1DRowComp.FreeMem();
		inv1DColComp.FreeMem();

		inv2DRowComp.FreeMem();

		inv2DInComp.FreeMem();

		tableOrientation.FreeMem();
		tableFrequency.FreeMem();

		Spatial.FreeMem();
		Dct.FreeMem();

		for(int v=0;v<blocksizeInner;v++)
			for(int u=0;u<blocksizeInner;u++)
				this->idata[u][v].ClearMemory();
		this->FreeMem();

		isInitial	= false;
	}
}
void KCosineTransform2D_Res::LoadSpatialImage(KArray2D<float> *imgSpatial)
{
	if(!imgSpatial->IsSameSize(dataWidth,dataHeight))
		ErrorMessage("[imgSpatial1] size doesn't match to Initialization parameter");

	for(int y=0;y<dataHeight;y++)
	for(int x=0;x<dataWidth;x++)
		Spatial.idata[x][y]	= (double)imgSpatial->idata[x][y];
}
void KCosineTransform2D_Res::LoadSpatialImage(KArray2D<double> *imgSpatial)
{
	if (!imgSpatial->IsSameSize(dataWidth, dataHeight))
		ErrorMessage("[imgSpatial1] size doesn't match to Initialization parameter");

	for (int y = 0; y<dataHeight; y++)
	for (int x = 0; x<dataWidth; x++)
		Spatial.idata[x][y] = (double)imgSpatial->idata[x][y];
}
void KCosineTransform2D_Res::LoadSpatialImage(KArray2D<int> *imgSpatial)
{
	if (!imgSpatial->IsSameSize(dataWidth, dataHeight))
		ErrorMessage("[imgSpatial1] size doesn't match to Initialization parameter");

	for (int y = 0; y<dataHeight; y++)
	for (int x = 0; x<dataWidth; x++)
		Spatial.idata[x][y] = (double)imgSpatial->idata[x][y];
}
void KCosineTransform2D_Res::ForwardTransform(KArray2D<int> *ROI, KArray1D<double> *SpatialWindowRow, KArray1D<double> *SpatialWindowCol)
{
	if(isInitial)
	{
		if(ROI!=NULL)
			if(!ROI->IsSameSize(dataWidth,dataHeight))
				ErrorMessage("[ROI] size doesn't match to Initialization parameter");
		if(SpatialWindowRow!=NULL)
			if(!SpatialWindowRow->IsSameSize(blocksizeOuter))
				ErrorMessage("[SpatialWindowRow] size doesn't match to Initialization parameter");
		if(SpatialWindowCol!=NULL)
			if(!SpatialWindowCol->IsSameSize(blocksizeOuter))
				ErrorMessage("[SpatialWindowCol] size doesn't match to Initialization parameter");

		for(int y=0;y<dataHeight;y++)
		for(int x=0;x<dataWidth;x++)
			tmp2DInComp.idata[x][y]	= Spatial.idata[x][y];

		for(int xOverlap=shift_2,xOutput=0;	xOverlap<dataWidth;	xOverlap+=shiftsize,xOutput+=blocksizeInner)
		{
			for(int yPixel=0;	yPixel<dataHeight;	yPixel++)
			{
				// =================================================== //
				// get spatial data, insert to 1D array (for each row) //
				for(int xComp=xOverlap-(outer_2),xTmpRow=0;	xTmpRow<blocksizeOuter;		xComp++,xTmpRow++)
				{
					if(xComp>=0&&xComp<dataWidth)
						tmp1DRowComp.idata[xTmpRow]	= tmp2DInComp.idata[xComp][yPixel];
					else
					{
						if(padding==COSINETRANSFORM_PADDING_MIRROR)
						{
							if(xComp<0)
								tmp1DRowComp.idata[xTmpRow]	= tmp2DInComp.idata[abs(xComp)][yPixel];
							else if(xComp>=dataWidth)
								tmp1DRowComp.idata[xTmpRow]	= tmp2DInComp.idata[int( dataWidth - (xComp-dataWidth)-1 )][yPixel];
							else
								tmp1DRowComp.idata[xTmpRow]	= 0;
						}
						else if(padding==COSINETRANSFORM_PADDING_VALUE)
							tmp1DRowComp.idata[xTmpRow]	= paddingValue;
						else
							tmp1DRowComp.idata[xTmpRow]	= 0;
					}
				}
				// =================================================== //

				// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //
				// transform 1D array by fourier transform 1D //
				// tmp1DRow size ( blocksizeOuter )
				transformRow.ForwardTransform(&tmp1DRowComp, SpatialWindowRow);
				// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //

				// ========================================================== //
				// get transform(row) data, insert to 2D array (for each row) //
				// tmp2DRow size ( blocksizeOuter ,dataHeight )               //
				for (int xTmpRow = 0; xTmpRow < blocksizeOuter; xTmpRow++)
					tmp2DRowComp.idata[xTmpRow][yPixel] = tmp1DRowComp.idata[xTmpRow];
				// ========================================================== //
			}
			for(int xPoint=pointStart,xOut=xOutput;	xPoint<pointEnd;	xPoint++,xOut++)
			{
				for(int yOverlap=shift_2,yOutput=0;	yOverlap<dataHeight;	yOverlap+=shiftsize,yOutput+=blocksizeInner)
				{
					// ============================================================== //
					// get col data(from tmp2DRow), insert to 1D array (for each col) //
					for(int yComp=yOverlap-(outer_2),yTmpCol=0;	yTmpCol<blocksizeOuter;	yComp++,yTmpCol++)
					{
						if(yComp>=0&&yComp<dataHeight)
							tmp1DColComp.idata[yTmpCol]	= tmp2DRowComp.idata[xPoint][yComp];
						else
						{
							if(padding==COSINETRANSFORM_PADDING_MIRROR)
							{
								if(yComp<0)
									tmp1DColComp.idata[yTmpCol]	= tmp2DRowComp.idata[xPoint][abs(yComp)];
								else if(yComp>=dataHeight)
									tmp1DColComp.idata[yTmpCol]	= tmp2DRowComp.idata[xPoint][int( dataHeight - (yComp-dataHeight)-1 )];
								else
									tmp1DColComp.idata[yTmpCol]	= 0;
							}
							else if(padding==COSINETRANSFORM_PADDING_VALUE)
								tmp1DColComp.idata[yTmpCol]	= paddingValue;
							else
								tmp1DColComp.idata[yTmpCol]	= 0;
						}
					}
					// ============================================================== //

					// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //
					// transform 1D array by fourier transform 1D //
					// tmp1DCol size ( blocksizeOuter )
					transformCol.ForwardTransform(&tmp1DColComp, SpatialWindowCol);
					// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //

					// ================================================================================================= //
					// get transform(col) data, insert to 2D array (OUTPUT)	                                             //
					// tmp2DOut size ( blocksizeInner*(dataWidth/overlapsize) ,blocksizeInner*(dataHeight/overlapsize) ) //
					for(int yTmpCol=pointStart,yOut=yOutput;	yTmpCol<pointEnd;	yTmpCol++,yOut++)
					{
						if(xOut>=0&&xOut<outputWidth&&yOut>=0&&yOut<outputHeight)
						{
							if(ROI!=NULL)
							{
								if (ROI->idata[xOverlap][yOverlap] != 0)
									Dct.idata[xOut][yOut]	= tmp1DColComp.idata[yTmpCol];
								else
									Dct.idata[xOut][yOut]	= 0;
							}
							else
								Dct.idata[xOut][yOut]	= tmp1DColComp.idata[yTmpCol];
						}
					}
					// ================================================================================================= //
				}
			}
		}
	}
	else
		ErrorMessage("Should be Initialization before using <ForwardTransform>");
}
void KCosineTransform2D_Res::InverseTransform(KArray2D<int> *ROI)
{
	if(isInitial)
	{
		if(ROI!=NULL)
			if(!ROI->IsSameSize(dataWidth,dataHeight))
				ErrorMessage("[ROI] size doesn't match to Initialization parameter");

		for(int y=0;y<outputHeight;y++)
		for(int x=0;x<outputWidth;x++)
			inv2DInComp.idata[x][y] = Dct.idata[x][y];

		for(int sx=0,xJump=0;	sx<outputWidth;		sx+=blocksizeInner,xJump+=shiftsize)
		{
			for(int yPixel=0;	yPixel<outputHeight;	yPixel++)
			{
				// ============================================//
				// get data, insert to 1D array (for each row) //
				for(int xArray=0;	xArray<blocksizeOuter;	xArray++)
				{
					if (xArray >= pointStart&&xArray < pointEnd)
						inv1DRowComp.idata[xArray] = inv2DInComp.idata[sx + (xArray - pointStart)][yPixel];
					else
						inv1DRowComp.idata[xArray]	= 0;
				}
				// =================================================== //

				// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //
				// transform 1D array by fourier transform 1D //
				transformRow.InverseTransform(&inv1DRowComp);
				// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //

				// ========================================================== //
				// get transform(row) data, insert to 2D array (for each row) //
				for (int xTmpRow = 0; xTmpRow < blocksizeOuter; xTmpRow++)
					inv2DRowComp.idata[xTmpRow][yPixel] = inv1DRowComp.idata[xTmpRow];
				// ========================================================== //
			}
			for(int xPoint=pointOvlpStart,xOut=xJump;	xPoint<pointEnd;	xPoint++,xOut++)
			{
				for(int yJump=0,yOverlap=0;	yJump<outputHeight;	yJump+=blocksizeInner,yOverlap+=shiftsize)
				{
					// ============================================================== //
					// get col data(from inv2DRow), insert to 1D array (for each col) //
					for(int yArray=0;	yArray<blocksizeOuter;	yArray++)
					{
						if (yArray >= pointStart&&yArray < pointEnd)
							inv1DColComp.idata[yArray] = inv2DRowComp.idata[xPoint][yJump + (yArray - pointStart)];
						else
							inv1DColComp.idata[yArray] = 0;
					}
					// ============================================================== //

					// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //
					// transform 1D array by fourier transform 1D //
					transformCol.InverseTransform(&inv1DColComp);
					// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //

					// ==================================================== //
					// get transform(col) data, insert to 2D array (OUTPUT) //
					for(int yArray=pointOvlpStart,yOut=yOverlap;	yArray<pointOvlpEnd;	yArray++,yOut++)
					{
						if(xOut>=0 && xOut<dataWidth && yOut>=0 && yOut<dataHeight)
						{
							if(ROI!=NULL)
							{
								if (ROI->idata[xOut][yOut] != 0)
									Spatial.idata[xOut][yOut]	= inv1DColComp.idata[yArray];
								else
									Spatial.idata[xOut][yOut]	= 0;
							}
							else
								Spatial.idata[xOut][yOut]	= inv1DColComp.idata[yArray];
						}
					}
					// ==================================================== //
				}
			}
		}
	}
	else
		ErrorMessage("Should be Initialization before using <InverseTransform>");
}
void KCosineTransform2D_Res::Plot(KGImage<double> *imgMagnitude)
{
	if(!imgMagnitude->IsSameSize(outputWidth,outputHeight))
		imgMagnitude->Resize(outputWidth,outputHeight);

	for(int y=0,v=0;v<blocksizeInner;y+=subH,v++)
	for(int x=0,u=0;u<blocksizeInner;x+=subW,u++)
	{
		if(u==0 && v==0)
		{
			for(int py=y,yy=0;py<y+subH;py++,yy++)
			for(int px=x,xx=0;px<x+subW;px++,xx++)
				imgMagnitude->idata[px][py]	= 0;
		}
		else
		{
			for(int py=y,yy=0;py<y+subH;py++,yy++)
			for(int px=x,xx=0;px<x+subW;px++,xx++)
				imgMagnitude->idata[px][py]	= this->idata[u][v].getMag(xx,yy);
		}
	}
}