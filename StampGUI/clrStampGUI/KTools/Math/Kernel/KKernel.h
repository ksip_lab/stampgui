// ------------------------------------------------ //
// contain:		Kernel
//
// developer:	Krisada Phromsuthirak, KSIP Member
//				Watcharapong Chaidee, KSIP Member
// ------------------------------------------------ //

#ifndef _KKernel_H
#define _KKernel_H

#include "KGeneral.h"

#define GAUSSIAN_MODE_CORNER	0
#define GAUSSIAN_MODE_CENTER	1

template <typename T> void KGaussian(KArray1D<T> *data, double std, char mode = GAUSSIAN_MODE_CORNER);
template <typename T> void KGaussian(KArray2D<T> *data, double std1, double std2, char mode = GAUSSIAN_MODE_CENTER);
template <typename T> void KGaussian(KArray2D<T> *data, double std1, double std2, double orientation, char mode = GAUSSIAN_MODE_CENTER);
template <typename T> void KGaussian(KArray3D<T> *data, double std1, double std2, double std3, char mode = GAUSSIAN_MODE_CORNER);
template <typename T> void KGaussian(KArray4D<T> *data, double std1, double std2, double std3, double std4, char mode = GAUSSIAN_MODE_CORNER);

template <typename T> void KGaussianBanpass(KArray2D<T> *data, double f_low, double f_high);
template <typename T> void KGaussianBanreject(KArray2D<T> *data, double f_low, double f_high);
template <typename T> void KGaussianLowpass(KArray2D<T> *data, double f_cutoff);
template <typename T> void KGaussianHighpass(KArray2D<T> *data, double f_cutoff);

template <typename T> void KGabor(KArray2D<T> *real, KArray2D<T> *imaginary, double orientation, double frequency, double standardDeviation, double spatialAspectRatio = 1.0, double phaseOffset = 0.0);

template <typename T> void KRaisedCosine(KArray1D<T> *data, double roll_off, double filter_length, char mode = RAISEDCOSINE_MODE_CORNER);
template <typename T> void KRaisedCosine(KArray1D<T> &data, double roll_off, double filter_length, char mode = RAISEDCOSINE_MODE_CORNER);
template <typename T> void KRaisedCosine(KArray2D<T> *data, double roll_off, double filter_length, char mode = RAISEDCOSINE_MODE_CORNER);
template <typename T> void KRaisedCosine(KArray2D<T> &data, double roll_off, double filter_length, char mode = RAISEDCOSINE_MODE_CORNER);

template <typename T> void KBartlettWindow(KArray1D<T> *data, int filterLength, char mode = KERNEL_MODE_CORNER);
template <typename T> void KBartlettWindow(KArray1D<T> &data, int filterLength, char mode = KERNEL_MODE_CORNER);
template <typename T> void KTriangularWindow(KArray1D<T> *data, int filterLength, char mode = KERNEL_MODE_CORNER);
template <typename T> void KTriangularWindow(KArray1D<T> &data, int filterLength, char mode = KERNEL_MODE_CORNER);
template <typename T> void KWelchWindow(KArray1D<T> *data, int filterLength, char mode = KERNEL_MODE_CORNER);
template <typename T> void KWelchWindow(KArray1D<T> &data, int filterLength, char mode = KERNEL_MODE_CORNER);
template <typename T> void KHammingWindow(KArray1D<T> *data, double alpha = 0.54);
template <typename T> void KHammingWindow(KArray1D<T> &data, double alpha = 0.54);
template <typename T> void KBlackmanWindow(KArray1D<T> *data, double alpha = 0.16);
template <typename T> void KBlackmanWindow(KArray1D<T> &data, double alpha = 0.16);
template <typename T> void KNuttalWindow(KArray1D<T> *data, double a0 = 0.355768, double a1 = 0.487396, double a2 = 0.144232, double a3 = 0.012604);
template <typename T> void KNuttalWindow(KArray1D<T> &data, double a0 = 0.355768, double a1 = 0.487396, double a2 = 0.144232, double a3 = 0.012604);
template <typename T> void KBlackmanNuttalWindow(KArray1D<T> *data, double a0 = 0.3635819, double a1 = 0.4891775, double a2 = 0.1365995, double a3 = 0.0106411);
template <typename T> void KBlackmanNuttalWindow(KArray1D<T> &data, double a0 = 0.3635819, double a1 = 0.4891775, double a2 = 0.1365995, double a3 = 0.0106411);
template <typename T> void KBlackmanHarrisWindow(KArray1D<T> *data, double a0 = 0.35875, double a1 = 0.48829, double a2 = 0.14128, double a3 = 0.01168);
template <typename T> void KBlackmanHarrisWindow(KArray1D<T> &data, double a0 = 0.35875, double a1 = 0.48829, double a2 = 0.14128, double a3 = 0.01168);
template <typename T> void KFlatTopWindow(KArray1D<T> *data, double a0 = 1.0, double a1 = 1.93, double a2 = 1.29, double a3 = 0.388, double a4 = 0.028);
template <typename T> void KFlatTopWindow(KArray1D<T> &data, double a0 = 1.0, double a1 = 1.93, double a2 = 1.29, double a3 = 0.388, double a4 = 0.028);
template <typename T> void KCosineWindow(KArray1D<T> *data);
template <typename T> void KCosineWindow(KArray1D<T> &data);
template <typename T> void KSineWindow(KArray1D<T> *data);
template <typename T> void KSineWindow(KArray1D<T> &data);
template <typename T> void KBohmanWindow(KArray1D<T> *data);
template <typename T> void KBohmanWindow(KArray1D<T> &data);
template <typename T> void KTukeyWindow(KArray1D<T> *data, double alpha = 0.5);
template <typename T> void KTukeyWindow(KArray1D<T> &data, double alpha = 0.5);
template <typename T> void KTaperedCosineWindow(KArray1D<T> *data, double alpha = 0.5);
template <typename T> void KTaperedCosineWindow(KArray1D<T> &data, double alpha = 0.5);

template <typename T> void KPrewittWindow(KArray2D<T> *compX, KArray2D<T> *compY);
template <typename T> void KPrewittWindow(KArray2D<T> &compX, KArray2D<T> &compY);
template <typename T> void KSobelWindow(KArray2D<T> *compX, KArray2D<T> *compY);
template <typename T> void KSobelWindow(KArray2D<T> &compX, KArray2D<T> &compY);
template <typename T> void KRobertWindow(KArray2D<T> *compX, KArray2D<T> *compY);
template <typename T> void KRobertWindow(KArray2D<T> &compX, KArray2D<T> &compY);

//==========================================================================//
// In construction //
template <typename T> void KGaussianFirstDerivative(KArray2D<T> *data, double zigma, char mode = GAUSSIAN_MODE_CENTER);
template <typename T> void KGaussianFirstDerivative(KArray2D<T> *data, double zigma, double direction, char mode = GAUSSIAN_MODE_CENTER);

template <typename T> void KGaussianFirstDerivative(KArray2D<T> *data, double zigma, char mode)
{
	if (data->isAlloc)
	{
		double C, z, centerX,centerY, sum;

		z = 2.0 * Pow(zigma, 2.0);
		C = 1.0 / (PI*z);

		if (mode == GAUSSIAN_MODE_CORNER)
		{
			centerX = 0.0;
			centerY = 0.0;
		}
		else if (mode == GAUSSIAN_MODE_CENTER)
		{
			centerX = ((double)((double)data->width - 1.0) / 2.0);
			centerY = ((double)((double)data->height - 1.0) / 2.0);
		}
		else
			ErrorMessage("Error [KGaussianDerivative] : wrong mode");

		sum = 0;
		for (int x = 0; x < data->width; x++)
		for (int y = 0; y < data->height; y++) 
		{
			data->idata[x][y] = (T)(C*((double)x - centerX)*exp(-(Pow((double)x - centerX, 2.0) + (Pow((double)y - centerY, 2.0))) / z));
			sum += ABS((double)data->idata[x][y]);
		}

		for (int x = 0; x < data->width; x++)
		for (int y = 0; y < data->height; y++) 
			data->idata[x][y] = (T)((double)data->idata[x][y] / sum);
	}
	else
		ErrorMessage("Error [KGaussianDerivative] : data didn't allocated");
}
template <typename T> void KGaussianFirstDerivative(KArray2D<T> *data, double zigma, double direction, char mode)
{
	if (data->isAlloc)
	{
		double C, z, centerX, centerY, sum;
		double x_new, y_new;

		z = 2.0 * Pow(zigma, 2.0);
		C = 1.0 / (PI*z);

		if (mode == GAUSSIAN_MODE_CORNER)
		{
			centerX = 0.0;
			centerY = 0.0;
		}
		else if (mode == GAUSSIAN_MODE_CENTER)
		{
			centerX = ((double)((double)data->width - 1.0) / 2.0);
			centerY = ((double)((double)data->height - 1.0) / 2.0);
		}
		else
			ErrorMessage("Error [KGaussianDerivative] : wrong mode");

		sum = 0;
		for (int x = 0; x < data->width; x++)
		for (int y = 0; y < data->height; y++)
		{
			x_new = (double)((double)(x - centerX)*cos(direction)) + (double)((double)(y - centerY)*sin(direction));
			y_new = (double)((double)(y - centerY)*cos(direction)) - (double)((double)(x - centerX)*sin(direction));

			data->idata[x][y] = (T)(C*(x_new)*exp(-(Pow(x_new, 2.0) + (Pow(y_new, 2.0))) / z));
			sum += ABS((double)data->idata[x][y]);
		}

		for (int x = 0; x < data->width; x++)
		for (int y = 0; y < data->height; y++)
			data->idata[x][y] = (T)((double)data->idata[x][y] / sum);
	}
	else
		ErrorMessage("Error [KGaussianDerivative] : data didn't allocated");
}
//==========================================================================//

template <typename T> void KGaussian(KArray1D<T> *data, double std, char mode)
{
	if (data->isAlloc)
	{
		double temp = 1.0 / std / sqrt(2.0 * PI);
		double mew;

		if (mode == GAUSSIAN_MODE_CENTER)
		{
			mew = (data->length - 1.0) / 2.0;
			for (int i = 0; i < data->length; i++)
				data->idata[i] = temp*exp(-1.0 / 2.0 * (Pow((i - mew) / std, 2.0)));
		}
		else
		{
			mew = 0;
			for (int i = 0; i < data->length; i++)
				data->idata[i] = temp*exp(-1.0 / 2.0 * (Pow((i - mew) / std, 2.0)));
		}
	}
	else
		ErrorMessage("Error [KGaussian] : data didn't allocated");
}
template <typename T> void KGaussian(KArray2D<T> *data, double std1, double std2, char mode)
{
	if (data->isAlloc)
	{
		double mew_x, mew_y;

		if (mode == GAUSSIAN_MODE_CENTER)
		{
			mew_x = (data->width - 1.0) / 2.0;
			mew_y = (data->height - 1.0) / 2.0;
			for (int i = 0; i < data->width; i++)
			for (int j = 0; j < data->height; j++)
				data->idata[i][j] = 1.0 / std1 / sqrt(2.0 * PI)*exp(-1.0 / 2.0 * (Pow((i - mew_x) / std1, 2.0)))  *
				1.0 / std2 / sqrt(2.0 * PI)*exp(-1.0 / 2.0 * (Pow((j - mew_y) / std2, 2.0)));
		}
		else
		{
			mew_x = 0;
			mew_y = 0;
			for (int i = 0; i < data->width; i++)
			for (int j = 0; j < data->height; j++)
				data->idata[i][j] = 1.0 / std1 / sqrt(2.0 * PI)*exp(-1.0 / 2.0 * (Pow((i - mew_x) / std1, 2.0)))  *
				1.0 / std2 / sqrt(2.0 * PI)*exp(-1.0 / 2.0 * (Pow((j - mew_y) / std2, 2.0)));
		}
	}
	else
		ErrorMessage("Error [KGaussian] : data didn't allocated");
}
template <typename T> void KGaussian(KArray2D<T> *data, double std1, double std2, double orientation, char mode)
{
	if (data->isAlloc)
	{
		double i_new, j_new;

		if (mode == GAUSSIAN_MODE_CENTER)
		{
			double cen_x, cen_y;

			cen_x = (data->width - 1.0) / 2.0;
			cen_y = (data->height - 1.0) / 2.0;

			for (int i = 0; i < data->width; i++)
			for (int j = 0; j < data->height; j++)
			{
				i_new = (double)((double)(i - cen_x)*cos(orientation)) + (double)((double)(j - cen_y)*sin(orientation));
				j_new = (double)((double)(j - cen_y)*cos(orientation)) - (double)((double)(i - cen_x)*sin(orientation));

				data->idata[i][j] = 1.0 / std1 / sqrt(2.0 * PI)*exp(-1.0 / 2.0 * (Pow((i_new) / std1, 2.0)))  *
					1.0 / std2 / sqrt(2.0 * PI)*exp(-1.0 / 2.0 * (Pow((j_new) / std2, 2.0)));
			}
		}
		else
		{
			for (int i = 0; i < data->width; i++)
			for (int j = 0; j < data->height; j++)
			{
				i_new = (double)((double)(i)*cos(orientation)) + (double)((double)(j)*sin(orientation));
				j_new = (double)((double)(j)*cos(orientation)) - (double)((double)(i)*sin(orientation));

				data->idata[i][j] = 1.0 / std1 / sqrt(2.0 * PI)*exp(-1.0 / 2.0 * (Pow((i_new) / std1, 2.0)))  *
					1.0 / std2 / sqrt(2.0 * PI)*exp(-1.0 / 2.0 * (Pow((j_new) / std2, 2.0)));
			}
		}
	}
	else
		ErrorMessage("Error [KGaussianDirection] : data didn't allocated");
}
template <typename T> void KGaussian(KArray3D<T> *data, double std1, double std2, double std3, char mode)
{
	if (data->isAlloc)
	{
		double mew_x, mew_y, mew_z;

		if (mode == GAUSSIAN_MODE_CENTER)
		{
			mew_x = (data->depth - 1.0) / 2.0;
			mew_y = (data->width - 1.0) / 2.0;
			mew_z = (data->height - 1.0) / 2.0;
			for (int i = 0; i < data->depth; i++)
			for (int j = 0; j < data->width; j++)
			for (int k = 0; k < data->height; k++)
				data->idata[i][j][k] = 1.0 / std1 / sqrt(2.0 * PI)*exp(-1.0 / 2.0 * (Pow((i - mew_x) / std1, 2.0)))  *
				1.0 / std2 / sqrt(2.0 * PI)*exp(-1.0 / 2.0 * (Pow((j - mew_y) / std2, 2.0)))  *
				1.0 / std3 / sqrt(2.0 * PI)*exp(-1.0 / 2.0 * (Pow((k - mew_z) / std3, 2.0)));
		}
		else
		{
			mew_x = 0;
			mew_y = 0;
			mew_z = 0;
			for (int i = 0; i < data->depth; i++)
			for (int j = 0; j < data->width; j++)
			for (int k = 0; k < data->height; k++)
				data->idata[i][j][k] = 1.0 / std1 / sqrt(2.0 * PI)*exp(-1.0 / 2.0 * (Pow((i - mew_x) / std1, 2.0)))  *
				1.0 / std2 / sqrt(2.0 * PI)*exp(-1.0 / 2.0 * (Pow((j - mew_y) / std2, 2.0)))  *
				1.0 / std3 / sqrt(2.0 * PI)*exp(-1.0 / 2.0 * (Pow((k - mew_z) / std3, 2.0)));
		}
	}
	else
		ErrorMessage("Error [KGaussian] : data didn't allocated");
}
template <typename T> void KGaussian(KArray4D<T> *data, double std1, double std2, double std3, double std4, char mode)
{
	if (data->isAlloc)
	{
		double mew_x, mew_y, mew_z, mew_t;

		if (mode == GAUSSIAN_MODE_CENTER)
		{
			mew_x = (data->frame - 1.0) / 2.0;
			mew_y = (data->depth - 1.0) / 2.0;
			mew_z = (data->width - 1.0) / 2.0;
			mew_t = (data->height - 1.0) / 2.0;
			for (int i = 0; i < data->frame; i++)
			for (int j = 0; j < data->depth; j++)
			for (int k = 0; k < data->width; k++)
			for (int l = 0; l < data->height; l++)
				data->idata[i][j][k][l] = 1.0 / std1 / sqrt(2.0 * PI)*exp(-1.0 / 2.0 * (Pow((i - mew_x) / std1, 2.0)))  *
				1.0 / std2 / sqrt(2.0 * PI)*exp(-1.0 / 2.0 * (Pow((j - mew_y) / std2, 2.0)))  *
				1.0 / std3 / sqrt(2.0 * PI)*exp(-1.0 / 2.0 * (Pow((k - mew_z) / std3, 2.0)))	*
				1.0 / std4 / sqrt(2.0 * PI)*exp(-1.0 / 2.0 * (Pow((l - mew_t) / std4, 2.0)));
		}
		else
		{
			mew_x = 0;
			mew_y = 0;
			mew_z = 0;
			mew_t = 0;
			for (int i = 0; i < data->frame; i++)
			for (int j = 0; j < data->depth; j++)
			for (int k = 0; k < data->width; k++)
			for (int l = 0; l < data->height; l++)
				data->idata[i][j][k][l] = 1.0 / std1 / sqrt(2.0 * PI)*exp(-1.0 / 2.0 * (Pow((i - mew_x) / std1, 2.0)))  *
				1.0 / std2 / sqrt(2.0 * PI)*exp(-1.0 / 2.0 * (Pow((j - mew_y) / std2, 2.0)))  *
				1.0 / std3 / sqrt(2.0 * PI)*exp(-1.0 / 2.0 * (Pow((k - mew_z) / std3, 2.0)))	*
				1.0 / std4 / sqrt(2.0 * PI)*exp(-1.0 / 2.0 * (Pow((l - mew_t) / std4, 2.0)));
		}
	}
	else
		ErrorMessage("Error [KGaussian] : data didn't allocated");
}
template <typename T> void KGaussianBanpass(KArray2D<T> *data, double f_low, double f_high)
{
	if (data->isAlloc)
	{
		double distance;
		for (int i = 0; i < data->width; i++)
		for (int j = 0; j < data->height; j++)
		{
			distance = sqrt((i - data->width / 2.0)*(i - data->width / 2.0) + (j - data->height / 2.0)*(j - data->height / 2.0));
			data->idata[i][j] = exp(-1.0 * Pow(distance, 2.0) / (2.0 * Pow(f_high, 2.0)))	*
				(1.0 - exp(-1.0 * Pow(distance, 2.0) / (2.0 * Pow(f_high, 2.0))));
		}
	}
	else
		ErrorMessage("Error [KGaussianBanpass] : data didn't allocated");
}
template <typename T> void KGaussianBanreject(KArray2D<T> *data, double f_low, double f_high)
{
	if (data->isAlloc)
	{
		double distance;
		for (int i = 0; i < data->width; i++)
		for (int j = 0; j < data->height; j++)
		{
			distance = sqrt((i - data->width / 2.0)*(i - data->width / 2.0) + (j - data->height / 2.0)*(j - data->height / 2.0));
			data->idata[i][j] = 1.0 - (exp(-1.0 * Pow(distance, 2.0) / (2.0 * Pow(f_high, 2.0)))	*
				(1.0 - exp(-1.0 * Pow(distance, 2.0) / (2.0 * Pow(f_high, 2.0)))));
		}
	}
	else
		ErrorMessage("Error [KGaussianBanreject] : data didn't allocated");
}
template <typename T> void KGaussianLowpass(KArray2D<T> *data, double f_cutoff)
{
	if (data->isAlloc)
	{
		double distance;
		for (int i = 0; i < data->width; i++)
		for (int j = 0; j < data->height; j++)
		{
			distance = sqrt((i - data->width / 2.0)*(i - data->width / 2.0) + (j - data->height / 2.0)*(j - data->height / 2.0));
			data->idata[i][j] = exp(-1.0 * Pow(distance, 2.0) / (2.0 * Pow(f_cutoff, 2.0)));
		}
	}
	else
		ErrorMessage("Error [KGaussianLowpass] : data didn't allocated");
}
template <typename T> void KGaussianHighpass(KArray2D<T> *data, double f_cutoff)
{
	if (data->isAlloc)
	{
		double distance;
		for (int i = 0; i < data->width; i++)
		for (int j = 0; j < data->height; j++)
		{
			distance = sqrt((i - data->width / 2.0)*(i - data->width / 2.0) + (j - data->height / 2.0)*(j - data->height / 2.0));
			data->idata[i][j] = 1.0 - exp(-1.0 * Pow(distance, 2.0) / (2.0 * Pow(f_cutoff, 2.0)));
		}
	}
	else
		ErrorMessage("Error [KGaussianHighpass] : data didn't allocated");
}
template <typename T> void KGabor(KArray2D<T> *real, KArray2D<T> *imaginary, double orientation, double frequency, double standardDeviation, double spatialAspectRatio, double phaseOffset)
{
	if (!real->isAlloc || !imaginary->isAlloc)
		ErrorMessage("Error [Gabor]: real or imaginary not allocated");

	if (!real->IsSameSize(imaginary->width, imaginary->height))
		ErrorMessage("Error [Gabor]: Real & Imaginary size not match");

	if (frequency <= 0)
		ErrorMessage("Error [Gabor]: frequency must be more than 0");

	double	x_new, y_new;
	double	x_cen, y_cen;
	double	wavelength;

	wavelength = real->width / frequency;
	x_cen = (double)real->width / 2.0;
	y_cen = (double)real->height / 2.0;

	for (int y = 0; y < real->height; y++)
	for (int x = 0; x < real->width; x++)
	{
		x_new = ((double)x - x_cen)*cos(orientation) + ((double)y - y_cen)*sin(orientation);
		y_new = ((double)y - y_cen)*cos(orientation) - ((double)x - x_cen)*sin(orientation);

		real->idata[x][y] = cos((2.0*PI*(x_new / wavelength)) + phaseOffset)*exp(-1.0 * (((x_new*x_new) + (spatialAspectRatio*spatialAspectRatio*y_new*y_new)) / (2.0*standardDeviation*standardDeviation)));
		imaginary->idata[x][y] = sin((2.0*PI*(x_new / wavelength)) + phaseOffset)*exp(-1.0 * (((x_new*x_new) + (spatialAspectRatio*spatialAspectRatio*y_new*y_new)) / (2.0*standardDeviation*standardDeviation)));
	}
}
template <typename T> void KRaisedCosine(KArray1D<T> *data, double roll_off, double filter_length, char mode)
{
	if (data->isAlloc)
	{
		if (roll_off < 0)
			ErrorMessage("Error [KRaisedCosine] : [roll_off] must be more than 0");

		if (mode == RAISEDCOSINE_MODE_CENTER)
		{
			double cond1, cond2;
			double symbol_rate;

			symbol_rate = (0.5) * (1.0 / filter_length);

			cond1 = (double)((1.0 - roll_off) / (2.0*(symbol_rate)));
			cond2 = (double)((1.0 + roll_off) / (2.0*(symbol_rate)));

			for (int i = 0, z = (-1)*(data->length >> 1); i < data->length; i++, z++)
			{
				if (ABS(z) <= cond1)
					data->idata[i] = 1.0;
				else if (cond1 < ABS(z) && ABS(z) <= cond2)
					data->idata[i] = (0.5) *(1.0 + cos((((PI * symbol_rate) / (roll_off)) * (ABS(z) - cond1))));
				else
					data->idata[i] = 0;
			}
		}
		else
		{
			double cond1, cond2;
			double symbol_rate;

			symbol_rate = (0.5) * (1.0 / filter_length);

			cond1 = (double)((1.0 - roll_off) / (2.0*(symbol_rate)));
			cond2 = (double)((1.0 + roll_off) / (2.0*(symbol_rate)));

			for (int i = 0; i < data->length; i++)
			{
				if (ABS(i) <= cond1)
					data->idata[i] = 1.0;
				else if (cond1 < ABS(i) && ABS(i) <= cond2)
					data->idata[i] = (0.5) *(1.0 + cos((((PI * symbol_rate) / (roll_off)) * (ABS(i) - cond1))));
				else
					data->idata[i] = 0;
			}
		}
	}
	else
		ErrorMessage("Error [KRaisedCosine] : data didn't allocated");
}
template <typename T> void KRaisedCosine(KArray1D<T> &data, double roll_off, double filter_length, char mode)
{
	KRaisedCosine(&data, roll_off, filter_length, mode);
}
template <typename T> void KRaisedCosine(KArray2D<T> *data, double roll_off, double filter_length, char mode)
{
	if (data->isAlloc)
	{
		if (roll_off < 0)
			ErrorMessage("Error [KRaisedCosine] : [roll_off] must be more than 0");

		if (mode == RAISEDCOSINE_MODE_CENTER)
		{
			double cond1, cond2;
			double symbol_rate;
			double r;

			symbol_rate = (0.5) * (1.0 / filter_length);

			cond1 = (double)((1.0 - roll_off) / (2.0*(symbol_rate)));
			cond2 = (double)((1.0 + roll_off) / (2.0*(symbol_rate)));

			for (int y = 0; y < data->height; y++)
			for (int x = 0; x < data->width; x++)
			{
				r = (double)SQRT(SQR(x - (data->width >> 1)) + SQR(y - (data->height >> 1)));
				if (ABS(r) <= cond1)
					data->idata[x][y] = 1.0;
				else if (cond1 < ABS(r) && ABS(r) <= cond2)
					data->idata[x][y] = (0.5) *(1.0 + cos((((PI * symbol_rate) / (roll_off)) * (ABS(r) - cond1))));
				else
					data->idata[x][y] = 0;
			}
		}
		else
		{
			double cond1, cond2;
			double symbol_rate;
			double r;

			symbol_rate = (0.5) * (1.0 / filter_length);

			cond1 = (double)((1.0 - roll_off) / (2.0*(symbol_rate)));
			cond2 = (double)((1.0 + roll_off) / (2.0*(symbol_rate)));

			for (int y = 0; y < data->height; y++)
			for (int x = 0; x < data->width; x++)
			{
				r = (double)SQRT(SQR(x) + SQR(y));
				if (ABS(r) <= cond1)
					data->idata[x][y] = 1.0;
				else if (cond1 < ABS(r) && ABS(r) <= cond2)
					data->idata[x][y] = (0.5) *(1.0 + cos((((PI * symbol_rate) / (roll_off)) * (ABS(r) - cond1))));
				else
					data->idata[x][y] = 0;
			}
		}
	}
	else
		ErrorMessage("Error [KRaisedCosine] : data didn't allocated");
}
template <typename T> void KRaisedCosine(KArray2D<T> &data, double roll_off, double filter_length, char mode)
{
	KRaisedCosine(&data, roll_off, filter_length, mode);
}
template <typename T> void KBartlettWindow(KArray1D<T> *data, int filterLength, char mode)
{
	if (data->isAlloc)
	{

		if (mode == KERNEL_MODE_CENTER)
		{
			for (int i = 0, u = (-1)*(data->length >> 1); i < data->length; i++, u++)
			{
				data->idata[i] = (T)(1.0 - ABS((double)((double)ABS(u - (data->length >> 1)) - (double)(data->length - 1) / 2.0) / (double)(filterLength >> 1)));
				if (data->idata[i] < 0)
					data->idata[i] = 0;
			}
		}
		else
		{
			for (int i = 0; i < data->length; i++)
			{
				data->idata[i] = (T)(1.0 - ABS((double)((double)((data->length >> 1) - i) - (double)(data->length - 1) / 2.0) / (double)(data->length >> 1)));
				if (data->idata[i] < 0)
					data->idata[i] = 0;
			}
		}
	}
	else
		ErrorMessage("Error [KBartlettWindow or KTriangularWindow] : data didn't allocated");
}
template <typename T> void KBartlettWindow(KArray1D<T> &data, int filterLength, char mode)
{
	KBartlettWindow(&data, filterLength, mode);
}
template <typename T> void KTriangularWindow(KArray1D<T> *data, int filterLength, char mode)
{
	KBartlettWindow(data, filterLength, mode);
}
template <typename T> void KTriangularWindow(KArray1D<T> &data, int filterLength, char mode)
{
	KBartlettWindow(&data, filterLength, mode);
}
template <typename T> void KWelchWindow(KArray1D<T> *data, int filterLength, char mode)
{
	if (data->isAlloc)
	{
		if (mode == KERNEL_MODE_CENTER)
		{
			for (int i = 0, u = (-1)*(data->length >> 1); i < data->length; i++, u++)
			{
				int uc = ABS(u - (data->length >> 1));
				data->idata[i] = (T)(1.0 - Pow((double)(((double)uc - (double)((data->length - 1.0) / 2.0)) / ((double)(filterLength - 1.0) / 2.0)), 2.0));
				if (data->idata[i] < 0)
					data->idata[i] = 0;
			}
		}
		else
		{
			for (int i = 0; i < data->length; i++)
			{
				int ic = (data->length >> 1) - i;
				data->idata[i] = (T)(1.0 - Pow((double)(((double)ic - (double)((data->length - 1.0) / 2.0)) / ((double)(filterLength - 1.0) / 2.0)), 2.0));
				if (data->idata[i] < 0)
					data->idata[i] = 0;
			}
		}
	}
	else
		ErrorMessage("Error [KWelchWindow] : data didn't allocated");
}
template <typename T> void KWelchWindow(KArray1D<T> &data, int filterLength, char mode)
{
	KWelchWindow(&data, filterLength, mode);
}
template <typename T> void KHammingWindow(KArray1D<T> *data, double alpha)
{
	if (data->isAlloc)
	{
		double beta = 1.0 - alpha;
		for (int i = 0, u = (-1)*(data->length >> 1); i < data->length; i++, u++)
		{
			int uc = ABS(u - (data->length >> 1));
			data->idata[i] = (T)(alpha - (beta*cos((double)(2.0*PI*uc) / ((double)(data->length - 1)))));
		}
	}
	else
		ErrorMessage("Error [KHammingWindow] : data didn't allocated");
}
template <typename T> void KHammingWindow(KArray1D<T> &data, double alpha)
{
	KHammingWindow(data, alpha);
}
template <typename T> void KBlackmanWindow(KArray1D<T> *data, double alpha)
{
	if (data->isAlloc)
	{
		double a0, a1, a2;

		a0 = (1.0 - alpha) / 2.0;
		a1 = 0.5;
		a2 = alpha / 2.0;

		for (int i = 0, u = (-1)*(data->length >> 1); i < data->length; i++, u++)
		{
			int uc = ABS(u - (data->length >> 1));
			data->idata[i] = (T)(a0 - (a1*cos((double)(2.0*PI*uc) / (double)(data->length - 1))) + (a2*cos((double)(4.0*PI*uc) / (double)(data->length - 1))));
		}
	}
	else
		ErrorMessage("Error [KBlackmanWindow] : data didn't allocated");
}
template <typename T> void KBlackmanWindow(KArray1D<T> &data, double alpha)
{
	KBlackmanWindow(&data, alpha);
}
template <typename T> void KNuttalWindow(KArray1D<T> *data, double a0, double a1, double a2, double a3)
{
	if (data->isAlloc)
	{
		for (int i = 0, u = (-1)*(data->length >> 1); i < data->length; i++, u++)
		{
			int uc = ABS(u - (data->length >> 1));
			data->idata[i] = (T)(a0 - (a1*cos((double)(2.0*PI*uc) / (double)(data->length - 1))) + (a2*cos((double)(4.0*PI*uc) / (double)(data->length - 1))) - (a3*cos((double)(6.0*PI*uc) / (double)(data->length - 1))));
		}
	}
	else
		ErrorMessage("Error [KNuttalWindow] : data didn't allocated");
}
template <typename T> void KNuttalWindow(KArray1D<T> &data, double a0, double a1, double a2, double a3)
{
	KNuttalWindow(&data, a0, a1, a2, a3);
}
template <typename T> void KBlackmanNuttalWindow(KArray1D<T> *data, double a0, double a1, double a2, double a3)
{
	if (data->isAlloc)
	{
		for (int i = 0, u = (-1)*(data->length >> 1); i < data->length; i++, u++)
		{
			int uc = ABS(u - (data->length >> 1));
			data->idata[i] = (T)(a0 - (a1*cos((double)(2.0*PI*uc) / (double)(data->length - 1))) + (a2*cos((double)(4.0*PI*uc) / (double)(data->length - 1))) - (a3*cos((double)(6.0*PI*uc) / (double)(data->length - 1))));
		}
	}
	else
		ErrorMessage("Error [KBlackmanNuttalWindow] : data didn't allocated");
}
template <typename T> void KBlackmanNuttalWindow(KArray1D<T> &data, double a0, double a1, double a2, double a3)
{
	KBlackmanNuttalWindow(&data, a0, a1, a2, a3);
}
template <typename T> void KBlackmanHarrisWindow(KArray1D<T> *data, double a0, double a1, double a2, double a3)
{
	if (data->isAlloc)
	{
		for (int i = 0, u = (-1)*(data->length >> 1); i < data->length; i++, u++)
		{
			int uc = ABS(u - (data->length >> 1));
			data->idata[i] = (T)(a0 - (a1*cos((double)(2.0*PI*uc) / (double)(data->length - 1))) + (a2*cos((double)(4.0*PI*uc) / (double)(data->length - 1))) - (a3*cos((double)(6.0*PI*uc) / (double)(data->length - 1))));
		}
	}
	else
		ErrorMessage("Error [KBlackmanHarrisWindow] : data didn't allocated");
}
template <typename T> void KBlackmanHarrisWindow(KArray1D<T> &data, double a0, double a1, double a2, double a3)
{
	KBlackmanHarrisWindow(&data, a0, a1, a2, a3);
}
template <typename T> void KFlatTopWindow(KArray1D<T> *data, double a0, double a1, double a2, double a3, double a4)
{
	if (data->isAlloc)
	{
		for (int i = 0, u = (-1)*(data->length >> 1); i < data->length; i++, u++)
		{
			int uc = ABS(u - (data->length >> 1));
			data->idata[i] = (T)(a0 - (a1*cos((double)(2.0*PI*uc) / (double)(data->length - 1))) + (a2*cos((double)(4.0*PI*uc) / (double)(data->length - 1))) - (a3*cos((double)(6.0*PI*uc) / (double)(data->length - 1))) + (a4*cos((double)(8.0*PI*uc) / (double)(data->length - 1))));
		}
	}
	else
		ErrorMessage("Error [KFlatTopWindow] : data didn't allocated");
}
template <typename T> void KFlatTopWindow(KArray1D<T> &data, double a0, double a1, double a2, double a3, double a4)
{
	KFlatTopWindow(&data, a0, a1, a2, a3, a4);
}
template <typename T> void KCosineWindow(KArray1D<T> *data)
{
	if (data->isAlloc)
	{
		for (int i = 0, u = (-1)*(data->length >> 1); i < data->length; i++, u++)
		{
			int uc = ABS(u - (data->length >> 1));
			data->idata[i] = (T)(sin((PI*(double)uc) / ((double)(data->length - 1))));
		}
	}
	else
		ErrorMessage("Error [KFlatTopWindow] : data didn't allocated");
}
template <typename T> void KCosineWindow(KArray1D<T> &data)
{
	KCosineWindow(&data);
}
template <typename T> void KSineWindow(KArray1D<T> *data)
{
	KCosineWindow(data);
}
template <typename T> void KSineWindow(KArray1D<T> &data)
{
	KCosineWindow(&data);
}
template <typename T> void KBohmanWindow(KArray1D<T> *data)
{
	KCosineWindow(data);
}
template <typename T> void KBohmanWindow(KArray1D<T> &data)
{
	KCosineWindow(&data);
}
template <typename T> void KTukeyWindow(KArray1D<T> *data, double alpha)
{
	if (data->isAlloc)
	{
		double cond1, cond2;
		cond1 = (double)(((double)alpha*((double)(data->length - 1.0))) / 2.0);
		cond2 = (double)(data->length - 1) * (double)(1.0 - ((double)(alpha / 2.0)));
		for (int i = 0, u = (-1)*(data->length >> 1); i < data->length; i++, u++)
		{
			int uc = ABS(u - (data->length >> 1));
			if (0 <= uc && uc < cond1)
				data->idata[i] = (T)(0.5 *(1.0 + cos(PI * ((((double)(2.0*uc)) / ((double)alpha * (double)(data->length - 1.0))) - 1.0))));
			else if (cond1 <= uc && uc < cond2)
				data->idata[i] = (T)1.0;
			else
				data->idata[i] = (T)(0.5 *(1.0 + cos(PI * ((((double)(2.0*uc)) / ((double)alpha * (double)(data->length - 1.0))) + 1.0 - (2.0 / alpha)))));
		}
	}
	else
		ErrorMessage("Error [KFlatTopWindow] : data didn't allocated");
}
template <typename T> void KTukeyWindow(KArray1D<T> &data, double alpha)
{
	KTukeyWindow(&data, alpha);
}
template <typename T> void KTaperedCosineWindow(KArray1D<T> *data, double alpha)
{
	KTukeyWindow(data, alph);
}
template <typename T> void KTaperedCosineWindow(KArray1D<T> &data, double alpha)
{
	KTukeyWindow(&data, alpha);
}
template <typename T> void KPrewittWindow(KArray2D<T> *compX, KArray2D<T> *compY)
{
	compX->Resize(3, 3);
	compY->Resize(3, 3);

	compX->idata[0][0] = compY->idata[0][0] = (T)(-1);	compX->idata[0][1] = compY->idata[1][0] = (T)(-1);	compX->idata[0][2] = compY->idata[2][0] = (T)(-1);
	compX->idata[1][0] = compY->idata[0][1] = (T)(0);	compX->idata[1][1] = compY->idata[1][1] = (T)(0);	compX->idata[1][2] = compY->idata[2][1] = (T)(0);
	compX->idata[2][0] = compY->idata[0][2] = (T)(1);	compX->idata[2][1] = compY->idata[1][2] = (T)(1);	compX->idata[2][2] = compY->idata[2][2] = (T)(1);
}
template <typename T> void KPrewittWindow(KArray2D<T> &compX, KArray2D<T> &compY)
{
	KPrewittWindow(&compX, &compY);
}
template <typename T> void KSobelWindow(KArray2D<T> *compX, KArray2D<T> *compY)
{
	compX->Resize(3, 3);
	compY->Resize(3, 3);

	compX->idata[0][0] = compY->idata[0][0] = (T)(-1);	compX->idata[0][1] = compY->idata[1][0] = (T)(-2);	compX->idata[0][2] = compY->idata[2][0] = (T)(-1);
	compX->idata[1][0] = compY->idata[0][1] = (T)(0);	compX->idata[1][1] = compY->idata[1][1] = (T)(0);	compX->idata[1][2] = compY->idata[2][1] = (T)(0);
	compX->idata[2][0] = compY->idata[0][2] = (T)(1);	compX->idata[2][1] = compY->idata[1][2] = (T)(2);	compX->idata[2][2] = compY->idata[2][2] = (T)(1);
}
template <typename T> void KSobelWindow(KArray2D<T> &compX, KArray2D<T> &compY)
{
	KSobelWindow(&compX, &compY);
}
template <typename T> void KRobertWindow(KArray2D<T> *compX, KArray2D<T> *compY)
{
	compX->Resize(2, 2);
	compY->Resize(2, 2);

	compX->idata[0][0] = compY->idata[0][1] = (T)(0);	compX->idata[1][0] = compY->idata[1][1] = (T)(1);
	compX->idata[0][1] = compY->idata[0][0] = (T)(-1);	compX->idata[1][1] = compY->idata[1][0] = (T)(0);
}
template <typename T> void KRobertWindow(KArray2D<T> &compX, KArray2D<T> &compY)
{
	KRobertWindow(&compX, &compY);
}

#endif