// ------------------------------------------------ //
// contain:		Histogram storage
//
// developer:	Krisada Phromsuthirak
// ------------------------------------------------ //

#ifndef _KHistogram_H
#define _KHistogram_H

#include "KGeneral.h"
#include "KStatistic.h"

template <typename TypeScale, typename TypeWeight>
class KHistogram : public KArray1D<TypeWeight>
{
private:
	bool isInit;

public:

	double resolution_scale;
	TypeScale min_value, max_value;

	KHistogram()
	{
		isInit = false;

		min_value = NULL;
		max_value = NULL;
		resolution_scale = 0;
	}
	~KHistogram()
	{

	}

	void Initialization(TypeScale min, TypeScale max, double ResolutionScale)
	{
		min_value = min;
		max_value = max;
		resolution_scale = ResolutionScale;

		this->Allocate((int)(((double)max - (double)min + (double)1.0) / (double)ResolutionScale));
		Reset();
		isInit = true;
	}
	void ClearMemory()
	{
		this->FreeMem();
		isInit = false;
	}
	void Reset()
	{
		for (int z = 0; z < this->length; z++)
			this->idata[z] = (TypeWeight)0;
	}

	void AddData(TypeScale data, TypeWeight weight)
	{
		if (this->isAlloc && this->isInit)
		{
			int selected_bin;
			selected_bin = (int)(((double)(data)-(double)(min_value)) / (double)resolution_scale);

			if (this->IsInBound(selected_bin))
			{
				this->idata[selected_bin] += weight;
			}
			else
				ErrorMessage("Error [KHistogram]: out of histogram range");
		}
		else
			ErrorMessage("Error [KHistogram]: must be [Initialization] before [AddData]");
	}
	void AddData(KArray1D<TypeScale> *data, TypeWeight each_weight)
	{
		if (data->isAlloc)
		{
			for (int z = 0; z < data->length; z++)
			{
				AddData(data->idata[z], each_weight);
			}
		}
		else
			ErrorMessage("Error [KHistogram]: data that add to histogram is not allocate");
	}
	void AddData(KArray1D<TypeScale> &data, TypeWeight each_weight)
	{
		AddData(&data, each_weight);
	}
	void AddData(KArray2D<TypeScale> *data, TypeWeight each_weight)
	{
		if (data->isAlloc)
		{
			for (int y = 0; y < data->height; y++)
			for (int x = 0; x < data->width; x++)
			{
				AddData(data->idata[x][y], each_weight);
			}
		}
		else
			ErrorMessage("Error [KHistogram]: data that add to histogram is not allocate");
	}
	void AddData(KArray2D<TypeScale> &data, TypeWeight each_weight)
	{
		AddData(&data, each_weight);
	}
	void AddData(KArray3D<TypeScale> *data, TypeWeight each_weight)
	{
		if (data->isAlloc)
		{
			for (int d = 0; d < data->depth; d++)
			for (int y = 0; y < data->height; y++)
			for (int x = 0; x < data->width; x++)
			{
				AddData(data->idata[d][x][y], each_weight);
			}
		}
		else
			ErrorMessage("Error [KHistogram]: data that add to histogram is not allocate");
	}
	void AddData(KArray3D<TypeScale> &data, TypeWeight each_weight)
	{
		AddData(&data, each_weight);
	}
	void AddData(KArray4D<TypeScale> *data, TypeWeight each_weight)
	{
		if (data->isAlloc)
		{
			for (int f = 0; f < data->frame; f++)
			for (int d = 0; d < data->depth; d++)
			for (int y = 0; y < data->height; y++)
			for (int x = 0; x < data->width; x++)
			{
				AddData(data->idata[f][d][x][y], each_weight);
			}
		}
		else
			ErrorMessage("Error [KHistogram]: data that add to histogram is not allocate");
	}
	void AddData(KArray4D<TypeScale> &data, TypeWeight each_weight)
	{
		AddData(&data, each_weight);
	}
	void AddData(KList1D<TypeScale> *data, TypeWeight each_weight)
	{
		for (unsigned int z = 0; z < data->size(); z++)
		{
			AddData(data->idata[z], each_weight);
		}
	}
	void AddData(KList1D<TypeScale> &data, TypeWeight each_weight)
	{
		AddData(&data, each_weight);
	}

};

#endif