// ------------------------------------------------ //
// contain:		class 4D linked list
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KList4D_H
#define _KList4D_H

#include "KHeader.h"
#include "KList1D.h"

template <class T> class KList4D :public KList1D<KList1D<KList1D<KList1D<T>>>>
{
public:

	KList4D()		{	}
	~KList4D()		{ 	}

	void FreeMem()
	{
		for (UINT h = 0; h<this->size(); h++)
		for (UINT i = 0; i<this->at(h).size(); i++)
		for (UINT j = 0; j<this->at(h).at(i).size(); j++)
			this->at(h).at(i).at(j).FreeMem();
		for (UINT i = 0; i<this->size(); i++)
		for (UINT j = 0; j<this->at(i).size(); j++)
			this->at(i).at(j).FreeMem();
		for (UINT i = 0; i<this->size(); i++)
			this->at(i).FreeMem();

		this->~KList4D();
	}
	friend istream& operator >> (istream& in, KList4D<T> &obj)
	{
		KList3D<T>		n;
		char			tmp;
		unsigned int	ListSize;
		in >> tmp;
		in >> ListSize;
		in >> tmp;
		for (unsigned int z = 0; z<ListSize; z++)
		{
			in >> n;
			obj.push_back(n);
			n.FreeMem();
		}
		return in;
	}
	friend ostream& operator << (ostream& out, KList4D<T> &obj)
	{
		KList4D<T>::iterator listIterator;
		out << "[" << obj.size() << "]" << endl;
		for (listIterator = obj.begin(); listIterator != obj.end(); ++listIterator)
			out << *listIterator << endl;
		return out;
	}
};

#endif