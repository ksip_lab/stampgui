// ------------------------------------------------ //
// contain:		[1] class 1D Array
//				[2] class 2D Array
//				[3] class 3D Array
//				[4]	class 4D Array
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KArray_H
#define _KArray_H

#include "KHeader.h"
#include "KMemory.h"

#include "KArray1D.h"
#include "KArray2D.h"
#include "KArray3D.h"
#include "KArray4D.h"

#endif