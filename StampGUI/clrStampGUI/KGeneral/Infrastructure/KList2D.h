// ------------------------------------------------ //
// contain:		class 2D linked list
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KList2D_H
#define _KList2D_H

#include "KHeader.h"
#include "KList1D.h"

template <class T> class KList2D :public KList1D<KList1D<T>>
{
public:

	KList2D()	{	}
	~KList2D()	{ 	}

	void FreeMem()
	{
		for (UINT i = 0; i<this->size(); i++)
			this->at(i).FreeMem();

		this->~KList2D();
	}
	friend istream& operator >> (istream& in, KList2D<T> &obj)
	{
		KList1D<T>		n;
		char			tmp;
		unsigned int	ListSize;
		in >> tmp;
		in >> ListSize;
		in >> tmp;
		for (unsigned int z = 0; z<ListSize; z++)
		{
			in >> n;
			obj.push_back(n);
			n.FreeMem();
		}
		return in;
	}
	friend ostream& operator << (ostream& out, KList2D<T> &obj)
	{
		KList2D<T>::iterator listIterator;
		out << "[" << obj.size() << "]" << endl;
		for (listIterator = obj.begin(); listIterator != obj.end(); ++listIterator)
			out << *listIterator << endl;
		return out;
	}
};

#endif