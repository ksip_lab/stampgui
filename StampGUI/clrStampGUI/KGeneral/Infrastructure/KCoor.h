// ------------------------------------------------ //
// contain:		Coordinate Class
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KCoor_H
#define _KCoor_H

#include "KHeader.h"

template<typename T> class KCoor
{
public:
	T x;
	T y;

	void operator()(T i, T j)
	{
		x = i;
		y = j;
	}
	void operator= (T c)
	{
		x = c;
		y = c;
	}
	void operator+=(T c)
	{
		x += c;
		y += c;
	}
	void operator-=(T c)
	{
		x -= c;
		y -= c;
	}
	void operator*=(T c)
	{
		x *= c;
		y *= c;
	}
	void operator/=(T c)
	{
		if (c == 0)
		{
			x = (T)INFINITY;
			y = (T)INFINITY;
		}
		else
		{
			x /= c;
			y /= c;
		}
	}
	void operator>>=(T c)
	{
		x >>= c;
		y >>= c;
	}
	void operator<<=(T c)
	{
		x <<= c;
		y <<= c;
	}
	bool operator==(const KCoor<T> &ref)
	{
		return x == ref.x && y == ref.y;
	}
	bool operator!=(const KCoor<T> &ref)
	{
		return x != ref.x || y != ref.y;
	}

	KCoor operator+ (T c)
	{
		KCoor<T> res;
		res.x = x + c;
		res.y = y + c;
		return res;
	}
	KCoor operator- (T c)
	{
		KCoor<T> res;
		res.x = x - c;
		res.y = y - c;
		return res;
	}
	KCoor operator* (T c)
	{
		KCoor<T> res;
		res.x = x*c;
		res.y = y*c;
		return res;
	}
	KCoor operator/ (T c)
	{
		KCoor<T> res;
		if (c == 0)
		{
			res.x = (T)INFINITY;
			res.y = (T)INFINITY;
		}
		else
		{
			res.x = x / c;
			res.y = y / c;
		}
		return res;
	}
	KCoor operator>>(T c)
	{
		KCoor<T> res;
		res.x = x >> c;
		res.y = y >> c;
		return res;
	}
	KCoor operator<<(T c)
	{
		KCoor<T> res;
		res.x = x << c;
		res.y = y << c;
		return res;
	}

	template <typename T2> void operator= (const KCoor<T2> &ref)
	{
		x = (T)ref.x;
		y = (T)ref.y;
	}
	template <typename T2> void operator+=(const KCoor<T2> &ref)
	{
		x += (T)ref.x;
		y += (T)ref.y;
	}
	template <typename T2> void operator-=(const KCoor<T2> &ref)
	{
		x -= (T)ref.x;
		y -= (T)ref.y;
	}
	template <typename T2> void operator*=(const KCoor<T2> &ref)
	{
		x *= (T)ref.x;
		y *= (T)ref.y;
	}
	template <typename T2> void operator/=(const KCoor<T2> &ref)
	{
		if (ref.x == 0 || ref.y == 0)
		{
			x = (T)INFINITY;
			y = (T)INFINITY;
		}
		else
		{
			x /= (T)ref.x;
			y /= (T)ref.y;
		}
	}
	template <typename T2> KCoor operator+ (const KCoor<T2> &ref)
	{
		KCoor<T> res;
		res.x = x + (T)ref.x;
		res.y = y + (T)ref.y;
		return res;
	}
	template <typename T2> KCoor operator- (const KCoor<T2> &ref)
	{
		KCoor<T> res;
		res.x = x - (T)ref.x;
		res.y = y - (T)ref.y;
		return res;
	}
	template <typename T2> KCoor operator* (const KCoor<T2> &ref)
	{
		KCoor<T> res;
		res.x = x * (T)ref.x;
		res.y = y * (T)ref.y;
		return res;
	}
	template <typename T2> KCoor operator/ (const KCoor<T2> &ref)
	{
		KCoor<T> res;
		if (ref.x == 0 || ref.y == 0)
		{
			res.x = (T)INFINITY;
			res.y = (T)INFINITY;
		}
		else
		{
			res.x = (T)(x / ref.x);
			res.y = (T)(y / ref.y);
		}
		return res;
	}
	template <typename T2> bool operator==(const KCoor<T2> &ref)
	{
		return  x == (T)ref.x && y == (T)ref.y;
	}
	template <typename T2, typename T3> void Set(T2 X, T3 Y)
	{
		x = (T)X;
		y = (T)Y;
	}
	friend istream& operator >> (istream& in, KCoor<T>& obj)
	{
		char tmp;
		in >> tmp;
		in >> obj.x;
		in >> tmp;
		in >> obj.y;
		in >> tmp;
		return in;
	}
	friend ostream& operator << (ostream& out, const KCoor<T>& obj)
	{
		out << '(' << obj.x << ',' << obj.y << ')';
		return out;
	}
};

#endif