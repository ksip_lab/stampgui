// ------------------------------------------------ //
// contain:		class 4D Array
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KArray4D_H
#define _KArray4D_H

#include "KHeader.h"
#include "KMemory.h"
#include "KBasic.h"

template <class T> class KArray4D
{
public:

	KArray4D()												{ isAlloc = false; }
	KArray4D(int Frame, int Depth, int Width, int Height)	{ isAlloc = false; Allocate(Frame, Depth, Width, Height); }
	~KArray4D()												{ FreeMem(); }

	bool	isAlloc;
	int		width;
	int		height;
	int		depth;
	int		frame;
	T		****idata;

	void Allocate(int Frame, int Depth, int Width, int Height)
	{
		if (Frame > 0 && Depth > 0 && Width > 0 && Height > 0)
		{
			if (!isAlloc)
			{
				isAlloc = true;
				idata = Alloc<T>(Frame, Depth, Width, Height);
				width = Width;
				height = Height;
				depth = Depth;
				frame = Frame;
			}
			else
				Resize(Frame, Depth, Width, Height);
		}
		else
			ErrorMessage("Error KArray4D [Allocate] : Frame & Depth & Width & Height must be more than 0");
	}
	void Resize(int Frame, int Depth, int Width, int Height)
	{
		if (Frame > 0 && Depth > 0 && Width > 0 && Height > 0)
		{
			if (!isAlloc)
				Allocate(Frame, Depth, Width, Height);
			else
			{
				if (width != Width || height != Height || depth != Depth || frame != Frame)
				{
					FreeMem();
					Allocate(Frame, Depth, Width, Height);
				}
			}
		}
		else
			ErrorMessage("Error KArray4D [Resize] : Frame & Depth & Width & Height must be more than 0");
	}
	void FreeMem()
	{
		if (isAlloc)
		{
			Free<T>(idata, frame, depth, width, height);
			width = 0;
			height = 0;
			depth = 0;
			frame = 0;
			isAlloc = false;
		}
	}
	bool IsSameSize(int Frame, int Depth, int Width, int Height)
	{
		if (width == Width&&height == Height&&depth == Depth&&frame == Frame)	return true;
		else																	return false;
	}
	bool IsSameSize(KArray4D<T> *Array4D)
	{
		if (width == Array4D->width && height == Array4D->height && depth == Array4D->depth && time == Array4D->frame)	return true;
		else																											return false;
	}
	bool IsInBound(int t, int z, int x, int y)
	{
		if (x >= 0 && x<width&&y >= 0 && y<height&&z >= 0 && z<depth&&t >= 0 && t<frame)	return true;
		else															return false;
	}
	template <typename T2>
	KArray4D<T>& operator = (const T2 &value)
	{
		if (isAlloc == false)
			ErrorMessage("Error [KArray4D] : must be allocated before add value");

		for (int t = 0; t < this->frame; t++)
		for (int z = 0; z < this->depth; z++)
		for (int y = 0; y < this->height; y++)
		for (int x = 0; x < this->width; x++)
			idata[t][z][x][y] = (T)value;
		return *this;
	}
	template <typename T2>
	KArray4D<T>&operator=(const KArray4D<T2> &prototype)
	{
		if (isAlloc == false)
			Allocate(prototype.frame, prototype.depth, prototype.width, prototype.height);
		else
		if (IsSameSize(prototype.width, prototype.height, prototype.depth, prototype.frame) == false)
			Resize(prototype.width, prototype.height, prototype.depth, prototype.frame);
		for (int t = 0; t<prototype.frame; t++)
		for (int z = 0; z<prototype.depth; z++)
		for (int y = 0; y<prototype.height; y++)
		for (int x = 0; x<prototype.width; x++)
			idata[t][z][x][y] = (T)prototype.idata[t][z][x][y];
		return *this;
	}
	KArray4D&operator=(const KArray4D<T> &prototype)
	{
		if (isAlloc == false)
			Allocate(prototype.frame, prototype.depth, prototype.width, prototype.height);
		else
		if (IsSameSize(prototype.width, prototype.height, prototype.depth, prototype.frame) == false)
			Resize(prototype.width, prototype.height, prototype.depth, prototype.frame);
		for (int t = 0; t<prototype.frame; t++)
		for (int z = 0; z<prototype.depth; z++)
		for (int y = 0; y<prototype.height; y++)
		for (int x = 0; x<prototype.width; x++)
			idata[t][z][x][y] = (T)prototype.idata[t][z][x][y];
		return *this;
	}
	T &operator() (int t, int z, int x, int y)
	{
		if (IsInBound(y, z, x, y))
			return idata[t][z][x][y];
		else
		{
			cout << "[attempt to access out of bound]" << endl;
			system("pause");
			return idata[0][0][0][0];
		}
	}
	friend istream& operator >> (istream& in, KArray4D<T>& obj)
	{
		int w, h, d, f;
		in >> f;
		in >> d;
		in >> w;
		in >> h;
		if (obj.width != w || obj.height != h || obj.depth != d || obj.frame != f)
			obj.Resize(f, d, w, h);
		for (int t = 0; t<obj.frame; t++)
		{
			for (int z = 0; z<obj.depth; z++)
			{
				for (int y = 0; y<obj.height; y++)
				{
					for (int x = 0; x<obj.width; x++)
					{
						in >> obj.idata[t][z][x][y];
					}
				}
			}
		}
		return in;
	}
	friend ostream& operator << (ostream& out, const KArray4D<T>& obj)
	{
		out << obj.frame << endl;
		out << obj.depth << endl;
		out << obj.width << endl;
		out << obj.height << endl << endl;
		for (int t = 0; t<obj.frame; t++)
		{
			for (int z = 0; z<obj.depth; z++)
			{
				for (int y = 0; y<obj.height; y++)
				{
					for (int x = 0; x<obj.width; x++)
					{
						out << obj.idata[z][x][y] << ' ';
					}
					out << endl;
				}
				out << endl;
			}
			out << endl;
		}
		return out;
	}
};

#endif