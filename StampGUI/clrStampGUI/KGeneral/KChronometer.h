// ------------------------------------------------ //
// contain:		Tool for evaluate computational complexity of algorithms
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KChronometer_H
#define _KChronometer_H

#include "KHeader.h"

class KChronometer
{
	//==============================================================//
	//----Chronometer - used to evaluate complexity of algorithm----//
	//To use this class in your program								//
	//1) declare object of this class.								//
	//2) call function "reset" and "start" before evaluated function//
	//3) call function "stop" and "display" after evaluated function//
	//==============================================================//

public:

	KChronometer();
	~KChronometer();

	bool	status;			// chronometer status: [false] = off, [true] = on
	long	mark, elapse;	// initial and elapsed time

	void Reset();
	void Start(char *s = NULL);
	void Stop();
	float Read();
	void Display(char *s = NULL);

};

#endif