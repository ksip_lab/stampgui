// ------------------------------------------------ //
// contain:		[1] Standard Header
//				[2] Standard Constant
//
// developer:	Krisada Phromsuthirak, KSIP Member
// ------------------------------------------------ //

#ifndef _KHeader_H
#define _KHeader_H

// ===================== //
// --- Macro Defines --- //
// ===================== //
#define _USE_MATH_DEFINES

// ======================= //
// --- Standard Header --- //
// ======================= //
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <crtdbg.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#include <conio.h>
#include <string.h>
#include <time.h>
#include <Windows.h>
#include <direct.h>
#include <ctype.h>
#include <locale.h>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <iostream>
#include <complex>
#include <iomanip>
#include <bitset>
#include <new>
#include <vector>
#include <stack>
#include <deque>
//#include <thread>

// ========================= //
// --- OpenCV Library --- //
// ========================= //
#include "opencv2/opencv.hpp"

// ================= //
// --- NameSpace --- //
// ================= //
using namespace std;
using namespace cv;

// ============================ //
// --- Constant Declaration --- //
// ============================ //
#define UNDEFINED	INFINITE				// Undefined
#define PI			(double)M_PI			// PI
#define PI_2		(double)M_PI_2			// PI/2
#define PI_4		(double)M_PI_4			// PI/4
#define _1_180		(double)(1.0/180.0)		// 1/180
#define _1_PI		(double)M_1_PI			// 1/PI
#define _2_PI		(double)M_2_PI			// 2/PI
#define _2_SQRTPI	(double)M_2_SQRTPI		// 2/Sqrt(PI)
#define _180_PI		(double)(180.0*_1_PI)	// 180/PI
#define PI_180		(double)(PI*_1_180)		// PI/180
#define SQRT2		(double)M_SQRT2			// Sqrt(2)
#define SQRT1_2		(double)M_SQRT1_2		// Sqrt(1/2)
#define M_E2		(double)M_E*M_E			// natural logarithms (e^2)
#define LOG2_E		(double)M_LOG2E			// The base-2 logarithm of e
#define LOG10_E		(double)M_LOG10E		// The base-10 logarithm of e
#define LN_2		(double)M_LN2			// The natural logarithm of 2
#define LN_10		(double)M_LN10			// The natural logarithm of 10

#endif