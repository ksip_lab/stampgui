#include "KBasic.h"

void ErrorMessage(char *message, bool isEndProcess)
{
	cout << message << endl;
	cvWaitKey();
	cvDestroyAllWindows();
	system("pause");
	if (isEndProcess)
		exit(0);
}
void ErrorMessage(string message, bool isEndProcess)
{
	ErrorMessage((char*)message.c_str(),isEndProcess);
}