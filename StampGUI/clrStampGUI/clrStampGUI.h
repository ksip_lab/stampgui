// clrOCR.h

#pragma once
#include "OCR.h"
#include "OCR.cpp"

//using namespace System;
using namespace System::Runtime::InteropServices;
using namespace System::Collections::Generic;

namespace StampOCR {
	public ref class StampOCRGUI
	{
	public:
		int dark_val = 50;
		int dark_thr = 20;
		int box_size = 6;
		int num_char = 16;

		int feature_sampling_rate = 1;
		int training_size = 1;
		int training_char_size = 1;

		int template_width = 36;
		int template_height = 50;

		List<System::String^>^ FilePath;
		
		StampOCRGUI();

		void UpdateParameter();

		void GetFileList(System::String^ folder_path, System::String^ file_extension, List<System::String^>^ train_set);
		void GetCharacterSize(List<System::String^>^ train_set, int* max_char_width, int* max_char_height);
		void GetActualSerialNumber(System::String^ sn_path, List<System::String^>^ sn_string);

		void GenerateTemplate(List<System::String^>^ train_set, int template_char_width, int template_char_height, System::String^ sav_path);
		/*void DetectCharPosition(KGImage<double> &in_img, vector<KCoor<int>> *topleft, vector<KCoor<int>> *bottomright);
		void DetectCharPosition(string filepath, vector<KCoor<int>> *topleft, vector<KCoor<int>> *bottomright);*/

		void GetTemplate(System::String^ template_path);
		void TemplateDownsampling();
		//void OCRProcess(System::String^ filepath, System::String^ serialnumber, List<int> TL_x, List<int> TL_y, List<int> BR_x, List<int> BR_y, List<double> char_score, double* ocr_score);
		//void OCRProcess(System::String^ filepath, System::String^ serialnumber, List<double>^ char_score, double* ocr_score);
		System::String^ OCRProcess(System::String^ filepath, List<int>^ TL_x, List<int>^ TL_y, List<int>^ BR_x, List<int>^ BR_y, List<double>^ char_score, double* ocr_score);
	private:
		std::string SystemStringToStdString(System::String^ str);
		System::String^ StdStringToSystemString(std::string str);
		//template<typename T> void ListToVector(List<T>^ list, std::vector<T>);
		template<typename T> void VectorToList(vector<T> vect, List<T>^);
		void ListStringToVector(List<System::String^>^ list, std::vector<string> *result);
		void VectorStringToList(vector<string> vect, List<System::String^>^ list_tmp);

		ClassStampOCR *cppStampOCR;
	};
}


//template<typename T> void StampOCR::StampOCRGUI::ListToVector(List<T>^ list, std::vector<T>);
//{
//	if (list == nullptr) throw gcnew ArgumentNullException(L"list");
//	std::vector<T> result(list->Count);
//	for each (T& elem in list)
//		result.push_back(elem);
//	return result;
//}

template<typename T> void StampOCR::StampOCRGUI::VectorToList(vector<T> vect, List<T>^ list_tmp)
{
	for (int i = 0; i < vect.size(); i++)
		list_tmp->Add(vect[i]);
}