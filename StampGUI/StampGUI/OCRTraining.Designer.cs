﻿namespace StampGUI
{
    partial class OCRTraining
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TrainPathButton = new System.Windows.Forms.Button();
            this.TrainPathText = new System.Windows.Forms.TextBox();
            this.SNTrainGroundText = new System.Windows.Forms.TextBox();
            this.SNTrainGroundButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TemplatePathText = new System.Windows.Forms.TextBox();
            this.TemplatePathTextButton = new System.Windows.Forms.Button();
            this.GetCharSizeButton = new System.Windows.Forms.Button();
            this.GenTemplateButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TrainPerChar = new System.Windows.Forms.NumericUpDown();
            this.UpdateParameterButton = new System.Windows.Forms.Button();
            this.CropSizeBar = new System.Windows.Forms.TrackBar();
            this.SNThrBar = new System.Windows.Forms.TrackBar();
            this.SNToneBar = new System.Windows.Forms.TrackBar();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TestButton = new System.Windows.Forms.Button();
            this.TestAccuracyText = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.SNTestGroundButton = new System.Windows.Forms.Button();
            this.SNTestGroundText = new System.Windows.Forms.TextBox();
            this.TestPathText = new System.Windows.Forms.TextBox();
            this.TestPathButton = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.CharWnum = new System.Windows.Forms.NumericUpDown();
            this.CharHnum = new System.Windows.Forms.NumericUpDown();
            this.SNToneText = new System.Windows.Forms.TextBox();
            this.SNThrText = new System.Windows.Forms.TextBox();
            this.CropSizeText = new System.Windows.Forms.TextBox();
            this.SNDigit = new System.Windows.Forms.NumericUpDown();
            this.Training = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrainPerChar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CropSizeBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SNThrBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SNToneBar)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CharWnum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CharHnum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SNDigit)).BeginInit();
            this.Training.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Training Path";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ground Truth S/N Path";
            // 
            // TrainPathButton
            // 
            this.TrainPathButton.Location = new System.Drawing.Point(512, 26);
            this.TrainPathButton.Name = "TrainPathButton";
            this.TrainPathButton.Size = new System.Drawing.Size(75, 23);
            this.TrainPathButton.TabIndex = 2;
            this.TrainPathButton.Text = "Browse";
            this.TrainPathButton.UseVisualStyleBackColor = true;
            this.TrainPathButton.Click += new System.EventHandler(this.TrainPathButton_Click);
            // 
            // TrainPathText
            // 
            this.TrainPathText.Location = new System.Drawing.Point(142, 26);
            this.TrainPathText.Name = "TrainPathText";
            this.TrainPathText.Size = new System.Drawing.Size(355, 20);
            this.TrainPathText.TabIndex = 3;
            // 
            // SNTrainGroundText
            // 
            this.SNTrainGroundText.Location = new System.Drawing.Point(142, 55);
            this.SNTrainGroundText.Name = "SNTrainGroundText";
            this.SNTrainGroundText.Size = new System.Drawing.Size(355, 20);
            this.SNTrainGroundText.TabIndex = 4;
            // 
            // SNTrainGroundButton
            // 
            this.SNTrainGroundButton.Location = new System.Drawing.Point(512, 55);
            this.SNTrainGroundButton.Name = "SNTrainGroundButton";
            this.SNTrainGroundButton.Size = new System.Drawing.Size(75, 23);
            this.SNTrainGroundButton.TabIndex = 5;
            this.SNTrainGroundButton.Text = "Browse";
            this.SNTrainGroundButton.UseVisualStyleBackColor = true;
            this.SNTrainGroundButton.Click += new System.EventHandler(this.SNTrainGroundButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Character Width";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(319, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Character Height";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Template Save Path";
            // 
            // TemplatePathText
            // 
            this.TemplatePathText.Location = new System.Drawing.Point(142, 86);
            this.TemplatePathText.Name = "TemplatePathText";
            this.TemplatePathText.Size = new System.Drawing.Size(355, 20);
            this.TemplatePathText.TabIndex = 11;
            // 
            // TemplatePathTextButton
            // 
            this.TemplatePathTextButton.Location = new System.Drawing.Point(512, 86);
            this.TemplatePathTextButton.Name = "TemplatePathTextButton";
            this.TemplatePathTextButton.Size = new System.Drawing.Size(75, 23);
            this.TemplatePathTextButton.TabIndex = 12;
            this.TemplatePathTextButton.Text = "Browse";
            this.TemplatePathTextButton.UseVisualStyleBackColor = true;
            this.TemplatePathTextButton.Click += new System.EventHandler(this.TemplatePathTextButton_Click);
            // 
            // GetCharSizeButton
            // 
            this.GetCharSizeButton.Location = new System.Drawing.Point(45, 149);
            this.GetCharSizeButton.Name = "GetCharSizeButton";
            this.GetCharSizeButton.Size = new System.Drawing.Size(226, 86);
            this.GetCharSizeButton.TabIndex = 13;
            this.GetCharSizeButton.Text = "Get Character Size";
            this.GetCharSizeButton.UseVisualStyleBackColor = true;
            this.GetCharSizeButton.Click += new System.EventHandler(this.GetCharSizeButton_Click);
            // 
            // GenTemplateButton
            // 
            this.GenTemplateButton.Enabled = false;
            this.GenTemplateButton.Location = new System.Drawing.Point(322, 149);
            this.GenTemplateButton.Name = "GenTemplateButton";
            this.GenTemplateButton.Size = new System.Drawing.Size(226, 86);
            this.GenTemplateButton.TabIndex = 14;
            this.GenTemplateButton.Text = "Generate Template";
            this.GenTemplateButton.UseVisualStyleBackColor = true;
            this.GenTemplateButton.Click += new System.EventHandler(this.GenTemplateButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SNDigit);
            this.groupBox1.Controls.Add(this.CropSizeText);
            this.groupBox1.Controls.Add(this.SNThrText);
            this.groupBox1.Controls.Add(this.SNToneText);
            this.groupBox1.Controls.Add(this.TrainPerChar);
            this.groupBox1.Controls.Add(this.UpdateParameterButton);
            this.groupBox1.Controls.Add(this.CropSizeBar);
            this.groupBox1.Controls.Add(this.SNThrBar);
            this.groupBox1.Controls.Add(this.SNToneBar);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(612, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(377, 250);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Parameters";
            // 
            // TrainPerChar
            // 
            this.TrainPerChar.Location = new System.Drawing.Point(184, 184);
            this.TrainPerChar.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.TrainPerChar.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.TrainPerChar.Name = "TrainPerChar";
            this.TrainPerChar.Size = new System.Drawing.Size(176, 20);
            this.TrainPerChar.TabIndex = 21;
            this.TrainPerChar.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // UpdateParameterButton
            // 
            this.UpdateParameterButton.Location = new System.Drawing.Point(157, 215);
            this.UpdateParameterButton.Name = "UpdateParameterButton";
            this.UpdateParameterButton.Size = new System.Drawing.Size(75, 23);
            this.UpdateParameterButton.TabIndex = 20;
            this.UpdateParameterButton.Text = "Update";
            this.UpdateParameterButton.UseVisualStyleBackColor = true;
            this.UpdateParameterButton.Click += new System.EventHandler(this.UpdateParameterButton_Click);
            // 
            // CropSizeBar
            // 
            this.CropSizeBar.Location = new System.Drawing.Point(102, 139);
            this.CropSizeBar.Name = "CropSizeBar";
            this.CropSizeBar.Size = new System.Drawing.Size(211, 45);
            this.CropSizeBar.TabIndex = 19;
            this.CropSizeBar.Value = 6;
            this.CropSizeBar.Scroll += new System.EventHandler(this.CropSizeBar_Scroll);
            // 
            // SNThrBar
            // 
            this.SNThrBar.Location = new System.Drawing.Point(102, 103);
            this.SNThrBar.Maximum = 30;
            this.SNThrBar.Name = "SNThrBar";
            this.SNThrBar.Size = new System.Drawing.Size(211, 45);
            this.SNThrBar.TabIndex = 19;
            this.SNThrBar.TickFrequency = 5;
            this.SNThrBar.Value = 20;
            this.SNThrBar.Scroll += new System.EventHandler(this.SNThrBar_Scroll);
            // 
            // SNToneBar
            // 
            this.SNToneBar.Location = new System.Drawing.Point(102, 63);
            this.SNToneBar.Maximum = 100;
            this.SNToneBar.Name = "SNToneBar";
            this.SNToneBar.Size = new System.Drawing.Size(211, 45);
            this.SNToneBar.TabIndex = 18;
            this.SNToneBar.TickFrequency = 5;
            this.SNToneBar.Value = 50;
            this.SNToneBar.Scroll += new System.EventHandler(this.SNToneBar_Scroll);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(135, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "S/N Digit";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 186);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(159, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Maximum Training per Character";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 145);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Crop Size";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 110);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "S/N Threshold";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 70);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "S/N Tone";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TestButton);
            this.groupBox2.Controls.Add(this.TestAccuracyText);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.SNTestGroundButton);
            this.groupBox2.Controls.Add(this.SNTestGroundText);
            this.groupBox2.Controls.Add(this.TestPathText);
            this.groupBox2.Controls.Add(this.TestPathButton);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Location = new System.Drawing.Point(4, 266);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(985, 81);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Testing";
            this.groupBox2.Visible = false;
            // 
            // TestButton
            // 
            this.TestButton.Location = new System.Drawing.Point(762, 48);
            this.TestButton.Name = "TestButton";
            this.TestButton.Size = new System.Drawing.Size(75, 23);
            this.TestButton.TabIndex = 14;
            this.TestButton.Text = "Test!";
            this.TestButton.UseVisualStyleBackColor = true;
            // 
            // TestAccuracyText
            // 
            this.TestAccuracyText.Location = new System.Drawing.Point(707, 22);
            this.TestAccuracyText.Name = "TestAccuracyText";
            this.TestAccuracyText.Size = new System.Drawing.Size(258, 20);
            this.TestAccuracyText.TabIndex = 13;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(605, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(90, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Testing Accuracy";
            // 
            // SNTestGroundButton
            // 
            this.SNTestGroundButton.Location = new System.Drawing.Point(514, 48);
            this.SNTestGroundButton.Name = "SNTestGroundButton";
            this.SNTestGroundButton.Size = new System.Drawing.Size(75, 23);
            this.SNTestGroundButton.TabIndex = 11;
            this.SNTestGroundButton.Text = "Browse";
            this.SNTestGroundButton.UseVisualStyleBackColor = true;
            // 
            // SNTestGroundText
            // 
            this.SNTestGroundText.Location = new System.Drawing.Point(144, 48);
            this.SNTestGroundText.Name = "SNTestGroundText";
            this.SNTestGroundText.Size = new System.Drawing.Size(355, 20);
            this.SNTestGroundText.TabIndex = 10;
            // 
            // TestPathText
            // 
            this.TestPathText.Location = new System.Drawing.Point(144, 19);
            this.TestPathText.Name = "TestPathText";
            this.TestPathText.Size = new System.Drawing.Size(355, 20);
            this.TestPathText.TabIndex = 9;
            // 
            // TestPathButton
            // 
            this.TestPathButton.Location = new System.Drawing.Point(514, 19);
            this.TestPathButton.Name = "TestPathButton";
            this.TestPathButton.Size = new System.Drawing.Size(75, 23);
            this.TestPathButton.TabIndex = 8;
            this.TestPathButton.Text = "Browse";
            this.TestPathButton.UseVisualStyleBackColor = true;
            this.TestPathButton.Click += new System.EventHandler(this.TestPathButton_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 50);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(118, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "Ground Truth S/N Path";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "Testing Path";
            // 
            // CharWnum
            // 
            this.CharWnum.Location = new System.Drawing.Point(151, 119);
            this.CharWnum.Name = "CharWnum";
            this.CharWnum.Size = new System.Drawing.Size(120, 20);
            this.CharWnum.TabIndex = 20;
            // 
            // CharHnum
            // 
            this.CharHnum.Location = new System.Drawing.Point(428, 119);
            this.CharHnum.Name = "CharHnum";
            this.CharHnum.Size = new System.Drawing.Size(120, 20);
            this.CharHnum.TabIndex = 21;
            // 
            // SNToneText
            // 
            this.SNToneText.Location = new System.Drawing.Point(319, 67);
            this.SNToneText.Name = "SNToneText";
            this.SNToneText.Size = new System.Drawing.Size(40, 20);
            this.SNToneText.TabIndex = 22;
            // 
            // SNThrText
            // 
            this.SNThrText.Location = new System.Drawing.Point(319, 103);
            this.SNThrText.Name = "SNThrText";
            this.SNThrText.Size = new System.Drawing.Size(40, 20);
            this.SNThrText.TabIndex = 23;
            // 
            // CropSizeText
            // 
            this.CropSizeText.Location = new System.Drawing.Point(319, 142);
            this.CropSizeText.Name = "CropSizeText";
            this.CropSizeText.Size = new System.Drawing.Size(40, 20);
            this.CropSizeText.TabIndex = 24;
            // 
            // SNDigit
            // 
            this.SNDigit.Location = new System.Drawing.Point(193, 27);
            this.SNDigit.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SNDigit.Name = "SNDigit";
            this.SNDigit.Size = new System.Drawing.Size(57, 20);
            this.SNDigit.TabIndex = 25;
            this.SNDigit.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            // 
            // Training
            // 
            this.Training.Controls.Add(this.CharHnum);
            this.Training.Controls.Add(this.CharWnum);
            this.Training.Controls.Add(this.GenTemplateButton);
            this.Training.Controls.Add(this.GetCharSizeButton);
            this.Training.Controls.Add(this.TemplatePathTextButton);
            this.Training.Controls.Add(this.TemplatePathText);
            this.Training.Controls.Add(this.label5);
            this.Training.Controls.Add(this.label4);
            this.Training.Controls.Add(this.label3);
            this.Training.Controls.Add(this.SNTrainGroundButton);
            this.Training.Controls.Add(this.SNTrainGroundText);
            this.Training.Controls.Add(this.TrainPathText);
            this.Training.Controls.Add(this.TrainPathButton);
            this.Training.Controls.Add(this.label2);
            this.Training.Controls.Add(this.label1);
            this.Training.Location = new System.Drawing.Point(4, 10);
            this.Training.Name = "Training";
            this.Training.Size = new System.Drawing.Size(599, 249);
            this.Training.TabIndex = 22;
            this.Training.TabStop = false;
            this.Training.Text = "Training";
            // 
            // OCRTraining
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 359);
            this.Controls.Add(this.Training);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "OCRTraining";
            this.Text = "OCR Training";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrainPerChar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CropSizeBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SNThrBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SNToneBar)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CharWnum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CharHnum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SNDigit)).EndInit();
            this.Training.ResumeLayout(false);
            this.Training.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button TrainPathButton;
        private System.Windows.Forms.TextBox TrainPathText;
        private System.Windows.Forms.TextBox SNTrainGroundText;
        private System.Windows.Forms.Button SNTrainGroundButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TemplatePathText;
        private System.Windows.Forms.Button TemplatePathTextButton;
        private System.Windows.Forms.Button GetCharSizeButton;
        private System.Windows.Forms.Button GenTemplateButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button UpdateParameterButton;
        private System.Windows.Forms.TrackBar CropSizeBar;
        private System.Windows.Forms.TrackBar SNThrBar;
        private System.Windows.Forms.TrackBar SNToneBar;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown TrainPerChar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button SNTestGroundButton;
        private System.Windows.Forms.TextBox SNTestGroundText;
        private System.Windows.Forms.TextBox TestPathText;
        private System.Windows.Forms.Button TestPathButton;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button TestButton;
        private System.Windows.Forms.TextBox TestAccuracyText;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown CharWnum;
        private System.Windows.Forms.NumericUpDown CharHnum;
        private System.Windows.Forms.TextBox CropSizeText;
        private System.Windows.Forms.TextBox SNThrText;
        private System.Windows.Forms.TextBox SNToneText;
        private System.Windows.Forms.NumericUpDown SNDigit;
        private System.Windows.Forms.GroupBox Training;
    }
}