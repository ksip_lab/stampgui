﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StampGUI
{
    public partial class OCRSetting : Form
    {
        public OCRSetting()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult result1 = MessageBox.Show("Generate new template may affect OCR accuracy. Please backup the previous template before do this. Are you sure?", "Important", MessageBoxButtons.YesNo);
            if (result1 == DialogResult.Yes)
            {
                OCRTraining ot = new OCRTraining();
                ot.ShowDialog();
            }
        }
    }
}
