﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using StampOCR;

namespace StampGUI
{
    public partial class OCRTraining : Form
    {
        StampOCRGUI StOCR;
        public OCRTraining()
        {
            InitializeComponent();
            StOCR = new StampOCRGUI();
            SNToneText.Text = SNToneBar.Value.ToString();
            CropSizeText.Text = CropSizeBar.Value.ToString();
            SNThrText.Text = SNThrBar.Value.ToString();
        }

        private void TrainPathButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult result = fbd.ShowDialog();
            TrainPathText.Text = fbd.SelectedPath;
        }

        private void SNTrainGroundButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Choose Serial Number (S/N) Ground Truth File";
            fdlg.Filter = "Text files (*.txt*)|*.txt*";
            fdlg.FilterIndex = 2;
            fdlg.RestoreDirectory = true;

            if (fdlg.ShowDialog() == DialogResult.OK)
                SNTrainGroundText.Text = fdlg.FileName;
        }

        private void TemplatePathTextButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                TemplatePathText.Text = saveFileDialog1.FileName;    
        }

        private unsafe void GetCharSizeButton_Click(object sender, EventArgs e)
        {
            List<String> l = new List<String>();
            String path = TrainPathText.Text;
            path += "\\";
            StOCR.GetFileList(path, ".jpg", l);
            int char_w, char_h;
            StOCR.GetCharacterSize(l, &char_w, &char_h);
            CharWnum.Value = char_w;
            CharHnum.Value = char_h;
            GenTemplateButton.Enabled = true;
        }

        private void GenTemplateButton_Click(object sender, EventArgs e)
        {
            //List<String> l = new List<String>();
            //String path = TrainPathText.Text;
            //path += "\\";
            //StOCR.GetFileList(path, ".jpg", l);
            StOCR.template_width = (int)CharWnum.Value;
            StOCR.template_height = (int)CharHnum.Value;
            StOCR.training_size = StOCR.FilePath.Count;
            StOCR.UpdateParameter();
            List<String> sn = new List<String>();
            String path = SNTrainGroundText.Text;
            StOCR.GetActualSerialNumber(path, sn);
            StOCR.GenerateTemplate(StOCR.FilePath, StOCR.template_width, StOCR.template_height, TemplatePathText.Text);
            groupBox2.Visible = true;
        }
        
        private void UpdateParameterButton_Click(object sender, EventArgs e)
        {
            StOCR.template_width = (int)CharWnum.Value;
            StOCR.template_height = (int)CharHnum.Value;

            StOCR.training_char_size = (int)TrainPerChar.Value;

            StOCR.dark_val = (int)SNToneBar.Value;
            StOCR.dark_thr = (int)SNThrBar.Value;
            StOCR.box_size = (int)CropSizeBar.Value;

            StOCR.num_char = (int)SNDigit.Value;
            StOCR.UpdateParameter();
        }

        private void SNToneBar_Scroll(object sender, EventArgs e)
        {
            SNToneText.Text = SNToneBar.Value.ToString();
        }

        private void SNThrBar_Scroll(object sender, EventArgs e)
        {
            SNThrText.Text = SNThrBar.Value.ToString();
        }

        private void CropSizeBar_Scroll(object sender, EventArgs e)
        {
            CropSizeText.Text = CropSizeBar.Value.ToString();
        }

        private void TestPathButton_Click(object sender, EventArgs e)
        {
        }
    }
}
